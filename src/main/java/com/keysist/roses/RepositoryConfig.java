package com.keysist.roses;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurer;

import com.keysist.roses.model.entity.Catalogo;
import com.keysist.roses.model.entity.EmpresaBloque;
import com.keysist.roses.model.entity.EmpresaMesa;
import com.keysist.roses.model.entity.EntregaVariedad;
import com.keysist.roses.model.entity.Entrega;
import com.keysist.roses.model.entity.Perfil;
import com.keysist.roses.model.entity.Usuario;
import com.keysist.roses.model.entity.EmpresaVariedad;
import com.keysist.roses.model.entity.Variedad;

@Configuration
public class RepositoryConfig implements RepositoryRestConfigurer {

	@Override
	public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
		config.exposeIdsFor(Usuario.class, Perfil.class, Entrega.class, EntregaVariedad.class, Variedad.class,
				Catalogo.class, EmpresaVariedad.class, EmpresaMesa.class, EmpresaBloque.class);
		config.getCorsRegistry().addMapping("/**").allowedOrigins("*").allowedHeaders("*")
				.allowedMethods("POST", "GET", "PUT", "DELETE", "OPTIONS").allowCredentials(true).maxAge(3600);
	}

}
