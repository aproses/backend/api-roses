package com.keysist.roses.view.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.keysist.roses.model.dto.RelacionComercialDTO;
import com.keysist.roses.model.services.RelacionComercialService;

@RestController
public class RelacionComercialController {
	@Autowired
	private RelacionComercialService relacionComercialService;

	@GetMapping("/relacioncomercial/findprov/{emp}")
	public ResponseEntity<?> findProveedores(@RequestParam("f") String filtro,@PathVariable("emp") Integer idEmpresa) {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(relacionComercialService.findProveedores(filtro,idEmpresa));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}
	
	@GetMapping("/relacioncomercial/findclie/{emp}")
	public ResponseEntity<?> findClientes(@RequestParam("f") String filtro,@PathVariable("emp") Integer idEmpresa) {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(relacionComercialService.findClientes(filtro,idEmpresa));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@DeleteMapping("/relacioncomercial/delete/{id}/")
	public ResponseEntity<?> delete(@PathVariable("id") Integer idUsuario) {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(relacionComercialService.delete(idUsuario));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@PostMapping("/relacioncomercial/save/")
	public ResponseEntity<?> save(@RequestBody RelacionComercialDTO u) {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(relacionComercialService.save(u));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

}
