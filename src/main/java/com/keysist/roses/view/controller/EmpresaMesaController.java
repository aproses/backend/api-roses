package com.keysist.roses.view.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.keysist.roses.model.dto.EmpresaMesaDTO;
import com.keysist.roses.model.services.EmpresaMesaService;

@RestController
public class EmpresaMesaController {
	@Autowired
	private EmpresaMesaService empresaMesaService;

	@PostMapping("/empresamesa/guardar")
	public ResponseEntity<?> registrar(@RequestBody EmpresaMesaDTO dto) {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(empresaMesaService.save(dto));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@RequestMapping("/empresamesa/byId/{idu}/")
	public ResponseEntity<?> obtener(@PathVariable("idu") Integer idEmpresaBloque) {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(empresaMesaService.findById(idEmpresaBloque));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}
	

	@DeleteMapping("/empresamesa/delete/{idu}/")
	public ResponseEntity<?> delete(@PathVariable("idu") Integer idEmpresaBloque) {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(empresaMesaService.delete(idEmpresaBloque));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@GetMapping("/empresamesa/by-empresa")
	public ResponseEntity<?> buscar(@RequestParam("idEmpresa") Integer idEmpresa) {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(empresaMesaService.consultar(idEmpresa));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

}
