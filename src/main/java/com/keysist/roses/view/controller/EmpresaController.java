package com.keysist.roses.view.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.keysist.roses.model.dto.EmpresaDTO;
import com.keysist.roses.model.dto.GenericResponse;
import com.keysist.roses.model.enums.ValorSiNoEnum;
import com.keysist.roses.model.services.EmpresaService;

@RestController
public class EmpresaController {
	@Autowired
	private EmpresaService empresaService;

	@PostMapping("/empresa/register/")
	public ResponseEntity<?> register(@RequestBody EmpresaDTO c) {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(empresaService.registrar(c));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@GetMapping("/empresa/changestatus/{id}")
	public ResponseEntity<?> buscar(@PathVariable("id") Integer idEmpresa, @RequestParam("est") Boolean activo) {
		try {
			return ResponseEntity.status(HttpStatus.OK)
					.body(empresaService.changeEstatus(idEmpresa, activo ? ValorSiNoEnum.SI : ValorSiNoEnum.NO));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@GetMapping("/empresa/filtrar")
	public ResponseEntity<?> findByFiltro(@RequestParam("f") String filtro) {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(empresaService.filtrar(filtro));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@GetMapping("/empresa/filtrar/{id}")
	public ResponseEntity<?> findToRelacionByFiltro(@PathVariable("id") Integer idEmpresa,
			@RequestParam("f") String filtro, @RequestParam("t") String tipo) {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(empresaService.filtrarDistinct(filtro, tipo, idEmpresa));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@PostMapping(value = "/empresa/updateLogo", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> updateLogo(@RequestParam(value = "file", required = true) MultipartFile file,
			@RequestParam("idEmpresa") Integer idEmpresa) {
		try {
			return ResponseEntity.status(HttpStatus.OK)
					.body(new GenericResponse("", empresaService.updateLogo(file, idEmpresa)));
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@GetMapping(value = "/empresa/logo/{idEmpresa}", produces = MediaType.IMAGE_PNG_VALUE)
	public ResponseEntity<?> getImage(@PathVariable("idEmpresa") Integer idEmpresa) {
		try {
			return new ResponseEntity<>(empresaService.obtenerImagen(idEmpresa), HttpStatus.OK);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
