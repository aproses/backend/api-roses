package com.keysist.roses.view.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.keysist.roses.model.entity.Catalogo;
import com.keysist.roses.model.services.CatalogoService;

@RestController
public class CatalogoController {
	@Autowired
	private CatalogoService catalogoService;

	@PostMapping("/catalogo/guardar/")
	public ResponseEntity<?> save(@RequestBody Catalogo c) {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(catalogoService.guardar(c));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@DeleteMapping("/catalogo/{id}/")
	public ResponseEntity<?> eliminarDetalle(@PathVariable("id") Integer idProducto) {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(catalogoService.eliminar(idProducto));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

}
