package com.keysist.roses.view.controller;

import java.sql.Timestamp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.keysist.roses.model.dto.CosechaDTO;
import com.keysist.roses.model.services.CosechaService;

@RestController
public class CosechaController {
	@Autowired
	private CosechaService cosechaService;

	@GetMapping("/cosecha/obtener/{idUsuario}/")
	public ResponseEntity<?> obtener(@PathVariable("idUsuario") Integer idUsuario, @RequestParam("e") Long fechaCorte) {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(cosechaService.obtener(idUsuario, fechaCorte));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@PostMapping("/cosecha/guardar")
	public ResponseEntity<?> guardar(@RequestBody CosechaDTO p) {
		try {
			return ResponseEntity.status(HttpStatus.CREATED).body(cosechaService.guardar(p));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	

	@DeleteMapping("/cosecha/variedad/{id}/")
	public ResponseEntity<?> eliminarDetalle(@PathVariable("id") Integer idDetalleVariedad) {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(cosechaService.eliminarCosechaDetalle(idDetalleVariedad));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}
	@GetMapping("/cosecha/obtenerbyid/{id}/")
	public ResponseEntity<?> findById(@PathVariable("id") Integer idCosecha) {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(cosechaService.obtenerById(idCosecha));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@GetMapping("/cosecha/consultar")
	public ResponseEntity<?> consultar(@RequestParam("ide") Integer idEmpresa, @RequestParam("fd") Long desde,
			@RequestParam("fh") Long hasta) {
		try {
			return ResponseEntity.status(HttpStatus.OK)
					.body(cosechaService.consultarFilter(idEmpresa, (new Timestamp(desde)).toLocalDateTime().toLocalDate(),
							(new Timestamp(hasta)).toLocalDateTime().toLocalDate()));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}


}
