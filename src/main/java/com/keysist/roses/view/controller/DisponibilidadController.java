package com.keysist.roses.view.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.keysist.roses.model.services.DisponibilidadService;

@RestController
public class DisponibilidadController {
	@Autowired
	private DisponibilidadService disponibilidadService;

	@GetMapping("/disponibilidad/recuperarRosaProcesada/")
	public ResponseEntity<?> recuperarRosaProcesada(@RequestParam("d") String dia,
			@RequestParam("v") Integer idVariedad) {
		try {
			return ResponseEntity.status(HttpStatus.OK)
					.body(disponibilidadService.recuperarRosaProcesada(dia, idVariedad));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

}
