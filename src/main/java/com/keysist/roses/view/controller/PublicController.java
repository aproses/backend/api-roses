package com.keysist.roses.view.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.keysist.roses.model.services.CatalogoService;

@RestController
public class PublicController {
	@Autowired
	private CatalogoService catalogoService;

	@GetMapping("/public/catalogo/by-grupo")
	public ResponseEntity<?> consultar(@RequestParam("codigoGrupo") String grupo) {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(catalogoService.findByGroup(grupo));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

}
