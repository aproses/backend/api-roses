package com.keysist.roses.view.controller;

import java.util.GregorianCalendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.RestController;

import com.keysist.roses.model.dto.AplicacionDTO;
import com.keysist.roses.model.dto.CosechaDTO;
import com.keysist.roses.model.dto.EmpresaProductoDTO;
import com.keysist.roses.model.dto.ProduccionDTO;
import com.keysist.roses.model.services.AplicacionService;
import com.keysist.roses.model.services.EmpresaProductoService;
import com.keysist.roses.model.services.ProduccionService;

@RestController
public class WebSocketDisponibilidadController {

	@Autowired
	SimpMessagingTemplate template;

	@Autowired
	private ProduccionService produccionService;

	@Autowired
	AplicacionService aplicacionService;

	@Autowired
	EmpresaProductoService empresaProductoService;

	@MessageMapping("/actualizarProduccion/{idProveedor}")
	public void actualizarProduccion(@DestinationVariable Integer idProveedor) {
		template.convertAndSend("/message/actualizarProduccion/" + idProveedor,
				produccionService.obtenerEstadoProcesadora());
	}

	@SendTo("/message/actualizarProduccion/{idProveedor}")
	public ProduccionDTO actualizarProduccion(@DestinationVariable Integer idProveedor, @Payload ProduccionDTO dto) {
		return dto;
	}

	@MessageMapping("/produccionanual/{idProveedor}")
	public void produccionanual(@DestinationVariable Integer idProveedor) {

	}

	@MessageMapping("/getCosechaDiaria/{idProveedor}")
	public void getCosechaDiaria(@DestinationVariable Integer idProveedor) {
		template.convertAndSend("/message/cosechaDiaria/" + idProveedor,
				produccionService.obtenerCosecha(idProveedor, GregorianCalendar.getInstance().getTimeInMillis()));
	}

	@MessageMapping("/getProduccionPorSemanualAnual/{idProveedor}")
	public void getProduccionPorSemanualAnual(@DestinationVariable Integer idProveedor) {
		template.convertAndSend("/message/produccionPorSemanualAnual/" + idProveedor,
				produccionService.obtenerProduccionSemanalAnual(idProveedor));
	}

	@MessageMapping("/getProduccionMensual/{idProveedor}")
	public void getProduccionMensual(@DestinationVariable Integer idProveedor) {
		template.convertAndSend("/message/produccionMensual/" + idProveedor,
				produccionService.obtenerCosechaMensual(idProveedor));
	}

	@MessageMapping("/getReportFertilizacion/{idProveedor}")
	public void getInfoFertilizacion(@DestinationVariable Integer idProveedor) {
		empresaProductoService.productosPorTerminar();
		aplicacionService.fumnigacionSinFinalizar();
		aplicacionService.fertilizacionSinFinalizar();
	}

	@SendTo("/message/produccionPorSemanualAnual/{idProveedor}")
	public CosechaDTO sendProduccionSemanalAnual(@DestinationVariable Integer idProveedor, CosechaDTO dto) {
		return dto;
	}

	@SendTo("/message/cosechaDiaria/{idProveedor}")
	public CosechaDTO sendCosecha(@DestinationVariable Integer idProveedor, CosechaDTO dto) {
		return dto;
	}

	@SendTo("/message/produccionMensual/{idProveedor}")
	public CosechaDTO sendProduccionMensual(@DestinationVariable Integer idProveedor, CosechaDTO dto) {
		return dto;
	}

	@SendTo("/message/sffumi/{idEmpresa}")
	public List<AplicacionDTO> sffumi(@DestinationVariable Integer idProveedor, @Payload List<AplicacionDTO> dtos) {
		return dtos;
	}

	@SendTo("/message/sffert/{idEmpresa}")
	public List<AplicacionDTO> sffert(@DestinationVariable Integer idProveedor, @Payload List<AplicacionDTO> dtos) {
		return dtos;
	}

	@SendTo("/message/prodter/{idEmpresa}")
	public List<EmpresaProductoDTO> prodter(@DestinationVariable Integer idProveedor,
			@Payload List<EmpresaProductoDTO> dtos) {
		return dtos;
	}

	@MessageMapping("/getAplicacionesPendiente/{idEmpresa}")
	public void getAplicacionesPendiente(@DestinationVariable Integer idEmpresa) {
		template.convertAndSend("/message/aplicacionesPendientes/" + idEmpresa,
				aplicacionService.obtenerPendientes(idEmpresa));
	}

	@SendTo("/message/aplicacionesPendientes/{empresaId}")
	public List<AplicacionDTO> aplicacionesPendientes(@DestinationVariable Integer empresaId,
			List<AplicacionDTO> list) {
		return list;
	}

}
