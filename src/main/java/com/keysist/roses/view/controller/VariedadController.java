package com.keysist.roses.view.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.keysist.roses.model.dto.VariedadDTO;
import com.keysist.roses.model.services.VariedadService;

@RestController
public class VariedadController {
	@Autowired
	private VariedadService variedadService;

	@GetMapping("/variedad/find-by-filter")
	public ResponseEntity<?> recuperarRosaProcesada(@RequestParam("n") String nombre) {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(variedadService.findByFilter(nombre));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@DeleteMapping("/variedad/delete/{id}/")
	public ResponseEntity<?> delete(@PathVariable("id") Integer idVariedad, @RequestParam("n") String nombre) {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(variedadService.delete(idVariedad, nombre));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@PostMapping("/variedad/save/")
	public ResponseEntity<?> save(@RequestBody VariedadDTO variedad, @RequestParam("n") String nombre) {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(variedadService.save(variedad, nombre));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

}
