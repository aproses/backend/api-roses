package com.keysist.roses.view.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.keysist.roses.model.dto.CostoVariedadDTO;
import com.keysist.roses.model.services.CostoVariedadService;

@RestController
public class CostoVariedadController {
	@Autowired
	private CostoVariedadService costoVariedadService;

	

	@PostMapping("/costovariedad/save/")
	public ResponseEntity<?> save(@RequestBody CostoVariedadDTO c) {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(costoVariedadService.save(c));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

}
