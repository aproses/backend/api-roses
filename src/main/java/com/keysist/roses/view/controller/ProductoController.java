package com.keysist.roses.view.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.keysist.roses.model.dto.ProductoDTO;
import com.keysist.roses.model.services.ProductoService;

@RestController
public class ProductoController {
	@Autowired
	private ProductoService productoService;

	@PostMapping("/producto/guardar/")
	public ResponseEntity<?> save(@RequestBody ProductoDTO c) {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(productoService.guardar(c));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@GetMapping("/producto/consultar")
	public ResponseEntity<?> consultar(@RequestParam("f") String filtro) {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(productoService.consultar(filtro));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}
	@GetMapping("/producto/buscar/{id}")
	public ResponseEntity<?> buscar(@PathVariable("id") Integer idProducto) {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(productoService.consultar(idProducto));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}
	@DeleteMapping("/producto/delete/{id}/")
	public ResponseEntity<?> eliminarProducto(@PathVariable("id") Integer idProducto) {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(productoService.eliminar(idProducto));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}
}
