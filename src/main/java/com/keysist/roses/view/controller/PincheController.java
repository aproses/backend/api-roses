package com.keysist.roses.view.controller;

import java.sql.Timestamp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.keysist.roses.model.dto.PincheDTO;
import com.keysist.roses.model.services.PincheService;

@RestController
public class PincheController {
	@Autowired
	private PincheService pincheServiceImpl;

	@GetMapping("/pinche/obtener")
	public ResponseEntity<?> consultar(@RequestParam("ide") Integer idEmpresa, @RequestParam("fd") Long desde,
			@RequestParam("fh") Long hasta) {
		try {
			return ResponseEntity.status(HttpStatus.OK)
					.body(pincheServiceImpl.consultar(idEmpresa, (new Timestamp(desde)).toLocalDateTime().toLocalDate(),
							(new Timestamp(hasta)).toLocalDateTime().toLocalDate()));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@GetMapping("/pinche/find")
	public ResponseEntity<?> find(@RequestParam("ide") Integer idEmpresa, @RequestParam("f") Long fechaPinche) {
		try {
			return ResponseEntity.status(HttpStatus.OK)
					.body(pincheServiceImpl.find(idEmpresa, (new Timestamp(fechaPinche)).toLocalDateTime().toLocalDate()));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@PostMapping("/pinche/registrar")
	public ResponseEntity<?> registrar(@RequestBody PincheDTO pinche) {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(pincheServiceImpl.registrar(pinche));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

}
