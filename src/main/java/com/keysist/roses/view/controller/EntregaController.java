package com.keysist.roses.view.controller;

import java.sql.Timestamp;
import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.keysist.roses.model.dto.EntregaDTO;
import com.keysist.roses.model.enums.EstadoEntregaEnum;
import com.keysist.roses.model.services.EntregaService;

@RestController
public class EntregaController {
	@Autowired
	private EntregaService entregaService;

	@GetMapping("/entrega/generarEntrega/{idProveedor}/{idCliente}/")
	public ResponseEntity<?> findByProveedorAnd(@PathVariable("idProveedor") Integer idProveedor,
			@PathVariable("idCliente") Integer idCliente, @RequestParam("fe") Long fechaEntrega) {
		try {
			return ResponseEntity.status(HttpStatus.OK)
					.body(entregaService.generarEntrega(idProveedor, idCliente, fechaEntrega));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@GetMapping("/entrega/obtenerbyid/{idEntrega}/")
	public ResponseEntity<?> findById(@PathVariable("idEntrega") Integer idEntrega) {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(entregaService.obtenerById(idEntrega));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@GetMapping("/entrega/consultarprov/{idEmpresa}/")
	public ResponseEntity<?> findByFechaAndProveedor(@PathVariable("idEmpresa") Integer idEmpresa,
			@RequestParam("fd") Long fechaDesde, @RequestParam("fh") Long fechaHasta) {
		try {
			return ResponseEntity.status(HttpStatus.OK)
					.body(entregaService.consultarByCliente(idEmpresa, fechaDesde, fechaHasta));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@GetMapping("/entrega/recuperarEntrega/")
	public ResponseEntity<?> findByFiltro(@RequestParam("f") String filtro,
			@RequestParam("e") EstadoEntregaEnum estado) {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(entregaService.recuperarEntrega(filtro, estado));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@PostMapping("/entrega/guardarEntrega")
	public ResponseEntity<?> guardarEntrega(@RequestBody EntregaDTO p) {
		try {
			return ResponseEntity.status(HttpStatus.CREATED).body(entregaService.enviarEntrega(p));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@DeleteMapping("/entrega/variedad/{id}/")
	public ResponseEntity<?> eliminarDetalle(@PathVariable("id") Integer idDetalleVariedad) {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(entregaService.eliminarEntregaDetalle(idDetalleVariedad));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}
	@GetMapping("/entrega/recuperaEntregas/")
	public ResponseEntity<?> recuperaEntregas(@RequestParam("pro") Integer idProveedor,
			@RequestParam("cli") Integer idCliente, @RequestParam("fei") Long inicio, @RequestParam("fef") Long fin) {
		try {
			LocalDate entregaDesde = (new Timestamp(inicio)).toLocalDateTime().toLocalDate();
			LocalDate entregaHasta = (new Timestamp(fin)).toLocalDateTime().toLocalDate();
			return ResponseEntity.status(HttpStatus.OK)
					.body(entregaService.recuperaEntregas(entregaDesde, entregaHasta, idProveedor, idCliente));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

}
