package com.keysist.roses.view.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.keysist.roses.model.dto.CamaRegistroDTO;
import com.keysist.roses.model.services.CamaService;

@RestController
public class CamaController {
	@Autowired
	private CamaService bloqueServiceImpl;

	@GetMapping("/cama/obtener/")
	public ResponseEntity<?> obtener(@RequestParam("idb") Integer idb) {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(bloqueServiceImpl.consultar(idb));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@PostMapping("/cama/crear/")
	public ResponseEntity<?> registrar(@RequestBody CamaRegistroDTO dto) {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(bloqueServiceImpl.crear(dto));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@DeleteMapping("/cama/inactivar/{id}/")
	public ResponseEntity<?> inactivar(@PathVariable("id") Integer idCama) {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(bloqueServiceImpl.inactivar(idCama));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

}
