package com.keysist.roses.view.controller;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.keysist.roses.model.dto.EntregaBoncheDTO;
import com.keysist.roses.model.dto.EntregaNacionalDTO;
import com.keysist.roses.model.dto.EntregaPoscosechaDTO;
import com.keysist.roses.model.dto.EmpresaVariedadPrecioDTO;
import com.keysist.roses.model.dto.EntregaTalloDTO;
import com.keysist.roses.model.services.PoscosechaService;

@RestController
public class PoscosechaController {
	@Autowired
	private PoscosechaService poscosechaService;

	@PostMapping("/poscosecha/guardarDetallePoscosecha")
	public ResponseEntity<?> guardarEntrega(@RequestBody List<EntregaPoscosechaDTO> list) {
		try {
			return ResponseEntity.status(HttpStatus.CREATED).body(poscosechaService.guardarDetallePoscosecha(list));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@PostMapping("/poscosecha/guardarDetallePoscosechaVariedad")
	public ResponseEntity<?> guardarEntregaVariedad(@RequestBody EntregaPoscosechaDTO ent) {
		try {
			return ResponseEntity.status(HttpStatus.CREATED)
					.body(poscosechaService.guardarDetallePoscosechaVariedad(ent));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@PostMapping("/poscosecha/procesar")
	public ResponseEntity<?> procesar(@RequestBody List<EntregaPoscosechaDTO> list) {
		try {
			return ResponseEntity.status(HttpStatus.CREATED).body(poscosechaService.procesar(list));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@PostMapping("/poscosecha/procesarVariedad")
	public ResponseEntity<?> procesarVariedad(@RequestBody EntregaPoscosechaDTO ent) {
		try {
			return ResponseEntity.status(HttpStatus.CREATED).body(poscosechaService.procesarVariedad(ent));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@PostMapping("/poscosecha/finalizar")
	public ResponseEntity<?> finalizar(@RequestBody List<EntregaPoscosechaDTO> list) {
		try {
			return ResponseEntity.status(HttpStatus.CREATED).body(poscosechaService.finalizar(list));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@PostMapping("/poscosecha/finalizarVariedad")
	public ResponseEntity<?> finalizarVariedad(@RequestBody EntregaPoscosechaDTO ent) {
		try {
			return ResponseEntity.status(HttpStatus.CREATED).body(poscosechaService.finalizarVariedad(ent));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@GetMapping("/poscosecha/recuperaDetallesEntregaProcesso/")
	public ResponseEntity<?> recuperaDetallesEntregaProceso(@RequestParam("pro") Integer idProveedor,
			@RequestParam("cli") Integer idCliente, @RequestParam("fe") Long f) {
		try {
			LocalDate fechaEntrega = (new Timestamp(f)).toLocalDateTime().toLocalDate();
			return ResponseEntity.status(HttpStatus.OK)
					.body(poscosechaService.recuperarDetallesEntrega(fechaEntrega, idProveedor, idCliente));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@GetMapping("/poscosecha/recuperaEntregaBonche/")
	public ResponseEntity<?> recuperaEntregaBonche(@RequestParam("pro") Integer idProveedor,
			@RequestParam("cli") Integer idCliente, @RequestParam("fe") Long f,
			@RequestParam("iev") Integer idEmpresaVariedad, @RequestParam("idl") Integer idLongitud,
			@RequestParam("idm") Integer idMesa, @RequestParam("ipc") Integer idPuntoCorte,
			@RequestParam("idb") Integer idTalloBonche, @RequestParam("idc") Integer idTipoCarton) {
		try {
			LocalDate fechaEntrega = (new Timestamp(f)).toLocalDateTime().toLocalDate();
			return ResponseEntity.status(HttpStatus.OK)
					.body(poscosechaService.recuperarEntregaBonche(fechaEntrega, idCliente, idProveedor,
							idEmpresaVariedad, idLongitud, idMesa, idPuntoCorte, idTalloBonche, idTipoCarton));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@PostMapping("/poscosecha/guardarEntregaBonche")
	public ResponseEntity<?> guardarEntrega(@RequestBody EntregaBoncheDTO dto) {
		try {
			return ResponseEntity.status(HttpStatus.CREATED).body(poscosechaService.guardarEntregaBonche(dto));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@GetMapping("/poscosecha/recuperaEntregaNacional/")
	public ResponseEntity<?> recuperaEntregaNacional(@RequestParam("pro") Integer idProveedor,
			@RequestParam("cli") Integer idCliente, @RequestParam("fe") Long fechaProceso,
			@RequestParam("iev") Integer idEmpresaVariedad, @RequestParam("idc") Integer idCategoria,
			@RequestParam("idSC") Integer idSubCategoria, @RequestParam("idB") Integer idBloque,
			@RequestParam("idCl") Integer idClasificador) {
		try {
			LocalDate fechaEntrega = (new Timestamp(fechaProceso)).toLocalDateTime().toLocalDate();
			return ResponseEntity.status(HttpStatus.OK)
					.body(poscosechaService.recuperarDetallesEntregaNacional(fechaEntrega, idCliente, idProveedor,
							idEmpresaVariedad, idCategoria, idSubCategoria, idBloque, idClasificador));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@PostMapping("/poscosecha/guardarEntregaNacional")
	public ResponseEntity<?> guardarEntregaNacional(@RequestBody EntregaNacionalDTO dto) {
		try {
			return ResponseEntity.status(HttpStatus.CREATED).body(poscosechaService.guardarEntregaNacional(dto));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@GetMapping("/poscosecha/recuperaEntregas/")
	public ResponseEntity<?> recuperaEntregas(@RequestParam("pro") Integer idProveedor,
			@RequestParam("cli") Integer idCliente, @RequestParam("fei") Long inicio, @RequestParam("fef") Long fin) {
		try {
			LocalDate entregaDesde = (new Timestamp(inicio)).toLocalDateTime().toLocalDate();
			LocalDate entregaHasta = (new Timestamp(fin)).toLocalDateTime().toLocalDate();
			return ResponseEntity.status(HttpStatus.OK)
					.body(poscosechaService.recuperaEntregas(entregaDesde, entregaHasta, idProveedor, idCliente));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@PostMapping("/poscosecha/registrarprecio")
	public ResponseEntity<?> registrarPrecio(@RequestBody EmpresaVariedadPrecioDTO dto) {
		try {
			return ResponseEntity.status(HttpStatus.CREATED).body(poscosechaService.registrarPrecio(dto));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@GetMapping("/poscosecha/filtrarvariedadprecio/")
	public ResponseEntity<?> getEmpresaVariedadPrecio(@RequestParam("iev") Integer idEmpresaVariedad,
			@RequestParam("cli") Integer idCliente, @RequestParam("fd") Long f, @RequestParam("fh") Long h,
			@RequestParam("idl") Integer idLongitud) {
		try {
			return ResponseEntity.status(HttpStatus.OK)
					.body(poscosechaService.filtrarVariedadPrecio(f, h, idLongitud, idEmpresaVariedad, idCliente));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@GetMapping("/poscosecha/recuperaEntregaTallo/")
	public ResponseEntity<?> recuperaEntregaTallo(@RequestParam("pro") Integer idProveedor,
			@RequestParam("cli") Integer idCliente, @RequestParam("fe") Long f,
			@RequestParam("iev") Integer idEmpresaVariedad, @RequestParam("idl") Integer idLongitud) {
		try {
			LocalDate fechaEntrega = (new Timestamp(f)).toLocalDateTime().toLocalDate();
			return ResponseEntity.status(HttpStatus.OK).body(poscosechaService.recuperarEntregaTallo(fechaEntrega,
					idCliente, idProveedor, idEmpresaVariedad, idLongitud));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@PostMapping("/poscosecha/guardarEntregaTallo")
	public ResponseEntity<?> guardarEntrega(@RequestBody EntregaTalloDTO dto) {
		try {
			return ResponseEntity.status(HttpStatus.CREATED).body(poscosechaService.guardarEntregaBoncheTallo(dto));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

}
