package com.keysist.roses.view.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.keysist.roses.model.dto.EmpresaVariedadDTO;
import com.keysist.roses.model.services.EmpresaVariedadService;

@RestController
public class EmpresaVariedadController {
	@Autowired
	private EmpresaVariedadService usuarioService;

	@PostMapping("/empresavariedad/save/")
	public ResponseEntity<?> save(@RequestBody EmpresaVariedadDTO u) {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(usuarioService.save(u));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}
	

	
	

}
