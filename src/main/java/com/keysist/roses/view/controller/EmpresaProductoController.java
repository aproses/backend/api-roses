package com.keysist.roses.view.controller;

import java.math.BigDecimal;
import java.math.BigInteger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.keysist.roses.model.dto.EmpresaProductoDTO;
import com.keysist.roses.model.services.EmpresaProductoService;

@RestController
public class EmpresaProductoController {
	@Autowired
	private EmpresaProductoService empresaProductoService;

	@PostMapping("/empresaproducto/guardar/")
	public ResponseEntity<?> save(@RequestBody EmpresaProductoDTO c) {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(empresaProductoService.guardar(c));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@PostMapping("/empresaproducto/compra/{idProducto}/")
	public ResponseEntity<?> registrarCompra(@PathVariable("idProducto") Integer idProducto,
			@RequestParam("c") BigInteger cantidad, @RequestParam("p") BigDecimal precio,
			@RequestParam("cu") BigDecimal cantidadPorUnidad) {
		try {
			return ResponseEntity.status(HttpStatus.OK)
					.body(empresaProductoService.registrarCompra(idProducto, cantidad, precio, cantidadPorUnidad));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@PostMapping("/empresaproducto/uso/{idProducto}/")
	public ResponseEntity<?> registrarUso(@PathVariable("idProducto") Integer idProducto,
			@RequestParam("c") BigDecimal cantidadGramos) {
		try {
			return ResponseEntity.status(HttpStatus.OK)
					.body(empresaProductoService.registrarUso(idProducto, cantidadGramos));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@GetMapping("/empresaproducto/disponibilidad/{idUsuario}/")
	public ResponseEntity<?> disponibilidad(@PathVariable("idUsuario") Integer idEmpresa,
			@RequestParam("e") String enfermedad) {
		try {
			return ResponseEntity.status(HttpStatus.OK)
					.body(empresaProductoService.consultarDisponibilidad(idEmpresa, enfermedad));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}
	@GetMapping("/empresaproducto/bytipo/{idEmpresa}/{idTipo}/")
	public ResponseEntity<?> disponibilidad(@PathVariable("idEmpresa") Integer idEmpresa,
			@PathVariable("idTipo") Integer idTipo,
			@RequestParam("e") String enfermedad) {
		try {
			return ResponseEntity.status(HttpStatus.OK)
					.body(empresaProductoService.consultarDisponibilidadAndTipo(idEmpresa, idTipo));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@GetMapping("/empresaproducto/porterminar/{idUsuario}/")
	public ResponseEntity<?> porTerminar(@PathVariable("idUsuario") Integer idEmpresa) {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(empresaProductoService.consultarPorTerminar(idEmpresa));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@GetMapping("/empresaproducto/consultar")
	public ResponseEntity<?> consultar(@RequestParam("id") Integer idEmpresa, @RequestParam("f") String filtro) {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(empresaProductoService.consultar(idEmpresa, filtro));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}

	}

	@GetMapping("/empresaproducto/buscar/{id}")
	public ResponseEntity<?> buscar(@PathVariable("id") Integer idEmpresaProducto) {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(empresaProductoService.consultar(idEmpresaProducto));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}
	
}
