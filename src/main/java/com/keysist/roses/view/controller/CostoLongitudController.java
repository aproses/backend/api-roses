package com.keysist.roses.view.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.keysist.roses.model.dto.CostoLongitudDTO;
import com.keysist.roses.model.services.CostoLongitudService;

@RestController
public class CostoLongitudController {
	
	@Autowired
	private CostoLongitudService costoLongitudService;


	@PostMapping("/costolongitud/save/")
	public ResponseEntity<?> save(@RequestBody List<CostoLongitudDTO> c) {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(costoLongitudService.save(c));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

}
