package com.keysist.roses.view.controller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.keysist.roses.model.dto.CorteDTO;
import com.keysist.roses.model.services.ProduccionService;
import com.keysist.roses.model.services.ReporteService;

@RestController
public class ReporteController {
	@Autowired
	private ReporteService iReporteService;

	@Autowired
	private ProduccionService produccionService;

	@GetMapping("/reporte/proveedorvariedad/")
	public ResponseEntity<?> proveedorvariedad(
			@RequestParam("fd") @DateTimeFormat(pattern = "dd/MM/yyyy") Date fechaDesde,
			@RequestParam("fh") @DateTimeFormat(pattern = "dd/MM/yyyy") Date fechaHasta) {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(iReporteService.proveedorVariedad(fechaDesde, fechaHasta));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@GetMapping("/reporte/proyeccion/{idEmpresaVariedad}/")
	public ResponseEntity<?> proyeccion(@PathVariable("idEmpresaVariedad") Integer idEmpresaVariedad,
			@RequestParam("fd") Long desde, @RequestParam("fh") Long hasta) {
		try {
			return ResponseEntity.status(HttpStatus.OK)
					.body(produccionService.obtenerProyeccion(idEmpresaVariedad, desde, hasta));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@GetMapping("/reporte/corte/{idEmpresaVariedad}/")
	public ResponseEntity<?> getCortes(@PathVariable("idEmpresaVariedad") Integer idEmpresaVariedad,
			@RequestParam("fd") Long desde, @RequestParam("fh") Long hasta) {
		try {
			return ResponseEntity.status(HttpStatus.OK)
					.body(produccionService.obtenerProyeccion(idEmpresaVariedad, desde, hasta));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@PostMapping("/reporte/corte/")
	public ResponseEntity<?> save(@RequestBody CorteDTO c) {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(produccionService.obtenerFechaCorte(c));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

}
