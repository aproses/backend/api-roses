package com.keysist.roses.view.controller;

import java.sql.Timestamp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.keysist.roses.model.dto.AplicacionDTO;
import com.keysist.roses.model.services.AplicacionService;

@RestController
public class AplicacionController {
	@Autowired
	private AplicacionService aplicacionService;

	@PostMapping("/aplicacion/guardar/")
	public ResponseEntity<?> save(@RequestBody AplicacionDTO c) {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(aplicacionService.guardar(c));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@GetMapping("/aplicacion/buscar")
	public ResponseEntity<?> buscar(@RequestParam("idu") Integer idUser, @RequestParam("fd") Long desde,
			@RequestParam("fh") Long hasta, @RequestParam("pl") String plaga, @RequestParam("it") Integer idTipo,
			@RequestParam("pub") String publico, @RequestParam("ip") Integer idProducto,
			@RequestParam("ieb") Integer idEmpresaBloque) {
		try {
			return ResponseEntity.status(HttpStatus.OK)
					.body(aplicacionService.buscar(desde, hasta, idUser, plaga, idTipo, idProducto, idEmpresaBloque));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@GetMapping("/aplicacion/consumonit")
	public ResponseEntity<?> consumonit(@RequestParam("idu") Integer idUser, @RequestParam("fd") Long desde,
			@RequestParam("fh") Long hasta) {
		try {
			return ResponseEntity.status(HttpStatus.OK)
					.body(aplicacionService.obtnerConsumo((new Timestamp(desde)).toLocalDateTime().toLocalDate(),
							(new Timestamp(hasta)).toLocalDateTime().toLocalDate(), idUser));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@GetMapping("/aplicacion/consumototal")
	public ResponseEntity<?> consumoTotal(@RequestParam("idu") Integer idUser, @RequestParam("fd") Long desde,
			@RequestParam("fh") Long hasta, @RequestParam("ideb") Integer idEmpresaBloque) {
		try {
			return ResponseEntity.status(HttpStatus.OK)
					.body(aplicacionService.obtenerConsumoTotal((new Timestamp(desde)).toLocalDateTime().toLocalDate(),
							(new Timestamp(hasta)).toLocalDateTime().toLocalDate(), idUser, idEmpresaBloque));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}
	@GetMapping("/aplicacion/consumototalmes")
	public ResponseEntity<?> consumoTotalMes(@RequestParam("idu") Integer idUser, @RequestParam("fd") Long desde,
			@RequestParam("fh") Long hasta, @RequestParam("ideb") Integer idEmpresaBloque) {
		try {
			return ResponseEntity.status(HttpStatus.OK)
					.body(aplicacionService.obtenerConsumoTotalPorMes((new Timestamp(desde)).toLocalDateTime().toLocalDate(),
							(new Timestamp(hasta)).toLocalDateTime().toLocalDate(), idUser, idEmpresaBloque));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@GetMapping("/aplicacion/iniciar/{id}/")
	public ResponseEntity<?> registrarInicio(@PathVariable("id") Integer idAplicacion) {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(aplicacionService.iniciar(idAplicacion));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@GetMapping("/aplicacion/finalizar/{id}/")
	public ResponseEntity<?> registrarFin(@PathVariable("id") Integer idAplicacion) {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(aplicacionService.finalizar(idAplicacion));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@GetMapping("/aplicacion/copiar/{id}/")
	public ResponseEntity<?> copiar(@PathVariable("id") Integer idAplicacion) {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(aplicacionService.copiar(idAplicacion));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@GetMapping("/aplicacion/obtener/{id}/")
	public ResponseEntity<?> obtener(@PathVariable("id") Integer idAplicacion) {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(aplicacionService.findById(idAplicacion));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@GetMapping("/aplicacion/pendientes")
	public ResponseEntity<?> pendientes(@RequestParam("idu") Integer idEmpresa) {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(aplicacionService.obtenerPendientes(idEmpresa));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@DeleteMapping("/aplicacion/eliminar/{idA}/")
	public ResponseEntity<?> eliminar(@PathVariable("idA") Integer idAplicacion) {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(aplicacionService.eliminar(idAplicacion));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

}
