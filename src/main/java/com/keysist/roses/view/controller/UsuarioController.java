package com.keysist.roses.view.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.keysist.roses.model.dto.GenericResponse;
import com.keysist.roses.model.dto.UsuarioDTO;
import com.keysist.roses.model.services.UsuarioService;

@RestController
public class UsuarioController {
	@Autowired
	private UsuarioService usuarioService;

	@GetMapping("/usuario/find-by-filter/{emp}")
	public ResponseEntity<?> recuperarUsuario(@RequestParam("f") String filtro,
			@PathVariable("emp") Integer idEmpresa) {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(usuarioService.findByFilter(filtro, idEmpresa));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@GetMapping("/usuario/find-by-perfil/{emp}")
	public ResponseEntity<?> filterByRol(@RequestParam("p") String perfil, @PathVariable("emp") Integer idEmpresa) {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(usuarioService.findByPerfil(perfil, idEmpresa));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@DeleteMapping("/usuario/delete/{id}/")
	public ResponseEntity<?> delete(@PathVariable("id") Integer idUsuario) {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(usuarioService.delete(idUsuario));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@PostMapping("/usuario/save/")
	public ResponseEntity<?> save(@RequestBody UsuarioDTO u, @RequestParam("f") String filtro) {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(usuarioService.save(u, filtro));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@PostMapping("/usuario/update/")
	public ResponseEntity<?> update(@RequestBody UsuarioDTO u) {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(usuarioService.update(u));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@GetMapping("/usuario/recoverPass")
	public ResponseEntity<?> recoverPass(@RequestParam("f") String cedula) {
		try {
			return ResponseEntity.status(HttpStatus.OK)
					.body(new GenericResponse("", usuarioService.recoverPass(cedula)));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@PostMapping("/usuario/changepass")
	public ResponseEntity<?> chanfePass(@RequestBody UsuarioDTO u) {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(new GenericResponse("", usuarioService.changePass(u)));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@PostMapping(value = "usuario/updateFoto", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> updateLogo(@RequestParam(value = "file", required = true) MultipartFile file,
			@RequestParam("idUsuario") Integer idUsuario) {
		try {
			return ResponseEntity.status(HttpStatus.OK)
					.body(new GenericResponse("", usuarioService.updateAvatar(file, idUsuario)));
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@GetMapping(value = "usuario/foto/{idUsuario}", produces = MediaType.IMAGE_PNG_VALUE)
	public ResponseEntity<?> getImage(@PathVariable("idUsuario") Integer id) {
		try {
			return new ResponseEntity<>(usuarioService.obtenerImagen(id), HttpStatus.OK);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GetMapping(value = "usuario/byid/{idUsuario}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> findById(@PathVariable("idUsuario") Integer id) {
		try {
			return new ResponseEntity<>(usuarioService.findUsuarioById(id), HttpStatus.OK);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
