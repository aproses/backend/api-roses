package com.keysist.roses.view.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.keysist.roses.model.dto.EmpresaBloqueDTO;
import com.keysist.roses.model.services.EmpresaBloqueService;

@RestController
public class EmpresaBloqueController {
	@Autowired
	private EmpresaBloqueService empresaBloqueService;

	@PostMapping("/empresabloque/guardar")
	public ResponseEntity<?> registrar(@RequestBody EmpresaBloqueDTO dto) {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(empresaBloqueService.save(dto));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@RequestMapping("/empresabloque/byId/{idu}/")
	public ResponseEntity<?> obtener(@PathVariable("idu") Integer idEmpresaBloque) {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(empresaBloqueService.findById(idEmpresaBloque));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}
	

	@DeleteMapping("/empresabloque/delete/{idu}/")
	public ResponseEntity<?> delete(@PathVariable("idu") Integer idEmpresaBloque) {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(empresaBloqueService.delete(idEmpresaBloque));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@GetMapping("/empresabloque/by-empresa")
	public ResponseEntity<?> buscar(@RequestParam("idEmpresa") Integer idEmpresa) {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(empresaBloqueService.consultar(idEmpresa));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

}
