package com.keysist.roses.scheduleds;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.keysist.roses.model.services.AplicacionService;

@Component
public class ScheduledTasksAplicaciones {

	private static final Logger log = LoggerFactory.getLogger(ScheduledTasksAplicaciones.class);

	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

	@Autowired
	AplicacionService aplicacionService;

	@Scheduled(cron = "0 0/10 7-16 * * *")
	@Transactional
	public void validarFumnigaciones() {
		log.info("Inicia verificación de aplicaciones de fumnigacion{}", dateFormat.format(new Date()));
		aplicacionService.fumnigacionSinFinalizar();
	}
	

	@Scheduled(cron = "0 0/120 10-16 * * *")
	@Transactional
	public void validarFertilizacion() {
		log.info("Inicia verificación de aplicaciones de fertilizacion{}", dateFormat.format(new Date()));
		aplicacionService.fertilizacionSinFinalizar();
	}
}
