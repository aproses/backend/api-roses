package com.keysist.roses.scheduleds;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.keysist.roses.model.services.EmpresaProductoService;

@Component
public class ScheduledTasksProductos {

	private static final Logger log = LoggerFactory.getLogger(ScheduledTasksProductos.class);

	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

	@Autowired
	EmpresaProductoService aplicacionService;

	@Scheduled(cron = "0 0 6,10,13,15 * * *")
	@Transactional
	public void validarFumnigaciones() {
		log.info("Inicia verificación de productos por términar {}", dateFormat.format(new Date()));
		aplicacionService.productosPorTerminar();
	}
	

}
