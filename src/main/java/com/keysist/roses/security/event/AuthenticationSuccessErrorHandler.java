package com.keysist.roses.security.event;

import java.time.LocalDateTime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.authentication.AuthenticationEventPublisher;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.stereotype.Component;

import com.keysist.roses.model.entity.Usuario;
import com.keysist.roses.model.enums.ValorSiNoEnum;
import com.keysist.roses.security.service.IUserService;
import com.keysist.roses.util.AES;

@Component
public class AuthenticationSuccessErrorHandler implements AuthenticationEventPublisher {

	private Logger log = LoggerFactory.getLogger(AuthenticationSuccessErrorHandler.class);

	@Autowired
	private Environment env;

	@Autowired
	private IUserService iUserService;

	@Override
	public void publishAuthenticationSuccess(Authentication authentication) {
		String username = "";
		try {
			if (authentication.getDetails() instanceof OAuth2AuthenticationDetails
					|| authentication.getDetails() instanceof WebAuthenticationDetails) {
				return;
			}
			username = (new AES()).decrypt(authentication.getName());
			log.info(String.format("Login sucess : %1s", username));
			Usuario usuarioDb = iUserService.findUserByCedula(username);
			log.info(String.format("El usuario %1s existe en la base", username));
			usuarioDb.setFechaLogin(LocalDateTime.now());
			usuarioDb.setIntentos(0);
			iUserService.update(usuarioDb);

		} catch (Exception e) {
			log.error(String.format("El usuario %1s no existe en el sistema", username));
		}

	}

	@Override
	public void publishAuthenticationFailure(AuthenticationException exception, Authentication authentication) {
		String username = "";
		try {
			String anonymousUser = env.getProperty("config.security.oauth.anonymous");
			String nameClient = env.getProperty("config.security.oauth.client.id");
			log.error(String.format("Mensaje Erro Login: %1s", exception.getMessage()));
			username = (new AES()).decrypt(authentication.getName());
			log.info(String.format("Erro para el usuario: %1s", username));

			if (username.equalsIgnoreCase(nameClient) || username.equalsIgnoreCase(anonymousUser)) {
				return; // si es igual a frontendapp se salen del método! o un anonimo
			}
			Usuario usuario = iUserService.findUserByCedula(username);
			if (usuario != null) {
				usuario.setIntentos(usuario.getIntentos() != null ? usuario.getIntentos() + 1 : 0);
				if (usuario.getIntentos() >= 3) {
					usuario.setActivo(ValorSiNoEnum.NO);
				}
				iUserService.update(usuario);
			}
		} catch (AuthenticationException e) {
			log.error(String.format("Error el usuario %1s no existe en el sistema", username));
		}

	}

}
