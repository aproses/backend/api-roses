package com.keysist.roses.security.service;

import com.keysist.roses.model.dto.UsuarioDTO;
import com.keysist.roses.model.entity.Usuario;

public interface IUserService {

	public UsuarioDTO findDtoByCedula(String username);

	public Usuario update(Usuario usuario);

	public Usuario findUserByCedula(String username);
	
	public Usuario findUserByTipo(String codigo);
}
