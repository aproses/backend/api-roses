package com.keysist.roses.security.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.keysist.roses.model.dto.UsuarioDTO;
import com.keysist.roses.model.entity.Usuario;
import com.keysist.roses.model.enums.ValorSiNoEnum;
import com.keysist.roses.model.services.UsuarioService;
import com.keysist.roses.util.AES;

@Service
public class UserService implements UserDetailsService, IUserService {

	private Logger log = LoggerFactory.getLogger(UserService.class);

	@Autowired
	private Environment env;

	@Autowired
	private UsuarioService iUsuarioService;

	@Override
	public UserDetails loadUserByUsername(String usernameEncrypted) throws UsernameNotFoundException {
		String username = "";
		try {
			String anonymousUser = env.getProperty("config.security.oauth.anonymous");
			String anonymousPass = env.getProperty("config.security.oauth.anonymous.pass");
			String anonymousRole = env.getProperty("config.security.oauth.anonymous.role");
			username = (new AES()).decrypt(usernameEncrypted);
			log.info(String.format("Username Login -> %1s", username));
			if (anonymousUser.equalsIgnoreCase(username)) {
				List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
				authorities.add(new SimpleGrantedAuthority(anonymousRole));
				return new User(usernameEncrypted, anonymousPass, true, true, true, true, authorities);
			} else {
				try {
					UsuarioDTO usuario = iUsuarioService.findDtoByCedula(username);
					if (usuario != null) {
						if (ValorSiNoEnum.SI.equals(usuario.getActivo())) {

							List<GrantedAuthority> authorities = usuario.getPerfiles().stream()
									.map(per -> new SimpleGrantedAuthority(per.getNombre()))
									.peek(rol -> log.info(String.format("Rol: %1s", rol.getAuthority())))
									.collect(Collectors.toList());

							return new User(usernameEncrypted, usuario.getContrasenia(),
									ValorSiNoEnum.SI.equals(usuario.getActivo()), true, true, true, authorities);
						} else {
							throw new UsernameNotFoundException("El usuario se encuentra INACTIVO");
						}
					} else {
						throw new UsernameNotFoundException("No existe el usuario");
					}
				} catch (Exception e) {
					e.printStackTrace();
					throw new UsernameNotFoundException("No existe el usuario");
				}
			}
		} catch (UsernameNotFoundException e) {
			e.printStackTrace();
			log.error(String.format("No existe el usuario con la identificación %1s", username));
			throw new UsernameNotFoundException(
					String.format("No existe el usuario con la identificación %1s", username));
		}

	}

	@Override
	public UsuarioDTO findDtoByCedula(String cedula) {
		return iUsuarioService.findDtoByCedula(cedula);
	}

	@Override
	public Usuario update(Usuario usuario) {
		return iUsuarioService.update(usuario);
	}

	@Override
	public Usuario findUserByCedula(String cedula) {
		return iUsuarioService.findUsuarioByCedula(cedula);
	}

	@Override
	public Usuario findUserByTipo(String codigo) {
		return iUsuarioService.findUsuarioByCedula(codigo);
	}

}
