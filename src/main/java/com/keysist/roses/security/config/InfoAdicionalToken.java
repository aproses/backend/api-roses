package com.keysist.roses.security.config;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.stereotype.Component;

import com.keysist.roses.model.dto.UsuarioDTO;
import com.keysist.roses.security.service.IUserService;
import com.keysist.roses.util.AES;

@Component
public class InfoAdicionalToken implements TokenEnhancer {

	private Logger log = LoggerFactory.getLogger(InfoAdicionalToken.class);

	@Autowired
	private IUserService iUsuarioService;

	@Autowired
	private Environment env;

	@Override
	public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
		Map<String, Object> info = new HashMap<>();
		String usernameEncrypted = authentication.getName();
		String username = (new AES()).decrypt(usernameEncrypted);
		log.info(String.format("Username OAuth2 : %1s", username));
		String anonymousUser = env.getProperty("config.security.oauth.anonymous");
		if (!anonymousUser.equalsIgnoreCase(username)) {
			UsuarioDTO user = iUsuarioService.findDtoByCedula(username);
			if (user != null) {
				info.put("id", user.getId());
				info.put("nombres", user.getNombres());
				info.put("apellidos", user.getApellidos());
				info.put("isAnonymous", "N");
				info.put("perfiles", user.getPerfiles());
				info.put("empresa", user.getEmpresa());
				info.put("empresaId", user.getEmpresaId());
			}
		} else {
			info.put("idUser", -1);
			info.put("isAnonymous", "S");
		}
		((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(info);
		return accessToken;
	}

}
