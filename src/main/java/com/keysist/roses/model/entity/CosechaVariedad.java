package com.keysist.roses.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.keysist.roses.model.enums.EstadoCosechaVariedadEnum;

@Entity
@Table(name = "s_cosecha_variedad")
public class CosechaVariedad implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5494631274223867891L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "scv_id")
	private Integer id;

	@Column(name = "scv_cant_malla")
	private Integer cantidadMalla;

	@Column(name = "scv_tallo_malla")
	private Integer talloMalla;

	@Column(name = "scv_tallo_total")
	private Integer totalTallo;
	
	@Column(name = "scv_tallo_total_nacional")
	private Integer totalTalloNacional;

	@Column(name = "scv_tallo_total_disponible")
	private Integer totalTalloDisponible;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_empresa_variedad")
	private EmpresaVariedad empresaVariedad;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "sev_cosecha")
	private Cosecha cosecha;

	@Column(name = "sev_estado")
	@Enumerated(EnumType.STRING)
	private EstadoCosechaVariedadEnum estado;

	public CosechaVariedad() {
		// TODO Auto-generated constructor stub
	}

	public CosechaVariedad(CosechaVariedad cosechaVariedad) {
		// TODO Auto-generated constructor stub
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getCantidadMalla() {
		return cantidadMalla;
	}

	public void setCantidadMalla(Integer cantidadMalla) {
		this.cantidadMalla = cantidadMalla;
	}

	public Integer getTalloMalla() {
		return talloMalla;
	}

	public void setTalloMalla(Integer talloMalla) {
		this.talloMalla = talloMalla;
	}

	public Integer getTotalTallo() {
		return totalTallo;
	}

	public void setTotalTallo(Integer totalTallo) {
		this.totalTallo = totalTallo;
	}

	public EmpresaVariedad getEmpresaVariedad() {
		return empresaVariedad;
	}

	public void setEmpresaVariedad(EmpresaVariedad empresaVariedad) {
		this.empresaVariedad = empresaVariedad;
	}

	public Cosecha getCosecha() {
		return cosecha;
	}

	public void setCosecha(Cosecha cosecha) {
		this.cosecha = cosecha;
	}

	public EstadoCosechaVariedadEnum getEstado() {
		return estado;
	}

	public void setEstado(EstadoCosechaVariedadEnum estado) {
		this.estado = estado;
	}

	public Integer getTotalTalloDisponible() {
		return totalTalloDisponible;
	}

	public void setTotalTalloDisponible(Integer totalTalloDisponible) {
		this.totalTalloDisponible = totalTalloDisponible;
	}

	public Integer getTotalTalloNacional() {
		return totalTalloNacional;
	}

	public void setTotalTalloNacional(Integer totalTalloNacional) {
		this.totalTalloNacional = totalTalloNacional;
	}
	

}
