package com.keysist.roses.model.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "s_producto_composicion")
public class ProductoComposicion implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5494631274223867891L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "spc_id")
	private Integer id;

	@Column(name = "spc_dosis")
	private BigDecimal dosis;

	@Column(name = "spc_porcentaje")
	private BigDecimal porcentaje;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "spc_elemento")
	private Catalogo elemento;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "spc_producto")
	private Producto producto;

	public ProductoComposicion() {
		// TODO Auto-generated constructor stub
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public BigDecimal getDosis() {
		return dosis;
	}

	public void setDosis(BigDecimal dosis) {
		this.dosis = dosis;
	}

	public BigDecimal getPorcentaje() {
		return porcentaje;
	}

	public void setPorcentaje(BigDecimal porcentaje) {
		this.porcentaje = porcentaje;
	}

	public Catalogo getElemento() {
		return elemento;
	}

	public void setElemento(Catalogo elemento) {
		this.elemento = elemento;
	}

	public Producto getProducto() {
		return producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}

}
