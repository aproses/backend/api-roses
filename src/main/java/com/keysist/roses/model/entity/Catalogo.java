package com.keysist.roses.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.keysist.roses.model.enums.ValorEstadoEnum;

@Entity
@Table(name = "s_catalogo")
public class Catalogo implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5494631274223867891L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "sc_id")
	private Integer id;

	@Column(name = "sc_codigo")
	private String codigo;

	@Column(name = "sc_codigoGrupo")
	private String codigoGrupo;

	@Column(name = "sc_nombre")
	private String nombre;

	@Column(name = "sc_valor")
	private Double valor;

	@Column(name = "sc_texto1")
	private String texto1;

	@Column(name = "sc_texto2")
	private String texto2;
	
	@Column(name = "sc_textos")
	private String[] textos;

	@Column(name = "sc_global")
	private String global;

	@Column(name = "sc_estado")
	@Enumerated(EnumType.STRING)
	private ValorEstadoEnum estado;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "sc_empresa")
	private Empresa empresa;

	@Column(name = "sc_orden")
	private Integer orden;

	public Catalogo() {
		// TODO Auto-generated constructor stub
	}

	public Catalogo(Catalogo c) {
		this.id = c.id;
		this.codigoGrupo = c.codigoGrupo;
		this.codigo = c.codigo;
		this.nombre = c.nombre;
		this.valor = c.valor;
		this.texto1 = c.texto1;
		this.texto2 = c.texto2;
		this.estado = c.estado;
	}

	public Catalogo(String codigo, String codigoGrupo, String nombre, Double valor, String texto1,
			String texto2, ValorEstadoEnum estado) {
		super();
		this.codigo = codigo;
		this.codigoGrupo = codigoGrupo;
		this.nombre = nombre;
		this.valor = valor;
		this.texto1 = texto1;
		this.texto2 = texto2;
		this.estado = estado;
	}
	

	public Catalogo(String codigo, String codigoGrupo, String nombre, String[] textos, String global,
			ValorEstadoEnum estado, Empresa empresa) {
		super();
		this.codigo = codigo;
		this.codigoGrupo = codigoGrupo;
		this.nombre = nombre;
		this.textos = textos;
		this.global = global;
		this.estado = estado;
		this.empresa = empresa;
	}

	public Catalogo(Integer id) {
		super();
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getCodigoGrupo() {
		return codigoGrupo;
	}

	public void setCodigoGrupo(String codigoGrupo) {
		this.codigoGrupo = codigoGrupo;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	/**
	 * Representa los valores segun el caso
	 * 
	 * Catalogo de tallo por malla: representa si es un valor por defecto
	 */
	public String getTexto1() {
		return texto1;
	}

	public void setTexto1(String texto1) {
		this.texto1 = texto1;
	}

	public String getTexto2() {
		return texto2;
	}

	public void setTexto2(String texto2) {
		this.texto2 = texto2;
	}

	public ValorEstadoEnum getEstado() {
		return estado;
	}

	public void setEstado(ValorEstadoEnum estado) {
		this.estado = estado;
	}

	public String getGlobal() {
		return global;
	}

	public void setGlobal(String global) {
		this.global = global;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public Integer getOrden() {
		return orden;
	}

	public void setOrden(Integer orden) {
		this.orden = orden;
	}

	public String[] getTextos() {
		return textos;
	}

	public void setTextos(String[] textos) {
		this.textos = textos;
	}
	

}
