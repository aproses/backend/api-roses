package com.keysist.roses.model.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.keysist.roses.model.enums.ValorSiNoEnum;

@Entity
@Table(name = "s_relacion_comercial")
public class RelacionComercial implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5494631274223867891L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "src_id")
	private Integer id;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "src_proveedor")
	private Empresa proveedor;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "src_cliente")
	private Empresa cliente;

	@Column(name = "src_proveedor_codigo")
	private String proveedorCodigo;

	@Column(name = "src_proveedor_efectividad")
	private BigDecimal proveedorEfectividad;

	@Column(name = "src_activo")
	@Enumerated(EnumType.STRING)
	private ValorSiNoEnum activo;

	@Column(name = "src_fecha_registro")
	private LocalDateTime fechaRegistro;

	public RelacionComercial() {
		// TODO Auto-generated constructor stub
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Empresa getProveedor() {
		return proveedor;
	}

	public void setProveedor(Empresa proveedor) {
		this.proveedor = proveedor;
	}

	public Empresa getCliente() {
		return cliente;
	}

	public void setCliente(Empresa cliente) {
		this.cliente = cliente;
	}

	public String getProveedorCodigo() {
		return proveedorCodigo;
	}

	public void setProveedorCodigo(String proveedorCodigo) {
		this.proveedorCodigo = proveedorCodigo;
	}

	public BigDecimal getProveedorEfectividad() {
		return proveedorEfectividad;
	}

	public void setProveedorEfectividad(BigDecimal proveedorEfectividad) {
		this.proveedorEfectividad = proveedorEfectividad;
	}

	public ValorSiNoEnum getActivo() {
		return activo;
	}

	public void setActivo(ValorSiNoEnum activo) {
		this.activo = activo;
	}

	public LocalDateTime getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(LocalDateTime fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

}
