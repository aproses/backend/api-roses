package com.keysist.roses.model.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.keysist.roses.model.dto.EntregaPoscosechaDTO;
import com.keysist.roses.model.enums.EstadoEntregaProcesoEnum;

@Entity
@Table(name = "s_entrega_poscosecha")
public class EntregaPoscosecha implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5494631274223867891L;

	@Id
	@GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
	@Column(name = "sep_id", unique = true, nullable = false)
	private Integer id;

	@Column(name = "sep_tallos")
	private Integer tallos;

	@Column(name = "sep_exportacion")
	private Integer exportacion;

	@Column(name = "sep_nacional")
	private Integer nacional;

	@Column(name = "sep_descabezado")
	private Integer descabezado;

	@Column(name = "sep_devolucion")
	private Integer devolucion;

	@Column(name = "sep_sobrante_anterior")
	private Integer sobranteAnterior;

	@Column(name = "sep_precio_sobrante")
	private BigDecimal precioSobrante;

	@Column(name = "sep_efectividad")
	private BigDecimal efectividad;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "sev_entrega")
	private Entrega entrega;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "sev_empresa_variedad")
	private EmpresaVariedad empresaVariedad;

	@Column(name = "sev_estado")
	@Enumerated(EnumType.STRING)
	private EstadoEntregaProcesoEnum estado;

	public EntregaPoscosecha() {
	}

	public EntregaPoscosecha(Integer tallos, Entrega entrega, EmpresaVariedad empresaVariedad,
			EstadoEntregaProcesoEnum estado) {
		super();
		this.tallos = tallos;
		this.entrega = entrega;
		this.empresaVariedad = empresaVariedad;
		this.estado = estado;
		this.nacional = 0;
		this.exportacion = 0;

	}

	public void setAtributesDto(EntregaPoscosechaDTO dto) {
		setNacional(dto.getNacional());
		setExportacion(dto.getExportable());
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getTallos() {
		return tallos != null ? tallos : 0;
	}

	public void setTallos(Integer tallos) {
		this.tallos = tallos;
	}

	public Integer getExportacion() {
		return exportacion != null ? exportacion : 0;
	}

	public void setExportacion(Integer exportacion) {
		this.exportacion = exportacion;
	}

	public Integer getNacional() {
		return nacional != null ? nacional : 0;
	}

	public void setNacional(Integer nacional) {
		this.nacional = nacional;
	}

	public Integer getSobranteAnterior() {
		return sobranteAnterior;
	}

	public void setSobranteAnterior(Integer sobranteAnterior) {
		this.sobranteAnterior = sobranteAnterior;
	}

	public BigDecimal getPrecioSobrante() {
		return precioSobrante;
	}

	public void setPrecioSobrante(BigDecimal precioSobrante) {
		this.precioSobrante = precioSobrante;
	}

	public BigDecimal getEfectividad() {
		return efectividad;
	}

	public void setEfectividad(BigDecimal efectividad) {
		this.efectividad = efectividad;
	}

	public Entrega getEntrega() {
		return entrega;
	}

	public void setEntrega(Entrega entrega) {
		this.entrega = entrega;
	}

	public EmpresaVariedad getEmpresaVariedad() {
		return empresaVariedad;
	}

	public void setEmpresaVariedad(EmpresaVariedad empresaVariedad) {
		this.empresaVariedad = empresaVariedad;
	}

	public EstadoEntregaProcesoEnum getEstado() {
		return estado;
	}

	public void setEstado(EstadoEntregaProcesoEnum estado) {
		this.estado = estado;
	}

	public Integer getDescabezado() {
		return descabezado;
	}

	public void setDescabezado(Integer descabezado) {
		this.descabezado = descabezado;
	}

	public Integer getDevolucion() {
		return devolucion;
	}

	public void setDevolucion(Integer devolucion) {
		this.devolucion = devolucion;
	}

}
