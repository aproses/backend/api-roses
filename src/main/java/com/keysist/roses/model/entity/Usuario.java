package com.keysist.roses.model.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.keysist.roses.model.enums.ValorSiNoEnum;

@Entity
@Table(name = "s_usuario")
public class Usuario implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5494631274223867891L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "su_id")
	private Integer id;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "su_empresa")
	private Empresa empresa;

	@Column(name = "su_nombres")
	private String nombres;

	@Column(name = "su_apellidos")
	private String apellidos;

	@Column(name = "su_identificacion", nullable = false, unique = true)
	private String identificacion;

	@Column(name = "su_fechaRegistro")
	private LocalDateTime fechaRegistro;

	@Column(name = "su_contrasenia")
	private String contrasenia;

	@Column(name = "su_intentos")
	private Integer intentos;

	@Column(name = "su_fechaLogin")
	private LocalDateTime fechaLogin;

	@Column(name = "su_activo")
	@Enumerated(EnumType.STRING)
	private ValorSiNoEnum activo;

	@Column(name = "su_correo")
	private String correo;

	@OneToMany(mappedBy = "usuario")
	private List<UsuarioPerfil> usuarioPerfiles;

	@Column(name = "su_foto")
	private byte[] foto;

	public Usuario() {
		// TODO Auto-generated constructor stub
	}

	public Usuario(Empresa empresa, String nombres, String apellidos, String identificacion,
			LocalDateTime fechaRegistro, String contrasenia, ValorSiNoEnum activo, String correo) {
		super();
		this.empresa = empresa;
		this.nombres = nombres;
		this.apellidos = apellidos;
		this.identificacion = identificacion;
		this.fechaRegistro = fechaRegistro;
		this.contrasenia = contrasenia;
		this.activo = activo;
		this.correo = correo;
	}

	public Usuario(Integer id, ValorSiNoEnum activo, String nombres, String apellidos, String identificacion) {
		this.id = id;
		this.activo = activo;
		this.nombres = nombres;
		this.apellidos = apellidos;
		this.identificacion = identificacion;
	}

	public Usuario(Usuario u) {
		this.id = u.getId();
		this.nombres = u.getNombres();
		this.apellidos = u.getApellidos();
		this.activo = u.getActivo();
		if (u.getEmpresa() != null) {
			empresa = u.getEmpresa();
		}
	}
	public Usuario(Usuario u,Empresa em) {
		this.id = u.getId();
		this.nombres = u.getNombres();
		this.apellidos = u.getApellidos();
		this.activo = u.getActivo();
		this.correo=u.getCorreo();
		this.identificacion=u.getIdentificacion();
		if (em != null) {
			empresa =new Empresa(em.getNombre(), em.getRuc(), em.getCorreo(), em.getCelular(), em.getActivo(), em.getTipo(),em.getFechaRegistro());
		}
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getIdentificacion() {
		return identificacion;
	}

	public void setIdentificacion(String identificacion) {
		this.identificacion = identificacion;
	}

	public LocalDateTime getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(LocalDateTime fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public String getContrasenia() {
		return contrasenia;
	}

	public void setContrasenia(String contrasenia) {
		this.contrasenia = contrasenia;
	}

	public Integer getIntentos() {
		return intentos;
	}

	public void setIntentos(Integer intentos) {
		this.intentos = intentos;
	}

	public LocalDateTime getFechaLogin() {
		return fechaLogin;
	}

	public void setFechaLogin(LocalDateTime fechaLogin) {
		this.fechaLogin = fechaLogin;
	}

	public ValorSiNoEnum getActivo() {
		return activo;
	}

	public void setActivo(ValorSiNoEnum activo) {
		this.activo = activo;
	}

	public List<UsuarioPerfil> getUsuarioPerfiles() {
		return usuarioPerfiles;
	}

	public void setUsuarioPerfiles(List<UsuarioPerfil> usuarioPerfiles) {
		this.usuarioPerfiles = usuarioPerfiles;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public byte[] getFoto() {
		return foto;
	}

	public void setFoto(byte[] foto) {
		this.foto = foto;
	}
	

}
