package com.keysist.roses.model.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "s_empresa_variedad_precio")
public class EmpresaVariedadPrecio implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5494631274223867891L;

	@Id
	@GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
	@Column(name = "evp_id", unique = true, nullable = false)
	private Integer id;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "evp_empresa_variedad")
	private EmpresaVariedad empresaVariedad;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "evp_cliente")
	private Empresa cliente;

	@Column(name = "seb_fecha_desde")
	private LocalDate fechaDesde;

	@Column(name = "seb_fecha_hasta")
	private LocalDate fechaHasta;

	@Column(name = "seb_precio")
	private BigDecimal precio;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "seb_longitud", nullable = false)
	private Catalogo longitud;

	public EmpresaVariedadPrecio(EmpresaVariedad empresaVariedad, LocalDate fechaDesde, LocalDate fechaHasta,
			BigDecimal precio, Catalogo longitud, Empresa cliente) {
		super();
		this.empresaVariedad = empresaVariedad;
		this.fechaDesde = fechaDesde;
		this.fechaHasta = fechaHasta;
		this.precio = precio;
		this.longitud = longitud;
	}

	public EmpresaVariedadPrecio() {
		// TODO Auto-generated constructor stub

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public EmpresaVariedad getEmpresaVariedad() {
		return empresaVariedad;
	}

	public void setEmpresaVariedad(EmpresaVariedad empresaVariedad) {
		this.empresaVariedad = empresaVariedad;
	}

	public LocalDate getFechaDesde() {
		return fechaDesde;
	}

	public void setFechaDesde(LocalDate fechaDesde) {
		this.fechaDesde = fechaDesde;
	}

	public LocalDate getFechaHasta() {
		return fechaHasta;
	}

	public void setFechaHasta(LocalDate fechaHasta) {
		this.fechaHasta = fechaHasta;
	}

	public BigDecimal getPrecio() {
		return precio;
	}

	public void setPrecio(BigDecimal precio) {
		this.precio = precio;
	}

	public Catalogo getLongitud() {
		return longitud;
	}

	public void setLongitud(Catalogo longitud) {
		this.longitud = longitud;
	}

	public Empresa getCliente() {
		return cliente;
	}

	public void setCliente(Empresa cliente) {
		this.cliente = cliente;
	}

	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		return this.id != null ? this.id.equals(((EmpresaVariedadPrecio) obj).id) : false;
	}

}
