package com.keysist.roses.model.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.keysist.roses.model.dto.EntregaTalloDTO;
import com.keysist.roses.model.enums.EstadoEntregaProcesoEnum;

@Entity
@Table(name = "s_entrega_tallo")
public class EntregaTallo implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5494631274223867891L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "set_id")
	private Integer id;

	@Column(name = "seb_tallo_total")
	private Integer talloTotal;

	@Column(name = "seb_precio")
	private BigDecimal precio;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "seb_longitud", nullable = false)
	private Catalogo longitud;

	@Column(name = "seb_fecha_proceso")
	private LocalDateTime fechaProceso;

	@Column(name = "seb_estado")
	@Enumerated(EnumType.STRING)
	private EstadoEntregaProcesoEnum estado;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "seb_entrega_poscosecha")
	private EntregaPoscosecha entregaPoscosecha;

	public EntregaTallo() {
		// TODO Auto-generated constructor stub
	}

	public EntregaTallo(Catalogo longitud) {
	}

	public EntregaTallo(Catalogo longi, EmpresaVariedad uv) {
		talloTotal = 0;
	}

	public EntregaTallo(EntregaTalloDTO dto) {
		super();
		this.id = dto.getId();
		this.talloTotal = dto.getTalloTotal();
		this.precio = dto.getPrecio();
		this.fechaProceso = dto.getFechaProceso();
		this.estado = dto.getEstado();

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public BigDecimal getPrecio() {
		return precio != null ? precio : BigDecimal.ZERO;
	}

	public void setPrecio(BigDecimal precio) {
		this.precio = precio;
	}

	public Catalogo getLongitud() {
		return longitud;
	}

	public void setLongitud(Catalogo longitud) {
		this.longitud = longitud;
	}

	public LocalDateTime getFechaProceso() {
		return fechaProceso;
	}

	public void setFechaProceso(LocalDateTime fechaProceso) {
		this.fechaProceso = fechaProceso;
	}

	public EstadoEntregaProcesoEnum getEstado() {
		return estado;
	}

	public void setEstado(EstadoEntregaProcesoEnum estado) {
		this.estado = estado;
	}

	public EntregaPoscosecha getEntregaPoscosecha() {
		return entregaPoscosecha;
	}

	public void setEntregaPoscosecha(EntregaPoscosecha entregaPoscosecha) {
		this.entregaPoscosecha = entregaPoscosecha;
	}

	public Integer getTalloTotal() {
		return talloTotal;
	}

	public void setTalloTotal(Integer talloTotal) {
		this.talloTotal = talloTotal;
	}

}
