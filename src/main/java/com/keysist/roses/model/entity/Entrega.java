package com.keysist.roses.model.entity;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.keysist.roses.model.dto.EntregaDTO;
import com.keysist.roses.model.enums.EstadoEntregaEnum;
import com.keysist.roses.model.enums.TipoEntregaEnum;

@Entity
@Table(name = "s_entrega")
public class Entrega implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5494631274223867891L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ser_id")
	private Integer id;

	@Column(name = "ser_fecha_entrega")
	private LocalDate fechaEntrega;

	@Basic
	@Column(name = "ser_hora_entrega")
	private LocalTime horaEntrega;

	@Column(name = "ser_fecha_recepcion")
	private LocalDate fechaRecepcion;

	@Basic
	@Column(name = "ser_hora_recepcion")
	private LocalTime horaRecepcion;

	@Column(name = "ser_fecha_procesamiento")
	private LocalDateTime fechaProcesamiento;

	@Basic
	@Column(name = "ser_hora_finalizado")
	private LocalTime horaFinalizado;

	@Column(name = "ser_fecha_finalizado")
	private LocalDateTime fechaFinalizado;

	@Column(name = "ser_observacion")
	private String observacion;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_proveedor")
	private Empresa proveedor;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_cliente")
	private Empresa cliente;

	@Column(name = "ser_estado")
	@Enumerated(EnumType.STRING)
	private EstadoEntregaEnum estado;

	@Column(name = "ser_tipo")
	@Enumerated(EnumType.STRING)
	private TipoEntregaEnum tipo;

	@Column(name = "ser_semana")
	private Integer semana;

	public Entrega() {
		// TODO Auto-generated constructor stub
	}

	public Entrega(EntregaDTO en) {
		this.id = en.getId();
		this.fechaEntrega = en.getFechaEntrega();
		this.horaEntrega = en.getHoraEntrega();
		this.fechaProcesamiento = en.getFechaProcesamiento();
		this.fechaFinalizado = en.getFechaFinalizado();
		this.observacion = en.getObservacion();
		this.estado = en.getEstado();
		this.horaEntrega = en.getHoraEntrega();
		this.semana = en.getSemana();
		this.tipo = en.getTipo();
	}

	public Entrega(Entrega entrega) {
		this.id = entrega.getId();
		this.fechaEntrega = entrega.getFechaEntrega();
		this.observacion = entrega.getObservacion();
		this.estado = entrega.getEstado();
		this.horaEntrega = entrega.getHoraEntrega();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public LocalDate getFechaEntrega() {
		return fechaEntrega;
	}

	public void setFechaEntrega(LocalDate fechaEntrega) {
		this.fechaEntrega = fechaEntrega;
	}

	public LocalTime getHoraEntrega() {
		return horaEntrega;
	}

	public void setHoraEntrega(LocalTime horaEntrega) {
		this.horaEntrega = horaEntrega;
	}

	public LocalDateTime getFechaProcesamiento() {
		return fechaProcesamiento;
	}

	public void setFechaProcesamiento(LocalDateTime fechaProcesamiento) {
		this.fechaProcesamiento = fechaProcesamiento;
	}

	public LocalDateTime getFechaFinalizado() {
		return fechaFinalizado;
	}

	public void setFechaFinalizado(LocalDateTime fechaFinalizado) {
		this.fechaFinalizado = fechaFinalizado;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public Empresa getProveedor() {
		return proveedor;
	}

	public void setProveedor(Empresa proveedor) {
		this.proveedor = proveedor;
	}

	public Empresa getCliente() {
		return cliente;
	}

	public void setCliente(Empresa cliente) {
		this.cliente = cliente;
	}

	public EstadoEntregaEnum getEstado() {
		return estado;
	}

	public void setEstado(EstadoEntregaEnum estado) {
		this.estado = estado;
	}

	public TipoEntregaEnum getTipo() {
		return tipo;
	}

	public void setTipo(TipoEntregaEnum tipo) {
		this.tipo = tipo;
	}

	public Integer getSemana() {
		return semana;
	}

	public void setSemana(Integer semana) {
		this.semana = semana;
	}

	public LocalDate getFechaRecepcion() {
		return fechaRecepcion;
	}

	public void setFechaRecepcion(LocalDate fechaRecepcion) {
		this.fechaRecepcion = fechaRecepcion;
	}

	public LocalTime getHoraRecepcion() {
		return horaRecepcion;
	}

	public void setHoraRecepcion(LocalTime horaRecepcion) {
		this.horaRecepcion = horaRecepcion;
	}

	public LocalTime getHoraFinalizado() {
		return horaFinalizado;
	}

	public void setHoraFinalizado(LocalTime horaFinalizado) {
		this.horaFinalizado = horaFinalizado;
	}

}
