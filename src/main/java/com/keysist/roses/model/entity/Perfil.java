package com.keysist.roses.model.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.keysist.roses.model.enums.ValorSiNoEnum;

@Entity
@Table(name = "s_perfil")
public class Perfil implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5494631274223867891L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "sp_id")
	private Integer id;

	@Column(name = "sp_nombre")
	private String nombre;

	@Column(name = "sp_activo")
	@Enumerated(EnumType.STRING)
	private ValorSiNoEnum activo;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "sp_tipo")
	private Catalogo tipo;

	@OneToMany(mappedBy = "perfil")
	private List<UsuarioPerfil> usuarioPerfiles;

	public Perfil() {
		// TODO Auto-generated constructor stub
	}

	public Perfil(Integer id, ValorSiNoEnum activo, String nombre) {
		super();
		this.id = id;
		this.activo = activo;
		this.nombre = nombre;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public ValorSiNoEnum getActivo() {
		return activo;
	}

	public void setActivo(ValorSiNoEnum activo) {
		this.activo = activo;
	}

	public List<UsuarioPerfil> getUsuarioPerfiles() {
		return usuarioPerfiles;
	}

	public void setUsuarioPerfiles(List<UsuarioPerfil> usuarioPerfiles) {
		this.usuarioPerfiles = usuarioPerfiles;
	}

	public UsuarioPerfil addUsuarioPerfile(UsuarioPerfil usuarioPerfile) {
		getUsuarioPerfiles().add(usuarioPerfile);
		usuarioPerfile.setPerfil(this);

		return usuarioPerfile;
	}

	public UsuarioPerfil removeUsuarioPerfile(UsuarioPerfil usuarioPerfile) {
		getUsuarioPerfiles().remove(usuarioPerfile);
		usuarioPerfile.setPerfil(null);

		return usuarioPerfile;
	}

	public Catalogo getTipo() {
		return tipo;
	}

	public void setTipo(Catalogo tipo) {
		this.tipo = tipo;
	}
	

}
