package com.keysist.roses.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "s_aplicacion_variedad")

public class AplicacionVariedad implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5494631274223867891L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "sav_id")
	private Integer id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "sav_aplicacion")
	private Aplicacion aplicacion;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "sapr_variedad")
	private EmpresaVariedad empresaVariedad;

	public AplicacionVariedad() {
		// TODO Auto-generated constructor stub
	}
	

	public AplicacionVariedad(AplicacionVariedad av) {
		super();
		this.empresaVariedad = av.empresaVariedad;
	}


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Aplicacion getAplicacion() {
		return aplicacion;
	}

	public void setAplicacion(Aplicacion aplicacion) {
		this.aplicacion = aplicacion;
	}

	public EmpresaVariedad getEmpresaVariedad() {
		return empresaVariedad;
	}

	public void setEmpresaVariedad(EmpresaVariedad empresaVariedad) {
		this.empresaVariedad = empresaVariedad;
	}

	
	

}
