package com.keysist.roses.model.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.keysist.roses.model.enums.ValorSiNoEnum;

@Entity
@Table(name = "s_costo_variedad")
public class CostoVariedad implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5494631274223867891L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "scv_id")
	private Integer id;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_empresa_variedad")
	private EmpresaVariedad empresaVariedad;

	@Column(name = "scv_fecha_inicio")
	private Date fechaInicio;

	@Column(name = "scv_fecha_fin")
	private Date fechaFin;

	@Column(name = "scv_estado")
	@Enumerated(EnumType.STRING)
	private ValorSiNoEnum estado;

	@OneToMany(mappedBy = "costoVariedad")
	private List<CostoLongitud> costoLongitudes;

	public CostoVariedad() {
		// TODO Auto-generated constructor stub
	}

	public CostoVariedad(CostoVariedad cv) {
		super();
		this.id = cv.getId();
		this.fechaInicio = cv.getFechaInicio();
		this.fechaFin = cv.getFechaFin();
		this.estado = cv.getEstado();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public EmpresaVariedad getEmpresaVariedad() {
		return empresaVariedad;
	}

	public void setEmpresaVariedad(EmpresaVariedad empresaVariedad) {
		this.empresaVariedad = empresaVariedad;
	}

	public Date getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public Date getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}

	public ValorSiNoEnum getEstado() {
		return estado;
	}

	public void setEstado(ValorSiNoEnum estado) {
		this.estado = estado;
	}

	public List<CostoLongitud> getCostoLongitudes() {
		return costoLongitudes;
	}

	public void setCostoLongitudes(List<CostoLongitud> costoLongitudes) {
		this.costoLongitudes = costoLongitudes;
	}

}
