package com.keysist.roses.model.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.keysist.roses.model.dto.EntregaNacionalDTO;
import com.keysist.roses.model.enums.EstadoEntregaProcesoEnum;

@Entity
@Table(name = "s_entrega_nacional")
public class EntregaNacional implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5494631274223867891L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "sen_id")
	private Integer id;

	@Column(name = "sen_tallo_total")
	private Integer talloTotal;

	@Column(name = "sen_precio")
	private BigDecimal precio;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "seb_entrega_poscosecha")
	private EntregaPoscosecha entregaPoscosecha;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "seb_categoria")
	private Catalogo categoria;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "seb_sub_categoria")
	private Catalogo subCategoria;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "seb_bloque")
	private EmpresaBloque bloque;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "seb_clasificador")
	private Usuario clasificador;
	
	@Column(name = "seb_estado")
	@Enumerated(EnumType.STRING)
	private EstadoEntregaProcesoEnum estado;

	
	@Column(name = "seb_fecha_proceso")
	private LocalDateTime fechaProceso;
	public EntregaNacional() {
		// TODO Auto-generated constructor stub
	}

	public EntregaNacional(EntregaNacionalDTO dto) {
		super();
		this.id = dto.getId();
		this.talloTotal = dto.getTalloTotal();
		this.precio = dto.getPrecio();
		

	}
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getTalloTotal() {
		return talloTotal;
	}

	public void setTalloTotal(Integer talloTotal) {
		this.talloTotal = talloTotal;
	}

	public BigDecimal getPrecio() {
		return precio;
	}

	public void setPrecio(BigDecimal precio) {
		this.precio = precio;
	}

	public Catalogo getCategoria() {
		return categoria;
	}

	public void setCategoria(Catalogo categoria) {
		this.categoria = categoria;
	}

	public EntregaPoscosecha getEntregaPoscosecha() {
		return entregaPoscosecha;
	}

	public void setEntregaPoscosecha(EntregaPoscosecha entregaPoscosecha) {
		this.entregaPoscosecha = entregaPoscosecha;
	}

	public Catalogo getSubCategoria() {
		return subCategoria;
	}

	public void setSubCategoria(Catalogo subCategoria) {
		this.subCategoria = subCategoria;
	}

	public EmpresaBloque getBloque() {
		return bloque;
	}

	public void setBloque(EmpresaBloque bloque) {
		this.bloque = bloque;
	}

	public Usuario getClasificador() {
		return clasificador;
	}

	public void setClasificador(Usuario clasificador) {
		this.clasificador = clasificador;
	}

	public EstadoEntregaProcesoEnum getEstado() {
		return estado;
	}

	public void setEstado(EstadoEntregaProcesoEnum estado) {
		this.estado = estado;
	}

	public LocalDateTime getFechaProceso() {
		return fechaProceso;
	}

	public void setFechaProceso(LocalDateTime fechaProceso) {
		this.fechaProceso = fechaProceso;
	}
	
	

}
