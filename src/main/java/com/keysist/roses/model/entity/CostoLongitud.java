package com.keysist.roses.model.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "s_costo_longitud")
public class CostoLongitud implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5494631274223867891L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "scl_id")
	private Integer id;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_costo_variedad")
	private CostoVariedad costoVariedad;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_longitud")
	private Catalogo longitud;

	@Column(name = "scl_precio")
	private BigDecimal precio;

	public CostoLongitud() {
		// TODO Auto-generated constructor stub
	}

	public CostoLongitud(CostoLongitud c) {
		super();
		this.id = c.getId();
		this.costoVariedad = c.getCostoVariedad();
		this.longitud = c.getLongitud();
		this.precio = c.getPrecio();

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public CostoVariedad getCostoVariedad() {
		return costoVariedad;
	}

	public void setCostoVariedad(CostoVariedad costoVariedad) {
		this.costoVariedad = costoVariedad;
	}

	public Catalogo getLongitud() {
		return longitud;
	}

	public void setLongitud(Catalogo longitud) {
		this.longitud = longitud;
	}

	public BigDecimal getPrecio() {
		return precio;
	}

	public void setPrecio(BigDecimal precio) {
		this.precio = precio;
	}

}
