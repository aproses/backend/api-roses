package com.keysist.roses.model.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "s_producto")
public class Producto implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5494631274223867891L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "spr_id")
	private Integer id;

	@Column(name = "spr_nombre")
	private String nombre;

	@Column(name = "spr_es_nitrato")
	private Boolean esNitrato;

	@Column(name = "spr_caracteristicas")
	private String caracteristicas;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "spr_unidad_medida")
	private Catalogo unidadMedida;

	@Column(name = "spr_dosis", precision = 4)
	private BigDecimal dosis;

	public Producto() {
		// TODO Auto-generated constructor stub
	}

	

	public Producto(String nombre, Boolean esNitrato,Catalogo unidadMedida, BigDecimal dosis) {
		super();
		this.nombre = nombre;
		this.esNitrato = esNitrato;
		this.unidadMedida = unidadMedida;
		this.dosis = dosis;
	}



	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getCaracteristicas() {
		return caracteristicas;
	}

	public void setCaracteristicas(String caracteristicas) {
		this.caracteristicas = caracteristicas;
	}

	public Catalogo getUnidadMedida() {
		return unidadMedida;
	}

	public void setUnidadMedida(Catalogo unidadMedida) {
		this.unidadMedida = unidadMedida;
	}

	public BigDecimal getDosis() {
		return dosis;
	}

	public void setDosis(BigDecimal dosis) {
		this.dosis = dosis;
	}

	public Boolean getEsNitrato() {
		return esNitrato;
	}

	public void setEsNitrato(Boolean esNitrato) {
		this.esNitrato = esNitrato;
	}

}
