package com.keysist.roses.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.keysist.roses.model.enums.ValorSiNoEnum;

@Entity
@Table(name = "s_usuario_perfil")
public class UsuarioPerfil implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5494631274223867891L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "sup_id")
	private Integer id;
	 
	@Column(name = "sup_activo")
	@Enumerated(EnumType.STRING)
	private ValorSiNoEnum activo;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_perfil")
	private Perfil perfil;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_usuario")
	private Usuario usuario;
		
	public UsuarioPerfil() {
		// TODO Auto-generated constructor stub
	}
	

	public UsuarioPerfil(ValorSiNoEnum activo, Perfil perfil, Usuario usuario) {
		super();
		this.activo = activo;
		this.perfil = perfil;
		this.usuario = usuario;
	}


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public ValorSiNoEnum getActivo() {
		return activo;
	}

	public void setActivo(ValorSiNoEnum activo) {
		this.activo = activo;
	}

	public Perfil getPerfil() {
		return perfil;
	}

	public void setPerfil(Perfil perfil) {
		this.perfil = perfil;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
}
