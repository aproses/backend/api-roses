package com.keysist.roses.model.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.keysist.roses.model.dto.EntregaBoncheDTO;
import com.keysist.roses.model.enums.EstadoEntregaProcesoEnum;

@Entity
@Table(name = "s_entrega_bonche")
public class EntregaBonche implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5494631274223867891L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "seb_id")
	private Integer id;

	@Column(name = "seb_bonches")
	private Integer bonches;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "seb_longitud", nullable = false)
	private Catalogo longitud;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "seb_tallo_bonche", nullable = false)
	private Catalogo talloBonche;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "seb_punto_corte")
	private Catalogo puntoCorte;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "seb_tipo_carton")
	private Catalogo tipoCarton;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "seb_mesa")
	private EmpresaMesa mesa;

	@Column(name = "seb_bonches_disponible")
	private Integer bonchesDisponibles;

	@Column(name = "seb_fecha_proceso")
	private LocalDateTime fechaProceso;

	@Column(name = "seb_estado")
	@Enumerated(EnumType.STRING)
	private EstadoEntregaProcesoEnum estado;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "seb_entrega_poscosecha")
	private EntregaPoscosecha entregaPoscosecha;

	@Column(name = "seb_etiqueta")
	private String etiqueta;

	public EntregaBonche() {
		// TODO Auto-generated constructor stub
	}

	public EntregaBonche(Catalogo longitud) {
	}

	public EntregaBonche(Catalogo longi, EmpresaVariedad uv) {
		bonches = 0;
	}

	public EntregaBonche(EntregaBoncheDTO dto) {
		super();
		this.id = dto.getId();
		this.bonches = dto.getBonches();
		this.bonchesDisponibles = dto.getBonches();
		this.bonchesDisponibles = dto.getBonchesDisponibles();
		this.fechaProceso = dto.getFechaProceso();
		this.estado = dto.getEstado();
		this.etiqueta = dto.getEtiqueta();

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getBonches() {
		return bonches;
	}

	public void setBonches(Integer bonches) {
		this.bonches = bonches;
	}

	public Catalogo getLongitud() {
		return longitud;
	}

	public void setLongitud(Catalogo longitud) {
		this.longitud = longitud;
	}

	public LocalDateTime getFechaProceso() {
		return fechaProceso;
	}

	public void setFechaProceso(LocalDateTime fechaProceso) {
		this.fechaProceso = fechaProceso;
	}

	public EstadoEntregaProcesoEnum getEstado() {
		return estado;
	}

	public void setEstado(EstadoEntregaProcesoEnum estado) {
		this.estado = estado;
	}

	public EntregaPoscosecha getEntregaPoscosecha() {
		return entregaPoscosecha;
	}

	public void setEntregaPoscosecha(EntregaPoscosecha entregaPoscosecha) {
		this.entregaPoscosecha = entregaPoscosecha;
	}

	public Catalogo getTalloBonche() {
		return talloBonche;
	}

	public void setTalloBonche(Catalogo talloBonche) {
		this.talloBonche = talloBonche;
	}

	public Integer getBonchesDisponibles() {
		return bonchesDisponibles;
	}

	public void setBonchesDisponibles(Integer bonchesDisponibles) {
		this.bonchesDisponibles = bonchesDisponibles;
	}

	public Catalogo getPuntoCorte() {
		return puntoCorte;
	}

	public void setPuntoCorte(Catalogo puntoCorte) {
		this.puntoCorte = puntoCorte;
	}

	public Catalogo getTipoCarton() {
		return tipoCarton;
	}

	public void setTipoCarton(Catalogo tipoCarton) {
		this.tipoCarton = tipoCarton;
	}

	public EmpresaMesa getMesa() {
		return mesa;
	}

	public void setMesa(EmpresaMesa mesa) {
		this.mesa = mesa;
	}

	public String getEtiqueta() {
		return etiqueta;
	}

	public void setEtiqueta(String etiqueta) {
		this.etiqueta = etiqueta;
	}

}
