package com.keysist.roses.model.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.keysist.roses.model.enums.ValorSiNoEnum;

@Entity
@Table(name = "s_empresa_variedad")
public class EmpresaVariedad implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5494631274223867891L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "sev_id")
	private Integer id;

	@Column(name = "sev_efectividad")
	private Double efectividad;

	@Column(name = "sev_produccion_mes")
	private BigDecimal produccionMes;

	@Column(name = "sev_dias_ciclo")
	private BigInteger diasCiclo;

	@Column(name = "sev_cantidad_plantas")
	private Integer cantidadPlantas;

	@Column(name = "sev_camas_estandart")
	private Integer camasEstandart;

	@Column(name = "sev_tallo_malla")
	private Integer talloMalla;

	@Column(name = "sev_tallo_malla_max")
	private Integer talloMallaMax;

	@Column(name = "sev_disponible")
	@Enumerated(EnumType.STRING)
	private ValorSiNoEnum disponible;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "sev_variedad")
	private Variedad variedad;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "sev_empresa")
	private Empresa empresa;

	@Column(name = "sev_produccion_perdida")
	private BigDecimal produccionPerdida;

	@Column(name = "spr_empresa_bloque")
	private String[] bloques;

	@Transient
	private Integer idEmpresa;
	@Transient
	private String nombreVariedad;
	@Transient
	private Integer idVariedad;

	public EmpresaVariedad() {
		// TODO Auto-generated constructor stub

	}

	public EmpresaVariedad(EmpresaVariedad ev) {
		id = ev.getId();
		efectividad = ev.getEfectividad();
		talloMalla = ev.getTalloMalla();
		disponible = ev.getDisponible();
		produccionMes = ev.getProduccionMes();
		diasCiclo = ev.getDiasCiclo();
		cantidadPlantas = ev.getCantidadPlantas();
		talloMallaMax = ev.getTalloMallaMax();
		produccionPerdida = ev.getProduccionPerdida();
		bloques = ev.bloques;
		camasEstandart=ev.getCamasEstandart();
		if (ev.getEmpresa() != null)
			this.idEmpresa = ev.getEmpresa().getId();
		if (ev.getVariedad() != null) {
			this.nombreVariedad = ev.getVariedad().getNombre();
			this.idVariedad = ev.getVariedad().getId();
		}

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Double getEfectividad() {
		return efectividad;
	}

	public void setEfectividad(Double efectividad) {
		this.efectividad = efectividad;
	}

	public BigDecimal getProduccionMes() {
		return produccionMes != null ? produccionMes : BigDecimal.ONE;
	}

	public void setProduccionMes(BigDecimal produccionMes) {
		this.produccionMes = produccionMes;
	}

	public BigInteger getDiasCiclo() {
		return diasCiclo != null ? diasCiclo : BigInteger.ZERO;
	}

	public void setDiasCiclo(BigInteger diasCiclo) {
		this.diasCiclo = diasCiclo;
	}

	public Integer getTalloMalla() {
		return talloMalla;
	}

	public void setTalloMalla(Integer talloMalla) {
		this.talloMalla = talloMalla;
	}

	public ValorSiNoEnum getDisponible() {
		return disponible;
	}

	public void setDisponible(ValorSiNoEnum disponible) {
		this.disponible = disponible;
	}

	public Variedad getVariedad() {
		return variedad;
	}

	public void setVariedad(Variedad variedad) {
		this.variedad = variedad;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public Integer getIdEmpresa() {
		return idEmpresa;
	}

	public void setIdEmpresa(Integer idEmpresa) {
		this.idEmpresa = idEmpresa;
	}

	public String getNombreVariedad() {
		return nombreVariedad;
	}

	public void setNombreVariedad(String nombreVariedad) {
		this.nombreVariedad = nombreVariedad;
	}

	public Integer getIdVariedad() {
		return idVariedad;
	}

	public void setIdVariedad(Integer idVariedad) {
		this.idVariedad = idVariedad;
	}

	public Integer getCantidadPlantas() {
		return cantidadPlantas != null ? cantidadPlantas : 0;
	}

	public void setCantidadPlantas(Integer cantidadPlantas) {
		this.cantidadPlantas = cantidadPlantas;
	}

	public Integer getTalloMallaMax() {
		return talloMallaMax;
	}

	public void setTalloMallaMax(Integer talloMallaMax) {
		this.talloMallaMax = talloMallaMax;
	}

	public BigDecimal getProduccionPerdida() {
		return produccionPerdida;
	}

	public void setProduccionPerdida(BigDecimal produccionPerdida) {
		this.produccionPerdida = produccionPerdida;
	}

	public Integer getCamasEstandart() {
		return camasEstandart;
	}

	public void setCamasEstandart(Integer camasEstandart) {
		this.camasEstandart = camasEstandart;
	}

	public String[] getBloques() {
		return bloques;
	}

	public void setBloques(String[] bloques) {
		this.bloques = bloques;
	}

	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		return this.id != null ? this.id.equals(((EmpresaVariedad) obj).id) : false;
	}

}
