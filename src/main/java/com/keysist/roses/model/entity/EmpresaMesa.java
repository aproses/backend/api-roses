package com.keysist.roses.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.keysist.roses.model.enums.ValorSiNoEnum;

@Entity
@Table(name = "s_empresa_mesa")
public class EmpresaMesa implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5494631274223867891L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "sem_id")
	private Integer id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "sem_empresa")
	private Empresa empresa;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "sem_mesa")
	private Catalogo mesa;
	
	@Transient
	private String nombre;

	@Column(name = "sem_activo")
	@Enumerated(EnumType.STRING)
	private ValorSiNoEnum activo;

	public EmpresaMesa() {
		// TODO Auto-generated constructor stub

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public Catalogo getMesa() {
		return mesa;
	}

	public void setMesa(Catalogo mesa) {
		this.mesa = mesa;
	}

	public ValorSiNoEnum getActivo() {
		return activo;
	}

	public void setActivo(ValorSiNoEnum activo) {
		this.activo = activo;
	}

	public String getNombre() {
		return mesa.getNombre();
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	

}
