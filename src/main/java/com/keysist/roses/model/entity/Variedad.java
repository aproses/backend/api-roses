package com.keysist.roses.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.keysist.roses.model.dto.VariedadDTO;
import com.keysist.roses.model.enums.ValorSiNoEnum;

@Entity
@Table(name = "s_variedad")
public class Variedad implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5494631274223867891L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "sv_id")
	private Integer id;

	@Column(name = "sv_nombre")
	private String nombre;

	@Column(name = "sv_codigo")
	private String codigo;

	@Column(name = "sv_caracteristicas")
	private String caracteristicas;

	@Column(name = "sv_pathFoto")
	private String pathFoto;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_color")
	private Catalogo color;

	@Column(name = "sv_disponible")
	@Enumerated(EnumType.STRING)
	private ValorSiNoEnum disponible;

	public Variedad() {
		// TODO Auto-generated constructor stub
	}

	public Variedad(VariedadDTO dto) {
		// TODO Auto-generated constructor stub
		this.id = dto.getId();
		this.nombre = dto.getNombre();
		this.caracteristicas = dto.getCaracteristicas();
		this.pathFoto = dto.getPathFoto();
		this.codigo = dto.getCodigo();
		this.disponible = dto.getDisponible();
	}

	public Variedad(String nombre, String codigo, ValorSiNoEnum disponible) {
		super();
		this.nombre = nombre;
		this.codigo = codigo;
		this.disponible = disponible;
	}

	public Variedad(Variedad var) {
		super();
		this.id = var.getId();
		this.nombre = var.getNombre();
		this.caracteristicas = var.getCaracteristicas();
		this.pathFoto = var.getPathFoto();
		this.codigo = var.getCodigo();
		this.disponible = var.getDisponible();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public ValorSiNoEnum getActivo() {
		return disponible;
	}

	public void setActivo(ValorSiNoEnum activo) {
		this.disponible = activo;
	}

	public String getNombre() {
		return nombre != null ? nombre : "";
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getCaracteristicas() {
		return caracteristicas;
	}

	public void setCaracteristicas(String caracteristicas) {
		this.caracteristicas = caracteristicas;
	}

	public String getPathFoto() {
		return pathFoto;
	}

	public void setPathFoto(String pathFoto) {
		this.pathFoto = pathFoto;
	}

	public ValorSiNoEnum getDisponible() {
		return disponible;
	}

	public void setDisponible(ValorSiNoEnum disponible) {
		this.disponible = disponible;
	}

	public Catalogo getColor() {
		return color;
	}

	public void setColor(Catalogo color) {
		this.color = color;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

}
