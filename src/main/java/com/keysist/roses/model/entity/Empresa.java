package com.keysist.roses.model.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.keysist.roses.model.enums.ValorSiNoEnum;

@Entity
@Table(name = "s_empresa")
public class Empresa implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5494631274223867891L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "se_id")
	private Integer id;

	@Column(name = "se_nombre")
	private String nombre;

	@Column(name = "se_ruc", nullable = false)
	private String ruc;

	@Column(name = "se_ruc_sobrante")
	private String rucSobrante;

	@Column(name = "se_direccion")
	private String direccion;

	@Column(name = "se_codigo")
	private String codigo;

	@Column(name = "em_logo")
	private byte[] logo;

	@Column(name = "se_correo")
	private String correo;

	@Column(name = "se_celular")
	private String celular;

	@Column(name = "se_activo")
	@Enumerated(EnumType.STRING)
	private ValorSiNoEnum activo;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "se_tipo")
	private Catalogo tipo;

	@Column(name = "se_produccion_semana")
	private BigDecimal produccionSemana;

	@Column(name = "se_fechaRegistro")
	private LocalDateTime fechaRegistro;

	

	public Empresa() {
		// TODO Auto-generated constructor stub
	}

	public Empresa(String nombre, String ruc, String correo, String celular, ValorSiNoEnum activo, Catalogo tipo,
			LocalDateTime fechaRegistro) {
		super();
		this.nombre = nombre;
		this.ruc = ruc;
		this.correo = correo;
		this.celular = celular;
		this.activo = activo;
		this.tipo = tipo;
		this.fechaRegistro = fechaRegistro;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getRuc() {
		return ruc;
	}

	public void setRuc(String ruc) {
		this.ruc = ruc;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public ValorSiNoEnum getActivo() {
		return activo;
	}

	public void setActivo(ValorSiNoEnum activo) {
		this.activo = activo;
	}

	public Catalogo getTipo() {
		return tipo;
	}

	public void setTipo(Catalogo tipo) {
		this.tipo = tipo;
	}

	public String getRucSobrante() {
		return rucSobrante;
	}

	public void setRucSobrante(String rucSobrante) {
		this.rucSobrante = rucSobrante;
	}

	public byte[] getLogo() {
		return logo;
	}

	public void setLogo(byte[] logo) {
		this.logo = logo;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public BigDecimal getProduccionSemana() {
		return produccionSemana != null ? produccionSemana : BigDecimal.valueOf(4.2);
	}

	public void setProduccionSemana(BigDecimal produccionSemana) {
		this.produccionSemana = produccionSemana;
	}

	public LocalDateTime getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(LocalDateTime fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	

}
