package com.keysist.roses.model.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "s_aplicacion_producto")
public class AplicacionProducto implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5494631274223867891L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "sapr_id")
	private Integer id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "sapr_aplicacion")
	private Aplicacion aplicacion;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "sapr_tanque")
	private Catalogo tanque;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "sapr_empresa_producto")
	private EmpresaProducto empresaProducto;

	@Column(name = "sapr_dosis_litro",precision = 4)
	private BigDecimal dosisLitro;

	@Column(name = "sapr_necesario",precision = 4)
	private BigDecimal necesario;

	@Column(name = "sapr_faltante",precision = 4)
	private BigDecimal faltante;

	@Column(name = "sapr_orden")
	private BigInteger orden;

	public AplicacionProducto() {
		// TODO Auto-generated constructor stub
	}

	public AplicacionProducto(AplicacionProducto apr) {
		super();
		this.tanque = apr.tanque;
		this.empresaProducto = apr.empresaProducto;
		this.dosisLitro = apr.dosisLitro;
		this.necesario = apr.necesario;
		this.orden = apr.orden;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Aplicacion getAplicacion() {
		return aplicacion;
	}

	public void setAplicacion(Aplicacion aplicacion) {
		this.aplicacion = aplicacion;
	}

	public Catalogo getTanque() {
		return tanque;
	}

	public void setTanque(Catalogo tanque) {
		this.tanque = tanque;
	}

	public EmpresaProducto getEmpresaProducto() {
		return empresaProducto;
	}

	public void setEmpresaProducto(EmpresaProducto empresaProducto) {
		this.empresaProducto = empresaProducto;
	}

	public BigDecimal getDosisLitro() {
		return dosisLitro;
	}

	public void setDosisLitro(BigDecimal dosisLitro) {
		this.dosisLitro = dosisLitro;
	}

	public BigDecimal getNecesario() {
		return necesario;
	}

	public void setNecesario(BigDecimal necesario) {
		this.necesario = necesario;
	}

	public BigDecimal getFaltante() {
		return faltante;
	}

	public void setFaltante(BigDecimal faltante) {
		this.faltante = faltante;
	}

	public BigInteger getOrden() {
		return orden;
	}

	public void setOrden(BigInteger orden) {
		this.orden = orden;
	}

}
