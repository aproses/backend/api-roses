package com.keysist.roses.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.keysist.roses.model.dto.EntregaVariedadDTO;
import com.keysist.roses.model.enums.EstadoEntregaEnum;

@Entity
@Table(name = "s_entrega_variedad")
public class EntregaVariedad implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5494631274223867891L;

	@Id
	@GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
	@Column(name = "sev_id", unique = true, nullable = false)
	private Integer id;

	@Column(name = "sev_mallas", nullable = false)
	private Integer mallas;

	@Column(name = "sev_cantidad_malla")
	private Integer cantidadPorMalla;

	@Column(name = "sev_total_tallos")
	private Integer totalTallos;

	/*
	 * @Column(name = "sev_cantidad_exportar") private Integer cantidadExportar;
	 * 
	 * @Column(name = "sev_cantidad_nacional") private Integer cantidadNacional;
	 * 
	 * @Column(name = "sev_sobrante") private Integer sobrante;
	 * 
	 * @Column(name = "sev_sobrante_anterior") private Integer sobranteAnterior;
	 * 
	 * @Column(name = "sev_precio_sobrante") private BigDecimal precioSobrante;
	 * 
	 * @Column(name = "sev_efectividad") private BigDecimal efectividad;
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "sev_entrega")
	private Entrega entrega;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "sev_empresa_variedad")
	private EmpresaVariedad empresaVariedad;

	@Column(name = "sev_activo")
	@Enumerated(EnumType.STRING)
	private EstadoEntregaEnum estado;

	public EntregaVariedad() {
	}

	public EntregaVariedad(Integer tallos, Entrega entrega, EmpresaVariedad empresaVariedad, EstadoEntregaEnum estado,
			Integer mallas) {
		super();
		this.totalTallos = tallos;
		this.entrega = entrega;
		this.empresaVariedad = empresaVariedad;
		this.estado = estado;
		this.mallas = mallas;
	}

	public EntregaVariedad(EntregaVariedadDTO dto) {
		setId(dto.getId());
		setMallas(dto.getMallas());
		setCantidadPorMalla(dto.getCantidadPorMalla());
		setTotalTallos(dto.getTotalTallos());
		setEstado(dto.getEstado());
	}

	public EntregaVariedad(EntregaVariedad det) {
		this.id = det.getId();
		this.mallas = det.getMallas();
		this.estado = det.getEstado();
		this.cantidadPorMalla = det.getCantidadPorMalla();
		this.totalTallos = det.getTotalTallos();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getMallas() {
		return mallas != null ? mallas : 0;
	}

	public void setMallas(Integer mallas) {
		this.mallas = mallas;
	}

	public Integer getCantidadPorMalla() {
		return cantidadPorMalla != null ? cantidadPorMalla : 0;
	}

	public void setCantidadPorMalla(Integer cantidadPorMalla) {
		this.cantidadPorMalla = cantidadPorMalla;
	}

	public Integer getTotalTallos() {
		return totalTallos != null ? totalTallos : 0;
	}

	public void setTotalTallos(Integer totalTallos) {
		this.totalTallos = totalTallos;
	}

	public Entrega getEntrega() {
		return entrega;
	}

	public void setEntrega(Entrega entrega) {
		this.entrega = entrega;
	}

	public EmpresaVariedad getEmpresaVariedad() {
		return empresaVariedad;
	}

	public void setEmpresaVariedad(EmpresaVariedad empresaVariedad) {
		this.empresaVariedad = empresaVariedad;
	}

	public EstadoEntregaEnum getEstado() {
		return estado;
	}

	public void setEstado(EstadoEntregaEnum estado) {
		this.estado = estado;
	}

	

}
