package com.keysist.roses.model.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.keysist.roses.model.enums.EstadoAplicacionEnum;

@Entity
@Table(name = "s_aplicacion")
public class Aplicacion implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5494631274223867891L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "sap_id")
	private Integer id;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_empresa_bloque")
	private EmpresaBloque empresaBloque;

	@Basic
	@Column(name = "sap_fecha_aplicacion")
	private LocalDate fechaAplicacion;

	@Basic
	@Column(name = "sap_hora_inicio")
	private LocalTime horaInicio;

	@Basic
	@Column(name = "sap_hora_fin")
	private LocalTime horaFin;

	@Basic
	@Column(name = "sap_duracion")
	private Long duracion;

	@Basic
	@Column(name = "sap_ph")
	private BigDecimal ph;

	@Basic
	@Column(name = "sap_dias_frecuencia")
	private Long diasFrecuencia;

	@Basic
	@Column(name = "sap_observacion")
	private String observacion;

	@Column(name = "sap_cantidad_litros")
	private Integer cantidadLitros;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "sap_tipo")
	private Catalogo tipo;

	@Column(name = "spr_plagas")
	private String[] plagas;

	@Column(name = "ser_estado")
	@Enumerated(EnumType.STRING)
	private EstadoAplicacionEnum estado;

	public Aplicacion() {
		// TODO Auto-generated constructor stub
	}

	public Aplicacion(Aplicacion app) {
		super();
		this.empresaBloque = app.empresaBloque;
		this.cantidadLitros = app.cantidadLitros;
		this.tipo = app.tipo;
		this.plagas = app.plagas;
		this.observacion = app.observacion;
		this.ph = app.ph;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public LocalDate getFechaAplicacion() {
		return fechaAplicacion;
	}

	public void setFechaAplicacion(LocalDate fechaAplicacion) {
		this.fechaAplicacion = fechaAplicacion;
	}

	public LocalTime getHoraInicio() {
		return horaInicio;
	}

	public void setHoraInicio(LocalTime horaInicio) {
		this.horaInicio = horaInicio;
	}

	public LocalTime getHoraFin() {
		return horaFin;
	}

	public void setHoraFin(LocalTime horaFin) {
		this.horaFin = horaFin;
	}

	public Long getDuracion() {
		return duracion;
	}

	public void setDuracion(Long duracion) {
		this.duracion = duracion;
	}

	public Integer getCantidadLitros() {
		return cantidadLitros;
	}

	public void setCantidadLitros(Integer cantidadLitros) {
		this.cantidadLitros = cantidadLitros;
	}

	public Catalogo getTipo() {
		return tipo;
	}

	public void setTipo(Catalogo tipo) {
		this.tipo = tipo;
	}

	public String[] getPlagas() {
		return plagas;
	}

	public void setPlagas(String[] plagas) {
		this.plagas = plagas;
	}

	public EstadoAplicacionEnum getEstado() {
		return estado;
	}

	public void setEstado(EstadoAplicacionEnum estado) {
		this.estado = estado;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public Long getDiasFrecuencia() {
		return diasFrecuencia;
	}

	public void setDiasFrecuencia(Long diasFrecuencia) {
		this.diasFrecuencia = diasFrecuencia;
	}

	public EmpresaBloque getEmpresaBloque() {
		return empresaBloque;
	}

	public void setEmpresaBloque(EmpresaBloque empresaBloque) {
		this.empresaBloque = empresaBloque;
	}

	public BigDecimal getPh() {
		return ph;
	}

	public void setPh(BigDecimal ph) {
		this.ph = ph;
	}

}
