package com.keysist.roses.model.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.keysist.roses.model.enums.ValorSiNoEnum;

@Entity
@Table(name = "s_empresa_producto")
public class EmpresaProducto implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5494631274223867891L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "sep_id")
	private Integer id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "sep_empresa")
	private Empresa empresa;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "sep_producto")
	private Producto producto;

	@Column(name = "sep_precio")
	private BigDecimal precio;

	@Column(name = "sep_dosis",precision = 4)
	private BigDecimal dosis;

	@Column(name = "sep_disponible")
	private BigDecimal disponible;

	@Column(name = "sep_disponible_minimo")
	private BigDecimal disponibleMinimo;

	@Column(name = "sep_cantidad_unidad")
	private BigDecimal cantidadPorUnidad;

	@Column(name = "sep_activo")
	@Enumerated(EnumType.STRING)
	private ValorSiNoEnum activo;

	public EmpresaProducto() {
		// TODO Auto-generated constructor stub

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public Producto getProducto() {
		return producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}

	public BigDecimal getPrecio() {
		return precio;
	}

	public void setPrecio(BigDecimal precio) {
		this.precio = precio;
	}

	public BigDecimal getDosis() {
		return dosis;
	}

	public void setDosis(BigDecimal dosis) {
		this.dosis = dosis;
	}

	public BigDecimal getDisponible() {
		return disponible != null ? disponible : BigDecimal.ZERO;
	}

	public void setDisponible(BigDecimal disponible) {
		this.disponible = disponible;
	}

	public ValorSiNoEnum getActivo() {
		return activo;
	}

	public void setActivo(ValorSiNoEnum activo) {
		this.activo = activo;
	}

	public BigDecimal getDisponibleMinimo() {
		return disponibleMinimo;
	}

	public void setDisponibleMinimo(BigDecimal disponibleMinimo) {
		this.disponibleMinimo = disponibleMinimo;
	}

	public BigDecimal getCantidadPorUnidad() {
		return cantidadPorUnidad;
	}

	public void setCantidadPorUnidad(BigDecimal cantidadPorUnidad) {
		this.cantidadPorUnidad = cantidadPorUnidad;
	}

}
