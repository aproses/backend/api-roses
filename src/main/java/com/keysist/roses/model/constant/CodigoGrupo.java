package com.keysist.roses.model.constant;

public class CodigoGrupo {

	static String SUCURSALES = "SUCURSALES";
	static String TIPOS_USUARIO = "TIPOS_USUARIO";
	static String COLORES_ROSAS = "COLORES_ROSAS";
	static String LONGITUD_TALLO = "LONGITUD_TALLO";
	static String ENFERMEDADES = "ENFERMEDADES";
	static String UNIDAD_MEDIDA = "UNIDAD_MEDIDA";
	static String TIPOS_APLICACIONES = "TIPOS_APLICACIONES";
	static String ELEMENTOS_QUIMICOS = "ELEMENTOS_QUIMICOS";
	static String TALLO_BONCHE = "TALLO_BONCHE";
	static String GRUPO_NACIONAL = "GRUPO_NACIONAL";
	static String CULTIVO = "CULTIVO";
	static String NACIONAL_SOBRANTE = "NACIONAL_SOBRANTE";
	static String SANIDAD_VEGETAL = "SANIDAD_VEGETAL";
	static String FERTILIZACION_RIEGO = "FERTILIZACION_RIEGO";
	static String PUNTO_CORTE = "PUNTO_CORTE";
	static String TIPO_CARTON = "TIPO_CARTON";
	static String BLOQUES = "BLOQUES";
	static String MESAS = "MESAS";
	static String GRUPOS_NACIONAL = "GRUPOS_NACIONAL";
	static String DIAS_APLICACION = "DIAS_APLICACION";
	static String FERTILIZACION = "FERTILIZACION";
	static String FUMNIGACION = "FUMNIGACION";
	static String DRENCH = "DRENCH";
}
