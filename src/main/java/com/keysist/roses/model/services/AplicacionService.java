package com.keysist.roses.model.services;

import static java.time.temporal.ChronoUnit.DAYS;
import static java.time.temporal.ChronoUnit.MINUTES;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.transaction.Transactional;
import javax.transaction.TransactionalException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import com.keysist.roses.model.dto.AplicacionDTO;
import com.keysist.roses.model.dto.AplicacionProductoDTO;
import com.keysist.roses.model.dto.AplicacionVariedadDTO;
import com.keysist.roses.model.dto.ProductoConsumoDTO;
import com.keysist.roses.model.entity.Aplicacion;
import com.keysist.roses.model.entity.AplicacionProducto;
import com.keysist.roses.model.entity.AplicacionVariedad;
import com.keysist.roses.model.entity.Catalogo;
import com.keysist.roses.model.entity.Empresa;
import com.keysist.roses.model.entity.EmpresaProducto;
import com.keysist.roses.model.enums.CodigoCatalogoEnum;
import com.keysist.roses.model.enums.EstadoAplicacionEnum;
import com.keysist.roses.model.repository.AplicacionProductoRepository;
import com.keysist.roses.model.repository.AplicacionRepository;
import com.keysist.roses.model.repository.AplicacionVariedadRepository;
import com.keysist.roses.model.repository.CatalogoRepository;
import com.keysist.roses.model.repository.EmpresaBloqueRepository;
import com.keysist.roses.model.repository.EmpresaProductoRepository;
import com.keysist.roses.model.repository.EmpresaRepository;
import com.keysist.roses.model.repository.EmpresaVariedadRepository;

@Service
public class AplicacionService {

	@Autowired
	private EmpresaBloqueRepository empresaBloqueRepository;

	@Autowired
	private EmpresaRepository empresaRepository;

	@Autowired
	private AplicacionRepository aplicacionRepository;

	@Autowired
	private CatalogoRepository catalogoRepository;

	@Autowired
	private AplicacionProductoRepository aplicacionProductoRepository;

	@Autowired
	private AplicacionVariedadRepository aplicacionVariedadRepository;

	@Autowired
	private EmpresaProductoRepository empresaProductoRepository;

	@Autowired
	private EmpresaVariedadRepository empresaVariedadRepository;

	@Autowired
	Environment env;

	@Autowired
	SimpMessagingTemplate template;

	@Transactional
	public AplicacionDTO guardar(AplicacionDTO ad) throws Exception {
		Aplicacion a = new Aplicacion();
		if (ad.getId() != null) {
			a = aplicacionRepository.findById(ad.getId()).get();
			List<AplicacionProducto> productos = aplicacionProductoRepository.findByAplicacion(ad.getId());
			for (AplicacionProducto aplicacionProducto : productos) {
				aplicacionProductoRepository.delete(aplicacionProducto);
			}
			List<AplicacionVariedad> variedades = aplicacionVariedadRepository.findByAplicacion(ad.getId());
			for (AplicacionVariedad aplicacionVariedad : variedades) {
				aplicacionVariedadRepository.delete(aplicacionVariedad);
			}
		} else {
			a.setEmpresaBloque(empresaBloqueRepository.findById(ad.getEmpresaBloqueId()).get());
		}
		if (ad.getFechaAplicacionTime() != null && ad.getFechaAplicacionTime() > 0) {
			a.setFechaAplicacion((new Timestamp(ad.getFechaAplicacionTime())).toLocalDateTime().toLocalDate());
		}
		a.setDiasFrecuencia(ad.getDiasFrecuencia());
		a.setCantidadLitros(ad.getCantidadLitros());
		a.setPlagas(ad.getPlagas());
		a.setTipo(catalogoRepository.findById(ad.getTipoId()).get());
		a.setEstado(ad.getEstado());
		a.setObservacion(ad.getObservacion());
		a.setPh(ad.getPh());
		if (ad.getEstado() == null)
			a.setEstado(EstadoAplicacionEnum.INGRESADO);
		if (ad.getHoraInicio() != null && ad.getHoraFin() != null) {
			a.setHoraInicio(ad.getHoraInicio());
			a.setHoraFin(ad.getHoraFin());
			a.setDuracion(MINUTES.between(a.getHoraInicio(), a.getHoraFin()));
			ad.setDuracion(a.getDuracion());
		}
		a = aplicacionRepository.save(a);
		ad.setId(a.getId());
		for (AplicacionProductoDTO apd : ad.getProductos()) {
			EmpresaProducto pr = empresaProductoRepository.findById(apd.getEmpresaProductoId()).get();
			BigDecimal necesario = "SI".equalsIgnoreCase(a.getTipo().getTexto1())
					? BigDecimal.valueOf(a.getCantidadLitros()).multiply(apd.getDosisLitro())
					: apd.getDosisLitro();
			AplicacionProducto ap = new AplicacionProducto();
			BigDecimal faltane = BigDecimal.ZERO;
			if (pr.getDisponible() != null && necesario.compareTo(pr.getDisponible()) == 1) {
				faltane = necesario.subtract(pr.getDisponible());
			}
			ap.setOrden(apd.getOrden());
			ap.setAplicacion(a);
			ap.setDosisLitro(apd.getDosisLitro());
			ap.setNecesario(necesario);
			ap.setFaltante(faltane);
			ap.setEmpresaProducto(pr);
			ap.setTanque(apd.getTanqueId() != null ? catalogoRepository.findById(apd.getTanqueId()).get() : null);
			ap = aplicacionProductoRepository.save(ap);
			apd.setNecesario(necesario);
			apd.setFaltante(faltane);
			apd.setId(ap.getId());
		}
		for (AplicacionVariedadDTO avd : ad.getVariedades()) {
			AplicacionVariedad av = new AplicacionVariedad();
			if (avd.getId() != null) {
				av = aplicacionVariedadRepository.findById(avd.getVariedadId()).get();
			}
			av.setAplicacion(a);
			av.setEmpresaVariedad(empresaVariedadRepository.findById(avd.getVariedadId()).get());
			av = aplicacionVariedadRepository.save(av);
			avd.setId(av.getId());
		}
		template.convertAndSend("/message/aplicacionesPendientes/" + a.getEmpresaBloque().getEmpresa().getId(),
				obtenerPendientes(a.getEmpresaBloque().getEmpresa().getId()));
		return ad;
	}

	@Transactional
	public void actualizarStock(Integer idEmpresa, EmpresaProducto pr) throws Exception {
		List<Aplicacion> list = aplicacionRepository.obtenerPendientes(idEmpresa);
		list.forEach(a -> {
			List<AplicacionProducto> lap = aplicacionProductoRepository.findByAplicacionAndProducto(a.getId(),
					pr.getId());
			if (!lap.isEmpty()) {
				AplicacionProducto ap = lap.get(0);
				BigDecimal necesario = "SI".equalsIgnoreCase(a.getTipo().getTexto1())
						? BigDecimal.valueOf(a.getCantidadLitros()).multiply(ap.getDosisLitro())
						: ap.getDosisLitro();
				BigDecimal faltane = BigDecimal.ZERO;
				if (necesario.compareTo(pr.getDisponible()) == 1) {
					faltane = necesario.subtract(pr.getDisponible());
				}
				ap.setNecesario(necesario);
				ap.setFaltante(faltane);
				ap = aplicacionProductoRepository.save(ap);
			}

		});
	}

	@Transactional
	public AplicacionDTO iniciar(Integer idAplicacion) throws Exception {
		Aplicacion app = aplicacionRepository.findById(idAplicacion).orElse(null);
		app.setFechaAplicacion(LocalDate.now());
		app.setHoraInicio(LocalTime.now().truncatedTo(ChronoUnit.SECONDS));
		app.setEstado(EstadoAplicacionEnum.INICIADO);
		aplicacionRepository.save(app);
		List<AplicacionProducto> productos = aplicacionProductoRepository.findByAplicacion(idAplicacion);
		for (AplicacionProducto apr : productos) {
			EmpresaProducto pr = apr.getEmpresaProducto();
			if (pr.getDisponible() == null)
				throw new TransactionalException(
						String.format("El producto %1s no tiene disponibilidad", pr.getProducto().getNombre()), null);

			if (apr.getNecesario().compareTo(pr.getDisponible()) != 1)
				pr.setDisponible(pr.getDisponible().subtract(apr.getNecesario()));
			else
				pr.setDisponible(pr.getDisponible().subtract(apr.getNecesario().subtract(apr.getFaltante())));
			empresaProductoRepository.save(pr);
		}
		template.convertAndSend("/message/aplicacionesPendientes/" + app.getEmpresaBloque().getEmpresa().getId(),
				obtenerPendientes(app.getEmpresaBloque().getEmpresa().getId()));
		return new AplicacionDTO(app, new ArrayList<>(), new ArrayList<>());
	}

	public List<AplicacionDTO> buscar(Long desde, Long hasta, Integer idEmpresa, String plaga, Integer idTipo,
			Integer idProducto, Integer idEmpresaBloque) throws Exception {
		LocalDate fechaDesde = (new Timestamp(desde)).toLocalDateTime().toLocalDate();
		LocalDate fechaHasta = (new Timestamp(hasta)).toLocalDateTime().toLocalDate();
		List<Aplicacion> lista = aplicacionRepository.findByFecha(fechaDesde, fechaHasta, idEmpresa, idTipo, idProducto,
				idEmpresaBloque);
		return lista.stream().filter(a -> Arrays.asList(a.getPlagas()).contains(plaga) || "".equalsIgnoreCase(plaga))
				.map(af -> {
					List<AplicacionVariedad> av = aplicacionVariedadRepository.findByAplicacion(af.getId());
					List<AplicacionProducto> ap = aplicacionProductoRepository.findByAplicacion(af.getId());
					return new AplicacionDTO(af, ap.stream().map(ava -> {
						AplicacionProductoDTO dto = new AplicacionProductoDTO(ava);
						dto.setDisponible(
								ava.getEmpresaProducto() != null && ava.getEmpresaProducto().getDisponible() != null
										? ava.getEmpresaProducto().getDisponible()
										: BigDecimal.ZERO);
						if (ava.getNecesario().compareTo(dto.getDisponible()) == 1) {
							// dto.setNecesario(ava.getNecesario().subtract(dto.getDisponible()));
							dto.setFaltante(ava.getNecesario().subtract(dto.getDisponible()));
						}
						return dto;
					}).toList(), av.stream().map(ava -> new AplicacionVariedadDTO(ava)).toList());
				}).toList();
	}

	public List<ProductoConsumoDTO> obtnerConsumo(LocalDate fechaDesde, LocalDate fechaHasta, Integer idEmpresa)
			throws Exception {
		List<Object[]> lista = aplicacionProductoRepository.obtenerConsumo(fechaDesde, fechaHasta, idEmpresa);
		return lista.stream().map(o -> {
			BigDecimal consumo = BigDecimal.valueOf(Double.valueOf(o[0].toString()));
			EmpresaProducto pr = empresaProductoRepository.findById((Integer) o[2]).get();
			return new ProductoConsumoDTO(pr.getId(), pr.getProducto().getNombre(), consumo,
					pr.getProducto().getUnidadMedida().getNombre());
		}).toList();
	}

	public List<ProductoConsumoDTO> obtenerConsumoTotal(LocalDate fechaDesde, LocalDate fechaHasta, Integer idEmpresa,
			Integer idEmpresaBloque) throws Exception {
		List<Object[]> lista = aplicacionProductoRepository.obtenerConsumoTotal(fechaDesde, fechaHasta, idEmpresa,
				idEmpresaBloque, EstadoAplicacionEnum.FINALIZADO);
		return lista.stream().map(o -> {
			BigDecimal consumo = BigDecimal.valueOf(Double.valueOf(o[0].toString()));
			EmpresaProducto pr = empresaProductoRepository.findById((Integer) o[2]).get();
			String tipoAplicacion = o[3].toString();

			BigDecimal costo = BigDecimal.ZERO;
			BigDecimal precio = BigDecimal.ZERO;
			BigDecimal cantidadPorUnidad = BigDecimal.ZERO;
			if (o[4] != null && consumo != null && o[5] != null) {
				MathContext mc = new MathContext(2, RoundingMode.HALF_UP);
				cantidadPorUnidad = BigDecimal.valueOf(Double.valueOf(o[4].toString()));
				precio = BigDecimal.valueOf(Double.valueOf(o[5].toString()));
				BigDecimal unidadesUsadas = consumo.divide(cantidadPorUnidad, mc);
				costo = unidadesUsadas.multiply(precio);
			}
			return new ProductoConsumoDTO(pr.getId(), pr.getProducto().getNombre(), consumo,
					pr.getProducto().getUnidadMedida().getNombre(), tipoAplicacion, costo, precio, cantidadPorUnidad);
		}).toList();
	}

	public List<ProductoConsumoDTO> obtenerConsumoTotalPorMes(LocalDate fechaDesde, LocalDate fechaHasta,
			Integer idEmpresa, Integer idEmpresaBloque) throws Exception {
		List<Object[]> lista = aplicacionProductoRepository.obtenerConsumoTotalByMes(fechaDesde, fechaHasta, idEmpresa,
				idEmpresaBloque, EstadoAplicacionEnum.FINALIZADO);
		return lista.stream().map(o -> {
			BigDecimal gasto = o[0] != null ? BigDecimal.valueOf(Double.valueOf(o[0].toString())) : BigDecimal.ZERO;
			String tipoAplicacion = o[1].toString();
			Integer mes = (Integer) o[2];
			return new ProductoConsumoDTO(gasto, tipoAplicacion, mes);
		}).toList();
	}

	@Transactional
	public AplicacionDTO finalizar(Integer idAplicacion) throws Exception {
		Aplicacion a = aplicacionRepository.findById(idAplicacion).get();
		a.setHoraFin(LocalTime.now().truncatedTo(ChronoUnit.SECONDS));
		a.setDuracion(MINUTES.between(a.getHoraInicio(), a.getHoraFin()));
		a.setEstado(EstadoAplicacionEnum.FINALIZADO);
		aplicacionRepository.save(a);
		if (a.getDiasFrecuencia() != null && a.getDiasFrecuencia() > 0) {
			copiar(idAplicacion);
		}
		template.convertAndSend("/message/aplicacionesPendientes/" + a.getEmpresaBloque().getEmpresa().getId(),
				obtenerPendientes(a.getEmpresaBloque().getEmpresa().getId()));
		return new AplicacionDTO(a, new ArrayList<>(), new ArrayList<>());
	}

	@Transactional
	public AplicacionDTO copiar(Integer idAplicacion) throws Exception {
		Aplicacion a = aplicacionRepository.findById(idAplicacion).get();
		Aplicacion copia = new Aplicacion(a);
		copia.setEstado(EstadoAplicacionEnum.INGRESADO);
		copia.setObservacion(a.getObservacion());
		if (a.getDiasFrecuencia() != null && a.getDiasFrecuencia() > 0) {
			copia.setDiasFrecuencia(a.getDiasFrecuencia());
			String codeDias = a.getTipo().getCodigo().concat(a.getEmpresaBloque().getEmpresa().getRuc());
			List<Catalogo> listDias = catalogoRepository.findByCodigoEmpresa(codeDias);
			List<DayOfWeek> dayOfWeeek = Arrays.asList(listDias.get(0).getTextos()).stream()
					.map(sd -> DayOfWeek.valueOf(sd)).toList();
			copia.setFechaAplicacion(getNexDay(a.getFechaAplicacion(), a.getDiasFrecuencia(), dayOfWeeek));
		}
		aplicacionRepository.save(copia);
		List<AplicacionProducto> productos = aplicacionProductoRepository.findByAplicacion(idAplicacion);
		for (AplicacionProducto aplicacionProducto : productos) {
			EmpresaProducto pr = aplicacionProducto.getEmpresaProducto();
			AplicacionProducto copiaP = new AplicacionProducto(aplicacionProducto);
			BigDecimal faltane = BigDecimal.ZERO;
			if (copiaP.getNecesario().compareTo(pr.getDisponible()) == 1) {
				faltane = copiaP.getNecesario().subtract(pr.getDisponible());
			}
			copiaP.setFaltante(faltane);
			copiaP.setAplicacion(copia);
			aplicacionProductoRepository.save(copiaP);
		}
		List<AplicacionVariedad> variedades = aplicacionVariedadRepository.findByAplicacion(idAplicacion);
		for (AplicacionVariedad aplicacionVariedad : variedades) {
			AplicacionVariedad avcopia = new AplicacionVariedad(aplicacionVariedad);
			avcopia.setAplicacion(copia);
			aplicacionVariedadRepository.save(avcopia);
		}
		return new AplicacionDTO(copia, new ArrayList<>(), new ArrayList<>());
	}

	private LocalDate getNexDay(LocalDate firstAplication, Long frecuencia, List<DayOfWeek> daysIsApply) {
		Integer count = 0;
		LocalDate nextDate = null;
		LocalDate currentLoop = firstAplication;
		while (count < frecuencia | nextDate == null) {
			currentLoop = currentLoop.plus(1, DAYS);
			if (daysIsApply.contains(currentLoop.getDayOfWeek())) {
				nextDate = currentLoop;
			}
			count++;
		}
		return nextDate;
	}

	public AplicacionDTO findById(Integer idAplicacion) throws Exception {
		Aplicacion a = aplicacionRepository.findById(idAplicacion).get();
		List<AplicacionProducto> productos = aplicacionProductoRepository.findByAplicacion(idAplicacion);
		List<AplicacionVariedad> variedades = aplicacionVariedadRepository.findByAplicacion(idAplicacion);
		return new AplicacionDTO(a, productos.stream().map(ava -> {
			AplicacionProductoDTO dto = new AplicacionProductoDTO(ava);
			dto.setDisponible(ava.getEmpresaProducto().getDisponible());
			if (ava.getNecesario() != null && ava.getNecesario().compareTo(dto.getDisponible()) == 1) {
				dto.setFaltante(ava.getNecesario().subtract(dto.getDisponible()));
			}
			return dto;
		}).toList(), variedades.stream().map(ava -> new AplicacionVariedadDTO(ava)).toList());
	}

	public List<AplicacionDTO> obtenerPendientes(Integer idEmpresa) {
		List<Aplicacion> lista = aplicacionRepository.obtenerPendientes(idEmpresa);
		return lista.stream().map(af -> {
			List<AplicacionVariedad> av = aplicacionVariedadRepository.findByAplicacion(af.getId());
			List<AplicacionProducto> ap = aplicacionProductoRepository.findByAplicacion(af.getId());
			return new AplicacionDTO(af, ap.stream().map(ava -> {
				AplicacionProductoDTO dto = new AplicacionProductoDTO(ava);
				dto.setDisponible(ava.getEmpresaProducto() != null && ava.getEmpresaProducto().getDisponible() != null
						? ava.getEmpresaProducto().getDisponible()
						: BigDecimal.ZERO);
				if (ava != null && ava.getNecesario() != null
						&& ava.getNecesario().compareTo(dto.getDisponible()) == 1) {
					dto.setNecesario(ava.getNecesario().subtract(dto.getDisponible()));
				}
				return dto;
			}).toList(), av.stream().map(ava -> new AplicacionVariedadDTO(ava)).toList());
		}).toList();
	}

	@Transactional
	public Integer eliminar(Integer idAplicacion) throws Exception {
		Aplicacion a = new Aplicacion();

		a = aplicacionRepository.findById(idAplicacion).get();
		List<AplicacionProducto> productos = aplicacionProductoRepository.findByAplicacion(idAplicacion);
		for (AplicacionProducto ap : productos) {
			if (!a.getEstado().equals(EstadoAplicacionEnum.INGRESADO)) {
				ap.getEmpresaProducto().setDisponible(ap.getEmpresaProducto().getDisponible().add(ap.getNecesario()));
				empresaProductoRepository.save(ap.getEmpresaProducto());
				actualizarStock(a.getEmpresaBloque().getEmpresa().getId(), ap.getEmpresaProducto());
			}
			aplicacionProductoRepository.delete(ap);
		}
		List<AplicacionVariedad> variedades = aplicacionVariedadRepository.findByAplicacion(idAplicacion);
		for (AplicacionVariedad aplicacionVariedad : variedades) {
			aplicacionVariedadRepository.delete(aplicacionVariedad);
		}

		aplicacionRepository.delete(a);
		template.convertAndSend("/message/aplicacionesPendientes/" + a.getEmpresaBloque().getEmpresa().getId(),
				obtenerPendientes(a.getEmpresaBloque().getEmpresa().getId()));
		return idAplicacion;
	}

	public void fumnigacionSinFinalizar() {
		try {
			List<Empresa> empresas = empresaRepository.findActive();
			empresas.forEach(em -> {
				Integer idEmpresa = em.getId();
				List<Aplicacion> lista = aplicacionRepository.obtenerByTipoAndEstado(idEmpresa,
						CodigoCatalogoEnum.FUMNIGACION.name(), CodigoCatalogoEnum.DRENCH.name(),
						EstadoAplicacionEnum.INICIADO);
				List<AplicacionDTO> dtos = lista.stream().map(af -> {
					List<AplicacionVariedad> av = aplicacionVariedadRepository.findByAplicacion(af.getId());
					return new AplicacionDTO(af, new ArrayList<>(),
							av.stream().map(ava -> new AplicacionVariedadDTO(ava)).toList());
				}).toList();
				if (!dtos.isEmpty()) {
					template.convertAndSend("/message/sffumi/" + idEmpresa, dtos);
				}
			});
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public void fertilizacionSinFinalizar() {
		try {
			List<Empresa> empresas = empresaRepository.findActive();
			empresas.forEach(em -> {
				Integer idEmpresa = em.getId();
				List<Aplicacion> lista = aplicacionRepository.obtenerByTipoAndEstado(idEmpresa,
						CodigoCatalogoEnum.FERTILIZACION.name(), CodigoCatalogoEnum.FERTILIZACION.name(),
						EstadoAplicacionEnum.INICIADO);
				List<AplicacionDTO> dtos = lista.stream().map(af -> {
					List<AplicacionVariedad> av = aplicacionVariedadRepository.findByAplicacion(af.getId());
					return new AplicacionDTO(af, new ArrayList<>(),
							av.stream().map(ava -> new AplicacionVariedadDTO(ava)).toList());
				}).toList();
				if (!dtos.isEmpty()) {
					template.convertAndSend("/message/sffert/" + idEmpresa, dtos);
				}
			});
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

}
