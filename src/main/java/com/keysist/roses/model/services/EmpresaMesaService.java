package com.keysist.roses.model.services;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.keysist.roses.model.dto.EmpresaMesaDTO;
import com.keysist.roses.model.entity.Empresa;
import com.keysist.roses.model.entity.EmpresaMesa;
import com.keysist.roses.model.enums.ValorSiNoEnum;
import com.keysist.roses.model.repository.CatalogoRepository;
import com.keysist.roses.model.repository.EmpresaMesaRepository;
import com.keysist.roses.model.repository.EmpresaRepository;

@Service
public class EmpresaMesaService {

	@Autowired
	private EmpresaRepository empresaRepository;
	@Autowired
	private CatalogoRepository catalogoRepository;
	@Autowired
	private EmpresaMesaRepository empresaBloqueRepository;

	@Transactional
	public EmpresaMesaDTO save(EmpresaMesaDTO dto) {
		Empresa empresa = empresaRepository.findById(dto.getIdEmpresa()).orElse(null);
		List<EmpresaMesa> list = empresaBloqueRepository.findByEmpresaAndCatalogo(dto.getIdEmpresa(), dto.getIdCatalogo());
		EmpresaMesa ev = new EmpresaMesa();
		ev.setId(dto.getId());
		if (list.isEmpty()) {
			ev.setEmpresa(empresa);
		} else {
			ev = list.get(0);
		}
		ev.setMesa(catalogoRepository.findById(dto.getIdCatalogo()).get());
		ev.setNombre(dto.getNombre());
		ev.setActivo(ValorSiNoEnum.SI);
		empresaBloqueRepository.save(ev);
		return new EmpresaMesaDTO(ev);
	};

	@Transactional
	public List<EmpresaMesaDTO> consultar(Integer idEmpresa) throws Exception {
		return empresaBloqueRepository.findByEmpresa(idEmpresa).stream().map(b -> new EmpresaMesaDTO(b)).toList();
	}

	public EmpresaMesaDTO findById(Integer id) {
		return new EmpresaMesaDTO(empresaBloqueRepository.findById(id).get());
	}

	public Integer delete(Integer id) {
		empresaBloqueRepository.deleteById(id);
		return id;
	}
}
