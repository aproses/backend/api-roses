package com.keysist.roses.model.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.keysist.roses.model.dto.DisponibilidadDTO;
import com.keysist.roses.model.dto.DisponibilidadDetalleDTO;
import com.keysist.roses.model.entity.Catalogo;
import com.keysist.roses.model.enums.CodigosGrupoEnum;
import com.keysist.roses.model.enums.EstadoEntregaEnum;
import com.keysist.roses.model.repository.CatalogoRepository;
import com.keysist.roses.model.repository.EntregaBoncheRepository;
import com.keysist.roses.model.repository.VariedadRepository;

@Service
public class DisponibilidadService {

	@Autowired
	private EntregaBoncheRepository entregaBoncheRepository;

	@Autowired
	private VariedadRepository variedadRepository;

	@Autowired
	private CatalogoRepository catalogoRepository;

	@Autowired
	Environment env;

	public List<DisponibilidadDTO> recuperarRosaProcesada(String filtroDia, Integer idVariedad) {
		List<DisponibilidadDTO> retorno = new ArrayList<>();
		int numeroDia = -1;
		if ("LUNES".equalsIgnoreCase(filtroDia.trim())) {
			numeroDia = 1;
		} else if ("MARTES".equalsIgnoreCase(filtroDia.trim())) {
			numeroDia = 2;
		} else if ("MIERCOLES".equalsIgnoreCase(filtroDia.trim())) {
			numeroDia = 3;
		} else if ("JUEVES".equalsIgnoreCase(filtroDia.trim())) {
			numeroDia = 4;
		} else if ("VIERNES".equalsIgnoreCase(filtroDia.trim())) {
			numeroDia = 5;
		} else if ("SABADO".equalsIgnoreCase(filtroDia.trim())) {
			numeroDia = 6;
		} else if ("DOMINGO".equalsIgnoreCase(filtroDia.trim())) {
			numeroDia = 7;
		}
		final Integer numeroDiaDinal = numeroDia;
		final List<Catalogo> longitudes = catalogoRepository.findByGrupo(CodigosGrupoEnum.LONGITUD_TALLO.name());
		variedadRepository.findByIdVariedad(idVariedad).stream().forEach(v -> {
			final DisponibilidadDTO di = new DisponibilidadDTO();
			di.setVariedad(v.getNombre());
			List<DisponibilidadDetalleDTO> detalles = new ArrayList<>(0);
			longitudes.stream().forEach(l -> {
				DisponibilidadDetalleDTO detall = new DisponibilidadDetalleDTO();
				detall.setLongitud(l.getValor().intValue());
				Long total = entregaBoncheRepository.findByDia(numeroDiaDinal, v.getId(), EstadoEntregaEnum.PROCESADO,
						l.getId());
				detall.setBonches(total != null ? total.intValue() : 0);
				detalles.add(detall);
			});
			di.setDetalle(detalles);
			retorno.add(di);
		});

		return retorno;
	}

}
