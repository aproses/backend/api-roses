package com.keysist.roses.model.services;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.temporal.WeekFields;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.keysist.roses.model.dto.CorteDTO;
import com.keysist.roses.model.dto.CosechaDTO;
import com.keysist.roses.model.dto.CosechaVariedadDTO;
import com.keysist.roses.model.dto.ProduccionDTO;
import com.keysist.roses.model.dto.ProduccionVariedadDTO;
import com.keysist.roses.model.dto.ProyeccionDTO;
import com.keysist.roses.model.dto.TalloVariedadDTO;
import com.keysist.roses.model.entity.EmpresaVariedad;
import com.keysist.roses.model.enums.EstadoEntregaEnum;
import com.keysist.roses.model.repository.CosechaVariedadRepository;
import com.keysist.roses.model.repository.EmpresaVariedadRepository;
import com.keysist.roses.model.repository.EntregaVariedadRepository;
import com.keysist.roses.model.repository.PincheVariedadRepository;

@Service
public class ProduccionService {

	@Autowired
	private EntregaVariedadRepository entregaVariedadaRepository;
	@Autowired
	private CosechaVariedadRepository cosechaVariedadRepository;

	@Autowired
	private EmpresaVariedadRepository empresaVariedadRepository;

	@Autowired
	private PincheVariedadRepository pincheVariedadRepository;

	@Autowired
	Environment env;

	@Transactional
	public ProduccionDTO obtenerEstadoProcesadora() {
		ProduccionDTO pdto = new ProduccionDTO();
		List<Object[]> list = entregaVariedadaRepository.getEntregaDiaria(EstadoEntregaEnum.ENVIADO,
				EstadoEntregaEnum.RECEPCIONADO, EstadoEntregaEnum.PROCESADO);
		List<ProduccionVariedadDTO> pvd = list.stream().map(ob -> new ProduccionVariedadDTO(ob)).toList();
		pdto.setProduccionVariedad(pvd);
		pdto.setTotal(
				pvd.stream().map(d -> d.getTotal() != null ? BigDecimal.valueOf(Double.valueOf(d.getTotal().toString()))
						: BigDecimal.ZERO).reduce(BigDecimal.ZERO, BigDecimal::add).intValue());
		pdto.setTotalMallasRecepcionadas(
				pvd.stream().filter(ddto -> EstadoEntregaEnum.RECEPCIONADO.equals(ddto.getEstadoEntregaEnum()))
						.map(d -> d.getTotal() != null ? BigDecimal.valueOf(Double.valueOf(d.getTotal().toString()))
								: BigDecimal.ZERO)
						.reduce(BigDecimal.ZERO, BigDecimal::add).intValue());
		pdto.setTotalMallasEnviadas(
				pvd.stream().filter(ddto -> EstadoEntregaEnum.ENVIADO.equals(ddto.getEstadoEntregaEnum()))
						.map(d -> d.getTotal() != null ? BigDecimal.valueOf(Double.valueOf(d.getTotal().toString()))
								: BigDecimal.ZERO)
						.reduce(BigDecimal.ZERO, BigDecimal::add).intValue());
		pdto.setTotalMallasProcesadas(
				pvd.stream().filter(ddto -> EstadoEntregaEnum.PROCESADO.equals(ddto.getEstadoEntregaEnum()))
						.map(d -> d.getTotal() != null ? BigDecimal.valueOf(Double.valueOf(d.getTotal().toString()))
								: BigDecimal.ZERO)
						.reduce(BigDecimal.ZERO, BigDecimal::add).intValue());
		return pdto;

	}

	@Transactional
	public CosechaDTO obtenerCosecha(Integer idProveedor, Long fecha) {
		LocalDate fechaActual = (new Timestamp(fecha)).toLocalDateTime().toLocalDate();
		CosechaDTO pdto = new CosechaDTO();
		List<Object[]> list = cosechaVariedadRepository.findCosecha(idProveedor, fechaActual);
		List<CosechaVariedadDTO> pvd = list.stream().map(dv -> {
			EmpresaVariedad uv = empresaVariedadRepository.findById((Integer) dv[0]).get();
			BigDecimal totalTallos = BigDecimal.valueOf(Long.valueOf(dv[1].toString()));
			Integer cantidadMallas = totalTallos.divide(BigDecimal.valueOf(uv.getTalloMalla()), RoundingMode.FLOOR)
					.intValue();
			BigDecimal tallosSobantes = totalTallos.subtract(BigDecimal.valueOf((cantidadMallas * uv.getTalloMalla())));
			return new CosechaVariedadDTO(cantidadMallas, uv.getId(), uv.getVariedad().getNombre(),
					tallosSobantes.intValue(), uv.getVariedad().getColor().getCodigo());
		}).toList();
		pdto.setCosechaVariedadDTOs(pvd);
		return pdto;

	}

	@Transactional
	public CosechaDTO obtenerCosechaMensual(Integer idProveedor) {
		CosechaDTO pdto = new CosechaDTO();
		List<Object[]> list = cosechaVariedadRepository.findCosechaMensualAnual(idProveedor);
		List<CosechaVariedadDTO> pvd = list.stream().map(dv -> {
			EmpresaVariedad uv = empresaVariedadRepository.findById((Integer) dv[0]).get();
			BigDecimal totalTallos = BigDecimal.valueOf(Long.valueOf(dv[1].toString()));
			Integer mes = (Integer) dv[2];
			Integer anio = (Integer) dv[3];
			return new CosechaVariedadDTO(uv.getId(), uv.getVariedad().getNombre(), totalTallos.intValue(), mes,
					uv.getVariedad().getColor().getCodigo(), anio);
		}).toList();
		pdto.setCosechaVariedadDTOs(pvd);
		return pdto;

	}

	@Transactional
	public CosechaDTO obtenerProduccionSemanalAnual(Integer idProveedor) {
		CosechaDTO pdto = new CosechaDTO();
		List<Object[]> list = cosechaVariedadRepository.findCosechaSemanaAnual(idProveedor);
		List<CosechaVariedadDTO> pvd = list.stream().map(dv -> {
			EmpresaVariedad uv = empresaVariedadRepository.findById((Integer) dv[0]).get();
			BigDecimal totalTallos = BigDecimal.valueOf(Long.valueOf(dv[1].toString()));
			Integer semana = (Integer) dv[2];
			Integer anio = (Integer) dv[3];
			return new CosechaVariedadDTO(uv.getId(), uv.getVariedad().getNombre(), totalTallos.intValue(), semana,
					uv.getVariedad().getColor().getCodigo(), anio);
		}).toList();
		pdto.setCosechaVariedadDTOs(pvd);
		return pdto;

	}

	@Transactional
	public ProyeccionDTO obtenerProyeccion(Integer idEmpresaVariedad, Long fechaDesde, Long fechaHasta) {
		EmpresaVariedad variedad = empresaVariedadRepository.findById(idEmpresaVariedad).orElse(null);
		WeekFields weekFields = WeekFields.of(Locale.getDefault());
		LocalDate fechaDesdeLD = (new Timestamp(fechaDesde)).toLocalDateTime().toLocalDate();
		LocalDate fechaHastaLD = (new Timestamp(fechaHasta)).toLocalDateTime().toLocalDate();
		int semanaCosechaDesde = fechaDesdeLD.get(weekFields.weekOfWeekBasedYear());
		int semanaCosechaHasta = fechaHastaLD.get(weekFields.weekOfWeekBasedYear());

		List<Object[]> produccionObtenida = cosechaVariedadRepository.findCosechaByVariedad(idEmpresaVariedad,
				semanaCosechaDesde, semanaCosechaHasta);

		LocalDate fechaDesdeLDC = fechaDesdeLD.minusDays(variedad.getDiasCiclo().intValue());
		LocalDate fechaHastaLDC = fechaHastaLD.minusDays(variedad.getDiasCiclo().intValue());
		int semanaDesde = fechaDesdeLDC.get(weekFields.weekOfWeekBasedYear());
		int semanaHasta = fechaHastaLDC.get(weekFields.weekOfWeekBasedYear());
		int semanasSiclo = variedad.getDiasCiclo().divide(BigInteger.valueOf(7)).intValue();
		ProyeccionDTO pdto = new ProyeccionDTO();
		List<Object[]> cosechaObtenida = cosechaVariedadRepository.findCosechaByVariedad(idEmpresaVariedad, semanaDesde,
				semanaHasta);
		List<Object[]> listPinche = pincheVariedadRepository.findCosechaByVariedad(idEmpresaVariedad, semanaDesde,
				semanaHasta);
		List<TalloVariedadDTO> pvd = new ArrayList<>();
		pvd.addAll(cosechaObtenida.stream().map(dv -> {
			BigDecimal totalTallos = BigDecimal.valueOf(Long.valueOf(dv[0].toString()));
			Integer semana = (Integer) dv[1];
			return new TalloVariedadDTO(variedad.getId(), variedad.getVariedad().getNombre(), totalTallos.intValue(),
					semana + semanasSiclo, "Cosecha", semana, "COS");
		}).toList());
		pvd.addAll(listPinche.stream().map(dv -> {
			BigDecimal totalTallos = BigDecimal.valueOf(Long.valueOf(dv[0].toString()));
			Integer semana = (Integer) dv[1];
			return new TalloVariedadDTO(variedad.getId(), variedad.getVariedad().getNombre(), totalTallos.intValue(),
					semana + semanasSiclo, "Pinche", semana, "PIN");
		}).toList());

		pvd.addAll(produccionObtenida.stream().map(dv -> {
			BigDecimal totalTallos = BigDecimal.valueOf(Long.valueOf(dv[0].toString()));
			Integer semana = (Integer) dv[1];
			return new TalloVariedadDTO(variedad.getId(), variedad.getVariedad().getNombre(), totalTallos.intValue(),
					semana, "Cosecha Obtenida", semana - semanasSiclo, "COSOB");
		}).toList());

		pdto.setTalloVariedadDTOs(pvd);
		return new ProyeccionDTO(variedad.getEmpresa().getProduccionSemana(), variedad.getProduccionPerdida(),
				variedad.getProduccionMes(), variedad.getCantidadPlantas(), pvd);

	}

	@Transactional
	public List<ProyeccionDTO> obtenerFechaCorte(CorteDTO corteDTO) {
		List<LocalDate> fechas = new ArrayList<>();
		List<ProyeccionDTO> cosechas = new ArrayList<>();
		WeekFields weekFields = WeekFields.of(Locale.getDefault());
		LocalDate fechaCosechaDesde = (new Timestamp(corteDTO.getFechaCosechaDesde())).toLocalDateTime().toLocalDate();
		LocalDate fechaCosechaHasta = (new Timestamp(corteDTO.getFechaCosechaHasta())).toLocalDateTime().toLocalDate();
		boolean semanaDiferente = true;
		while (semanaDiferente) {
			fechas.add(fechaCosechaDesde);
			int semanaCorte = fechaCosechaDesde.get(weekFields.weekOfWeekBasedYear());
			int semanaCosecha = fechaCosechaHasta.get(weekFields.weekOfWeekBasedYear());
			semanaDiferente = semanaCorte != semanaCosecha;
			fechaCosechaDesde = fechaCosechaDesde.plusWeeks(1);
		}
		cosechas.addAll(fechas.stream().map(fechaCosecha -> {
			LocalDate fechaCorte = corteDTO.getAdelante() ? fechaCosecha.plusDays(corteDTO.getCiclo())
					: fechaCosecha.minusDays(corteDTO.getCiclo());
			int semanaCorte = fechaCorte.get(weekFields.weekOfWeekBasedYear());
			int semanaCosecha = fechaCosecha.get(weekFields.weekOfWeekBasedYear());
			return new ProyeccionDTO(semanaCorte, semanaCosecha, fechaCorte, fechaCosecha);
		}).toList());

		return cosechas;
	}

}
