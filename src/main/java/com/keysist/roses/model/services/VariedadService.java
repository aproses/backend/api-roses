package com.keysist.roses.model.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.keysist.roses.model.dto.VariedadDTO;
import com.keysist.roses.model.entity.Variedad;
import com.keysist.roses.model.repository.CatalogoRepository;
import com.keysist.roses.model.repository.VariedadRepository;

@Service
public class VariedadService {

	@Autowired
	private VariedadRepository variedadRepository;

	@Autowired
	private CatalogoRepository catalogoRepository;

	@Autowired
	Environment env;

	public List<VariedadDTO> findByFilter(String nombre) {
		return variedadRepository.findAllByFilter(nombre).stream().map(v->new VariedadDTO(v)).toList();
	}

	public List<VariedadDTO> delete(Integer idVariedad, String nombre) {
		variedadRepository.deleteById(idVariedad);
		return findByFilter(nombre);
	}

	public VariedadDTO save(VariedadDTO dto, String nombre) {
		Variedad v = new Variedad(dto);
		v.setColor(catalogoRepository.findById(dto.getIdColor()).orElse(null));
		variedadRepository.save(v);
		dto.setId(v.getId());
		return dto;
	}

}
