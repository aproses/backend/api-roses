package com.keysist.roses.model.services;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.keysist.roses.model.dto.EmpresaVariedadPrecioDTO;
import com.keysist.roses.model.dto.EntregaBoncheDTO;
import com.keysist.roses.model.dto.EntregaNacionalDTO;
import com.keysist.roses.model.dto.EntregaPoscosechaDTO;
import com.keysist.roses.model.dto.EntregaTalloDTO;
import com.keysist.roses.model.entity.Catalogo;
import com.keysist.roses.model.entity.Empresa;
import com.keysist.roses.model.entity.EmpresaVariedad;
import com.keysist.roses.model.entity.EmpresaVariedadPrecio;
import com.keysist.roses.model.entity.Entrega;
import com.keysist.roses.model.entity.EntregaBonche;
import com.keysist.roses.model.entity.EntregaNacional;
import com.keysist.roses.model.entity.EntregaPoscosecha;
import com.keysist.roses.model.entity.EntregaTallo;
import com.keysist.roses.model.entity.EntregaVariedad;
import com.keysist.roses.model.entity.RelacionComercial;
import com.keysist.roses.model.enums.CodigosGrupoEnum;
import com.keysist.roses.model.enums.EstadoEntregaEnum;
import com.keysist.roses.model.enums.EstadoEntregaProcesoEnum;
import com.keysist.roses.model.enums.TipoEntregaEnum;
import com.keysist.roses.model.repository.CatalogoRepository;
import com.keysist.roses.model.repository.EmpresaBloqueRepository;
import com.keysist.roses.model.repository.EmpresaMesaRepository;
import com.keysist.roses.model.repository.EmpresaRepository;
import com.keysist.roses.model.repository.EmpresaVariedadPrecioRepository;
import com.keysist.roses.model.repository.EmpresaVariedadRepository;
import com.keysist.roses.model.repository.EntregaBoncheRepository;
import com.keysist.roses.model.repository.EntregaNacionalRepository;
import com.keysist.roses.model.repository.EntregaPoscosechaRepository;
import com.keysist.roses.model.repository.EntregaRepository;
import com.keysist.roses.model.repository.EntregaTalloRepository;
import com.keysist.roses.model.repository.EntregaVariedadRepository;
import com.keysist.roses.model.repository.RelacionComercialRepository;
import com.keysist.roses.model.repository.UsuarioRepository;

@Service
public class PoscosechaService {

	@Autowired
	private EntregaBoncheRepository entregaBoncheR;

	@Autowired
	private EntregaTalloRepository entregaTalloRepository;

	@Autowired
	private EntregaNacionalRepository entregaBoncheN;
	@Autowired
	private EntregaNacionalRepository entregaNacionalR;

	@Autowired
	private EntregaVariedadRepository entregaVariedadRepository;

	@Autowired
	private EntregaRepository entregaRepository;

	@Autowired
	private EmpresaVariedadRepository empresaVariedadRepository;

	@Autowired
	private EmpresaRepository empresaRepository;

	@Autowired
	private EmpresaMesaRepository empresaMesaRepository;

	@Autowired
	private EmpresaBloqueRepository empresaBloqueRepository;

	@Autowired
	private CatalogoRepository catalogoRepository;

	@Autowired
	private UsuarioRepository usuarioRepository;

	// @Autowired
	// private EntregaService entregaService;

	@Autowired
	private RelacionComercialRepository comercialRepository;

	@Autowired
	private EntregaPoscosechaRepository entregaPoscosechaR;

	@Autowired
	private EmpresaVariedadPrecioRepository empresaVariedadPrecioRepository;

	@Autowired
	Environment env;

	@Transactional
	public List<EntregaPoscosechaDTO> procesar(List<EntregaPoscosechaDTO> list) throws Exception {
		if (list.isEmpty())
			return list;
		for (EntregaPoscosechaDTO e : list) {
			procesarVariedad(e);
		}
		return list;
	}

	public void registrarIngreso(Entrega entrega) {
		List<Object[]> entregasPendientes = entregaVariedadRepository.findByEntregaAndEstado(entrega,
				EstadoEntregaEnum.ENVIADO);
		for (Object[] obj : entregasPendientes) {
			int total = 0;
			Integer empresaVariedad = 0;
			if (obj[0] != null)
				total = ((Long) obj[0]).intValue();
			if (obj[1] != null)
				empresaVariedad = (Integer) obj[1];
			if (total != 0 && empresaVariedad != 0) {
				EmpresaVariedad ev = empresaVariedadRepository.findById(empresaVariedad).get();
				Optional<EntregaPoscosecha> epOpt = entregaPoscosechaR.findEntregaAndEmpresaVariedad(entrega, ev);
				EntregaPoscosecha ep = new EntregaPoscosecha(total, entrega, ev, EstadoEntregaProcesoEnum.RECEPCIONADO);
				if (epOpt.isPresent()) {
					ep = epOpt.get();
					ep.setTallos(ep.getTallos() + total);
					ep.setEstado(EstadoEntregaProcesoEnum.RECEPCIONADO);
				}
				entregaPoscosechaR.save(ep);
			}
		}

	}

	/**
	 * Servicio que reaiza el proceso de clasificacion y enbonche
	 * 
	 * @param e
	 * @return
	 * @throws Exception
	 */
	@Transactional
	public EntregaPoscosechaDTO procesarVariedad(EntregaPoscosechaDTO e) throws Exception {
		EntregaPoscosecha ep = entregaPoscosechaR.findById(e.getId()).get();
		Entrega ent = ep.getEntrega();
		// EmpresaVariedad ev =
		// empresaVariedadRepository.findById(e.getEmpresaVariedadId()).get();
		Empresa empSobrante = empresaRepository.findByRucSobrante(ent.getCliente().getRuc().concat("_SOB"));
		if (empSobrante != null) {
			Optional<Entrega> entregaSobranteOpt = entregaRepository.findByUsuarioFechaTipo(empSobrante.getId(),
					ent.getCliente().getId(), ent.getFechaEntrega(), TipoEntregaEnum.SOBRANTE);
			Entrega entregaSobrante = new Entrega();
			if (entregaSobranteOpt.isEmpty()) {
				entregaSobrante.setTipo(TipoEntregaEnum.SOBRANTE);
				entregaSobrante.setProveedor(empSobrante);
				entregaSobrante.setCliente(ent.getCliente());
				entregaSobrante.setFechaEntrega(LocalDate.now());
				entregaSobrante.setEstado(EstadoEntregaEnum.RECEPCIONADO);
				entregaSobrante.setHoraEntrega(LocalTime.now());
				entregaRepository.save(entregaSobrante);
			} else
				entregaSobrante = entregaSobranteOpt.get();
			// Registramos los sobrantes solo si la entrega es de tipo normal(proveedor)
			// if (TipoEntregaEnum.PROVEEDOR.equals(ent.getTipo()) && e.getSobrante() !=
			// null && e.getSobrante() > 0) {
			// entregaService.registrarSobrante(entregaSobrante, e.getSobrante(), ev);
			// }
		}

		ent.setEstado(EstadoEntregaEnum.PROCESADO);
		ent.setFechaProcesamiento(LocalDateTime.now());
		entregaRepository.save(ent);

		e.getBoncheDTOs().parallelStream().forEach(d -> {
			if (d.getBonches() >= 0) {
				if (d.getId() != null) {
					EntregaBonche dp = entregaBoncheR.findById(d.getId()).get();
					dp.setEstado(EstadoEntregaProcesoEnum.RECEPCIONADO);
					dp.setFechaProceso(LocalDateTime.now());
					dp.setBonches(d.getBonches());
					entregaBoncheR.save(dp);
					d.setId(dp.getId());
				} else {
					EntregaBonche dp = new EntregaBonche();
					dp.setLongitud(catalogoRepository.findById(d.getLongitudId()).orElse(null));
					dp.setTalloBonche(catalogoRepository.findById(d.getTalloBoncheId()).orElse(null));
					dp.setEstado(EstadoEntregaProcesoEnum.RECEPCIONADO);
					dp.setFechaProceso(LocalDateTime.now());
					dp.setBonches(d.getBonches());
					dp.setBonchesDisponibles(0);
					entregaBoncheR.save(dp);
					d.setId(dp.getId());
				}

			}
		});
		return e;
	}

	@Transactional
	public List<EntregaPoscosechaDTO> finalizar(List<EntregaPoscosechaDTO> list) throws Exception {
		if (list.isEmpty())
			return list;
		for (EntregaPoscosechaDTO e : list) {
			finalizarVariedad(e);
		}

		return list;
	}

	@Transactional
	public EntregaPoscosechaDTO finalizarVariedad(EntregaPoscosechaDTO e) throws Exception {
		EntregaPoscosecha entrega = entregaPoscosechaR.findById(e.getId()).orElse(null);
		Entrega en = entrega.getEntrega();
		entrega.setEstado(EstadoEntregaProcesoEnum.FINALIZADO);
		en.setFechaFinalizado(LocalDateTime.now());
		en.setHoraFinalizado(LocalTime.now());
		entregaPoscosechaR.save(entrega);
		List<EntregaVariedad> evs = entregaVariedadRepository.findByEntAndEmpresaVariedad(entrega.getEntrega().getId(),
				e.getEmpresaVariedadId(), EstadoEntregaEnum.RECEPCIONADO);
		if (evs != null) {
			for (EntregaVariedad de : evs) {
				de.setEstado(EstadoEntregaEnum.FINALIZADO);
				entregaVariedadRepository.save(de);
			}
		}
		for (EntregaBoncheDTO ebdto : e.getBoncheDTOs()) {
			EntregaBonche eb = entregaBoncheR.findById(ebdto.getId()).get();
			eb.setEstado(EstadoEntregaProcesoEnum.FINALIZADO);
			eb.setBonchesDisponibles(eb.getBonches());
			entregaBoncheR.save(eb);
		}
		for (EntregaNacionalDTO ebdto : e.getNacionalDTOs()) {
			EntregaNacional eb = entregaBoncheN.findById(ebdto.getId()).get();
			eb.setEstado(EstadoEntregaProcesoEnum.FINALIZADO);
			entregaBoncheN.save(eb);
		}
		return e;

	}

	@Transactional
	public List<EntregaPoscosechaDTO> guardarDetallePoscosecha(List<EntregaPoscosechaDTO> list) {
		list.parallelStream().forEach(e -> {
			guardarDetallePoscosechaVariedad(e);
		});
		return list;
	}

	@Transactional
	public EntregaPoscosechaDTO guardarDetallePoscosechaVariedad(EntregaPoscosechaDTO e) {
		EntregaPoscosecha de = entregaPoscosechaR.findById(e.getId()).orElse(null);
		if (de != null) {
			de.setAtributesDto(e);
			entregaPoscosechaR.save(de);
			e.getBoncheDTOs().parallelStream().forEach(d -> {
				if (d.getBonches() >= 0) {
					EntregaBonche dp = new EntregaBonche(d);
					dp.setLongitud(catalogoRepository.findById(d.getLongitudId()).orElse(null));
					dp.setEntregaPoscosecha(de);
					dp.setTalloBonche(catalogoRepository.findById(d.getTalloBoncheId()).orElse(null));
					entregaBoncheR.save(dp);
					d.setId(dp.getId());
				}
			});
		}

		return e;
	}

	public List<EntregaPoscosechaDTO> recuperarDetallesEntrega(LocalDate fechaEntrega, Integer idProveedor,
			Integer idCliente) {
		List<EntregaPoscosechaDTO> retorno = new ArrayList<>();
		List<Catalogo> longitudes = catalogoRepository.findByGrupo(CodigosGrupoEnum.LONGITUD_TALLO.name());
		List<Catalogo> tallosPorBonche = catalogoRepository.findByGrupo(CodigosGrupoEnum.TALLO_BONCHE.name());
		List<Catalogo> categoriaNacional = catalogoRepository.findByGrupo(CodigosGrupoEnum.GRUPO_NACIONAL.name());
		List<Catalogo> subCategoriaNacional = new ArrayList<>();
		for (Catalogo catalogo : categoriaNacional) {
			subCategoriaNacional.addAll(catalogoRepository.findByGrupo(catalogo.getCodigo()));
		}
		Optional<RelacionComercial> rcOpt = comercialRepository.findByProveedorAndClienteId(idProveedor, idCliente);
		if (rcOpt.isPresent()) {
			RelacionComercial rc = rcOpt.get();
			List<EmpresaVariedad> variedades = entregaPoscosechaR.findEmpresaVariedad(idProveedor, idCliente,
					fechaEntrega, TipoEntregaEnum.PROVEEDOR);
			for (EmpresaVariedad empVar : variedades) {
				// obtenemso las entregas pendientes
				Optional<EntregaPoscosecha> epOpt = entregaPoscosechaR.findByEntregaVariedad(idProveedor, idCliente,
						fechaEntrega, TipoEntregaEnum.PROVEEDOR, empVar.getId());
				if (epOpt.isPresent()) {
					EntregaPoscosecha ep = epOpt.get();
					EntregaPoscosechaDTO dto = new EntregaPoscosechaDTO(ep, empVar);
					dto.setValores(rc != null ? rc.getProveedorCodigo() : "", ep.getTallos(), ep.getNacional(),
							ep.getExportacion(), ep.getDescabezado(), ep.getDevolucion());
					// generamos los bonches
					List<EntregaBoncheDTO> list = new ArrayList<>();
					List<EntregaBonche> bonches = entregaBoncheR.findByEntregaPoscosecha(epOpt.get().getId());
					for (Catalogo txb : tallosPorBonche) {
						EntregaBoncheDTO detPost = new EntregaBoncheDTO();
						for (Catalogo longi : longitudes) {
							Optional<EntregaBonche> listDp = bonches.stream()
									.filter(dp -> dp.getLongitud().getId().equals(longi.getId())
											&& dp.getTalloBonche().getId().equals(txb.getId()))
									.findFirst();
							if (listDp.isPresent()) {
								detPost = new EntregaBoncheDTO(listDp.get());
								list.add(detPost);
							}
						}

					}
					dto.setBoncheDTOs(list);
					// generamos la nacional
					List<EntregaNacionalDTO> nacioanalDtos = new ArrayList<>();
					List<EntregaNacional> nacionales = entregaNacionalR.findByEntregaPoscosecha(epOpt.get().getId());
					for (Catalogo categoria : categoriaNacional) {
						List<Catalogo> subCategorias = subCategoriaNacional.stream()
								.filter(o -> o.getCodigoGrupo().equals(categoria.getCodigo())).toList();
						for (Catalogo subCategoria : subCategorias) {
							EntregaNacionalDTO detPost = new EntregaNacionalDTO();
							Optional<EntregaNacional> listDp = nacionales.stream()
									.filter(dp -> dp.getCategoria().getId().equals(categoria.getId())
											&& dp.getSubCategoria().getId().equals(subCategoria.getId()))
									.findFirst();
							if (listDp.isPresent()) {
								detPost = new EntregaNacionalDTO(listDp.get());
								nacioanalDtos.add(detPost);
							}

						}
					}
					dto.setNacionalDTOs(nacioanalDtos);
					retorno.add(dto);
				}
			}
		}
		return retorno;
	}

	public EntregaBoncheDTO recuperarEntregaBonche(LocalDate fechaEntrega, Integer idCliente, Integer idProveedor,
			Integer idEmpresaVariedad, Integer idLongitud, Integer idMesa, Integer idPuntoCorte, Integer idTalloBonche,
			Integer idTipoCarton) throws Exception {

		Optional<EntregaPoscosecha> epOpt = entregaPoscosechaR.findByEntregaVariedad(idProveedor, idCliente,
				fechaEntrega, TipoEntregaEnum.PROVEEDOR, idEmpresaVariedad, EstadoEntregaProcesoEnum.RECEPCIONADO);
		if (epOpt.isEmpty())
			throw new Exception("No existe datos para los filtros ingresados");
		Optional<EntregaBonche> entregaBonche = entregaBoncheR.findByLongitud(fechaEntrega, idEmpresaVariedad,
				idLongitud, idMesa, idPuntoCorte, idTalloBonche, idTipoCarton);
		EntregaBoncheDTO detPost = new EntregaBoncheDTO();
		if (entregaBonche.isPresent()) {
			detPost = new EntregaBoncheDTO(entregaBonche.get());
		} else {
			detPost = new EntregaBoncheDTO(epOpt.get(), catalogoRepository.findById(idLongitud).get(),
					catalogoRepository.findById(idTalloBonche).get(), empresaMesaRepository.findById(idMesa).get(),
					catalogoRepository.findById(idPuntoCorte).get(), catalogoRepository.findById(idTipoCarton).get());
		}
		return detPost;
	}

	public EntregaTalloDTO recuperarEntregaTallo(LocalDate fechaEntrega, Integer idCliente, Integer idProveedor,
			Integer idEmpresaVariedad, Integer idLongitud) throws Exception {

		Optional<EntregaPoscosecha> epOpt = entregaPoscosechaR.findByEntregaVariedad(idProveedor, idCliente,
				fechaEntrega, TipoEntregaEnum.PROVEEDOR, idEmpresaVariedad, EstadoEntregaProcesoEnum.RECEPCIONADO);
		if (epOpt.isEmpty())
			throw new Exception("No existe datos para los filtros ingresados");
		Optional<EntregaTallo> entregaBonche = entregaTalloRepository.findTallosByLongitud(fechaEntrega,
				idEmpresaVariedad, idLongitud);
		EntregaTalloDTO detPost = new EntregaTalloDTO();
		if (entregaBonche.isPresent()) {
			detPost = new EntregaTalloDTO(entregaBonche.get());
		} else {
			detPost = new EntregaTalloDTO(epOpt.get(), catalogoRepository.findById(idLongitud).get());
		}
		return detPost;
	}

	@Transactional
	public Integer guardarEntregaBoncheTallo(EntregaTalloDTO d) {
		EntregaTallo dp = new EntregaTallo(d);
		EntregaPoscosecha p = entregaPoscosechaR.findById(d.getEntregaPoscosechaId()).get();
		LocalDateTime fechaProceso = (new Timestamp(d.getFechaProcesoTime())).toLocalDateTime();
		dp.setLongitud(catalogoRepository.findById(d.getLongitudId()).orElse(null));
		dp.setEntregaPoscosecha(p);
		dp.setFechaProceso(fechaProceso);
		dp.setEstado(EstadoEntregaProcesoEnum.RECEPCIONADO);
		if (d.getId() == null) {
			p.setExportacion(p.getExportacion() + d.getTalloTotal());
		} else {
			EntregaTallo dpTemp = entregaTalloRepository.findById(d.getId()).get();
			p.setExportacion(p.getExportacion() - dpTemp.getTalloTotal());
			p.setExportacion(p.getExportacion() + d.getTalloTotal());
		}
		p.setDescabezado(d.getTallosDescab());
		p.setDevolucion(d.getTallosDevoluc());
		entregaTalloRepository.save(dp);
		// p.setSobrante(p.getTallos() - p.getExportacion() - p.getNacional());
		entregaPoscosechaR.save(p);
		return dp.getId();

	}

	@Transactional
	public Integer guardarEntregaBonche(EntregaBoncheDTO d) {
		EntregaBonche dp = new EntregaBonche(d);
		EntregaPoscosecha p = entregaPoscosechaR.findById(d.getEntregaPoscosechaId()).get();
		LocalDateTime fechaProceso = (new Timestamp(d.getFechaProcesoTime())).toLocalDateTime();
		dp.setLongitud(catalogoRepository.findById(d.getLongitudId()).orElse(null));
		dp.setEntregaPoscosecha(p);
		dp.setEstado(EstadoEntregaProcesoEnum.RECEPCIONADO);
		dp.setTalloBonche(catalogoRepository.findById(d.getTalloBoncheId()).orElse(null));
		dp.setMesa(empresaMesaRepository.findById(d.getMesaId()).orElse(null));
		dp.setPuntoCorte(catalogoRepository.findById(d.getPuntoCorteId()).orElse(null));
		dp.setTipoCarton(catalogoRepository.findById(d.getTipoCartonId()).orElse(null));
		dp.setFechaProceso(fechaProceso);
		entregaBoncheR.save(dp);
		return dp.getId();

	}

	public EntregaNacionalDTO recuperarDetallesEntregaNacional(LocalDate fechaProceso, Integer idCliente,
			Integer idProveedor, Integer idEmpresaVariedad, Integer idCategoria, Integer idSubCategoria,
			Integer idBloque, Integer idClasificador) throws Exception {
		Optional<EntregaPoscosecha> epOpt = entregaPoscosechaR.findByEntregaVariedad(idProveedor, idCliente,
				fechaProceso, TipoEntregaEnum.PROVEEDOR, idEmpresaVariedad, EstadoEntregaProcesoEnum.RECEPCIONADO);
		if (epOpt.isEmpty())
			throw new Exception("No existe datos para los filtros ingresados");
		Optional<EntregaNacional> entregaBonche = entregaBoncheN.findByFilter(fechaProceso, idEmpresaVariedad,
				idCategoria, idSubCategoria, idBloque, idClasificador);
		EntregaNacionalDTO detPost = new EntregaNacionalDTO();
		if (entregaBonche.isPresent()) {
			detPost = new EntregaNacionalDTO(entregaBonche.get());
		} else {
			detPost = new EntregaNacionalDTO(epOpt.get());
		}
		return detPost;
	}

	@Transactional
	public Integer guardarEntregaNacional(EntregaNacionalDTO d) {
		EntregaPoscosecha p = entregaPoscosechaR.findById(d.getEntregaPoscosechaId()).get();
		EntregaNacional dp = new EntregaNacional(d);
		LocalDateTime fechaProceso = (new Timestamp(d.getFechaProcesoTime())).toLocalDateTime();
		dp.setBloque(empresaBloqueRepository.findById(d.getBloqueId()).orElse(null));
		dp.setCategoria(catalogoRepository.findById(d.getCategoriaId()).orElse(null));
		dp.setSubCategoria(catalogoRepository.findById(d.getSubCategoriaId()).orElse(null));
		dp.setClasificador(usuarioRepository.findById(d.getClasificadorId()).orElse(null));
		dp.setFechaProceso(fechaProceso);
		dp.setEntregaPoscosecha(p);
		// p.setSobrante(p.getTallos() - p.getExportacion() - p.getNacional());
		if (d.getId() == null) {
			p.setNacional(p.getNacional() + d.getTalloTotal());
		} else {
			EntregaNacional dpTemp = entregaNacionalR.findById(d.getId()).get();
			p.setNacional(p.getNacional() - dpTemp.getTalloTotal());
			p.setNacional(p.getNacional() + d.getTalloTotal());
		}
		entregaPoscosechaR.save(p);
		entregaNacionalR.save(dp);
		return dp.getId();

	}

	public List<EntregaPoscosechaDTO> recuperaEntregas(LocalDate desde, LocalDate hasta, Integer idProveedor,
			Integer idCliente) {
		List<Catalogo> longitudes = catalogoRepository.findByGrupo(CodigosGrupoEnum.LONGITUD_TALLO.name());
		List<EntregaPoscosechaDTO> retorno = new ArrayList<>();
		List<Object[]> list = entregaPoscosechaR.findByRange(idProveedor, idCliente, desde, hasta,
				TipoEntregaEnum.PROVEEDOR);
		for (Object[] obj : list) {
			Integer id = 0;
			if (obj[0] != null)
				id = ((Integer) obj[0]);
			EntregaPoscosecha ep = entregaPoscosechaR.findById(id).get();
			EntregaPoscosechaDTO dto = new EntregaPoscosechaDTO(ep);
			List<EntregaTallo> bonches = entregaTalloRepository.findByEntregaPoscosecha(ep.getId());
			List<EntregaTalloDTO> list3 = new ArrayList<>();
			for (Catalogo l : longitudes) {
				List<EntregaTallo> listLong = bonches.stream().filter(b -> b.getLongitud().equals(l)).toList();
				Optional<EntregaTallo> bonche = listLong.stream().findFirst();
				EntregaTalloDTO detPost = new EntregaTalloDTO();
				if (bonche.isPresent())
					detPost = new EntregaTalloDTO(bonche.get());
				else
					detPost = new EntregaTalloDTO(l, 0, BigDecimal.ZERO);

				list3.add(detPost);
			}

			dto.setTalloDTOs(list3);
			// generamos la nacional
			List<EntregaNacionalDTO> nacioanalDtos = new ArrayList<>();
			List<EntregaNacional> nacionales = entregaNacionalR.findByEntregaPoscosecha(ep.getId());
			for (EntregaNacional entregaPoscosecha : nacionales) {
				EntregaNacionalDTO detPost = new EntregaNacionalDTO(entregaPoscosecha);
				nacioanalDtos.add(detPost);
			}
			dto.setNacionalDTOs(nacioanalDtos);
			retorno.add(dto);
		}
		return retorno;
	}

	@Transactional
	public EmpresaVariedadPrecioDTO registrarPrecio(EmpresaVariedadPrecioDTO e) throws Exception {
		LocalDate desde = (new Timestamp(e.getDesde())).toLocalDateTime().toLocalDate();
		LocalDate hasta = (new Timestamp(e.getHasta())).toLocalDateTime().toLocalDate();
		List<EmpresaVariedadPrecio> list = empresaVariedadPrecioRepository.findPrecioByLongitud(desde, hasta,
				e.getIdEmpresaVariedad(), e.getIdLongitud(), e.getId() != null ? e.getId() : -1, e.getIdCliente());
		if (!list.isEmpty())
			throw new Exception("Las fechas ingresadas ya estan dento de otro periodo");

		EmpresaVariedadPrecio evp = new EmpresaVariedadPrecio();
		if (e.getId() != null)
			evp = empresaVariedadPrecioRepository.findById(e.getId()).get();
		EmpresaVariedad ev = empresaVariedadRepository.findById(e.getIdEmpresaVariedad()).get();
		Catalogo longitud = catalogoRepository.findById(e.getIdLongitud()).get();
		evp.setEmpresaVariedad(ev);
		evp.setLongitud(longitud);
		evp.setFechaDesde(desde);
		evp.setFechaHasta(hasta);
		evp.setPrecio(e.getPrecio());
		evp.setCliente(empresaRepository.findById(e.getIdCliente()).get());
		evp = empresaVariedadPrecioRepository.save(evp);
		return new EmpresaVariedadPrecioDTO(evp);

	}

	@Transactional
	public List<EmpresaVariedadPrecioDTO> filtrarVariedadPrecio(Long desdeL, Long hastaL, Integer idLongitud,
			Integer idEmpresaVariedad, Integer idCliente) throws Exception {
		LocalDate desde = (new Timestamp(desdeL)).toLocalDateTime().toLocalDate();
		LocalDate hasta = (new Timestamp(hastaL)).toLocalDateTime().toLocalDate();
		List<EmpresaVariedadPrecio> list = empresaVariedadPrecioRepository.filterPrecioByLongitud(desde, hasta,
				idEmpresaVariedad, idLongitud, -1, idCliente);
		return list.stream().map(e -> new EmpresaVariedadPrecioDTO(e)).toList();

	}

}
