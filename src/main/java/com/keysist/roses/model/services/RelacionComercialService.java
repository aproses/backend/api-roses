package com.keysist.roses.model.services;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.keysist.roses.model.dto.RelacionComercialDTO;
import com.keysist.roses.model.entity.Empresa;
import com.keysist.roses.model.entity.RelacionComercial;
import com.keysist.roses.model.repository.EmpresaRepository;
import com.keysist.roses.model.repository.RelacionComercialRepository;

@Service
public class RelacionComercialService {

	@Autowired
	private EmpresaRepository empresaRepository;
	@Autowired
	private RelacionComercialRepository relacionComercialRepository;

	public List<RelacionComercialDTO> findProveedores(String filtro, Integer idEmpresa) {
		return relacionComercialRepository.findProveedoresByFilter(filtro, idEmpresa).stream()
				.map(u -> new RelacionComercialDTO(u)).toList();

	}

	public List<RelacionComercialDTO> findClientes(String filtro, Integer idEmpresa) {
		return relacionComercialRepository.findClienteByFilter(filtro, idEmpresa).stream()
				.map(u -> new RelacionComercialDTO(u)).toList();
	}

	public boolean delete(Integer idRelacionComercial) {
		relacionComercialRepository.deleteById(idRelacionComercial);
		return true;
	}

	@Transactional
	public RelacionComercialDTO save(RelacionComercialDTO dto) throws Exception {
		try {
			Empresa proveedor = empresaRepository.findById(dto.getProveedorId()).orElse(null);
			Empresa cliente = empresaRepository.findById(dto.getClienteId()).orElse(null);
			Optional<RelacionComercial> opts = relacionComercialRepository.findByProveedorAndCliente(proveedor.getRuc(),
					cliente.getRuc());
			RelacionComercial rc = new RelacionComercial();
			if (!opts.isPresent()) {
				rc.setFechaRegistro(LocalDateTime.now());
				rc.setCliente(empresaRepository.findById(dto.getClienteId()).orElse(null));
				rc.setProveedor(empresaRepository.findById(dto.getProveedorId()).orElse(null));
			} else {
				rc = opts.get();
			}
			relacionComercialRepository.save(dto.buildEntiti(rc));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return dto;
	}
}
