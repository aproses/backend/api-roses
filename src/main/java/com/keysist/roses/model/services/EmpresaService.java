package com.keysist.roses.model.services;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;

import javax.mail.MessagingException;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.keysist.roses.model.dto.EmpresaDTO;
import com.keysist.roses.model.dto.UsuarioDTO;
import com.keysist.roses.model.entity.Catalogo;
import com.keysist.roses.model.entity.Empresa;
import com.keysist.roses.model.entity.EmpresaBloque;
import com.keysist.roses.model.entity.Parametro;
import com.keysist.roses.model.entity.Perfil;
import com.keysist.roses.model.entity.Usuario;
import com.keysist.roses.model.entity.UsuarioPerfil;
import com.keysist.roses.model.enums.CodigosGrupoEnum;
import com.keysist.roses.model.enums.TemplateEmailEnum;
import com.keysist.roses.model.enums.ValorEstadoEnum;
import com.keysist.roses.model.enums.ValorSiNoEnum;
import com.keysist.roses.model.repository.CatalogoRepository;
import com.keysist.roses.model.repository.EmpresaBloqueRepository;
import com.keysist.roses.model.repository.EmpresaRepository;
import com.keysist.roses.model.repository.ParametroRepository;
import com.keysist.roses.model.repository.PerfilRepository;
import com.keysist.roses.model.repository.UsuarioPerfilRepository;
import com.keysist.roses.model.repository.UsuarioRepository;
import com.keysist.roses.util.AES;
import com.keysist.roses.util.TemplateEmail;

@Service
public class EmpresaService {

	@Autowired
	private EmpresaRepository empresaRepository;

	@Autowired
	private ParametroRepository parametroRepository;

	@Autowired
	private EmpresaBloqueRepository empresaBloqueRepository;

	@Autowired
	private UsuarioRepository usuarioRepository;

	@Autowired
	private CatalogoRepository catalogoRepository;

	@Autowired
	private PerfilRepository perfilRepository;

	@Autowired
	private UsuarioPerfilRepository usuarioPerfilRepository;

	@Autowired
	private BCryptPasswordEncoder passwordEncoder;

	@Autowired
	private EmailServiceImpl emailServiceImpl;

	@Transactional
	public UsuarioDTO registrar(EmpresaDTO dto) throws Exception {
		AES aes = new AES();
		String celular = aes.decrypt(dto.getCelular());
		String correo = aes.decrypt(dto.getCorreo());
		String ruc = aes.decrypt(dto.getRuc());
		String password = aes.decrypt(dto.getContrasenia());
		LocalDateTime fechaRegistro = LocalDateTime.now();
		if (!empresaRepository.findByRuc(ruc).isEmpty())
			throw new Exception("Ya existe una empresa con el mismo ruc");
		Empresa em = new Empresa(dto.getNombre(), ruc, correo, celular, ValorSiNoEnum.NO,
				catalogoRepository.findById(dto.getTipoId()).get(), fechaRegistro);
		em.setFechaRegistro(LocalDateTime.now());
		empresaRepository.save(em);
		// Si es tipo postocsecha creamos una empresa proveedor sobrante
		if (em.getTipo().getCodigo().equalsIgnoreCase("TU_POSC")) {
			Empresa emSobrante = new Empresa(dto.getNombre(), ruc.concat("_SOB"), correo, celular, ValorSiNoEnum.NO,
					catalogoRepository.findById(dto.getTipoId()).get(), fechaRegistro);
			empresaRepository.save(emSobrante);
		}
		// Registramos los dias de aplicacion por default
		List<Catalogo> tiposApp = catalogoRepository.findByGrupoRest(CodigosGrupoEnum.TIPOS_APLICACIONES.name());
		List<Catalogo> diasAplicacion = catalogoRepository.findByGrupoRest(CodigosGrupoEnum.DIAS_SEMANA.name());
		String[] diasAplicacionArray = diasAplicacion.stream().map(d -> d.getCodigo()).toList()
				.toArray(new String[diasAplicacion.size()]);
		for (Catalogo catalogo : tiposApp) {
			Catalogo cat = new Catalogo(catalogo.getCodigo().concat(em.getRuc()), catalogo.getCodigo(),
					" dias ".concat(catalogo.getNombre()), diasAplicacionArray, "NO", ValorEstadoEnum.ACTIVO, em);
			catalogoRepository.save(cat);
		}
		// creamos el usuario admin
		Usuario user = new Usuario(em, dto.getNombre(), "Admin", ruc, LocalDateTime.now(),
				passwordEncoder.encode(password), ValorSiNoEnum.SI, correo);

		EmpresaBloque eb = new EmpresaBloque();
		eb.setEmpresa(em);
		eb.setNumero("1");
		empresaBloqueRepository.save(eb);
		usuarioRepository.save(user);
		// Registramos todos los roles al usuario adin
		List<Perfil> perfiles = perfilRepository.getAllPerfilByTipo(em.getTipo().getId());
		for (Perfil perfil : perfiles) {
			UsuarioPerfil up = new UsuarioPerfil(ValorSiNoEnum.SI, perfil, user);
			usuarioPerfilRepository.save(up);
		}
		// correo deregistro
		Parametro links = parametroRepository.findById("LINKS_VIDEOS").get();
		HashMap<String, String> map = new HashMap<>();
		map.put("usuario", String.format(" %1s", em.getNombre()));
		map.put("links", links.getValor());
		emailServiceImpl.sendSimpleMessage(user.getCorreo(), "Bienvenido a Roses",
				TemplateEmail.buildEmail(map, TemplateEmailEnum.REGISTRO));
		return new UsuarioDTO(user.getId());
	};

	@Transactional
	public EmpresaDTO changeEstatus(Integer idEmpresa, ValorSiNoEnum activo) throws MessagingException, IOException {
		Empresa em = empresaRepository.findById(idEmpresa).get();
		em.setActivo(activo);
		empresaRepository.save(em);
		if (ValorSiNoEnum.SI.equals(activo)) {
			Parametro page = parametroRepository.findById("URL_PAGE").get();
			HashMap<String, String> map = new HashMap<>();
			map.put("usuario", String.format(" %1s", em.getNombre()));
			map.put("url_page", page.getValor());
			emailServiceImpl.sendSimpleMessage(em.getCorreo(), "Activación de Usuario",
					TemplateEmail.buildEmail(map, TemplateEmailEnum.ACTIVACION));
		}
		return new EmpresaDTO(em.getId(), em.getActivo());
	};

	@Transactional
	public List<EmpresaDTO> filtrar(String filtro) {
		return empresaRepository.filter(filtro).stream().map(e -> new EmpresaDTO(e)).toList();
	};

	@Transactional
	public List<EmpresaDTO> filtrarDistinct(String filtro, String tipo, Integer idEmpresa) {
		return empresaRepository.filterDistinct(filtro, tipo, idEmpresa).stream().map(e -> new EmpresaDTO(e)).toList();
	};

	public String updateLogo(MultipartFile logo, Integer idEmpresa) throws IOException {
		Empresa emp = empresaRepository.findById(idEmpresa).orElse(null);
		if (logo != null) {
			emp.setLogo(logo.getBytes());
			empresaRepository.save(emp);
		}
		return "OK";
	}

	public byte[] obtenerImagen(Integer idEmpresa) throws IOException {
		Empresa em = empresaRepository.findById(idEmpresa).orElse(null);
		return em != null && em.getLogo() != null ? em.getLogo() : new byte[0];
	}
}
