package com.keysist.roses.model.services;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.temporal.WeekFields;
import java.util.List;
import java.util.Locale;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.keysist.roses.model.dto.PincheDTO;
import com.keysist.roses.model.dto.PincheVariedadDTO;
import com.keysist.roses.model.entity.Pinche;
import com.keysist.roses.model.entity.PincheVariedad;
import com.keysist.roses.model.enums.ValorSiNoEnum;
import com.keysist.roses.model.repository.EmpresaRepository;
import com.keysist.roses.model.repository.EmpresaVariedadRepository;
import com.keysist.roses.model.repository.PincheRepository;
import com.keysist.roses.model.repository.PincheVariedadRepository;

@Service
public class PincheService {

	@Autowired
	private PincheRepository pincheRepository;
	@Autowired
	private PincheVariedadRepository pincheVariedadRepository;

	@Autowired
	private EmpresaVariedadRepository empresaVariedadRepository;

	@Autowired
	private EmpresaRepository empresaRepository;

	@Autowired
	Environment env;

	@Transactional
	public PincheDTO find(Integer idEmpresa, LocalDate fechaPinche) throws Exception {
		WeekFields weekFields = WeekFields.of(Locale.getDefault());
		int weekNumber = fechaPinche.get(weekFields.weekOfWeekBasedYear());
		List<Pinche> pinche = pincheRepository.findBySemanaAndEmpresa(idEmpresa, weekNumber, fechaPinche.getYear());
		if (!pinche.isEmpty()) {
			return buildToDto(pinche.get(0));
		} else {
			PincheDTO dto = new PincheDTO();
			dto.setIdEmpresa(idEmpresa);
			dto.setFechaPinche(java.sql.Date.valueOf(fechaPinche).getTime());
			dto.setPincheVariedad(empresaVariedadRepository.findByUsuarioAndDisponible(idEmpresa, ValorSiNoEnum.SI)
					.stream().map(uv -> new PincheVariedadDTO(uv)).toList());
			return dto;
		}

	}

	@Transactional
	public PincheDTO registrar(PincheDTO pincheDTO) throws Exception {
		Pinche p = new Pinche();
		LocalDate fechaPiche = (new Timestamp(pincheDTO.getFechaPinche())).toLocalDateTime().toLocalDate();
		WeekFields weekFields = WeekFields.of(Locale.getDefault());
		int weekNumber = fechaPiche.get(weekFields.weekOfWeekBasedYear());

		List<Pinche> pinche = pincheRepository.findBySemanaAndEmpresa(pincheDTO.getIdEmpresa(), weekNumber,
				fechaPiche.getYear());
		if (!pinche.isEmpty())
			p = pinche.get(0);
		p.setFechaPinche(fechaPiche);
		p.setAnio(fechaPiche.getYear());
		p.setEmpresa(empresaRepository.findById(pincheDTO.getIdEmpresa()).get());

		p.setSemana(weekNumber);
		final Pinche ps = pincheRepository.save(p);
		pincheDTO.getPincheVariedad().stream().forEach(pdt -> {
			PincheVariedad pc = new PincheVariedad();
			if (pdt.getId() == null) {
				pc.setEmpresaVariedad(empresaVariedadRepository.findById(pdt.getEmpresaVariedadId()).get());
			} else {
				pc = pincheVariedadRepository.findById(pdt.getId()).get();
			}
			pc.setPinche(ps);
			pc.setCantidad(pdt.getCantidad());
			pincheVariedadRepository.save(pc);
		});

		return buildToDto(ps);
	}

	@Transactional
	public List<PincheDTO> consultar(Integer idEmpresa, LocalDate desde, LocalDate hasta) throws Exception {
		List<Pinche> pinches = pincheRepository.consultar(idEmpresa, desde, hasta);
		return pinches.stream().map(p -> buildToDto(p)).toList();

	}

	private PincheDTO buildToDto(Pinche pinche) {
		PincheDTO dto = new PincheDTO();
		dto.setId(pinche.getId());
		dto.setSemana(pinche.getSemana());
		dto.setIdEmpresa(pinche.getEmpresa().getId());
		dto.setFechaPinche((java.sql.Date.valueOf(pinche.getFechaPinche()).getTime()));
		List<PincheVariedad> list = pincheVariedadRepository.findByPincheOrderById(pinche);
		if (!list.isEmpty())
			dto.setPincheVariedad(
					list.stream().map(p -> new PincheVariedadDTO(p.getId(), p.getEmpresaVariedad().getId(),
							p.getEmpresaVariedad().getVariedad().getNombre(), p.getCantidad())).toList());
		else {
			dto.setPincheVariedad(
					empresaVariedadRepository.findByUsuarioAndDisponible(pinche.getEmpresa().getId(), ValorSiNoEnum.SI)
							.stream().map(uv -> new PincheVariedadDTO(uv)).toList());
		}
		BigDecimal total = dto.getPincheVariedad().stream().map(dt -> BigDecimal.valueOf(dt.getCantidad()))
				.reduce(BigDecimal.ZERO, BigDecimal::add);
		dto.setTotal(total != null ? total.intValue() : 0);
		return dto;
	}

}
