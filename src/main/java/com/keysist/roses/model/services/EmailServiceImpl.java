package com.keysist.roses.model.services;

import java.io.IOException;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

@Component
public class EmailServiceImpl {

	@Autowired
	private JavaMailSender emailSender;

	public void sendSimpleMessage(String to, String subject, String text) throws MessagingException, IOException {

		MimeMessage msg = emailSender.createMimeMessage();

		
        
		MimeMessageHelper helper = new MimeMessageHelper(msg, true);
		helper.setFrom(new InternetAddress("approses2023@gmail.com","App Roses"));
		helper.setTo(new InternetAddress(to,"App Roses 2"));
		helper.setSubject(subject);
		helper.setText(text, true);

		// hard coded a file path
		// FileSystemResource file = new FileSystemResource(new
		// File("path/android.png"));
		// helper.addAttachment("my_photo.png", new ClassPathResource("android.png"));
		emailSender.send(msg);
	}
}
