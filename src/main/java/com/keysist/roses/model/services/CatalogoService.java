package com.keysist.roses.model.services;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;
import javax.transaction.TransactionalException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.keysist.roses.model.entity.Catalogo;
import com.keysist.roses.model.enums.ValorEstadoEnum;
import com.keysist.roses.model.repository.CatalogoRepository;

@Service
public class CatalogoService {

	@Autowired
	private CatalogoRepository catalogoRepository;

	@Autowired
	private UsuarioService usuarioServiceImpl;

	@Autowired
	private CostoLongitudService costoLongitudServiceImpl;

	@Transactional
	public Catalogo guardar(Catalogo c) throws Exception {
		boolean crearProveedor = false;
		boolean crearCostoLongitud = false;
		List<Catalogo> lista = new ArrayList<>();
		if (c.getId() != null) {
			lista = catalogoRepository.findByCodigoAndId(c.getCodigo(), c.getId());
		} else {
			lista = catalogoRepository.findByCodigo(c.getCodigo());
		}
		if (lista.isEmpty()) {
			catalogoRepository.save(c);
			crearProveedor = true;
			crearCostoLongitud = true;
		} else {
			Catalogo catDb = lista.get(0);
			if (ValorEstadoEnum.ACTIVO.equals(catDb.getEstado())) {
				throw new TransactionalException("Ya existe un catálogo con el mismo código", null);
			} else {
				Catalogo nuevo = lista.get(0);
				nuevo.setNombre(c.getNombre());
				nuevo.setEstado(ValorEstadoEnum.ACTIVO);
				nuevo.setTexto1(c.getTexto1());
				nuevo.setTexto2(c.getTexto2());
				nuevo.setValor(c.getValor());
				catalogoRepository.save(nuevo);
				c = nuevo;
				crearProveedor = true;
			}
		}

		if (crearCostoLongitud) {
			switch (c.getCodigoGrupo()) {
			case "SUCURSALES":
				usuarioServiceImpl.crearProveedorSobrante(c, "");
				break;

			default:
				break;
			}
		}
		if (crearProveedor) {
			switch (c.getCodigoGrupo()) {
			case "LONGITUD_TALLO":
				costoLongitudServiceImpl.loadprice(c);
				break;

			default:
				break;
			}
		}
		return c;
	}

	public boolean eliminar(Integer idProducto) {
		catalogoRepository.deleteById(idProducto);
		return true;
	}
	public List<Catalogo> findByGroup(String grupo){
		return catalogoRepository.findByGrupoRest(grupo);
	}

}
