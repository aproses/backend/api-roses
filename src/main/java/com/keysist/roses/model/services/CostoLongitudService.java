package com.keysist.roses.model.services;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.keysist.roses.model.dto.CostoLongitudDTO;
import com.keysist.roses.model.entity.Catalogo;
import com.keysist.roses.model.entity.CostoLongitud;
import com.keysist.roses.model.entity.CostoVariedad;
import com.keysist.roses.model.repository.CostoLongitudRepository;
import com.keysist.roses.model.repository.CostoVariedadRepository;

@Service
public class CostoLongitudService {

	@Autowired
	private CostoLongitudRepository costoLongitudRepository;
	@Autowired
	private CostoVariedadRepository costoVariedadRepository;

	public List<CostoLongitud> save(List<CostoLongitudDTO> listCostL) {
		if (!listCostL.isEmpty()) {
			for (CostoLongitudDTO clDto : listCostL) {
				CostoLongitud cl = costoLongitudRepository.findById(clDto.getId()).orElse(null);
				if (cl != null) {
					cl.setPrecio(clDto.getPrecio());
					costoLongitudRepository.save(cl);
				}
			}
		}
		return null;
	};

	public void loadprice(Catalogo longitud) {
		List<CostoVariedad> cv = costoVariedadRepository.findAll();
		if (!cv.isEmpty()) {
			for (CostoVariedad c : cv) {
				CostoVariedad costoVariedad = costoVariedadRepository.findById(c.getId()).orElse(null);
				if (costoVariedad != null) {
					CostoLongitud cl = new CostoLongitud();
					cl.setCostoVariedad(costoVariedad);
					cl.setLongitud(longitud);
					cl.setPrecio(new BigDecimal(0));
					costoLongitudRepository.save(cl);
				}
			}
		}
	}

}
