package com.keysist.roses.model.services;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import com.keysist.roses.model.dto.EmpresaProductoDTO;
import com.keysist.roses.model.dto.ProductoComposicionDTO;
import com.keysist.roses.model.dto.ProductoDTO;
import com.keysist.roses.model.entity.Empresa;
import com.keysist.roses.model.entity.EmpresaProducto;
import com.keysist.roses.model.entity.Producto;
import com.keysist.roses.model.enums.ValorSiNoEnum;
import com.keysist.roses.model.repository.CatalogoRepository;
import com.keysist.roses.model.repository.EmpresaProductoRepository;
import com.keysist.roses.model.repository.EmpresaRepository;
import com.keysist.roses.model.repository.ProductoComposicionRepository;
import com.keysist.roses.model.repository.ProductoRepository;

@Service
public class EmpresaProductoService {

	@Autowired
	private EmpresaRepository empresaRepository;

	@Autowired
	private ProductoRepository productoRepository;

	@Autowired
	private ProductoService productoService;

	@Autowired
	private EmpresaProductoRepository empresaProductoRepository;

	@Autowired
	private ProductoComposicionRepository composicionRepository;

	@Autowired
	private AplicacionService aplicacionService;

	@Autowired
	private CatalogoRepository catalogoRepository;

	@Autowired
	SimpMessagingTemplate template;

	@Transactional
	public EmpresaProductoDTO guardar(EmpresaProductoDTO c) throws Exception {
		List<EmpresaProducto> list = empresaProductoRepository.findByEmpresaAndProducto(c.getEmpresaId(),
				c.getProductoId());
		EmpresaProducto p = new EmpresaProducto();
		if (!list.isEmpty()) {
			p = list.get(0);
		} else {
			p.setEmpresa(empresaRepository.findById(c.getEmpresaId()).get());
		}
		Producto pr = new Producto();
		if (c.getProductoId() != -1) {
			pr = productoRepository.findById(c.getProductoId()).orElse(null);
		} else {
			pr = new Producto(c.getNombre(), c.getEsNitrato(),
					catalogoRepository.findById(c.getUnidadMedidaId()).orElse(null), c.getDosis());
			pr = productoRepository.save(pr);
		}
		p.setProducto(pr);
		p.setActivo(c.getActivo());
		p.setDisponible(c.getDisponible());
		p.setDisponibleMinimo(c.getDisponibleMinimo());
		p.setDosis(c.getDosis());
		p.setPrecio(c.getPrecio());
		p.setCantidadPorUnidad(c.getCantidadPorUnidad());
		empresaProductoRepository.save(p);
		aplicacionService.actualizarStock(p.getEmpresa().getId(), p);
		return new EmpresaProductoDTO(p);
	}

	@Transactional
	public EmpresaProductoDTO registrarCompra(Integer idProducto, BigInteger cantidad, BigDecimal precio,
			BigDecimal cantidadPorUnidad) throws Exception {
		EmpresaProducto p = empresaProductoRepository.findById(idProducto).get();
		p.setDisponible(p.getDisponible().add(BigDecimal.valueOf(cantidad.doubleValue())));
		p.setPrecio(precio);
		p.setCantidadPorUnidad(cantidadPorUnidad);
		empresaProductoRepository.save(p);
		aplicacionService.actualizarStock(p.getEmpresa().getId(), p);
		return new EmpresaProductoDTO(p);
	}

	public List<EmpresaProductoDTO> consultarDisponibilidad(Integer idEmpresa, String enfermedad) throws Exception {
		return empresaProductoRepository.findByEmpresa(idEmpresa).stream()
				.map(pu -> new EmpresaProductoDTO(pu.getDisponible(), pu.getDosis(), pu.getProducto().getNombre(),
						pu.getId(), pu.getProducto().getUnidadMedida().getNombre(), pu.getPrecio(),
						pu.getCantidadPorUnidad()))
				.toList();
	}

	public List<EmpresaProductoDTO> consultarDisponibilidadAndTipo(Integer idEmpresa, Integer idTipo) throws Exception {
		return empresaProductoRepository.findByEmpresaAndTipo(idEmpresa, idTipo).stream().map(pu -> {
			EmpresaProductoDTO dto = new EmpresaProductoDTO(pu.getDisponible(), pu.getDosis(),
					pu.getProducto().getNombre(), pu.getId(), pu.getProducto().getUnidadMedida().getNombre(),
					pu.getPrecio(), pu.getCantidadPorUnidad());
			dto.setComposicion(composicionRepository.findByProducto(pu.getProducto()).stream()
					.map(pc -> new ProductoComposicionDTO(pc)).toList());
			return dto;
		}).toList();
	}

	public List<EmpresaProductoDTO> consultarPorTerminar(Integer idEmpresa) {
		return empresaProductoRepository.findByEmpresaMinimo(idEmpresa, ValorSiNoEnum.SI).stream()
				.map(pu -> new EmpresaProductoDTO(pu.getDisponible(), pu.getDosis(), pu.getProducto().getNombre(),
						pu.getId(), pu.getProducto().getUnidadMedida().getNombre(), pu.getPrecio(),
						pu.getCantidadPorUnidad()))
				.toList();
	}

	public List<EmpresaProductoDTO> consultar(Integer idEmpresa, String filtro) throws Exception {
		return empresaProductoRepository.findByEmpresaFilter(idEmpresa, filtro, ValorSiNoEnum.SI).stream()
				.map(pu -> new EmpresaProductoDTO(pu)).toList();
	}

	@Transactional
	public EmpresaProductoDTO registrarUso(Integer idProducto, BigDecimal cantidadGramos) throws Exception {
		EmpresaProducto p = empresaProductoRepository.findById(idProducto).get();
		p.setDisponible(p.getDisponible().subtract(cantidadGramos));
		return new EmpresaProductoDTO(p);
	}

	public EmpresaProductoDTO consultar(Integer id) throws Exception {
		Optional<EmpresaProducto> p = empresaProductoRepository.findById(id);
		EmpresaProductoDTO dto = new EmpresaProductoDTO();
		if (p.isPresent()) {
			dto = new EmpresaProductoDTO(p.get());
			ProductoDTO proDto = productoService.consultar(p.get().getProducto().getId());
			dto.setPlagas(proDto.getPlagas());
			dto.setComposicion(proDto.getComposicion());
			dto.setTipos(proDto.getTipos());
		}
		return dto;
	}

	public void productosPorTerminar() {
		try {
			List<Empresa> empresas = empresaRepository.findActive();
			empresas.forEach(em -> {
				Integer idEmpresa = em.getId();
				List<EmpresaProductoDTO> dtos = consultarPorTerminar(idEmpresa);
				if (!dtos.isEmpty()) {
					template.convertAndSend("/message/prodter/" + idEmpresa, dtos);
				}
			});
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
}
