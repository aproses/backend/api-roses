package com.keysist.roses.model.services;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.keysist.roses.model.dto.ProductoComposicionDTO;
import com.keysist.roses.model.dto.ProductoDTO;
import com.keysist.roses.model.entity.Catalogo;
import com.keysist.roses.model.entity.Producto;
import com.keysist.roses.model.entity.ProductoComposicion;
import com.keysist.roses.model.entity.ProductoPlaga;
import com.keysist.roses.model.entity.ProductoTipo;
import com.keysist.roses.model.enums.ValorSiNoEnum;
import com.keysist.roses.model.repository.CatalogoRepository;
import com.keysist.roses.model.repository.ProductoComposicionRepository;
import com.keysist.roses.model.repository.ProductoPlagaRepository;
import com.keysist.roses.model.repository.ProductoRepository;
import com.keysist.roses.model.repository.ProductoTipoRepository;

@Service
public class ProductoService {

	@Autowired
	private CatalogoRepository catalogoRepository;

	@Autowired
	private ProductoRepository productoRepository;

	@Autowired
	private ProductoComposicionRepository productoComposicionRepository;

	@Autowired
	private ProductoPlagaRepository productoPlagaRepository;

	@Autowired
	private ProductoTipoRepository productoTipoRepository;

	@Transactional
	public ProductoDTO guardar(ProductoDTO c) throws Exception {

		Producto p = new Producto();
		if (c.getId() != null) {
			p = productoRepository.findById(c.getId()).get();
			List<Catalogo> plagas = c.getPlagas().stream().map(pp -> catalogoRepository.findById(pp.getId()).get())
					.toList();
			List<ProductoPlaga> notIn = productoPlagaRepository.findByPlagaNotInAndProducto(plagas, p);
			productoPlagaRepository.deleteAll(notIn);
			if (!c.getComposicion().isEmpty()) {
				List<Catalogo> elementos = c.getComposicion().stream()
						.map(pp -> catalogoRepository.findById(pp.getElementoId()).get()).toList();
				List<ProductoComposicion> notInElement = productoComposicionRepository
						.findByElementoNotInAndProducto(elementos, p);
				productoComposicionRepository.deleteAll(notInElement);
			} else {
				productoComposicionRepository.deleteAll(productoComposicionRepository.findByProducto(p));
			}

		}
		p.setEsNitrato(c.getEsNitrato());
		p.setCaracteristicas(c.getCaracteristicas());
		p.setDosis(c.getDosis());
		p.setNombre(c.getNombre());
		p.setUnidadMedida(catalogoRepository.findById(c.getUnidadMedidaId()).get());
		productoRepository.save(p);
		if (c.getComposicion() != null)
			for (ProductoComposicionDTO pc : c.getComposicion()) {
				ProductoComposicion pcd = new ProductoComposicion();
				if (pc.getId() != null) {
					Optional<ProductoComposicion> popt = productoComposicionRepository.findById(pc.getId());
					if (popt.isPresent())
						pcd = popt.get();
				}
				pcd.setDosis(pc.getDosis());
				pcd.setElemento(catalogoRepository.findById(pc.getElementoId()).get());
				pcd.setPorcentaje(pc.getPorcentaje());
				pcd.setProducto(p);
				productoComposicionRepository.save(pcd);
			}
		for (Catalogo pc : c.getPlagas()) {
			ProductoPlaga pcd = new ProductoPlaga();
			Catalogo plaga = catalogoRepository.findById(pc.getId()).get();
			Optional<ProductoPlaga> aux = productoPlagaRepository.findByProductoAndPlaga(p, plaga);
			if (!aux.isPresent()) {
				pcd = new ProductoPlaga();
				pcd.setPlaga(plaga);
				pcd.setProducto(p);
				productoPlagaRepository.save(pcd);
			}
		}
		for (ProductoTipo pt : productoTipoRepository.findByProducto(p)) {
			pt.setActivo(ValorSiNoEnum.NO);
			productoTipoRepository.save(pt);
		}
		for (Catalogo pc : c.getTipos()) {
			ProductoTipo pcd = new ProductoTipo();
			Catalogo tipo = catalogoRepository.findById(pc.getId()).get();

			Optional<ProductoTipo> aux = productoTipoRepository.findByProductoAndTipo(p, tipo);
			if (!aux.isPresent()) {
				pcd = new ProductoTipo();
				pcd.setTipo(tipo);
				pcd.setProducto(p);

			} else {
				pcd = aux.get();
				pcd.setActivo(ValorSiNoEnum.SI);
			}
			productoTipoRepository.save(pcd);
		}

		return new ProductoDTO(p);
	}

	public List<ProductoDTO> consultar(String filtro) throws Exception {
		return productoRepository.findByFilter(filtro).stream().map(pu -> {
			ProductoDTO dto = new ProductoDTO(pu);
			dto.setPlagas(productoPlagaRepository.findByProducto(pu).stream().map(pp -> pp.getPlaga()).toList());
			dto.setTipos(productoTipoRepository.findByProductoAndActivo(pu, ValorSiNoEnum.SI).stream()
					.map(pp -> pp.getTipo()).toList());
			return dto;
		}).toList();
	}

	public ProductoDTO consultar(Integer id) throws Exception {
		Optional<Producto> p = productoRepository.findById(id);
		ProductoDTO dto = new ProductoDTO();
		if (p.isPresent()) {
			dto = new ProductoDTO(p.get());
			dto.setComposicion(productoComposicionRepository.findByProducto(p.get()).stream()
					.map(pc -> new ProductoComposicionDTO(pc)).toList());
			dto.setPlagas(productoPlagaRepository.findByProducto(p.get()).stream().map(pp -> pp.getPlaga()).toList());
			dto.setTipos(productoTipoRepository.findByProductoAndActivo(p.get(), ValorSiNoEnum.SI).stream()
					.map(pp -> pp.getTipo()).toList());
		}
		return dto;
	}

	@Transactional
	public boolean eliminar(Integer idProducto) {
		Producto p = productoRepository.findById(idProducto).get();
		productoComposicionRepository.deleteAllByProductoIs(p);
		productoPlagaRepository.deleteAllByProductoIs(p);
		productoTipoRepository.deleteAllByProductoIs(p);
		productoRepository.deleteById(idProducto);
		return true;
	}
}
