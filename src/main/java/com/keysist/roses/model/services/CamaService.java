package com.keysist.roses.model.services;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.keysist.roses.model.dto.CamaDTO;
import com.keysist.roses.model.dto.CamaRegistroDTO;
import com.keysist.roses.model.entity.EmpresaBloque;
import com.keysist.roses.model.entity.Cama;
import com.keysist.roses.model.enums.ValorSiNoEnum;
import com.keysist.roses.model.repository.EmpresaBloqueRepository;
import com.keysist.roses.model.repository.CamaRepository;
import com.keysist.roses.model.repository.EmpresaVariedadRepository;

@Service
public class CamaService {

	@Autowired
	private EmpresaBloqueRepository bloqueRepository;

	@Autowired
	private CamaRepository camaRepository;

	@Autowired
	private EmpresaVariedadRepository empresaVariedadRepository;

	@Autowired
	Environment env;

	@Transactional
	public List<CamaDTO> crear(CamaRegistroDTO dto) throws Exception {
		EmpresaBloque b = bloqueRepository.findById(dto.getBloqueId()).get();
		for (int i = 0; i < dto.getCantidadCamas(); i++) {
			Cama c = new Cama();
			c.setBloque(b);
			c.setNumero(String.valueOf(i));
			c.setActivo(ValorSiNoEnum.SI);
			c.setEmpresaVariedad(empresaVariedadRepository.findById(dto.getEmpresaVariedadId()).get());
			camaRepository.save(c);
		}

		return consultar(dto.getBloqueId());
	}
	@Transactional
	public List<CamaDTO> inactivar(Integer idCama) throws Exception {
		Cama b = camaRepository.findById(idCama).get();
		b.setActivo(ValorSiNoEnum.NO);
		camaRepository.save(b);
		return consultar(b.getBloque().getId());
	}

	public List<CamaDTO> consultar(Integer idBloque) throws Exception {
		return camaRepository.findByBloque(bloqueRepository.findById(idBloque).get()).stream()
				.filter(b -> ValorSiNoEnum.SI.equals(b.getActivo())).map(b -> new CamaDTO(b.getId(), b.getNumero(),
						b.getEmpresaVariedad().getVariedad().getNombre(), b.getEmpresaVariedad().getId()))
				.toList();
	}

}
