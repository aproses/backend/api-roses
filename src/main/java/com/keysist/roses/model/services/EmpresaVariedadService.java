package com.keysist.roses.model.services;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.keysist.roses.model.dto.EmpresaVariedadDTO;
import com.keysist.roses.model.entity.Empresa;
import com.keysist.roses.model.entity.EmpresaVariedad;
import com.keysist.roses.model.entity.Variedad;
import com.keysist.roses.model.enums.ValorSiNoEnum;
import com.keysist.roses.model.repository.EmpresaRepository;
import com.keysist.roses.model.repository.EmpresaVariedadRepository;
import com.keysist.roses.model.repository.VariedadRepository;

@Service
public class EmpresaVariedadService {

	@Autowired
	private EmpresaRepository empresaRepository;
	@Autowired
	private EmpresaVariedadRepository empresaVariedadRepository;

	@Autowired
	private VariedadRepository variedadRepository;

	@Transactional
	public EmpresaVariedadDTO save(EmpresaVariedadDTO dto) {
		boolean esEdision = dto.getId() != null;
		Variedad variedad = null;
		if (dto.getIdVariedad() != -1) {
			variedad = variedadRepository.findById(dto.getIdVariedad()).orElse(null);
		} else {
			variedad = new Variedad(dto.getNombreVariedad(), dto.getNombreVariedad().toUpperCase().replaceAll(" ", "_"),
					ValorSiNoEnum.SI);
			variedad = variedadRepository.save(variedad);
		}
		Empresa empresa = empresaRepository.findById(dto.getIdEmpresa()).orElse(null);
		List<EmpresaVariedad> list = empresaVariedadRepository.findByEmpresaAndVariedad(empresa, variedad);
		EmpresaVariedad ev = new EmpresaVariedad();
		if (list.isEmpty()) {
			ev.setEmpresa(empresa);
		} else {
			ev = list.get(0);

		}
		ev.setDisponible(dto.getDisponible());
		ev.setVariedad(variedad);
		ev.setTalloMalla(dto.getTalloMalla());
		ev.setDisponible(dto.getDisponible());
		ev.setDiasCiclo(dto.getDiasCiclo());
		ev.setProduccionMes(dto.getProduccionMes());
		ev.setTalloMallaMax(dto.getTalloMallaMax());
		ev.setProduccionPerdida(dto.getProduccionPerdida());
		ev.setCamasEstandart(dto.getCamasEstandart());
		ev.setBloques(dto.getBloques());
		empresaVariedadRepository.save(ev);
		if (!esEdision) {
			Empresa usuarioSobratante = empresaRepository.findByRucSobrante(empresa.getRuc());
			if (usuarioSobratante != null) {
				List<EmpresaVariedad> listSobrante = empresaVariedadRepository
						.findByEmpresaAndVariedad(usuarioSobratante, variedad);
				if (listSobrante.isEmpty()) {
					EmpresaVariedad uvs = new EmpresaVariedad();
					uvs.setTalloMalla(dto.getTalloMalla());
					uvs.setEmpresa(usuarioSobratante);
					uvs.setVariedad(variedad);
					uvs.setDisponible(ValorSiNoEnum.SI);
					empresaVariedadRepository.save(uvs);
				}
			}

		}

		return dto;
	};
}
