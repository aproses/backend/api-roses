package com.keysist.roses.model.services;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.keysist.roses.model.dto.EmpresaBloqueDTO;
import com.keysist.roses.model.entity.Empresa;
import com.keysist.roses.model.entity.EmpresaBloque;
import com.keysist.roses.model.repository.EmpresaBloqueRepository;
import com.keysist.roses.model.repository.EmpresaRepository;

@Service
public class EmpresaBloqueService {

	@Autowired
	private EmpresaRepository empresaRepository;
	@Autowired
	private EmpresaBloqueRepository empresaBloqueRepository;

	@Transactional
	public EmpresaBloqueDTO save(EmpresaBloqueDTO dto) {
		Empresa empresa = empresaRepository.findById(dto.getIdEmpresa()).orElse(null);
		List<EmpresaBloque> list = empresaBloqueRepository.findByEmpresaAndNumero(dto.getIdEmpresa(), dto.getNumero());
		EmpresaBloque ev = new EmpresaBloque();
		ev.setId(dto.getId());
		if (list.isEmpty()) {
			ev.setEmpresa(empresa);
		} else {
			ev = list.get(0);
		}
		ev.setNumero(dto.getNumero());
		empresaBloqueRepository.save(ev);
		return new EmpresaBloqueDTO(ev);
	};

	@Transactional
	public List<EmpresaBloqueDTO> consultar(Integer idEmpresa) throws Exception {
		return empresaBloqueRepository.findByEmpresa(idEmpresa).stream().map(b -> new EmpresaBloqueDTO(b)).toList();
	}

	public EmpresaBloqueDTO findById(Integer id) {
		return new EmpresaBloqueDTO(empresaBloqueRepository.findById(id).get());
	}

	public Integer delete(Integer id) {
		empresaBloqueRepository.deleteById(id);
		return id;
	}
}
