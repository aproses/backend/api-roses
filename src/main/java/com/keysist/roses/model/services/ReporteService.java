package com.keysist.roses.model.services;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.keysist.roses.model.dto.ReporteIngresoDTO;
import com.keysist.roses.model.dto.ReporteIngresoVariedadDTO;
import com.keysist.roses.model.enums.EstadoEntregaEnum;
import com.keysist.roses.model.repository.EntregaRepository;
import com.keysist.roses.model.repository.EntregaVariedadRepository;

@Service
public class ReporteService {

	@Autowired
	private EntregaRepository entregaRepository;
	@Autowired
	private EntregaVariedadRepository entregaVariedadRepository;

	public List<ReporteIngresoDTO> proveedorVariedad(Date fechaDesde, Date fechaHasta) throws Exception {
		// TODO Auto-generated method stub
		return entregaRepository.getProveedores(fechaDesde, fechaHasta, EstadoEntregaEnum.ENVIADO,
				EstadoEntregaEnum.PROCESADO, EstadoEntregaEnum.PAGADO).stream().map(u -> {
					ReporteIngresoDTO rp = new ReporteIngresoDTO(u.getProveedorCodigo(), u.getProveedor().getNombre(),
							0);
					List<Object[]> detalle = entregaVariedadRepository.obtenerTotalVariedad(u.getId(), fechaDesde,
							fechaHasta, EstadoEntregaEnum.ENVIADO, EstadoEntregaEnum.PROCESADO,
							EstadoEntregaEnum.PAGADO);
					rp.setDetalle(detalle.stream().map(d -> new ReporteIngresoVariedadDTO(d[0], d[1]))
							.collect(Collectors.toList()));
					BigDecimal total = detalle.parallelStream().map(
							d -> d[1] != null ? BigDecimal.valueOf(Double.valueOf(d[1].toString())) : BigDecimal.ZERO)
							.reduce(BigDecimal.ZERO, BigDecimal::add);
					rp.setTotal(total.intValue());
					return rp;
				}).collect(Collectors.toList());
	}

}
