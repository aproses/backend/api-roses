package com.keysist.roses.model.services;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import javax.mail.MessagingException;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.keysist.roses.model.dto.PerfilDTO;
import com.keysist.roses.model.dto.UsuarioDTO;
import com.keysist.roses.model.entity.Catalogo;
import com.keysist.roses.model.entity.Empresa;
import com.keysist.roses.model.entity.EmpresaBloque;
import com.keysist.roses.model.entity.Perfil;
import com.keysist.roses.model.entity.Usuario;
import com.keysist.roses.model.entity.UsuarioPerfil;
import com.keysist.roses.model.enums.CodigoCatalogoEnum;
import com.keysist.roses.model.enums.CodigosGrupoEnum;
import com.keysist.roses.model.enums.TemplateEmailEnum;
import com.keysist.roses.model.enums.ValorEstadoEnum;
import com.keysist.roses.model.enums.ValorSiNoEnum;
import com.keysist.roses.model.repository.CatalogoRepository;
import com.keysist.roses.model.repository.EmpresaBloqueRepository;
import com.keysist.roses.model.repository.EmpresaRepository;
import com.keysist.roses.model.repository.PerfilRepository;
import com.keysist.roses.model.repository.UsuarioPerfilRepository;
import com.keysist.roses.model.repository.UsuarioRepository;
import com.keysist.roses.util.AES;
import com.keysist.roses.util.TemplateEmail;

@Service
public class UsuarioService {

	@Autowired
	private UsuarioRepository usuarioRepository;

	@Autowired
	private EmpresaRepository empresaRepository;

	@Autowired
	private EmpresaBloqueRepository empresaBloqueRepository;
	@Autowired
	private PerfilRepository perfilRepository;
	@Autowired
	private CatalogoRepository catalogoRepository;
	@Autowired
	private UsuarioPerfilRepository usuarioPerfilRepository;
	@Autowired
	private Environment env;
	@Autowired
	private BCryptPasswordEncoder passwordEncoder;

	@Autowired
	private EmailServiceImpl emailServiceImpl;

	public UsuarioDTO findDtoByCedula(String username) {
		Usuario user = usuarioRepository.findByIdentificacion(username);
		UsuarioDTO usuarioDTO = null;
		if (user != null) {
			String anonymousUser = env.getProperty("config.security.oauth.anonymous");
			usuarioDTO = new UsuarioDTO(user);
			List<PerfilDTO> perfiles = perfilRepository.findByUserAndActivo(user.getId(), ValorSiNoEnum.SI).stream()
					.map(p -> new PerfilDTO(p)).toList();
			usuarioDTO.setListRolesId(new ArrayList<>());
			final List<Integer> rolds = new ArrayList<>();
			usuarioDTO.setPerfiles(new ArrayList<>());
			if (user.getEmpresa() != null) {
				if (ValorSiNoEnum.SI.equals(user.getEmpresa().getActivo())) {
					usuarioDTO.setPerfiles(perfiles);
				}
			} else {
				usuarioDTO.setPerfiles(perfiles);
			}

			perfiles.forEach(c -> {
				rolds.add(c.getId());
			});
			usuarioDTO.setListRolesId(rolds);
			usuarioDTO.setIsAnonymous(anonymousUser.equalsIgnoreCase(username) ? "S" : "N");
		}
		return usuarioDTO;
	}

	public Usuario findUsuarioByCedula(String username) {
		return usuarioRepository.findByIdentificacion(username);
	}

	public UsuarioDTO findUsuarioById(Integer idUsuario) {
		Usuario u = usuarioRepository.findByIdUsuario(idUsuario);
		List<Catalogo> tiposApp = catalogoRepository.findByGrupoRest(CodigosGrupoEnum.TIPOS_APLICACIONES.name());
		UsuarioDTO dto = new UsuarioDTO(u);
		List<Catalogo> list = tiposApp.stream().map(ta -> {
			String codigo=ta.getCodigo().concat(u.getEmpresa().getRuc());
			List<Catalogo> listA = catalogoRepository.findByCodigoEmpresa(codigo);
			return listA.isEmpty() ? null : listA.get(0);
		}).filter(c -> c != null).toList();
		dto.setDiasAplicaciones(list);
		return dto;
	}

	public List<Usuario> findUsuarioByTipo(String codigo) {
		return usuarioRepository.findByTipo(codigo);
	}

	public Usuario update(Usuario usuario) {
		return usuarioRepository.save(usuario);
	}

	public List<UsuarioDTO> findByFilter(String filtro, Integer idEmpresa) {
		return usuarioRepository.findByFilter(filtro, idEmpresa).stream().map(u -> {
			List<UsuarioPerfil> ups = u.getUsuarioPerfiles().stream()
					.filter(r -> r.getActivo().equals(ValorSiNoEnum.SI))
					.sorted((x, y) -> x.getPerfil().getNombre().compareTo(y.getPerfil().getNombre())).toList();
			List<String> roles = ups.stream()
					.map(up -> up.getPerfil().getNombre().substring(5, up.getPerfil().getNombre().length())).toList();
			List<Integer> rolesId = ups.stream().map(up -> up.getPerfil().getId()).toList();

			UsuarioDTO dto = new UsuarioDTO(u);
			dto.setRoles(roles);
			dto.setListRolesId(rolesId);
			return dto;
		}).toList();
	}

	public List<UsuarioDTO> findByPerfil(String perfil, Integer idEmpresa) {
		return usuarioRepository.findByPerfil(perfil, idEmpresa).stream().map(u -> {
			UsuarioDTO dto = new UsuarioDTO(u);
			return dto;
		}).toList();
	}

	public boolean delete(Integer idUsuario) {
		usuarioRepository.deleteById(idUsuario);
		return true;
	};

	public String recoverPass(String cedua) throws MessagingException, IOException {
		AES aes = new AES();
		String celular = aes.decrypt(cedua);
		Usuario user = usuarioRepository.findByIdentificacion(celular);
		if (user != null) {
			String newPass = generateCommonLangPassword();
			user.setContrasenia(passwordEncoder.encode(newPass));
			user.setActivo(ValorSiNoEnum.SI);
			user.setIntentos(0);
			usuarioRepository.save(user);
			HashMap<String, String> map = new HashMap<>();
			map.put("usuario", String.format(" %1s %2s", user.getNombres(), user.getApellidos()));
			map.put("clave", newPass);
			emailServiceImpl.sendSimpleMessage(user.getCorreo(), "Recuperación de Contraseña",
					TemplateEmail.buildEmail(map, TemplateEmailEnum.RECUPERACION_CLAVE));
			return String.format("Se envío su nueva contraseña al correo %1s", user.getCorreo());
		}
		return "";
	}

	public String changePass(UsuarioDTO dto) throws MessagingException, IOException {
		AES aes = new AES();
		String pass = aes.decrypt(dto.getContrasenia());
		Usuario user = usuarioRepository.findById(dto.getId()).get();
		if (user != null) {
			user.setContrasenia(passwordEncoder.encode(pass));
			user.setIntentos(0);
			usuarioRepository.save(user);
			HashMap<String, String> map = new HashMap<>();
			map.put("usuario", String.format(" %1s %2s", user.getNombres(), user.getApellidos()));
			map.put("clave", pass);
			emailServiceImpl.sendSimpleMessage(user.getCorreo(), "Cambio de Contraseña",
					TemplateEmail.buildEmail(map, TemplateEmailEnum.CAMBIO_CLAVE));
			return String.format("Se envío la nueva contraseña al correo %1s", user.getCorreo());
		}
		return "";
	}

	public String generateCommonLangPassword() {
		String upperCaseLetters = RandomStringUtils.random(2, 65, 90, true, true);
		String lowerCaseLetters = RandomStringUtils.random(1, 97, 122, true, true);
		String numbers = RandomStringUtils.randomNumeric(2);
		String totalChars = RandomStringUtils.randomAlphanumeric(1);
		String combinedChars = upperCaseLetters.concat(lowerCaseLetters).concat(numbers).concat(totalChars);
		List<Character> pwdChars = combinedChars.chars().mapToObj(c -> (char) c).collect(Collectors.toList());
		Collections.shuffle(pwdChars);
		String password = pwdChars.stream().collect(StringBuilder::new, StringBuilder::append, StringBuilder::append)
				.toString();
		return password;
	}

	public UsuarioDTO save(UsuarioDTO dto, String filtro) throws Exception {
		try {
			Usuario user = new Usuario();
			// u.setTipo(catalogoRepository.findById(u.getIdTipo()).orElse(null));
			if (dto.getId() != null) {
				user = usuarioRepository.findById(dto.getId()).orElse(null);
				if (dto.getResetearPassword()) {
					user.setContrasenia(passwordEncoder.encode(dto.getIdentificacion()));
				}
			} else {
				user.setFechaRegistro(LocalDateTime.now());
				user.setContrasenia(passwordEncoder.encode(dto.getIdentificacion()));
				user.setEmpresa(empresaRepository.findById(dto.getEmpresaId()).orElse(null));
			}
			user.setActivo(dto.getActivo());
			user.setApellidos(dto.getApellidos());
			user.setCorreo(dto.getCorreo());
			user.setIdentificacion(dto.getIdentificacion());
			user.setNombres(dto.getNombres());
			usuarioRepository.save(user);
			List<UsuarioPerfil> rolesUsuario = usuarioPerfilRepository.findByUsuario(user);
			if (rolesUsuario != null && !rolesUsuario.isEmpty()) {
				for (UsuarioPerfil usuarioPerfil : rolesUsuario) {
					usuarioPerfil.setActivo(ValorSiNoEnum.NO);
					usuarioPerfilRepository.save(usuarioPerfil);
				}
			}

			List<Integer> roles = dto.getListRolesId();

			for (Integer id : roles) {
				Perfil p = perfilRepository.findById(id).orElse(null);
				if (p != null) {
					List<UsuarioPerfil> usuPer = usuarioPerfilRepository.findByUsuarioAndPerfil(user, p);
					if (usuPer != null && !usuPer.isEmpty()) {
						UsuarioPerfil up = usuPer.get(0);
						up.setActivo(ValorSiNoEnum.SI);
						usuarioPerfilRepository.save(up);
					} else {
						UsuarioPerfil up = new UsuarioPerfil();
						up.setActivo(ValorSiNoEnum.SI);
						up.setUsuario(user);
						up.setPerfil(p);
						usuarioPerfilRepository.save(up);
					}
				}
			}
		} catch (DataIntegrityViolationException e) {
			e.printStackTrace();
			throw new Exception("Ya existe un usuario con la misma cédula");
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return dto;
	};

	public UsuarioDTO update(UsuarioDTO dto) throws Exception {
		try {
			Usuario user = new Usuario();
			if (dto.getId() != null) {
				user = usuarioRepository.findById(dto.getId()).orElse(null);
			}
			user.setApellidos(dto.getApellidos());
			user.setCorreo(dto.getCorreo());
			user.setIdentificacion(dto.getIdentificacion());
			user.setNombres(dto.getNombres());
			usuarioRepository.save(user);

			Empresa emp = new Empresa();
			if (dto.getEmpresa().getId() != null) {
				emp = empresaRepository.findById(dto.getEmpresa().getId()).orElse(null);
			}
			emp.setNombre(dto.getEmpresa().getNombre());
			emp.setRuc(dto.getEmpresa().getRuc());
			emp.setCorreo(dto.getEmpresa().getCorreo());
			emp.setCelular(dto.getEmpresa().getCelular());
			emp.setDireccion(dto.getEmpresa().getDireccion());
			empresaRepository.save(emp);
			if (empresaBloqueRepository.findByEmpresa(emp.getId()).isEmpty()) {
				EmpresaBloque eb = new EmpresaBloque();
				eb.setEmpresa(emp);
				eb.setNumero("UNO");
				empresaBloqueRepository.save(eb);
			}

			// Registramos los dias de aplicacion por default
			List<Catalogo> tiposApp = catalogoRepository.findByGrupoRest(CodigosGrupoEnum.TIPOS_APLICACIONES.name());
			List<Catalogo> diasAplicacion = catalogoRepository
					.findByGrupoOrderesRest(CodigosGrupoEnum.DIAS_SEMANA.name());
			String[] diasAplicacionArray = diasAplicacion.stream().map(d -> d.getCodigo()).toList()
					.toArray(new String[diasAplicacion.size()]);
			for (Catalogo catalogo : tiposApp) {
				List<Catalogo> list = dto.getDiasAplicaciones().stream()
						.filter(da -> da.getCodigoGrupo().equals(catalogo.getCodigo())).toList();
				if (list.isEmpty()) {
					Catalogo cat = new Catalogo(catalogo.getCodigo().concat(emp.getRuc()), catalogo.getCodigo(),
							" dias ".concat(catalogo.getNombre()), diasAplicacionArray, "NO", ValorEstadoEnum.ACTIVO,
							emp);
					catalogoRepository.save(cat);
					dto.getDiasAplicaciones().add(cat);
				} else {
					Catalogo cat=list.get(0);
					cat.setEmpresa(emp);
					catalogoRepository.save(cat);
				}

			}

		} catch (DataIntegrityViolationException e) {
			e.printStackTrace();
			throw new Exception("Ya existe un usuario con la misma cédula");
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return dto;
	};

	public Empresa crearProveedorSobrante(Catalogo c, String rucEmpresa) {
		Empresa nuevo = new Empresa();
		try {
			Empresa exist = empresaRepository.findByRucSobrante(rucEmpresa);
			if (exist != null) {
				exist.setActivo(ValorSiNoEnum.SI);
				empresaRepository.save(exist);
				nuevo = exist;
			} else {
				nuevo.setNombre("PROVEEDOR SOBRANTE");
				nuevo.setActivo(ValorSiNoEnum.SI);
				nuevo.setRucSobrante(rucEmpresa);
				nuevo.setRuc(c.getCodigo());
				nuevo.setTipo(catalogoRepository.findByCodigo(CodigoCatalogoEnum.TU_PRO.name()).get(0));
				empresaRepository.save(nuevo);
			}
		} catch (Exception e) {
		}
		return nuevo;
	};

	public String updateAvatar(MultipartFile logo, Integer idEmpresa) throws IOException {
		Usuario emp = usuarioRepository.findById(idEmpresa).orElse(null);
		if (logo != null) {
			emp.setFoto(logo.getBytes());
			usuarioRepository.save(emp);
		}
		return "OK";
	}

	public byte[] obtenerImagen(Integer idEmpresa) throws IOException {
		Usuario emp = usuarioRepository.findById(idEmpresa).orElse(null);
		return emp.getFoto() != null ? emp.getFoto() : new byte[0];
	}
}
