package com.keysist.roses.model.services;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.keysist.roses.model.dto.CostoVariedadDTO;
import com.keysist.roses.model.entity.Catalogo;
import com.keysist.roses.model.entity.CostoLongitud;
import com.keysist.roses.model.entity.CostoVariedad;
import com.keysist.roses.model.entity.Empresa;
import com.keysist.roses.model.entity.EmpresaVariedad;
import com.keysist.roses.model.entity.Variedad;
import com.keysist.roses.model.enums.CodigosGrupoEnum;
import com.keysist.roses.model.repository.CatalogoRepository;
import com.keysist.roses.model.repository.CostoLongitudRepository;
import com.keysist.roses.model.repository.CostoVariedadRepository;
import com.keysist.roses.model.repository.EmpresaRepository;
import com.keysist.roses.model.repository.EmpresaVariedadRepository;
import com.keysist.roses.model.repository.VariedadRepository;

@Service
public class CostoVariedadService {

	@Autowired
	private EmpresaVariedadRepository empresaVariedadRepository;
	@Autowired
	private CostoVariedadRepository costoVariedadRepository;
	@Autowired
	private CostoLongitudRepository costoLongitudRepository;
	@Autowired
	private CatalogoRepository catalogoRepository;
	@Autowired
	private VariedadRepository variedadRepository;

	@Autowired
	private EmpresaRepository empresaRepository;

	public List<CostoVariedad> save(CostoVariedadDTO dto) {
		Integer idEmpresa = dto.getIdEmpresa();
		Variedad variedad = variedadRepository.findById(dto.getIdVariedad()).orElse(null);
		Empresa empresa = empresaRepository.findById(dto.getIdEmpresa()).orElse(null);
		List<EmpresaVariedad> list = empresaVariedadRepository.findByEmpresaAndVariedad(empresa, variedad);
		if (!list.isEmpty()) {
			CostoVariedad cv = new CostoVariedad();
			EmpresaVariedad ev = list.get(0);
			List<CostoVariedad> cvList = costoVariedadRepository.findByEmpresaVariedad(ev);
			if (cvList.isEmpty()) {
				cv.setEstado(dto.getEstado());
				cv.setFechaInicio(dto.getFechaInicio());
				cv.setFechaFin(dto.getFechaFin());
				cv.setEmpresaVariedad(ev);
				costoVariedadRepository.save(cv);
				saveCostoLongitud(dto);
			} else {
				CostoVariedad cv2 = cvList.get(0);
				cv2.setFechaInicio(dto.getFechaInicio());
				cv2.setFechaFin(dto.getFechaFin());
				cv2.setEstado(dto.getEstado());
				costoVariedadRepository.save(cv2);
			}
		}
		return costoVariedadRepository.findByIdEmpresa(idEmpresa);
	};

	public void saveCostoLongitud(CostoVariedadDTO cv) {
		List<Catalogo> longitudes = catalogoRepository.findByGrupo(CodigosGrupoEnum.LONGITUD_TALLO.name());
		if (!longitudes.isEmpty()) {
			for (Catalogo l : longitudes) {
				CostoLongitud cl = new CostoLongitud();
				cl.setCostoVariedad(costoVariedadRepository.findById(cv.getId()).get());
				cl.setLongitud(l);
				cl.setPrecio(new BigDecimal(0));
				costoLongitudRepository.save(cl);
			}
		}
	};

}
