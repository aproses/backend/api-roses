package com.keysist.roses.model.services;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.temporal.WeekFields;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import com.keysist.roses.model.dto.EntregaDTO;
import com.keysist.roses.model.dto.EntregaPoscosechaDTO;
import com.keysist.roses.model.dto.EntregaVariedadDTO;
import com.keysist.roses.model.entity.Empresa;
import com.keysist.roses.model.entity.EmpresaVariedad;
import com.keysist.roses.model.entity.Entrega;
import com.keysist.roses.model.entity.EntregaPoscosecha;
import com.keysist.roses.model.entity.EntregaVariedad;
import com.keysist.roses.model.enums.EstadoEntregaEnum;
import com.keysist.roses.model.enums.EstadoEntregaProcesoEnum;
import com.keysist.roses.model.enums.TipoEntregaEnum;
import com.keysist.roses.model.enums.ValorSiNoEnum;
import com.keysist.roses.model.repository.CosechaVariedadRepository;
import com.keysist.roses.model.repository.EmpresaRepository;
import com.keysist.roses.model.repository.EmpresaVariedadRepository;
import com.keysist.roses.model.repository.EntregaPoscosechaRepository;
import com.keysist.roses.model.repository.EntregaRepository;
import com.keysist.roses.model.repository.EntregaVariedadRepository;

@Service
public class EntregaService {

	@Autowired
	SimpMessagingTemplate template;

	@Autowired
	private EntregaVariedadRepository entregaVariedadRepository;
	
	
	@Autowired
	private EntregaPoscosechaRepository entregaPoscosechaRepository;

	@Autowired
	private EntregaRepository entregaRepository;
	

	@Autowired
	private EmpresaRepository empresaRepository;

	@Autowired
	private EmpresaVariedadRepository empresaVariedadRepository;

	@Autowired
	private ProduccionService produccionService;

	@Autowired
	private CosechaService cosechaService;

	@Autowired
	private CosechaVariedadRepository cosechaVariedadRepository;
	
	
	@Autowired
	private PoscosechaService poscosechaService;

	@Autowired
	Environment env;

	public List<EntregaDTO> consultarByCliente(Integer idEmpresa, Long desde, Long hasta) {
		LocalDate fechaDesde = (new Timestamp(desde)).toLocalDateTime().toLocalDate();
		LocalDate fechaHasta = (new Timestamp(hasta)).toLocalDateTime().toLocalDate();

		Empresa em = empresaRepository.findById(idEmpresa).orElse(null);
		List<Entrega> list = new ArrayList<>();
		if ("TE_PROV".equalsIgnoreCase(em.getTipo().getCodigo()))
			list = entregaRepository.findByFechaAndProveedor(idEmpresa, fechaDesde, fechaHasta);
		else
			list = entregaRepository.findByFechaAndCliente(idEmpresa, fechaDesde, fechaHasta);
		return list.stream().map(l -> build(l)).toList();
	}

	public List<EntregaDTO> obtenerById(Integer idEntrega) {
		List<Entrega> entregas = entregaRepository.findByIdEntrega(idEntrega);
		return entregas.stream().map(l -> build(l)).toList();
	}

	public List<EntregaDTO> generarEntrega(Integer idProveedor, Integer idCliente, Long fechaEntrega) {
		LocalDate fechaActual = (new Timestamp(fechaEntrega)).toLocalDateTime().toLocalDate();
		List<Entrega> entregas = entregaRepository.findByProveedorClienteFecha(idProveedor, idCliente, fechaActual);
		List<EntregaDTO> list = new ArrayList<>();
		final List<Object[]> cosechaVariedad = cosechaVariedadRepository.findToEntregaProveedor(idProveedor,
				fechaActual);
		EntregaDTO entrega = new EntregaDTO();
		if (entregas.isEmpty()) {

			entrega.setFechaEntrega(fechaActual);
			entrega.setFechaEntregaTime(fechaEntrega);
			entrega.setEstado(EstadoEntregaEnum.BORRADOR);
			entrega.setProveedorId(idProveedor);
			entrega.setClienteId(idCliente);

			if (!cosechaVariedad.isEmpty()) {
				final List<EntregaVariedadDTO> detalle = new ArrayList<>();
				cosechaVariedad.forEach((dv) -> {
					EmpresaVariedad uv = empresaVariedadRepository.findById((Integer) dv[0]).get();
					BigDecimal totalTallos = BigDecimal.valueOf(Long.valueOf(dv[1].toString()));
					Boolean esMenor = totalTallos.intValue() < uv.getTalloMalla().intValue();
					Integer cantidadMallas = esMenor ? 1
							: totalTallos.divide(BigDecimal.valueOf(uv.getTalloMalla()), RoundingMode.FLOOR).intValue();
					BigDecimal tallosSobantes = esMenor ? BigDecimal.valueOf(0)
							: totalTallos.subtract(BigDecimal.valueOf((cantidadMallas * uv.getTalloMalla())));
					EntregaVariedadDTO nd = new EntregaVariedadDTO();
					nd.setMallas(cantidadMallas);
					nd.setCantidadPorMalla(esMenor ? totalTallos.intValue() : uv.getTalloMalla());
					nd.setEstado(EstadoEntregaEnum.BORRADOR);
					nd.setEmpresaVariedadId(uv.getId());
					nd.setEmpresaVariedadNombre(uv.getVariedad().getNombre());
					nd.setTotalTallos(esMenor ? totalTallos.intValue() : cantidadMallas * uv.getTalloMalla());
					detalle.add(nd);
					if (tallosSobantes.intValue() > 0) {
						nd = new EntregaVariedadDTO();
						nd.setMallas(1);
						nd.setCantidadPorMalla(tallosSobantes.intValue());
						nd.setEstado(EstadoEntregaEnum.BORRADOR);
						nd.setEmpresaVariedadId(uv.getId());
						nd.setEmpresaVariedadNombre(uv.getVariedad().getNombre());
						nd.setTotalTallos(tallosSobantes.intValue());
						detalle.add(nd);
					}
				});
				entrega.setEntregaVariedads(detalle);
				list.add(entrega);
			} else {
				List<EmpresaVariedad> usVariedad = empresaVariedadRepository.findByEmpresaAndDisponible(
						empresaRepository.findById(idProveedor).orElse(null), ValorSiNoEnum.SI);
				final List<EntregaVariedadDTO> detalle = usVariedad.stream()
						.filter(uvf -> ValorSiNoEnum.SI.equals(uvf.getVariedad().getActivo())).map((dv) -> {
							return new EntregaVariedadDTO(dv);
						}).toList();
				entrega.setEntregaVariedads(detalle);
				list.add(entrega);
			}
		} else {
			entrega=build(entregas.get(0));
			final List<EntregaVariedadDTO> detalle = new ArrayList<>();
			detalle.addAll(entrega.getEntregaVariedads());
			for (Object[] dv : cosechaVariedad) {
				EmpresaVariedad uv = empresaVariedadRepository.findById((Integer) dv[0]).get();
				BigDecimal totalTallos = BigDecimal.valueOf(Long.valueOf(dv[1].toString()));
				Boolean esMenor = totalTallos.intValue() < uv.getTalloMalla().intValue();
				Integer cantidadMallas = esMenor ? 1
						: totalTallos.divide(BigDecimal.valueOf(uv.getTalloMalla()), RoundingMode.FLOOR).intValue();
				BigDecimal tallosSobantes = esMenor ? BigDecimal.valueOf(0)
						: totalTallos.subtract(BigDecimal.valueOf((cantidadMallas * uv.getTalloMalla())));
				EntregaVariedadDTO nd = new EntregaVariedadDTO();
				nd.setMallas(cantidadMallas);
				nd.setCantidadPorMalla(esMenor ? totalTallos.intValue() : uv.getTalloMalla());
				nd.setEstado(EstadoEntregaEnum.BORRADOR);
				nd.setEmpresaVariedadId(uv.getId());
				nd.setEmpresaVariedadNombre(uv.getVariedad().getNombre());
				nd.setTotalTallos(esMenor ? totalTallos.intValue() : cantidadMallas * uv.getTalloMalla());
				detalle.add(nd);
				if (tallosSobantes.intValue() > 0) {
					nd = new EntregaVariedadDTO();
					nd.setMallas(1);
					nd.setCantidadPorMalla(tallosSobantes.intValue());
					nd.setEstado(EstadoEntregaEnum.BORRADOR);
					nd.setEmpresaVariedadId(uv.getId());
					nd.setEmpresaVariedadNombre(uv.getVariedad().getNombre());
					nd.setTotalTallos(tallosSobantes.intValue());
					detalle.add(nd);
				}
			}
			entrega.setEntregaVariedads(detalle);
			list.add(entrega);
		}

		return list;
	}

	EntregaDTO build(Entrega entrega) {
		EntregaDTO dto = new EntregaDTO(entrega);
		dto.setFechaEntregaTime(java.sql.Date.valueOf(entrega.getFechaEntrega()).getTime());
		List<EntregaVariedadDTO> detalle = entregaVariedadRepository.findByEntrega(entrega.getId()).stream()
				.map(ev -> new EntregaVariedadDTO(ev)).toList();
		dto.setEntregaVariedads(detalle);
		dto.setMallas(detalle.stream().map(dt -> BigDecimal.valueOf(dt.getMallas()))
				.reduce(BigDecimal.ZERO, BigDecimal::add).intValue());
		dto.setTotalTallos(detalle.stream().map(dt -> BigDecimal.valueOf(dt.getTotalTallos()))
				.reduce(BigDecimal.ZERO, BigDecimal::add).intValue());
		return dto;
	}

	@Transactional
	public EntregaDTO enviarEntrega(EntregaDTO dto) {
		Entrega entrega = new Entrega(dto);
		entrega.setFechaEntrega(new java.sql.Date(dto.getFechaEntregaTime()).toLocalDate());
		entrega.setCliente(empresaRepository.findById(dto.getClienteId()).orElse(null));
		entrega.setProveedor(empresaRepository.findById(dto.getProveedorId()).orElse(null));
		entrega.setTipo(TipoEntregaEnum.PROVEEDOR);
		entrega.setHoraEntrega(LocalTime.now());
		WeekFields weekFields = WeekFields.of(Locale.getDefault());
		int weekNumber = ((new Timestamp(dto.getFechaEntregaTime())).toLocalDateTime().toLocalDate())
				.get(weekFields.weekOfWeekBasedYear());
		entrega.setSemana(weekNumber);

		List<EntregaVariedadDTO> detallesNew = new ArrayList<>(0);
		List<EntregaVariedadDTO> detallesNewToRegister = new ArrayList<>(0);
		if(EstadoEntregaEnum.RECEPCIONADO.equals(entrega.getEstado())) {
			poscosechaService.registrarIngreso(entrega);
		}
		if (entrega.getId() != null) {
			entregaRepository.save(entrega);
			for (EntregaVariedadDTO de : dto.getEntregaVariedads()) {
				EntregaVariedad ev = new EntregaVariedad(de);
				if (de.getId() != null) {
					ev.setEntrega(entrega);
					ev.setEmpresaVariedad(empresaVariedadRepository.findById(de.getEmpresaVariedadId()).orElse(null));
					entregaVariedadRepository.save(ev);
					de = new EntregaVariedadDTO(ev);
				} else {
					if (de.getMallas() != 0) {
						EntregaVariedad den = new EntregaVariedad(de);
						den.setEntrega(entrega);
						den.setEmpresaVariedad(
								empresaVariedadRepository.findById(de.getEmpresaVariedadId()).orElse(null));
						entregaVariedadRepository.save(den);
						de = new EntregaVariedadDTO(den);
						detallesNewToRegister.add(de);
					}
				}
				detallesNew.add(de);
			}
		} else {
			entregaRepository.save(entrega);
			dto.setId(entrega.getId());
			for (EntregaVariedadDTO de : dto.getEntregaVariedads()) {
				if (de.getMallas() != 0) {
					EntregaVariedad den = new EntregaVariedad(de);
					den.setEntrega(entrega);
					den.setEmpresaVariedad(empresaVariedadRepository.findById(de.getEmpresaVariedadId()).orElse(null));
					entregaVariedadRepository.save(den);
					de = new EntregaVariedadDTO(den);
					detallesNewToRegister.add(de);
				}
				detallesNew.add(de);
			}
		}
		dto.setEntregaVariedads(detallesNew);
		if (EstadoEntregaEnum.ENVIADO.equals(entrega.getEstado())) {
			cosechaService.registrarEnvio(detallesNewToRegister, entrega.getFechaEntrega());
		}
		template.convertAndSend("/message/actualizarProduccion", produccionService.obtenerEstadoProcesadora());
		return dto;
	}

	@Transactional
	public void registrarSobrante(Entrega entregaSobrante, Integer cantidadSobrante, EmpresaVariedad ev)
			throws Exception {
		EntregaPoscosecha epo = new EntregaPoscosecha();
		Optional<EntregaPoscosecha> entregaPoscosecha = entregaPoscosechaRepository.findEntregaAndEmpresaVariedad(entregaSobrante, ev);
		if (entregaPoscosecha.isEmpty()) {
			// Obtiene el sobrante del mes anterior
			epo.setEmpresaVariedad(ev);
			epo.setEntrega(entregaSobrante);
			epo.setEstado(EstadoEntregaProcesoEnum.RECEPCIONADO);
			epo.setTallos(cantidadSobrante);
			entregaPoscosechaRepository.save(epo);
		} else {
			epo = entregaPoscosecha.get();
			epo.setTallos(epo.getTallos()+cantidadSobrante);
			entregaPoscosechaRepository.save(epo);
		}

	}

	public List<Entrega> recuperarEntrega(String Filtro, EstadoEntregaEnum Estado) {
		List<Entrega> entrega = entregaRepository.findByFiltro(Filtro, Estado);
		return entrega;
	}

	public boolean eliminarEntregaDetalle(Integer idEntregaVariedad) {
		entregaVariedadRepository.deleteById(idEntregaVariedad);
		return true;
	}

	public List<EntregaPoscosechaDTO> recuperaEntregas(LocalDate desde, LocalDate hasta, Integer idProveedor,
			Integer idCliente) {
		List<EntregaPoscosechaDTO> retorno = new ArrayList<>();
		List<Object[]> list = entregaPoscosechaRepository.findProveedorByRange(idProveedor, idCliente, desde, hasta,
				TipoEntregaEnum.PROVEEDOR);
		for (Object[] obj : list) {
			Integer id = 0;
			if (obj[0] != null)
				id = ((Integer) obj[0]);
			EntregaPoscosecha ep = entregaPoscosechaRepository.findById(id).get();
			EntregaPoscosechaDTO dto = new EntregaPoscosechaDTO(ep);
			retorno.add(dto);
		}
		return retorno;
	}
}
