package com.keysist.roses.model.services;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.temporal.WeekFields;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import com.keysist.roses.model.dto.CosechaDTO;
import com.keysist.roses.model.dto.CosechaVariedadDTO;
import com.keysist.roses.model.dto.EntregaVariedadDTO;
import com.keysist.roses.model.entity.Cosecha;
import com.keysist.roses.model.entity.CosechaVariedad;
import com.keysist.roses.model.entity.EmpresaVariedad;
import com.keysist.roses.model.enums.EstadoCosechaVariedadEnum;
import com.keysist.roses.model.repository.CosechaRepository;
import com.keysist.roses.model.repository.CosechaVariedadRepository;
import com.keysist.roses.model.repository.EmpresaRepository;
import com.keysist.roses.model.repository.EmpresaVariedadRepository;

@Service
public class CosechaService {

	@Autowired
	private CosechaRepository cosechaRepository;
	@Autowired
	private CosechaVariedadRepository cosechaVariedadRepository;

	@Autowired
	private EmpresaVariedadRepository empresaVariedadRepository;

	@Autowired
	private EmpresaRepository empresaRepository;

	@Autowired
	Environment env;

	@Autowired
	SimpMessagingTemplate template;

	@Autowired
	private ProduccionService produccionService;

	@Transactional
	public CosechaDTO obtener(Integer idEmpresa, Long fechaCorte) {
		LocalDate fechaCorteD = (new Timestamp(fechaCorte)).toLocalDateTime().toLocalDate();
		Optional<Cosecha> c = cosechaRepository.findByEmpresaFecha(idEmpresa, fechaCorteD);
		CosechaDTO dto = new CosechaDTO();
		dto.setEmpresaId(idEmpresa);
		if (c.isPresent()) {
			Cosecha cdb = c.get();
			dto.setId(cdb.getId());
			dto.setCosechaVariedadDTOs(cosechaVariedadRepository.findByCosecha(cdb.getId()).stream()
					.map(cv -> new CosechaVariedadDTO(cv)).toList());
			dto.setObservacion(cdb.getObservacion());
		} else {
			dto.setFechaCorte(fechaCorte);
			List<EmpresaVariedad> uVar = empresaVariedadRepository.findByUsuario(idEmpresa);
			dto.setCosechaVariedadDTOs(uVar.stream().map(uv -> new CosechaVariedadDTO(uv)).toList());
		}
		return dto;
	}

	@Transactional
	public CosechaDTO obtenerById(Integer idCosecha) {
		Optional<Cosecha> c = cosechaRepository.findById(idCosecha);
		CosechaDTO dto = new CosechaDTO(c.get());
		if (c.isPresent()) {
			dto.setCosechaVariedadDTOs(cosechaVariedadRepository.findByCosecha(c.get().getId()).stream()
					.map(cv -> new CosechaVariedadDTO(cv)).toList());
		}
		return dto;
	}

	@Transactional
	public CosechaDTO guardar(CosechaDTO dto) {
		Integer idProveedor = dto.getEmpresaId();
		Cosecha co = new Cosecha();
		Optional<Cosecha> optCo = cosechaRepository.findByEmpresaFecha(dto.getEmpresaId(),
				(new Timestamp(dto.getFechaCorte())).toLocalDateTime().toLocalDate());
		if (optCo.isPresent()) {
			co = optCo.get();
		} else {
			co.setEmpresa(empresaRepository.findById(dto.getEmpresaId()).get());
		}

		co.setObservacion(dto.getObservacion());
		co.setFechaCorte((new Timestamp(dto.getFechaCorte())).toLocalDateTime().toLocalDate());
		WeekFields weekFields = WeekFields.of(Locale.getDefault());
		int weekNumber = co.getFechaCorte().get(weekFields.weekOfWeekBasedYear());
		co.setSemana(weekNumber);
		final Cosecha con = cosechaRepository.save(co);

		dto.getCosechaVariedadDTOs().forEach(cvd -> {
			CosechaVariedad cv = new CosechaVariedad();
			if (cvd.getId() != null) {
				cv = cosechaVariedadRepository.findById(cvd.getId()).get();
			} else {
				cv.setEstado(EstadoCosechaVariedadEnum.COSECHADO);
				cv.setEmpresaVariedad(empresaVariedadRepository.findById(cvd.getEmpresaVariedadId()).get());
				cv.setTotalTalloDisponible(cvd.getTotalTallo());
			}
			cv.setCosecha(con);
			cv.setCantidadMalla(cvd.getCantidadMalla());
			cv.setTalloMalla(cvd.getTalloMalla());
			cv.setTotalTalloNacional(cvd.getTotalTalloNacional());
			cv.setTotalTallo(cvd.getTotalTallo());
			if (EstadoCosechaVariedadEnum.COSECHADO.equals(cv.getEstado())) {
				cv.setTotalTalloDisponible(cv.getTotalTallo());
			}
			cosechaVariedadRepository.save(cv);
			cvd.setId(cv.getId());
		});
		dto.setId(con.getId());
		// TODO: Modificar
		template.convertAndSend("/message/produccionPorSemanualAnual/" + idProveedor,
				produccionService.obtenerProduccionSemanalAnual(idProveedor));
		template.convertAndSend("/message/cosechaDiaria/" + idProveedor,
				produccionService.obtenerCosecha(idProveedor, GregorianCalendar.getInstance().getTimeInMillis()));
		template.convertAndSend("/message/produccionMensual/" + idProveedor,
				produccionService.obtenerCosechaMensual(idProveedor));
		return dto;
	}

	@Transactional
	public void registrarEnvio(List<EntregaVariedadDTO> entregaVariedades, LocalDate fechaEntrega) {
		entregaVariedades.stream().forEach(dto -> {
			EmpresaVariedad uv = empresaVariedadRepository.findById(dto.getEmpresaVariedadId()).get();
			Integer totalTallos = dto.getTotalTallos();
			List<CosechaVariedad> cosecha = cosechaVariedadRepository.getCosecha(uv.getId(),
					EstadoCosechaVariedadEnum.ENVIADO, fechaEntrega);
			for (CosechaVariedad cv : cosecha) {
				if (cv.getTotalTalloDisponible() <= totalTallos) {
					totalTallos = totalTallos - cv.getTotalTalloDisponible();
					cv.setTotalTalloDisponible(0);
					cv.setEstado(EstadoCosechaVariedadEnum.ENVIADO);
					cosechaVariedadRepository.save(cv);
					List<CosechaVariedad> noEnviados = cosechaVariedadRepository
							.findNoEnviadoByCosecha(cv.getCosecha().getId(), EstadoCosechaVariedadEnum.ENVIADO);
					if (noEnviados.isEmpty()) {
						cosechaRepository.save(cv.getCosecha());
					}
				} else {
					cv.setTotalTalloDisponible(cv.getTotalTalloDisponible() - totalTallos);
					cv.setEstado(EstadoCosechaVariedadEnum.ENVIADO_PARCIAL);
					cosechaVariedadRepository.save(cv);
					totalTallos = 0;
					break;
				}

			}

		});
	}

	public boolean eliminarCosechaDetalle(Integer idCosechaVariedad) {
		cosechaVariedadRepository.deleteById(idCosechaVariedad);
		return true;
	}

	@Transactional
	public List<CosechaDTO> consultarFilter(Integer idEmpresa, LocalDate desde, LocalDate hasta) throws Exception {
		List<Cosecha> pinches = cosechaRepository.findByFecha(idEmpresa, desde, hasta);
		return pinches.stream().map(p -> buildToDto(p)).toList();

	}

	private CosechaDTO buildToDto(Cosecha pinche) {
		CosechaDTO dto = new CosechaDTO();
		dto.setId(pinche.getId());
		dto.setSemana(pinche.getSemana());
		dto.setEmpresaId(pinche.getEmpresa().getId());
		dto.setFechaCorte((java.sql.Date.valueOf(pinche.getFechaCorte()).getTime()));
		dto.setFechaCorteL(pinche.getFechaCorte());
		dto.setCosechaVariedadDTOs(cosechaVariedadRepository.findByCosecha(pinche.getId()).stream()
				.map(p -> new CosechaVariedadDTO(p)).toList());
		dto.setTotalTallos(dto.getCosechaVariedadDTOs().stream().map(dt -> BigDecimal.valueOf(dt.getTotalTallo()))
				.reduce(BigDecimal.ZERO, BigDecimal::add).intValue());
		dto.setTotalMallas(dto.getCosechaVariedadDTOs().stream().map(dt -> BigDecimal.valueOf(dt.getCantidadMalla()))
				.reduce(BigDecimal.ZERO, BigDecimal::add).intValue());
		return dto;
	}

}
