package com.keysist.roses.model.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import com.keysist.roses.model.entity.Empresa;
import com.keysist.roses.model.enums.ValorSiNoEnum;

public class EmpresaDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5494631274223867891L;

	private Integer id;
	private String nombre;
	private String ruc;
	private String direccion;
	private byte[] logo;
	private String correo;
	private String codigo;
	private String celular;
	private ValorSiNoEnum activo;
	private Boolean activoB;
	private Integer tipoId;
	private String tipoNombre;
	private String tipoCodigo;
	private String contrasenia;
	
	private BigDecimal produccionSemana;
	
	private LocalDateTime fechaRegistro;

	public EmpresaDTO() {
		// TODO Auto-generated constructor stub
	}

	public EmpresaDTO(Empresa e) {
		this.id = e.getId();
		this.nombre = e.getNombre();
		this.ruc = e.getRuc();
		this.direccion = e.getDireccion();
		this.correo = e.getCorreo();
		this.celular = e.getCelular();
		this.tipoId = e.getTipo().getId();
		this.tipoNombre = e.getTipo().getNombre();
		this.tipoCodigo = e.getTipo().getCodigo();
		this.activo = e.getActivo();
		this.activoB = ValorSiNoEnum.SI.equals(e.getActivo());
		this.codigo = e.getCodigo();
		this.produccionSemana = e.getProduccionSemana();
		this.fechaRegistro=e.getFechaRegistro();
		// TODO Auto-generated constructor stub
	}

	public EmpresaDTO(Integer id, ValorSiNoEnum activo) {
		super();
		this.id = id;
		this.activo = activo;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getRuc() {
		return ruc;
	}

	public void setRuc(String ruc) {
		this.ruc = ruc;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public byte[] getLogo() {
		return logo;
	}

	public void setLogo(byte[] logo) {
		this.logo = logo;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public ValorSiNoEnum getActivo() {
		return activo;
	}

	public void setActivo(ValorSiNoEnum activo) {
		this.activo = activo;
	}

	public Integer getTipoId() {
		return tipoId;
	}

	public void setTipoId(Integer tipoId) {
		this.tipoId = tipoId;
	}

	public String getTipoNombre() {
		return tipoNombre;
	}

	public void setTipoNombre(String tipoNombre) {
		this.tipoNombre = tipoNombre;
	}

	public String getContrasenia() {
		return contrasenia;
	}

	public void setContrasenia(String contrasenia) {
		this.contrasenia = contrasenia;
	}

	public Boolean getActivoB() {
		return activoB;
	}

	public void setActivoB(Boolean activoB) {
		this.activoB = activoB;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getTipoCodigo() {
		return tipoCodigo;
	}

	public void setTipoCodigo(String tipoCodigo) {
		this.tipoCodigo = tipoCodigo;
	}

	public BigDecimal getProduccionSemana() {
		return produccionSemana;
	}

	public void setProduccionSemana(BigDecimal produccionSemana) {
		this.produccionSemana = produccionSemana;
	}

	public LocalDateTime getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(LocalDateTime fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}
	

}
