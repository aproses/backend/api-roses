package com.keysist.roses.model.dto;

import java.io.Serializable;
import java.util.List;

public class PincheDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6388777771853861328L;
	private Integer id;
	private Integer idEmpresa;
	private Long fechaPinche;
	private Integer total;
	private Integer semana;
	private List<PincheVariedadDTO> pincheVariedad;

	public PincheDTO() {
		// TODO Auto-generated constructor stub
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getIdEmpresa() {
		return idEmpresa;
	}

	public void setIdEmpresa(Integer idEmpresa) {
		this.idEmpresa = idEmpresa;
	}

	public Long getFechaPinche() {
		return fechaPinche;
	}

	public void setFechaPinche(Long fechaPinche) {
		this.fechaPinche = fechaPinche;
	}

	public List<PincheVariedadDTO> getPincheVariedad() {
		return pincheVariedad;
	}

	public void setPincheVariedad(List<PincheVariedadDTO> pincheVariedad) {
		this.pincheVariedad = pincheVariedad;
	}

	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

	public Integer getSemana() {
		return semana;
	}

	public void setSemana(Integer semana) {
		this.semana = semana;
	}

}
