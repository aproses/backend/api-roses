package com.keysist.roses.model.dto;

import java.io.Serializable;
import java.util.List;

public class ReporteIngresoDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6388777771853861328L;

	private String proveedorCodigo;
	private String proveedorNombre;
	private Integer total;

	private List<ReporteIngresoVariedadDTO> detalle;

	public ReporteIngresoDTO() {
	}
	

	public ReporteIngresoDTO(String proveedorCodigo, String proveedorNombre, Integer total) {
		super();
		this.proveedorCodigo = proveedorCodigo;
		this.proveedorNombre = proveedorNombre;
		this.total = total;
	}


	public String getProveedorCodigo() {
		return proveedorCodigo;
	}

	public void setProveedorCodigo(String proveedorCodigo) {
		this.proveedorCodigo = proveedorCodigo;
	}

	public String getProveedorNombre() {
		return proveedorNombre;
	}

	public void setProveedorNombre(String proveedorNombre) {
		this.proveedorNombre = proveedorNombre;
	}

	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

	public List<ReporteIngresoVariedadDTO> getDetalle() {
		return detalle;
	}

	public void setDetalle(List<ReporteIngresoVariedadDTO> detalle) {
		this.detalle = detalle;
	}
	

}
