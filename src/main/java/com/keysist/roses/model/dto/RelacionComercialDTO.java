package com.keysist.roses.model.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import com.keysist.roses.model.entity.RelacionComercial;
import com.keysist.roses.model.enums.ValorSiNoEnum;

public class RelacionComercialDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5494631274223867891L;

	private Integer id;

	private Integer proveedorId;
	private String proveedorNombre;
	private String proveedorRuc;

	private String proveedorCodigo;
	private BigDecimal proveedorEfectividad;

	private Integer clienteId;
	private String clienteNombre;
	private String clienteRuc;
	private ValorSiNoEnum activo;

	private LocalDateTime fechaRegistro;
	public RelacionComercialDTO() {
		// TODO Auto-generated constructor stub
	}

	public RelacionComercialDTO(RelacionComercial entidad) {
		super();
		this.id = entidad.getId();
		this.proveedorId = entidad.getProveedor().getId();
		this.proveedorNombre = entidad.getProveedor().getNombre();
		this.proveedorRuc = entidad.getProveedor().getRuc();
		this.proveedorCodigo = entidad.getProveedorCodigo();
		this.proveedorEfectividad = entidad.getProveedorEfectividad();
		this.clienteId = entidad.getCliente().getId();
		this.clienteNombre = entidad.getCliente().getNombre();
		this.clienteRuc = entidad.getCliente().getRuc();
		this.activo = entidad.getCliente().getActivo();
		this.fechaRegistro=entidad.getFechaRegistro();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getProveedorId() {
		return proveedorId;
	}

	public void setProveedorId(Integer proveedorId) {
		this.proveedorId = proveedorId;
	}

	public String getProveedorNombre() {
		return proveedorNombre;
	}

	public void setProveedorNombre(String proveedorNombre) {
		this.proveedorNombre = proveedorNombre;
	}

	public String getProveedorRuc() {
		return proveedorRuc;
	}

	public void setProveedorRuc(String proveedorRuc) {
		this.proveedorRuc = proveedorRuc;
	}

	public String getProveedorCodigo() {
		return proveedorCodigo;
	}

	public void setProveedorCodigo(String proveedorCodigo) {
		this.proveedorCodigo = proveedorCodigo;
	}

	public BigDecimal getProveedorEfectividad() {
		return proveedorEfectividad;
	}

	public void setProveedorEfectividad(BigDecimal proveedorEfectividad) {
		this.proveedorEfectividad = proveedorEfectividad;
	}

	public Integer getClienteId() {
		return clienteId;
	}

	public void setClienteId(Integer clienteId) {
		this.clienteId = clienteId;
	}

	public String getClienteNombre() {
		return clienteNombre;
	}

	public void setClienteNombre(String clienteNombre) {
		this.clienteNombre = clienteNombre;
	}

	public String getClienteRuc() {
		return clienteRuc;
	}

	public void setClienteRuc(String clienteRuc) {
		this.clienteRuc = clienteRuc;
	}

	public ValorSiNoEnum getActivo() {
		return activo;
	}

	public void setActivo(ValorSiNoEnum activo) {
		this.activo = activo;
	}
	public RelacionComercial buildEntiti(RelacionComercial entidad) {
		entidad.setActivo(activo);
		entidad.setProveedorCodigo(proveedorCodigo);
		entidad.setProveedorEfectividad(proveedorEfectividad);
		return entidad;
	}

	public LocalDateTime getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(LocalDateTime fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}
	

}
