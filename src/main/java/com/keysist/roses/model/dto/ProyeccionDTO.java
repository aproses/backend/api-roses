package com.keysist.roses.model.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

public class ProyeccionDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5494631274223867891L;

	private BigDecimal produccionSemana;
	private BigDecimal produccionPerdida;
	private BigDecimal produccionMes;
	private Integer cantidadPlantas;
	private Integer semanaCorte;
	private Integer semanaCosecha;
	private LocalDate fechaCorte;
	private LocalDate fechaCosecha;
	private String variedad;

	private List<TalloVariedadDTO> talloVariedadDTOs;

	public ProyeccionDTO() {
		// TODO Auto-generated constructor stub
	}

	public ProyeccionDTO(BigDecimal produccionSemana, BigDecimal produccionPerdida, BigDecimal produccionMes,
			Integer cantidadPlantas, List<TalloVariedadDTO> talloVariedadDTOs) {
		super();
		this.produccionSemana = produccionSemana;
		this.produccionPerdida = produccionPerdida;
		this.produccionMes = produccionMes;
		this.cantidadPlantas = cantidadPlantas;
		this.talloVariedadDTOs = talloVariedadDTOs;
	}

	

	
	public ProyeccionDTO(BigDecimal produccionSemana, BigDecimal produccionPerdida, BigDecimal produccionMes,
			Integer cantidadPlantas, Integer semanaCorte, Integer semanaCosecha, LocalDate fechaCorte,
			LocalDate fechaCosecha, String variedad) {
		super();
		this.produccionSemana = produccionSemana;
		this.produccionPerdida = produccionPerdida;
		this.produccionMes = produccionMes;
		this.cantidadPlantas = cantidadPlantas;
		this.semanaCorte = semanaCorte;
		this.semanaCosecha = semanaCosecha;
		this.fechaCorte = fechaCorte;
		this.fechaCosecha = fechaCosecha;
		this.variedad = variedad;
	}
	

	public ProyeccionDTO(Integer semanaCorte, Integer semanaCosecha, LocalDate fechaCorte, LocalDate fechaCosecha) {
		super();
		this.semanaCorte = semanaCorte;
		this.semanaCosecha = semanaCosecha;
		this.fechaCorte = fechaCorte;
		this.fechaCosecha = fechaCosecha;
	}

	public BigDecimal getProduccionSemana() {
		return produccionSemana;
	}

	public void setProduccionSemana(BigDecimal produccionSemana) {
		this.produccionSemana = produccionSemana;
	}

	public BigDecimal getProduccionPerdida() {
		return produccionPerdida;
	}

	public void setProduccionPerdida(BigDecimal produccionPerdida) {
		this.produccionPerdida = produccionPerdida;
	}

	public List<TalloVariedadDTO> getTalloVariedadDTOs() {
		return talloVariedadDTOs;
	}

	public void setTalloVariedadDTOs(List<TalloVariedadDTO> talloVariedadDTOs) {
		this.talloVariedadDTOs = talloVariedadDTOs;
	}

	public BigDecimal getProduccionMes() {
		return produccionMes;
	}

	public void setProduccionMes(BigDecimal produccionMes) {
		this.produccionMes = produccionMes;
	}

	public Integer getCantidadPlantas() {
		return cantidadPlantas;
	}

	public void setCantidadPlantas(Integer cantidadPlantas) {
		this.cantidadPlantas = cantidadPlantas;
	}

	public Integer getSemanaCorte() {
		return semanaCorte;
	}

	public void setSemanaCorte(Integer semanaCorte) {
		this.semanaCorte = semanaCorte;
	}

	public Integer getSemanaCosecha() {
		return semanaCosecha;
	}

	public void setSemanaCosecha(Integer semanaCosecha) {
		this.semanaCosecha = semanaCosecha;
	}

	public LocalDate getFechaCorte() {
		return fechaCorte;
	}

	public void setFechaCorte(LocalDate fechaCorte) {
		this.fechaCorte = fechaCorte;
	}

	public LocalDate getFechaCosecha() {
		return fechaCosecha;
	}

	public void setFechaCosecha(LocalDate fechaCosecha) {
		this.fechaCosecha = fechaCosecha;
	}

	public String getVariedad() {
		return variedad;
	}

	public void setVariedad(String variedad) {
		this.variedad = variedad;
	}
	

}
