package com.keysist.roses.model.dto;

import java.io.Serializable;

import com.keysist.roses.model.enums.EstadoEntregaEnum;

public class ProduccionVariedadDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6388777771853861328L;
	private Integer total;
	private String variedad;
	private EstadoEntregaEnum estadoEntregaEnum;
	public ProduccionVariedadDTO() {
		// TODO Auto-generated constructor stub
	}

	public ProduccionVariedadDTO(Object[] obj) {
		int indice = 0;
		if (obj[indice] != null)
			estadoEntregaEnum = (EstadoEntregaEnum) obj[indice];
		indice++;
		if (obj[indice] != null)
			variedad = (String)obj[indice];
		indice++;
		if (obj[indice] != null)
			total = ((Long)obj[indice]).intValue();
	}

	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

	public String getVariedad() {
		return variedad;
	}

	public void setVariedad(String variedad) {
		this.variedad = variedad;
	}

	public EstadoEntregaEnum getEstadoEntregaEnum() {
		return estadoEntregaEnum;
	}

	public void setEstadoEntregaEnum(EstadoEntregaEnum estadoEntregaEnum) {
		this.estadoEntregaEnum = estadoEntregaEnum;
	}

}
