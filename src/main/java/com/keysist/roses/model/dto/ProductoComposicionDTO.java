package com.keysist.roses.model.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import com.keysist.roses.model.entity.ProductoComposicion;

public class ProductoComposicionDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5494631274223867891L;

	private Integer id;

	private BigDecimal dosis;

	private BigDecimal porcentaje;

	private String elementoNombre;

	private Integer elementoId;

	private String productoNombre;
	private Integer productoId;

	public ProductoComposicionDTO() {
		// TODO Auto-generated constructor stub
	}

	public ProductoComposicionDTO(ProductoComposicion pc) {
		// TODO Auto-generated constructor stub
		id = pc.getId();
		dosis = pc.getDosis();
		porcentaje = pc.getPorcentaje();
		elementoNombre = pc.getElemento().getNombre();
		elementoId = pc.getElemento().getId();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public BigDecimal getDosis() {
		return dosis;
	}

	public void setDosis(BigDecimal dosis) {
		this.dosis = dosis;
	}

	public BigDecimal getPorcentaje() {
		return porcentaje;
	}

	public void setPorcentaje(BigDecimal porcentaje) {
		this.porcentaje = porcentaje;
	}

	public String getElementoNombre() {
		return elementoNombre;
	}

	public void setElementoNombre(String elementoNombre) {
		this.elementoNombre = elementoNombre;
	}

	public Integer getElementoId() {
		return elementoId;
	}

	public void setElementoId(Integer elementoId) {
		this.elementoId = elementoId;
	}

	public String getProductoNombre() {
		return productoNombre;
	}

	public void setProductoNombre(String productoNombre) {
		this.productoNombre = productoNombre;
	}

	public Integer getProductoId() {
		return productoId;
	}

	public void setProductoId(Integer productoId) {
		this.productoId = productoId;
	}

}
