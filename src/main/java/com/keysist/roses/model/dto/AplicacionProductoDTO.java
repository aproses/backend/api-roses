package com.keysist.roses.model.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;

import com.keysist.roses.model.entity.AplicacionProducto;

public class AplicacionProductoDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5494631274223867891L;

	private Integer id;
	private Integer tanqueId;
	private String tanqueNombre;
	private Integer empresaProductoId;
	private String productoNombre;
	private BigDecimal dosisLitro;
	private BigDecimal faltante;
	private BigDecimal necesario;
	private BigDecimal disponible;
	private String unidadMedida;
	private BigInteger orden;

	public AplicacionProductoDTO() {
		// TODO Auto-generated constructor stub
	}

	public AplicacionProductoDTO(AplicacionProducto ap) {
		setId(ap.getId());
		if (ap.getTanque() != null) {
			setTanqueId(ap.getTanque().getId());
			setTanqueNombre(ap.getTanque().getNombre());
		}
		if (ap.getEmpresaProducto() != null) {
			setEmpresaProductoId(ap.getEmpresaProducto().getId());
			setProductoNombre(ap.getEmpresaProducto().getProducto().getNombre());
			setUnidadMedida(ap.getEmpresaProducto().getProducto().getUnidadMedida().getNombre());
		}
		setDosisLitro(ap.getDosisLitro());
		setNecesario(ap.getNecesario());
		setFaltante(ap.getFaltante());

		setOrden(ap.getOrden());
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getTanqueId() {
		return tanqueId;
	}

	public void setTanqueId(Integer tanqueId) {
		this.tanqueId = tanqueId;
	}

	public Integer getEmpresaProductoId() {
		return empresaProductoId;
	}

	public void setEmpresaProductoId(Integer empresaProductoId) {
		this.empresaProductoId = empresaProductoId;
	}

	public String getProductoNombre() {
		return productoNombre;
	}

	public void setProductoNombre(String productoNombre) {
		this.productoNombre = productoNombre;
	}

	public BigDecimal getDosisLitro() {
		return dosisLitro;
	}

	public void setDosisLitro(BigDecimal dosisLitro) {
		this.dosisLitro = dosisLitro;
	}

	public String getTanqueNombre() {
		return tanqueNombre;
	}

	public String getUnidadMedida() {
		return unidadMedida;
	}

	public void setUnidadMedida(String unidadMedida) {
		this.unidadMedida = unidadMedida;
	}

	public void setTanqueNombre(String tanqueNombre) {
		this.tanqueNombre = tanqueNombre;
	}

	public BigDecimal getFaltante() {
		return faltante;
	}

	public void setFaltante(BigDecimal faltante) {
		this.faltante = faltante;
	}

	public BigDecimal getNecesario() {
		return necesario;
	}

	public void setNecesario(BigDecimal necesario) {
		this.necesario = necesario;
	}

	public BigDecimal getDisponible() {
		return disponible;
	}

	public void setDisponible(BigDecimal disponible) {
		this.disponible = disponible;
	}

	public BigInteger getOrden() {
		return orden;
	}

	public void setOrden(BigInteger orden) {
		this.orden = orden;
	}

}
