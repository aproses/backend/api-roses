package com.keysist.roses.model.dto;

import java.io.Serializable;
import java.util.List;

import com.keysist.roses.model.entity.EmpresaVariedad;
import com.keysist.roses.model.entity.EntregaVariedad;
import com.keysist.roses.model.enums.EstadoEntregaEnum;

public class EntregaVariedadDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5494631274223867891L;

	private Integer id;
	private Integer entregaId;
	private Integer mallas;
	private Integer cantidadPorMalla;
	private Integer totalTallos;
	private Integer empresaVariedadId;
	private String empresaVariedadNombre;
	private EstadoEntregaEnum estado;
	private List<EntregaBoncheDTO> entregaBonches;

	public EntregaVariedadDTO() {
		// TODO Auto-generated constructor stub
	}

	public EntregaVariedadDTO(EntregaVariedad det) {
		this.id = det.getId();
		this.mallas = det.getMallas();
		this.estado = det.getEstado();
		this.entregaId = det.getEntrega().getId();
		this.cantidadPorMalla = det.getCantidadPorMalla();
		this.totalTallos = det.getTotalTallos();
		if (det.getEmpresaVariedad() != null) {
			setEmpresaVariedadId(det.getEmpresaVariedad().getId());
			setEmpresaVariedadNombre(det.getEmpresaVariedad().getVariedad().getNombre());
		}
	}

	public EntregaVariedadDTO(EmpresaVariedad uv) {
		this.empresaVariedadId = uv.getId();
		this.empresaVariedadNombre = uv.getVariedad().getNombre();
		this.cantidadPorMalla = uv.getTalloMalla();
		this.estado = EstadoEntregaEnum.BORRADOR;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getEntregaId() {
		return entregaId;
	}

	public void setEntregaId(Integer entregaId) {
		this.entregaId = entregaId;
	}

	public Integer getMallas() {
		return mallas;
	}

	public void setMallas(Integer mallas) {
		this.mallas = mallas;
	}

	public Integer getCantidadPorMalla() {
		return cantidadPorMalla;
	}

	public void setCantidadPorMalla(Integer cantidadPorMalla) {
		this.cantidadPorMalla = cantidadPorMalla;
	}

	public Integer getTotalTallos() {
		return totalTallos;
	}

	public void setTotalTallos(Integer totalTallos) {
		this.totalTallos = totalTallos;
	}

	public Integer getEmpresaVariedadId() {
		return empresaVariedadId;
	}

	public void setEmpresaVariedadId(Integer empresaVariedadId) {
		this.empresaVariedadId = empresaVariedadId;
	}

	public String getEmpresaVariedadNombre() {
		return empresaVariedadNombre;
	}

	public void setEmpresaVariedadNombre(String empresaVariedadNombre) {
		this.empresaVariedadNombre = empresaVariedadNombre;
	}

	public EstadoEntregaEnum getEstado() {
		return estado;
	}

	public void setEstado(EstadoEntregaEnum estado) {
		this.estado = estado;
	}

	public List<EntregaBoncheDTO> getEntregaBonches() {
		return entregaBonches;
	}

	public void setEntregaBonches(List<EntregaBoncheDTO> entregaBonches) {
		this.entregaBonches = entregaBonches;
	}

	
}
