package com.keysist.roses.model.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.keysist.roses.model.enums.ValorSiNoEnum;

public class CostoVariedadDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1024807160274937932L;

	private Integer idEmpresa;
	private Integer id;
	private Integer idEmpresaVariedad;
	private Integer idVariedad;
	private String nombreVariedad;
	private Integer idCostoVariedad;
	private Date fechaInicio;
	private Date fechaFin;
	private ValorSiNoEnum estado;
	private List<CostoLongitudDTO> costoLongitudes;

	public CostoVariedadDTO() {
		// TODO Auto-generated constructor stub
	}

	public Integer getIdEmpresa() {
		return idEmpresa;
	}

	public void setIdEmpresa(Integer idEmpresa) {
		this.idEmpresa = idEmpresa;
	}

	public List<CostoLongitudDTO> getCostoLongitudes() {
		return costoLongitudes;
	}

	public void setCostoLongitudes(List<CostoLongitudDTO> costoLongitudes) {
		this.costoLongitudes = costoLongitudes;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getIdEmpresaVariedad() {
		return idEmpresaVariedad;
	}

	public void setIdEmpresaVariedad(Integer idEmpresaVariedad) {
		this.idEmpresaVariedad = idEmpresaVariedad;
	}

	public Integer getIdVariedad() {
		return idVariedad;
	}

	public void setIdVariedad(Integer idVariedad) {
		this.idVariedad = idVariedad;
	}

	public String getNombreVariedad() {
		return nombreVariedad;
	}

	public void setNombreVariedad(String nombreVariedad) {
		this.nombreVariedad = nombreVariedad;
	}

	public Integer getIdCostoVariedad() {
		return idCostoVariedad;
	}

	public void setIdCostoVariedad(Integer idCostoVariedad) {
		this.idCostoVariedad = idCostoVariedad;
	}

	public Date getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public Date getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}

	public ValorSiNoEnum getEstado() {
		return estado;
	}

	public void setEstado(ValorSiNoEnum estado) {
		this.estado = estado;
	}

}
