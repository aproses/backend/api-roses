package com.keysist.roses.model.dto;

import java.io.Serializable;

public class TalloVariedadDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5494631274223867891L;

	private Integer empresaVariedadId;
	private String empresaVariedadNombre;
	private Integer total;
	private Integer semana;
	private Integer semanaDatos;
	private String tipoReporte;
	private String tipoReporteCodigo;

	public TalloVariedadDTO() {
		// TODO Auto-generated constructor stub
	}

	public TalloVariedadDTO(Integer empresaVariedadId, String empresaVariedadNombre, Integer total, Integer semana,
			String tipoReporte,Integer semanaDatos,String tipoReporteCodigo) {
		super();
		this.empresaVariedadId = empresaVariedadId;
		this.empresaVariedadNombre = empresaVariedadNombre;
		this.total = total;
		this.semana = semana;
		this.tipoReporte = tipoReporte;
		this.semanaDatos=semanaDatos;
		this.tipoReporteCodigo=tipoReporteCodigo;
	}

	public Integer getEmpresaVariedadId() {
		return empresaVariedadId;
	}

	public void setEmpresaVariedadId(Integer empresaVariedadId) {
		this.empresaVariedadId = empresaVariedadId;
	}

	public String getEmpresaVariedadNombre() {
		return empresaVariedadNombre;
	}

	public void setEmpresaVariedadNombre(String empresaVariedadNombre) {
		this.empresaVariedadNombre = empresaVariedadNombre;
	}

	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

	public Integer getSemana() {
		return semana;
	}

	public void setSemana(Integer semana) {
		this.semana = semana;
	}

	public String getTipoReporte() {
		return tipoReporte;
	}

	public void setTipoReporte(String tipoReporte) {
		this.tipoReporte = tipoReporte;
	}

	public Integer getSemanaDatos() {
		return semanaDatos;
	}

	public void setSemanaDatos(Integer semanaDatos) {
		this.semanaDatos = semanaDatos;
	}

	public String getTipoReporteCodigo() {
		return tipoReporteCodigo;
	}

	public void setTipoReporteCodigo(String tipoReporteCodigo) {
		this.tipoReporteCodigo = tipoReporteCodigo;
	}
	
	

}
