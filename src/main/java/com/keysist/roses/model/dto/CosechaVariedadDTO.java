package com.keysist.roses.model.dto;

import java.io.Serializable;
import java.time.LocalDate;

import com.keysist.roses.model.entity.CosechaVariedad;
import com.keysist.roses.model.entity.EmpresaVariedad;
import com.keysist.roses.model.enums.EstadoCosechaVariedadEnum;

public class CosechaVariedadDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5494631274223867891L;

	private Integer id;
	private Integer cantidadMalla;
	private Integer talloMalla;
	private Integer totalTallo;
	private Integer totalTalloNacional;
	private Integer empresaVariedadId;
	private String empresaVariedadNombre;
	private String color;
	private EstadoCosechaVariedadEnum estado;
	private Integer totalTalloDisponible;
	private LocalDate fechaCorte;

	private Integer semana;
	private Integer anio;

	public CosechaVariedadDTO() {
		// TODO Auto-generated constructor stub
	}

	public CosechaVariedadDTO(CosechaVariedad cv) {
		// TODO Auto-generated constructor stub
		id = cv.getId();
		cantidadMalla = cv.getCantidadMalla();
		talloMalla = cv.getTalloMalla();
		empresaVariedadId = cv.getEmpresaVariedad().getId();
		empresaVariedadNombre = cv.getEmpresaVariedad().getVariedad().getNombre();
		totalTallo = cv.getTotalTallo();
		totalTalloNacional = cv.getTotalTalloNacional();
		totalTalloDisponible = cv.getTotalTalloDisponible();
		estado = cv.getEstado();
	}

	public CosechaVariedadDTO(EmpresaVariedad uv) {
		// TODO Auto-generated constructor stub
		cantidadMalla = 0;
		talloMalla = uv.getTalloMalla();
		empresaVariedadId = uv.getId();
		empresaVariedadNombre = uv.getVariedad().getNombre();
		estado = EstadoCosechaVariedadEnum.COSECHADO;
	}

	public CosechaVariedadDTO(Integer cantidadMalla, Integer empresaVariedadId, String empresaVariedadNombre,
			Integer totalTalloDisponible, String color) {
		super();
		this.cantidadMalla = cantidadMalla;
		this.empresaVariedadId = empresaVariedadId;
		this.empresaVariedadNombre = empresaVariedadNombre;
		this.totalTalloDisponible = totalTalloDisponible;
		this.color = color;
	}

	public CosechaVariedadDTO(Integer empresaVariedadId, String empresaVariedadNombre, Integer totalTalloDisponible,
			LocalDate fechaCorte) {
		this(null, empresaVariedadId, empresaVariedadNombre, totalTalloDisponible, null);
		this.fechaCorte = fechaCorte;
	}

	public CosechaVariedadDTO(Integer empresaVariedadId, String empresaVariedadNombre, Integer totalTalloDisponible,
			Integer semana) {
		this(null, empresaVariedadId, empresaVariedadNombre, totalTalloDisponible, null);
		this.semana = semana;
	}

	public CosechaVariedadDTO(Integer empresaVariedadId, String empresaVariedadNombre, Integer totalTalloDisponible,
			Integer semana, String codigoColor, Integer anio) {
		this(null, empresaVariedadId, empresaVariedadNombre, totalTalloDisponible, codigoColor);
		this.semana = semana;
		this.anio = anio;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getCantidadMalla() {
		return cantidadMalla;
	}

	public void setCantidadMalla(Integer cantidadMalla) {
		this.cantidadMalla = cantidadMalla;
	}

	public Integer getTalloMalla() {
		return talloMalla;
	}

	public void setTalloMalla(Integer talloMalla) {
		this.talloMalla = talloMalla;
	}

	public Integer getTotalTallo() {
		return totalTallo;
	}

	public void setTotalTallo(Integer totalTallo) {
		this.totalTallo = totalTallo;
	}

	public Integer getEmpresaVariedadId() {
		return empresaVariedadId;
	}

	public void setEmpresaVariedadId(Integer empresaVariedadId) {
		this.empresaVariedadId = empresaVariedadId;
	}

	public String getEmpresaVariedadNombre() {
		return empresaVariedadNombre;
	}

	public void setEmpresaVariedadNombre(String empresaVariedadNombre) {
		this.empresaVariedadNombre = empresaVariedadNombre;
	}

	public EstadoCosechaVariedadEnum getEstado() {
		return estado;
	}

	public void setEstado(EstadoCosechaVariedadEnum estado) {
		this.estado = estado;
	}

	public Integer getTotalTalloDisponible() {
		return totalTalloDisponible;
	}

	public void setTotalTalloDisponible(Integer totalTalloDisponible) {
		this.totalTalloDisponible = totalTalloDisponible;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public LocalDate getFechaCorte() {
		return fechaCorte;
	}

	public void setFechaCorte(LocalDate fechaCorte) {
		this.fechaCorte = fechaCorte;
	}

	public Integer getSemana() {
		return semana;
	}

	public void setSemana(Integer semana) {
		this.semana = semana;
	}

	public Integer getTotalTalloNacional() {
		return totalTalloNacional;
	}

	public void setTotalTalloNacional(Integer totalTalloNacional) {
		this.totalTalloNacional = totalTalloNacional;
	}

	public Integer getAnio() {
		return anio;
	}

	public void setAnio(Integer anio) {
		this.anio = anio;
	}

}
