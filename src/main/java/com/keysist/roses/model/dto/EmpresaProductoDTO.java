package com.keysist.roses.model.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import com.keysist.roses.model.entity.Catalogo;
import com.keysist.roses.model.entity.EmpresaProducto;
import com.keysist.roses.model.enums.ValorSiNoEnum;

public class EmpresaProductoDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5494631274223867891L;

	private Integer id;
	private Integer productoId;
	private String nombre;
	private Integer empresaId;
	private String caracteristicas;
	private BigDecimal precio;
	private Integer unidadMedidaId;
	private String unidadMedidaNombre;
	private BigDecimal dosis;
	private BigDecimal disponible;
	private ValorSiNoEnum activo;
	public List<Catalogo> plagas;
	public List<ProductoComposicionDTO> composicion;
	private Boolean esNitrato;
	private BigDecimal disponibleMinimo;
	private BigDecimal cantidadPorUnidad;
	private List<Catalogo> tipos;
	public EmpresaProductoDTO() {
		// TODO Auto-generated constructor stub
	}

	public EmpresaProductoDTO(EmpresaProducto p) {
		setProductoId(p.getProducto().getId());
		setActivo(p.getActivo());
		setCaracteristicas(p.getProducto().getCaracteristicas());
		setDisponible(p.getDisponible());
		setDosis(p.getDosis());
		setId(p.getId());
		setNombre(p.getProducto().getNombre());
		setPrecio(p.getPrecio());
		setUnidadMedidaId(p.getProducto().getUnidadMedida().getId());
		setUnidadMedidaNombre(p.getProducto().getUnidadMedida().getNombre());
		setEmpresaId(p.getEmpresa().getId());
		setEsNitrato(p.getProducto().getEsNitrato());
		setDisponibleMinimo(p.getDisponibleMinimo());
		setCantidadPorUnidad(p.getCantidadPorUnidad());
	}

	public EmpresaProductoDTO(BigDecimal disponible, BigDecimal dosis, String nombre, Integer id,
			String unidadMedidaNombre,BigDecimal precio, BigDecimal cantidadUnidad) {
		setDisponible(disponible);
		setDosis(dosis);
		setId(id);
		setNombre(nombre);
		setUnidadMedidaNombre(unidadMedidaNombre);
		setPrecio(precio);
		setCantidadPorUnidad(cantidadUnidad);
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getCaracteristicas() {
		return caracteristicas;
	}

	public void setCaracteristicas(String caracteristicas) {
		this.caracteristicas = caracteristicas;
	}

	public BigDecimal getPrecio() {
		return precio;
	}

	public void setPrecio(BigDecimal precio) {
		this.precio = precio;
	}

	public Integer getUnidadMedidaId() {
		return unidadMedidaId;
	}

	public void setUnidadMedidaId(Integer unidadMedidaId) {
		this.unidadMedidaId = unidadMedidaId;
	}

	public String getUnidadMedidaNombre() {
		return unidadMedidaNombre;
	}

	public void setUnidadMedidaNombre(String unidadMedidaNombre) {
		this.unidadMedidaNombre = unidadMedidaNombre;
	}

	public BigDecimal getDosis() {
		return dosis;
	}

	public void setDosis(BigDecimal dosis) {
		this.dosis = dosis;
	}

	public BigDecimal getDisponible() {
		return disponible != null ? disponible : BigDecimal.ZERO;
	}

	public void setDisponible(BigDecimal disponible) {
		this.disponible = disponible;
	}

	public ValorSiNoEnum getActivo() {
		return activo;
	}

	public Integer getEmpresaId() {
		return empresaId;
	}

	public void setEmpresaId(Integer empresaId) {
		this.empresaId = empresaId;
	}

	public void setActivo(ValorSiNoEnum activo) {
		this.activo = activo;
	}

	public List<Catalogo> getPlagas() {
		return plagas;
	}

	public void setPlagas(List<Catalogo> plagas) {
		this.plagas = plagas;
	}

	public List<ProductoComposicionDTO> getComposicion() {
		return composicion;
	}

	public void setComposicion(List<ProductoComposicionDTO> composicion) {
		this.composicion = composicion;
	}

	public Boolean getEsNitrato() {
		return esNitrato;
	}

	public void setEsNitrato(Boolean esNitrato) {
		this.esNitrato = esNitrato;
	}

	public Integer getProductoId() {
		return productoId;
	}

	public void setProductoId(Integer productoId) {
		this.productoId = productoId;
	}

	public BigDecimal getDisponibleMinimo() {
		return disponibleMinimo;
	}

	public void setDisponibleMinimo(BigDecimal disponibleMinimo) {
		this.disponibleMinimo = disponibleMinimo;
	}

	public BigDecimal getCantidadPorUnidad() {
		return cantidadPorUnidad;
	}

	public void setCantidadPorUnidad(BigDecimal cantidadPorUnidad) {
		this.cantidadPorUnidad = cantidadPorUnidad;
	}

	public List<Catalogo> getTipos() {
		return tipos;
	}

	public void setTipos(List<Catalogo> tipos) {
		this.tipos = tipos;
	}
	
	

}
