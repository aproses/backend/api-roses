package com.keysist.roses.model.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import com.keysist.roses.model.entity.Catalogo;
import com.keysist.roses.model.entity.Producto;

public class ProductoDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5494631274223867891L;

	private Integer id;
	private String nombre;
	private String caracteristicas;
	private Integer unidadMedidaId;
	private String unidadMedidaNombre;
	private BigDecimal dosis;
	public List<Catalogo> plagas;
	public List<ProductoComposicionDTO> composicion;
	private List<Catalogo> tipos;

	private Boolean esNitrato;

	public ProductoDTO() {
		// TODO Auto-generated constructor stub
	}

	public ProductoDTO(Producto p) {
		setCaracteristicas(p.getCaracteristicas());
		setDosis(p.getDosis());
		setId(p.getId());
		setNombre(p.getNombre());
		setUnidadMedidaId(p.getUnidadMedida().getId());
		setUnidadMedidaNombre(p.getUnidadMedida().getNombre());
		setEsNitrato(p.getEsNitrato());
	}

	public ProductoDTO(BigDecimal dosis, String nombre, Integer id, String unidadMedidaNombre) {
		setDosis(dosis);
		setId(id);
		setNombre(nombre);
		setUnidadMedidaNombre(unidadMedidaNombre);
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getCaracteristicas() {
		return caracteristicas;
	}

	public void setCaracteristicas(String caracteristicas) {
		this.caracteristicas = caracteristicas;
	}

	public Integer getUnidadMedidaId() {
		return unidadMedidaId;
	}

	public void setUnidadMedidaId(Integer unidadMedidaId) {
		this.unidadMedidaId = unidadMedidaId;
	}

	public String getUnidadMedidaNombre() {
		return unidadMedidaNombre;
	}

	public void setUnidadMedidaNombre(String unidadMedidaNombre) {
		this.unidadMedidaNombre = unidadMedidaNombre;
	}

	public BigDecimal getDosis() {
		return dosis;
	}

	public void setDosis(BigDecimal dosis) {
		this.dosis = dosis;
	}

	public Boolean getEsNitrato() {
		return esNitrato;
	}

	public void setEsNitrato(Boolean esNitrato) {
		this.esNitrato = esNitrato;
	}

	public List<Catalogo> getPlagas() {
		return plagas;
	}

	public void setPlagas(List<Catalogo> plagas) {
		this.plagas = plagas;
	}

	public List<ProductoComposicionDTO> getComposicion() {
		return composicion;
	}

	public void setComposicion(List<ProductoComposicionDTO> composicion) {
		this.composicion = composicion;
	}

	public List<Catalogo> getTipos() {
		return tipos;
	}

	public void setTipos(List<Catalogo> tipos) {
		this.tipos = tipos;
	}

}
