package com.keysist.roses.model.dto;

import java.io.Serializable;

import com.keysist.roses.model.entity.AplicacionVariedad;

public class AplicacionVariedadDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5494631274223867891L;

	private Integer id;
	private Integer variedadId;
	private String variedadNombre;

	private Integer camasEstandart;
	public AplicacionVariedadDTO() {
		// TODO Auto-generated constructor stub
	}

	public AplicacionVariedadDTO(AplicacionVariedad av) {
		// TODO Auto-generated constructor stub
		setId(av.getId());
		setVariedadId(av.getEmpresaVariedad().getId());
		setVariedadNombre(av.getEmpresaVariedad().getVariedad().getNombre());
		setCamasEstandart(av.getEmpresaVariedad().getCamasEstandart());
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getVariedadId() {
		return variedadId;
	}

	public void setVariedadId(Integer variedadId) {
		this.variedadId = variedadId;
	}

	public String getVariedadNombre() {
		return variedadNombre;
	}

	public void setVariedadNombre(String variedadNombre) {
		this.variedadNombre = variedadNombre;
	}

	public Integer getCamasEstandart() {
		return camasEstandart;
	}

	public void setCamasEstandart(Integer camasEstandart) {
		this.camasEstandart = camasEstandart;
	}
	

}
