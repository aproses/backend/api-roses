package com.keysist.roses.model.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

import com.keysist.roses.model.entity.Aplicacion;
import com.keysist.roses.model.enums.EstadoAplicacionEnum;

public class AplicacionDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5494631274223867891L;
	private Integer id;
	private Integer empresaBloqueId;
	private String empresaBloqueNumero;
	private LocalDate fechaAplicacion;
	private Long fechaAplicacionTime;
	private Long diasFrecuencia;

	private LocalTime horaInicio;
	private LocalTime horaFin;
	private Long duracion;
	private Integer cantidadLitros;
	private Integer tipoId;
	private String tipoCodigo;
	private String tipoNombre;
	private String[] plagas;
	private List<AplicacionProductoDTO> productos;
	private List<AplicacionVariedadDTO> variedades;
	private EstadoAplicacionEnum estado;
	private String observacion;
	private BigDecimal ph;

	public AplicacionDTO() {
		// TODO Auto-generated constructor stub
	}

	public AplicacionDTO(Aplicacion ap, List<AplicacionProductoDTO> productos, List<AplicacionVariedadDTO> variedades) {
		setId(ap.getId());
		setDuracion(ap.getDuracion());
		if (ap.getFechaAplicacion() != null) {
			setFechaAplicacion(ap.getFechaAplicacion());
			setFechaAplicacionTime(java.sql.Date.valueOf(ap.getFechaAplicacion()).getTime());
		}
		if (ap.getEmpresaBloque() != null) {
			setEmpresaBloqueId(ap.getEmpresaBloque().getId());
			setEmpresaBloqueNumero(ap.getEmpresaBloque().getNumero());
		}
		setPh(ap.getPh());
		setHoraInicio(ap.getHoraInicio());
		setHoraFin(ap.getHoraFin());
		setCantidadLitros(ap.getCantidadLitros());
		setPlagas(ap.getPlagas());
		setTipoId(ap.getTipo().getId());
		setTipoNombre(ap.getTipo().getNombre());
		setProductos(productos);
		setVariedades(variedades);
		setEstado(ap.getEstado());
		setTipoCodigo(ap.getTipo().getCodigo());
		setObservacion(ap.getObservacion());
		setDiasFrecuencia(ap.getDiasFrecuencia());
		if (ap.getEmpresaBloque() != null) {
			setEmpresaBloqueId(ap.getEmpresaBloque().getId());
			setEmpresaBloqueNumero(ap.getEmpresaBloque().getNumero());
		}

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getEmpresaBloqueId() {
		return empresaBloqueId;
	}

	public void setEmpresaBloqueId(Integer empresaBloqueId) {
		this.empresaBloqueId = empresaBloqueId;
	}

	public LocalDate getFechaAplicacion() {
		return fechaAplicacion;
	}

	public void setFechaAplicacion(LocalDate fechaAplicacion) {
		this.fechaAplicacion = fechaAplicacion;
	}

	public LocalTime getHoraInicio() {
		return horaInicio;
	}

	public void setHoraInicio(LocalTime horaInicio) {
		this.horaInicio = horaInicio;
	}

	public LocalTime getHoraFin() {
		return horaFin;
	}

	public void setHoraFin(LocalTime horaFin) {
		this.horaFin = horaFin;
	}

	public Long getDuracion() {
		return duracion;
	}

	public void setDuracion(Long duracion) {
		this.duracion = duracion;
	}

	public Integer getCantidadLitros() {
		return cantidadLitros;
	}

	public void setCantidadLitros(Integer cantidadLitros) {
		this.cantidadLitros = cantidadLitros;
	}

	public Integer getTipoId() {
		return tipoId;
	}

	public void setTipoId(Integer tipoId) {
		this.tipoId = tipoId;
	}

	public String getTipoNombre() {
		return tipoNombre;
	}

	public void setTipoNombre(String tipoNombre) {
		this.tipoNombre = tipoNombre;
	}

	public String[] getPlagas() {
		return plagas;
	}

	public void setPlagas(String[] plagas) {
		this.plagas = plagas;
	}

	public List<AplicacionProductoDTO> getProductos() {
		return productos;
	}

	public void setProductos(List<AplicacionProductoDTO> productos) {
		this.productos = productos;
	}

	public List<AplicacionVariedadDTO> getVariedades() {
		return variedades;
	}

	public void setVariedades(List<AplicacionVariedadDTO> variedades) {
		this.variedades = variedades;
	}

	public EstadoAplicacionEnum getEstado() {
		return estado;
	}

	public void setEstado(EstadoAplicacionEnum estado) {
		this.estado = estado;
	}

	public String getTipoCodigo() {
		return tipoCodigo;
	}

	public void setTipoCodigo(String tipoCodigo) {
		this.tipoCodigo = tipoCodigo;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public Long getFechaAplicacionTime() {
		return fechaAplicacionTime;
	}

	public void setFechaAplicacionTime(Long fechaAplicacionTime) {
		this.fechaAplicacionTime = fechaAplicacionTime;
	}

	public Long getDiasFrecuencia() {
		return diasFrecuencia;
	}

	public void setDiasFrecuencia(Long diasFrecuencia) {
		this.diasFrecuencia = diasFrecuencia;
	}

	public String getEmpresaBloqueNumero() {
		return empresaBloqueNumero;
	}

	public void setEmpresaBloqueNumero(String empresaBloqueNumero) {
		this.empresaBloqueNumero = empresaBloqueNumero;
	}

	public BigDecimal getPh() {
		return ph;
	}

	public void setPh(BigDecimal ph) {
		this.ph = ph;
	}

}
