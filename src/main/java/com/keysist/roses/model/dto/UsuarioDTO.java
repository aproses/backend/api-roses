package com.keysist.roses.model.dto;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import com.keysist.roses.model.entity.Catalogo;
import com.keysist.roses.model.entity.Usuario;
import com.keysist.roses.model.enums.ValorSiNoEnum;

public class UsuarioDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6388777771853861328L;

	private Integer id;
	private EmpresaDTO empresa;
	private String nombres;
	private String apellidos;
	private String identificacion;
	private Integer intentos;
	private LocalDateTime fechaLogin;
	private ValorSiNoEnum activo;
	private Integer empresaId;
	private String contrasenia;
	private String isAnonymous;
	private Boolean resetearPassword;
	private List<PerfilDTO> perfiles;
	private List<Integer> listRolesId;
	private List<String> roles;
	private String correo;
	private List<Catalogo> diasAplicaciones;

	public UsuarioDTO() {
		// TODO Auto-generated constructor stub
	}

	public UsuarioDTO(Usuario u) {
		this.id = u.getId();
		this.nombres = u.getNombres();
		this.apellidos = u.getApellidos();
		this.identificacion = u.getIdentificacion();
		this.correo = u.getCorreo();
		if (u.getEmpresa() != null) {
			this.empresa = new EmpresaDTO(u.getEmpresa());
			this.empresaId = u.getEmpresa().getId();

		}
		this.activo = u.getActivo();
		this.contrasenia = u.getContrasenia();
		this.roles = new ArrayList<>();
	}

	public UsuarioDTO(Integer id) {
		super();
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public EmpresaDTO getEmpresa() {
		return empresa;
	}

	public void setEmpresa(EmpresaDTO empresa) {
		this.empresa = empresa;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getIdentificacion() {
		return identificacion;
	}

	public void setIdentificacion(String identificacion) {
		this.identificacion = identificacion;
	}

	public Integer getIntentos() {
		return intentos;
	}

	public void setIntentos(Integer intentos) {
		this.intentos = intentos;
	}

	public LocalDateTime getFechaLogin() {
		return fechaLogin;
	}

	public void setFechaLogin(LocalDateTime fechaLogin) {
		this.fechaLogin = fechaLogin;
	}

	public ValorSiNoEnum getActivo() {
		return activo;
	}

	public void setActivo(ValorSiNoEnum activo) {
		this.activo = activo;
	}

	public Integer getEmpresaId() {
		return empresaId;
	}

	public void setEmpresaId(Integer empresaId) {
		this.empresaId = empresaId;
	}

	public String getIsAnonymous() {
		return isAnonymous;
	}

	public void setIsAnonymous(String isAnonymous) {
		this.isAnonymous = isAnonymous;
	}

	public Boolean getResetearPassword() {
		return resetearPassword;
	}

	public void setResetearPassword(Boolean resetearPassword) {
		this.resetearPassword = resetearPassword;
	}

	public List<PerfilDTO> getPerfiles() {
		return perfiles;
	}

	public void setPerfiles(List<PerfilDTO> perfiles) {
		this.perfiles = perfiles;
	}

	public List<Integer> getListRolesId() {
		return listRolesId;
	}

	public void setListRolesId(List<Integer> listRolesId) {
		this.listRolesId = listRolesId;
	}

	public String getContrasenia() {
		return contrasenia;
	}

	public void setContrasenia(String contrasenia) {
		this.contrasenia = contrasenia;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public List<String> getRoles() {
		return roles;
	}

	public void setRoles(List<String> roles) {
		this.roles = roles;
	}

	public List<Catalogo> getDiasAplicaciones() {
		return diasAplicaciones;
	}

	public void setDiasAplicaciones(List<Catalogo> diasAplicaciones) {
		this.diasAplicaciones = diasAplicaciones;
	}

}
