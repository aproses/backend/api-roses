package com.keysist.roses.model.dto;

import java.io.Serializable;

public class DisponibilidadDetalleDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6388777771853861328L;
	private Integer longitud;
	private Integer bonches;

	public DisponibilidadDetalleDTO() {
	}

	public Integer getLongitud() {
		return longitud;
	}

	public void setLongitud(Integer longitud) {
		this.longitud = longitud;
	}

	public Integer getBonches() {
		return bonches;
	}

	public void setBonches(Integer bonches) {
		this.bonches = bonches;
	}

}
