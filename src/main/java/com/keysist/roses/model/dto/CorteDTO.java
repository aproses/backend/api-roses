package com.keysist.roses.model.dto;

import java.io.Serializable;
import java.util.List;

public class CorteDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5494631274223867891L;

	private long fechaCosechaDesde;
	private long fechaCosechaHasta;
	private long ciclo;
	private Boolean adelante;

	private List<Integer> idEmpresaVariedads;

	public CorteDTO(long fechaCosechaDesde, long fechaCosechaHasta, List<Integer> idEmpresaVariedads) {
		super();
		this.fechaCosechaDesde = fechaCosechaDesde;
		this.fechaCosechaHasta = fechaCosechaHasta;
		this.idEmpresaVariedads = idEmpresaVariedads;
	}

	public long getFechaCosechaDesde() {
		return fechaCosechaDesde;
	}

	public void setFechaCosechaDesde(long fechaCosechaDesde) {
		this.fechaCosechaDesde = fechaCosechaDesde;
	}

	public long getFechaCosechaHasta() {
		return fechaCosechaHasta;
	}

	public void setFechaCosechaHasta(long fechaCosechaHasta) {
		this.fechaCosechaHasta = fechaCosechaHasta;
	}

	public List<Integer> getIdEmpresaVariedads() {
		return idEmpresaVariedads;
	}

	public void setIdEmpresaVariedads(List<Integer> idEmpresaVariedads) {
		this.idEmpresaVariedads = idEmpresaVariedads;
	}

	public long getCiclo() {
		return ciclo;
	}

	public void setCiclo(long ciclo) {
		this.ciclo = ciclo;
	}

	public Boolean getAdelante() {
		return adelante;
	}

	public void setAdelante(Boolean adelante) {
		this.adelante = adelante;
	}

}
