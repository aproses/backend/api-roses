package com.keysist.roses.model.dto;

import java.io.Serializable;

import com.keysist.roses.model.entity.Perfil;
import com.keysist.roses.model.enums.ValorSiNoEnum;


public class PerfilDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5494631274223867891L;
	private Integer id;
	private String nombre;
	private ValorSiNoEnum activo;

	public PerfilDTO() {
		// TODO Auto-generated constructor stub
	}

	public PerfilDTO(Perfil p) {
		super();
		this.id = p.getId();
		this.activo = p.getActivo();
		this.nombre = p.getNombre();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public ValorSiNoEnum getActivo() {
		return activo;
	}

	public void setActivo(ValorSiNoEnum activo) {
		this.activo = activo;
	}

}
