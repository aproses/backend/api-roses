package com.keysist.roses.model.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class ProductoConsumoDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5494631274223867891L;

	private Integer productoId;
	private String productoNombre;
	private BigDecimal usado;
	private BigDecimal costo;
	private BigDecimal costoUnitario;
	private String unidadMedida;
	private String tipoAplicacion;
	private BigDecimal cantidadPorUnidad;
	private Integer mes;

	public ProductoConsumoDTO() {
		// TODO Auto-generated constructor stub
	}
	

	public ProductoConsumoDTO(BigDecimal costo, String tipoAplicacion, Integer mes) {
		super();
		this.costo = costo;
		this.tipoAplicacion = tipoAplicacion;
		this.mes = mes;
	}


	public ProductoConsumoDTO(Integer productoId, String productoNombre, BigDecimal usado, String unidadMedida) {
		super();
		this.productoId = productoId;
		this.productoNombre = productoNombre;
		this.usado = usado;
		this.unidadMedida = unidadMedida;
	}

	public ProductoConsumoDTO(Integer productoId, String productoNombre, BigDecimal usado, String unidadMedida,
			String tipoAplicacion, BigDecimal costo, BigDecimal costoUnitario, BigDecimal cantidadPorUnidad) {
		this(productoId, productoNombre, usado, unidadMedida);
		this.tipoAplicacion = tipoAplicacion;
		this.costo = costo;
		this.costoUnitario = costoUnitario;
		this.cantidadPorUnidad = cantidadPorUnidad;

	}

	public Integer getProductoId() {
		return productoId;
	}

	public void setProductoId(Integer productoId) {
		this.productoId = productoId;
	}

	public String getProductoNombre() {
		return productoNombre;
	}

	public void setProductoNombre(String productoNombre) {
		this.productoNombre = productoNombre;
	}

	public BigDecimal getUsado() {
		return usado;
	}

	public void setUsado(BigDecimal usado) {
		this.usado = usado;
	}

	public String getUnidadMedida() {
		return unidadMedida;
	}

	public void setUnidadMedida(String unidadMedida) {
		this.unidadMedida = unidadMedida;
	}

	public String getTipoAplicacion() {
		return tipoAplicacion;
	}

	public void setTipoAplicacion(String tipoAplicacion) {
		this.tipoAplicacion = tipoAplicacion;
	}

	public BigDecimal getCosto() {
		return costo;
	}

	public void setCosto(BigDecimal costo) {
		this.costo = costo;
	}

	public BigDecimal getCostoUnitario() {
		return costoUnitario;
	}

	public void setCostoUnitario(BigDecimal costoUnitario) {
		this.costoUnitario = costoUnitario;
	}

	public BigDecimal getCantidadPorUnidad() {
		return cantidadPorUnidad;
	}

	public void setCantidadPorUnidad(BigDecimal cantidadPorUnidad) {
		this.cantidadPorUnidad = cantidadPorUnidad;
	}

	public Integer getMes() {
		return mes;
	}

	public void setMes(Integer mes) {
		this.mes = mes;
	}

}
