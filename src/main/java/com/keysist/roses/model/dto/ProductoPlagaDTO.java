package com.keysist.roses.model.dto;

import java.io.Serializable;

import com.keysist.roses.model.entity.ProductoPlaga;

public class ProductoPlagaDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5494631274223867891L;

	private Integer id;

	private String plagaNombre;

	private Integer plagaId;

	private String productoNombre;
	private Integer productoId;

	public ProductoPlagaDTO() {
		// TODO Auto-generated constructor stub
	}
	
	public ProductoPlagaDTO(ProductoPlaga pp) {
		// TODO Auto-generated constructor stub
		id=pp.getId();
		plagaId=pp.getPlaga().getId();
		plagaNombre=pp.getPlaga().getNombre();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getPlagaNombre() {
		return plagaNombre;
	}

	public void setPlagaNombre(String plagaNombre) {
		this.plagaNombre = plagaNombre;
	}

	public Integer getPlagaId() {
		return plagaId;
	}

	public void setPlagaId(Integer plagaId) {
		this.plagaId = plagaId;
	}

	public String getProductoNombre() {
		return productoNombre;
	}

	public void setProductoNombre(String productoNombre) {
		this.productoNombre = productoNombre;
	}

	public Integer getProductoId() {
		return productoId;
	}

	public void setProductoId(Integer productoId) {
		this.productoId = productoId;
	}

}
