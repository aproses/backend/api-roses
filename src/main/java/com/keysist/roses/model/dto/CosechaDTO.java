package com.keysist.roses.model.dto;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

import com.keysist.roses.model.entity.Cosecha;

public class CosechaDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5494631274223867891L;

	private Integer id;
	private String observacion;
	private Integer empresaId;
	private Integer semana;
	private Long fechaCorte;

	private LocalDate fechaCorteL;
	private Integer totalMallas;
	private Integer totalTallos;

	private List<CosechaVariedadDTO> cosechaVariedadDTOs;

	public CosechaDTO() {
		// TODO Auto-generated constructor stub
	}

	public CosechaDTO(Cosecha c) {
		// TODO Auto-generated constructor stub
		id = c.getId();
		empresaId = c.getEmpresa().getId();
		fechaCorte = java.sql.Date.valueOf(c.getFechaCorte()).getTime();
		fechaCorteL=c.getFechaCorte();
		observacion = c.getObservacion();

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getEmpresaId() {
		return empresaId;
	}

	public void setEmpresaId(Integer empresaId) {
		this.empresaId = empresaId;
	}

	public Long getFechaCorte() {
		return fechaCorte;
	}

	public void setFechaCorte(Long fechaCorte) {
		this.fechaCorte = fechaCorte;
	}

	public List<CosechaVariedadDTO> getCosechaVariedadDTOs() {
		return cosechaVariedadDTOs;
	}

	public void setCosechaVariedadDTOs(List<CosechaVariedadDTO> cosechaVariedadDTOs) {
		this.cosechaVariedadDTOs = cosechaVariedadDTOs;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public Integer getSemana() {
		return semana;
	}

	public void setSemana(Integer semana) {
		this.semana = semana;
	}

	public Integer getTotalMallas() {
		return totalMallas;
	}

	public void setTotalMallas(Integer totalMallas) {
		this.totalMallas = totalMallas;
	}

	public Integer getTotalTallos() {
		return totalTallos;
	}

	public void setTotalTallos(Integer totalTallos) {
		this.totalTallos = totalTallos;
	}

	public LocalDate getFechaCorteL() {
		return fechaCorteL;
	}

	public void setFechaCorteL(LocalDate fechaCorteL) {
		this.fechaCorteL = fechaCorteL;
	}
	

}
