package com.keysist.roses.model.dto;

import java.io.Serializable;

import javax.persistence.Transient;

public class CamaDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6388777771853861328L;
	private Integer id;
	private String camaNumero;
	private String empresaVariedadNombre;
	private Integer empresaVariedadId;

	@Transient
	private Integer bloqueId;

	@Transient
	private String bloqueNombre;

	public CamaDTO() {
		// TODO Auto-generated constructor stub
	}

	public CamaDTO(Integer id, String camaNumero, String empresaVariedadNombre, Integer empresaVariedadId) {
		super();
		this.id = id;
		this.camaNumero = camaNumero;
		this.empresaVariedadNombre = empresaVariedadNombre;
		this.empresaVariedadId = empresaVariedadId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCamaNumero() {
		return camaNumero;
	}

	public void setCamaNumero(String camaNumero) {
		this.camaNumero = camaNumero;
	}

	public String getEmpresaVariedadNombre() {
		return empresaVariedadNombre;
	}

	public void setEmpresaVariedadNombre(String empresaVariedadNombre) {
		this.empresaVariedadNombre = empresaVariedadNombre;
	}

	public Integer getEmpresaVariedadId() {
		return empresaVariedadId;
	}

	public void setEmpresaVariedadId(Integer empresaVariedadId) {
		this.empresaVariedadId = empresaVariedadId;
	}

	public Integer getBloqueId() {
		return bloqueId;
	}

	public void setBloqueId(Integer bloqueId) {
		this.bloqueId = bloqueId;
	}

	public String getBloqueNombre() {
		return bloqueNombre;
	}

	public void setBloqueNombre(String bloqueNombre) {
		this.bloqueNombre = bloqueNombre;
	}

}
