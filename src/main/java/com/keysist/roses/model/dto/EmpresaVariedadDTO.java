package com.keysist.roses.model.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;

import com.keysist.roses.model.entity.EmpresaVariedad;
import com.keysist.roses.model.enums.ValorSiNoEnum;

public class EmpresaVariedadDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5494631274223867891L;

	private Integer id;
	private Integer idEmpresa;
	private String nombreVariedad;
	private Integer idVariedad;
	private ValorSiNoEnum disponible;
	private Integer talloMalla;
	private Integer talloMallaMax;
	private Double efectividad;
	private BigInteger diasCiclo;
	private BigDecimal produccionMes;
	private BigDecimal produccionPerdida;
	private Integer camasEstandart;
	private String[] bloques;

	public EmpresaVariedadDTO() {
		// TODO Auto-generated constructor stub
	}

	public EmpresaVariedadDTO(EmpresaVariedad v) {
		super();
		this.id = v.getId();
		this.idEmpresa = v.getEmpresa().getId();
		this.nombreVariedad = v.getVariedad().getNombre();
		this.idVariedad = v.getVariedad().getId();
		this.disponible = v.getDisponible();
		this.talloMalla = v.getTalloMalla();
		this.efectividad = v.getEfectividad();
		this.produccionMes = v.getProduccionMes();
		this.diasCiclo = v.getDiasCiclo();
		this.talloMallaMax = v.getTalloMallaMax();
		this.produccionPerdida = v.getProduccionPerdida();
		this.camasEstandart = v.getCamasEstandart();
		this.bloques = v.getBloques();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getTalloMalla() {
		return talloMalla;
	}

	public void setTalloMalla(Integer talloMalla) {
		this.talloMalla = talloMalla;
	}

	public ValorSiNoEnum getDisponible() {
		return disponible;
	}

	public void setDisponible(ValorSiNoEnum disponible) {
		this.disponible = disponible;
	}

	public Integer getIdVariedad() {
		return idVariedad;
	}

	public void setIdVariedad(Integer idVariedad) {
		this.idVariedad = idVariedad;
	}

	public String getNombreVariedad() {
		return nombreVariedad;
	}

	public void setNombreVariedad(String nombreVariedad) {
		this.nombreVariedad = nombreVariedad;
	}

	public Integer getIdEmpresa() {
		return idEmpresa;
	}

	public void setIdEmpresa(Integer idEmpresa) {
		this.idEmpresa = idEmpresa;
	}

	public Double getEfectividad() {
		return efectividad;
	}

	public void setEfectividad(Double efectividad) {
		this.efectividad = efectividad;
	}

	public BigInteger getDiasCiclo() {
		return diasCiclo;
	}

	public void setDiasCiclo(BigInteger diasCiclo) {
		this.diasCiclo = diasCiclo;
	}

	public BigDecimal getProduccionMes() {
		return produccionMes;
	}

	public void setProduccionMes(BigDecimal produccionMes) {
		this.produccionMes = produccionMes;
	}

	public Integer getTalloMallaMax() {
		return talloMallaMax;
	}

	public void setTalloMallaMax(Integer talloMallaMax) {
		this.talloMallaMax = talloMallaMax;
	}

	public BigDecimal getProduccionPerdida() {
		return produccionPerdida;
	}

	public void setProduccionPerdida(BigDecimal produccionPerdida) {
		this.produccionPerdida = produccionPerdida;
	}

	public Integer getCamasEstandart() {
		return camasEstandart;
	}

	public void setCamasEstandart(Integer camasEstandart) {
		this.camasEstandart = camasEstandart;
	}

	public String[] getBloques() {
		return bloques;
	}

	public void setBloques(String[] bloques) {
		this.bloques = bloques;
	}

}
