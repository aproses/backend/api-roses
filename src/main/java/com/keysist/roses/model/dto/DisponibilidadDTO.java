package com.keysist.roses.model.dto;

import java.io.Serializable;
import java.util.List;

public class DisponibilidadDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6388777771853861328L;
	private Integer id;
	private String variedad;
	private List<DisponibilidadDetalleDTO> detalle;

	public DisponibilidadDTO() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getVariedad() {
		return variedad;
	}

	public void setVariedad(String variedad) {
		this.variedad = variedad;
	}

	public List<DisponibilidadDetalleDTO> getDetalle() {
		return detalle;
	}

	public void setDetalle(List<DisponibilidadDetalleDTO> detalle) {
		this.detalle = detalle;
	}
	

}
