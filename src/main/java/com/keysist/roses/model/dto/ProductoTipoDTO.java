package com.keysist.roses.model.dto;

import java.io.Serializable;

import com.keysist.roses.model.entity.ProductoTipo;

public class ProductoTipoDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5494631274223867891L;

	private Integer id;
	private Integer tipoId;
	private String tipoNombre;

	public ProductoTipoDTO() {
		// TODO Auto-generated constructor stub
	}

	public ProductoTipoDTO(ProductoTipo av) {
		// TODO Auto-generated constructor stub
		setId(av.getId());
		setTipoId(av.getTipo().getId());
		setTipoNombre(av.getTipo().getNombre());

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getTipoId() {
		return tipoId;
	}

	public void setTipoId(Integer tipoId) {
		this.tipoId = tipoId;
	}

	public String getTipoNombre() {
		return tipoNombre;
	}

	public void setTipoNombre(String tipoNombre) {
		this.tipoNombre = tipoNombre;
	}

}
