package com.keysist.roses.model.dto;

import java.io.Serializable;

import com.keysist.roses.model.entity.EmpresaMesa;

public class EmpresaMesaDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5494631274223867891L;

	private Integer id;
	private Integer idEmpresa;
	private Integer idCatalogo;
	private String nombre;

	public EmpresaMesaDTO() {
		// TODO Auto-generated constructor stub
	}

	public EmpresaMesaDTO(EmpresaMesa v) {
		super();
		this.id = v.getId();
		this.idEmpresa = v.getEmpresa().getId();
		this.nombre = v.getMesa().getNombre();
		this.idCatalogo = v.getMesa().getId();

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getIdEmpresa() {
		return idEmpresa;
	}

	public void setIdEmpresa(Integer idEmpresa) {
		this.idEmpresa = idEmpresa;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Integer getIdCatalogo() {
		return idCatalogo;
	}

	public void setIdCatalogo(Integer idCatalogo) {
		this.idCatalogo = idCatalogo;
	}
	

}
