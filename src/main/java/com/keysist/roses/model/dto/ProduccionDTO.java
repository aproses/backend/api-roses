package com.keysist.roses.model.dto;

import java.io.Serializable;
import java.util.List;

public class ProduccionDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6388777771853861328L;
	private Integer totalMallasProcesadas;
	private Integer totalMallasRecepcionadas;
	private Integer totalMallasEnviadas;
	private Integer totalCosechado;
	private Integer totalEnviado;
	private Integer total;
	private List<ProduccionVariedadDTO> produccionVariedad;

	public ProduccionDTO() {
		// TODO Auto-generated constructor stub
	}

	public Integer getTotalMallasProcesadas() {
		return totalMallasProcesadas;
	}

	public void setTotalMallasProcesadas(Integer totalMallasProcesadas) {
		this.totalMallasProcesadas = totalMallasProcesadas;
	}

	public Integer getTotalMallasRecepcionadas() {
		return totalMallasRecepcionadas;
	}

	public void setTotalMallasRecepcionadas(Integer totalMallasRecepcionadas) {
		this.totalMallasRecepcionadas = totalMallasRecepcionadas;
	}

	public List<ProduccionVariedadDTO> getProduccionVariedad() {
		return produccionVariedad;
	}

	public void setProduccionVariedad(List<ProduccionVariedadDTO> produccionVariedad) {
		this.produccionVariedad = produccionVariedad;
	}

	public Integer getTotalMallasEnviadas() {
		return totalMallasEnviadas;
	}

	public void setTotalMallasEnviadas(Integer totalMallasEnviadas) {
		this.totalMallasEnviadas = totalMallasEnviadas;
	}

	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

	public Integer getTotalCosechado() {
		return totalCosechado;
	}

	public void setTotalCosechado(Integer totalCosechado) {
		this.totalCosechado = totalCosechado;
	}

	public Integer getTotalEnviado() {
		return totalEnviado;
	}

	public void setTotalEnviado(Integer totalEnviado) {
		this.totalEnviado = totalEnviado;
	}
	
	

}
