package com.keysist.roses.model.dto;

import java.io.Serializable;

import com.keysist.roses.model.entity.EmpresaBloque;

public class EmpresaBloqueDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5494631274223867891L;

	private Integer id;
	private Integer idEmpresa;
	private String numero;

	public EmpresaBloqueDTO() {
		// TODO Auto-generated constructor stub
	}

	public EmpresaBloqueDTO(EmpresaBloque v) {
		super();
		this.id = v.getId();
		this.idEmpresa = v.getEmpresa().getId();
		this.numero = v.getNumero();

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getIdEmpresa() {
		return idEmpresa;
	}

	public void setIdEmpresa(Integer idEmpresa) {
		this.idEmpresa = idEmpresa;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

}
