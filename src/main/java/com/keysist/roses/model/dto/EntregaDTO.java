package com.keysist.roses.model.dto;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;

import com.keysist.roses.model.entity.Entrega;
import com.keysist.roses.model.enums.EstadoEntregaEnum;
import com.keysist.roses.model.enums.TipoEntregaEnum;

public class EntregaDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6388777771853861328L;
	private Integer id;
	private String observacion;
	private LocalDate fechaEntrega;
	private LocalTime horaEntrega;
	private Long fechaEntregaTime;
	private LocalDateTime fechaProcesamiento;
	private Integer clienteId;
	private String clienteNombre;
	private Integer proveedorId;
	private String proveedorNombre;
	private LocalDateTime fechaFinalizado;
	private EstadoEntregaEnum estado;
	private Integer semana;
	private List<EntregaVariedadDTO> entregaVariedads;
	private TipoEntregaEnum tipo;
	private Integer mallas;
	private Integer totalTallos;

	public EntregaDTO() {
	}

	public EntregaDTO(Entrega er) {
		this.id = er.getId();
		this.observacion = er.getObservacion();
		this.fechaEntrega = er.getFechaEntrega();
		this.setFechaEntregaTime(java.sql.Date.valueOf( er.getFechaEntrega()).getTime());
		this.horaEntrega = er.getHoraEntrega();
		this.fechaProcesamiento = er.getFechaProcesamiento();
		if (er.getCliente() != null) {
			this.clienteId = er.getCliente().getId();
			this.clienteNombre = er.getCliente().getNombre();
		} else {
			this.clienteId = -1;
		}
		this.fechaFinalizado = er.getFechaFinalizado();
		this.estado = er.getEstado();
		this.semana = er.getSemana();
		this.proveedorId = er.getProveedor().getId();
		this.proveedorNombre = er.getProveedor().getNombre();
		this.tipo = er.getTipo();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public LocalDate getFechaEntrega() {
		return fechaEntrega;
	}

	public void setFechaEntrega(LocalDate fechaEntrega) {
		this.fechaEntrega = fechaEntrega;
	}

	public LocalTime getHoraEntrega() {
		return horaEntrega;
	}

	public void setHoraEntrega(LocalTime horaEntrega) {
		this.horaEntrega = horaEntrega;
	}

	public Long getFechaEntregaTime() {
		return fechaEntregaTime;
	}

	public void setFechaEntregaTime(Long fechaEntregaTime) {
		this.fechaEntregaTime = fechaEntregaTime;
	}

	public LocalDateTime getFechaProcesamiento() {
		return fechaProcesamiento;
	}

	public void setFechaProcesamiento(LocalDateTime fechaProcesamiento) {
		this.fechaProcesamiento = fechaProcesamiento;
	}

	public Integer getClienteId() {
		return clienteId;
	}

	public void setClienteId(Integer clienteId) {
		this.clienteId = clienteId;
	}

	public String getClienteNombre() {
		return clienteNombre;
	}

	public void setClienteNombre(String clienteNombre) {
		this.clienteNombre = clienteNombre;
	}

	public LocalDateTime getFechaFinalizado() {
		return fechaFinalizado;
	}

	public void setFechaFinalizado(LocalDateTime fechaFinalizado) {
		this.fechaFinalizado = fechaFinalizado;
	}

	public EstadoEntregaEnum getEstado() {
		return estado;
	}

	public void setEstado(EstadoEntregaEnum estado) {
		this.estado = estado;
	}

	public Integer getSemana() {
		return semana;
	}

	public void setSemana(Integer semana) {
		this.semana = semana;
	}

	public List<EntregaVariedadDTO> getEntregaVariedads() {
		return entregaVariedads;
	}

	public void setEntregaVariedads(List<EntregaVariedadDTO> entregaVariedads) {
		this.entregaVariedads = entregaVariedads;
	}

	public Integer getProveedorId() {
		return proveedorId;
	}

	public void setProveedorId(Integer proveedorId) {
		this.proveedorId = proveedorId;
	}

	public String getProveedorNombre() {
		return proveedorNombre;
	}

	public void setProveedorNombre(String proveedorNombre) {
		this.proveedorNombre = proveedorNombre;
	}

	public TipoEntregaEnum getTipo() {
		return tipo;
	}

	public void setTipo(TipoEntregaEnum tipo) {
		this.tipo = tipo;
	}

	public Integer getMallas() {
		return mallas;
	}

	public void setMallas(Integer mallas) {
		this.mallas = mallas;
	}

	public Integer getTotalTallos() {
		return totalTallos;
	}

	public void setTotalTallos(Integer totalTallos) {
		this.totalTallos = totalTallos;
	}

}
