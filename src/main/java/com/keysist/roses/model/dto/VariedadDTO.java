package com.keysist.roses.model.dto;

import java.io.Serializable;

import com.keysist.roses.model.entity.Variedad;
import com.keysist.roses.model.enums.ValorSiNoEnum;

public class VariedadDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5494631274223867891L;

	private Integer id;

	private String nombre;

	private String codigo;

	private String caracteristicas;

	private String pathFoto;

	private ValorSiNoEnum disponible;

	private Integer idColor;

	private String nombreColor;

	public VariedadDTO() {
		// TODO Auto-generated constructor stub
	}

	public VariedadDTO(Variedad v) {
		id = v.getId();
		nombre = v.getNombre();
		codigo = v.getCodigo();
		caracteristicas=v.getCaracteristicas();
		if (v.getColor() != null) {
			idColor = v.getColor().getId();
			nombreColor = v.getColor().getNombre();
		}
		disponible = v.getDisponible();
		// TODO Auto-generated constructor stub
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getCaracteristicas() {
		return caracteristicas;
	}

	public void setCaracteristicas(String caracteristicas) {
		this.caracteristicas = caracteristicas;
	}

	public String getPathFoto() {
		return pathFoto;
	}

	public void setPathFoto(String pathFoto) {
		this.pathFoto = pathFoto;
	}

	public ValorSiNoEnum getDisponible() {
		return disponible;
	}

	public void setDisponible(ValorSiNoEnum disponible) {
		this.disponible = disponible;
	}

	public Integer getIdColor() {
		return idColor;
	}

	public void setIdColor(Integer idColor) {
		this.idColor = idColor;
	}

	public String getNombreColor() {
		return nombreColor;
	}

	public void setNombreColor(String nombreColor) {
		this.nombreColor = nombreColor;
	}

}
