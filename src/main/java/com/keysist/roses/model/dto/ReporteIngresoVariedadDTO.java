package com.keysist.roses.model.dto;

import java.io.Serializable;

public class ReporteIngresoVariedadDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6388777771853861328L;

	private String variedadCodigo;
	private String variedadNombre;
	private Integer total;

	public ReporteIngresoVariedadDTO() {
	}

	public ReporteIngresoVariedadDTO(String variedadNombre, Integer total) {
		super();
		this.variedadNombre = variedadNombre;
		this.total = total;
	}

	public ReporteIngresoVariedadDTO(Object variedadNombre, Object total) {
		super();
		if (variedadNombre != null)
			this.variedadNombre = variedadNombre.toString();
		if (total != null)
			this.total = Integer.valueOf(total.toString());
	}

	public String getVariedadCodigo() {
		return variedadCodigo;
	}

	public void setVariedadCodigo(String variedadCodigo) {
		this.variedadCodigo = variedadCodigo;
	}

	public String getVariedadNombre() {
		return variedadNombre;
	}

	public void setVariedadNombre(String variedadNombre) {
		this.variedadNombre = variedadNombre;
	}

	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

}
