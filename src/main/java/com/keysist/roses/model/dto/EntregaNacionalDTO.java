package com.keysist.roses.model.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import com.keysist.roses.model.entity.Catalogo;
import com.keysist.roses.model.entity.EntregaNacional;
import com.keysist.roses.model.entity.EntregaPoscosecha;

public class EntregaNacionalDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5494631274223867891L;

	private Integer id;
	private BigDecimal precio;
	private Integer talloTotal;
	private Integer tallosIngresados;
	private Integer tallosProcesados;
	private Integer tallosNacionalTotal;
	private Long fechaProcesoTime;
	private LocalDateTime fechaProceso;
	private Integer entregaPoscosechaId;

	private Integer categoriaId;
	private String categoriaNombre;

	private Integer subCategoriaId;
	private String subCategoriaNombre;

	private Integer bloqueId;
	private String bloqueNumero;

	private Integer clasificadorId;
	private String clasificadorNombre;

	public EntregaNacionalDTO() {
	}

	public EntregaNacionalDTO(EntregaPoscosecha entregaPoscosecha) {
		// TODO Auto-generated constructor stub
		this.entregaPoscosechaId = entregaPoscosecha.getId();
		this.tallosIngresados = entregaPoscosecha.getTallos();
		this.tallosIngresados = entregaPoscosecha.getTallos();
		this.tallosProcesados = entregaPoscosecha.getExportacion();
		this.tallosNacionalTotal = entregaPoscosecha.getNacional();
	}

	public EntregaNacionalDTO(Catalogo categoria, Catalogo subCategoria, EntregaPoscosecha entregaPoscosecha) {
		this(entregaPoscosecha);
		this.categoriaId = categoria.getId();
		this.categoriaNombre = categoria.getNombre();
		this.subCategoriaId = subCategoria.getId();
		this.subCategoriaNombre = subCategoria.getNombre();
		this.talloTotal = 0;
	}

	public EntregaNacionalDTO(EntregaNacional det) {
		super();
		this.id = det.getId();
		this.talloTotal = det.getTalloTotal();
		this.precio = det.getPrecio();
		this.entregaPoscosechaId = det.getEntregaPoscosecha().getId();
		this.categoriaId = det.getCategoria().getId();
		this.categoriaNombre = det.getCategoria().getNombre();
		this.subCategoriaId = det.getSubCategoria().getId();
		this.subCategoriaNombre = det.getSubCategoria().getNombre();
		this.bloqueId = det.getBloque().getId();
		this.bloqueNumero = det.getBloque().getNumero();
		this.clasificadorId = det.getClasificador().getId();
		this.clasificadorNombre = det.getClasificador().getNombres();
		this.tallosIngresados = det.getEntregaPoscosecha().getTallos();
		this.tallosProcesados = det.getEntregaPoscosecha().getExportacion();
		this.tallosNacionalTotal = det.getEntregaPoscosecha().getNacional();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public BigDecimal getPrecio() {
		return precio;
	}

	public void setPrecio(BigDecimal precio) {
		this.precio = precio;
	}

	public Integer getTalloTotal() {
		return talloTotal != null ? talloTotal : 0;
	}

	public void setTalloTotal(Integer talloTotal) {
		this.talloTotal = talloTotal;
	}

	public Integer getEntregaPoscosechaId() {
		return entregaPoscosechaId;
	}

	public void setEntregaPoscosechaId(Integer entregaPoscosechaId) {
		this.entregaPoscosechaId = entregaPoscosechaId;
	}

	public Integer getCategoriaId() {
		return categoriaId;
	}

	public void setCategoriaId(Integer categoriaId) {
		this.categoriaId = categoriaId;
	}

	public String getCategoriaNombre() {
		return categoriaNombre;
	}

	public void setCategoriaNombre(String categoriaNombre) {
		this.categoriaNombre = categoriaNombre;
	}

	public Integer getSubCategoriaId() {
		return subCategoriaId;
	}

	public void setSubCategoriaId(Integer subCategoriaId) {
		this.subCategoriaId = subCategoriaId;
	}

	public String getSubCategoriaNombre() {
		return subCategoriaNombre;
	}

	public void setSubCategoriaNombre(String subCategoriaNombre) {
		this.subCategoriaNombre = subCategoriaNombre;
	}

	public Integer getBloqueId() {
		return bloqueId;
	}

	public void setBloqueId(Integer bloqueId) {
		this.bloqueId = bloqueId;
	}

	public String getBloqueNumero() {
		return bloqueNumero;
	}

	public void setBloqueNumero(String bloqueNumero) {
		this.bloqueNumero = bloqueNumero;
	}

	public Integer getClasificadorId() {
		return clasificadorId;
	}

	public void setClasificadorId(Integer clasificadorId) {
		this.clasificadorId = clasificadorId;
	}

	public String getClasificadorNombre() {
		return clasificadorNombre;
	}

	public void setClasificadorNombre(String clasificadorNombre) {
		this.clasificadorNombre = clasificadorNombre;
	}

	public Long getFechaProcesoTime() {
		return fechaProcesoTime;
	}

	public void setFechaProcesoTime(Long fechaProcesoTime) {
		this.fechaProcesoTime = fechaProcesoTime;
	}

	public LocalDateTime getFechaProceso() {
		return fechaProceso;
	}

	public void setFechaProceso(LocalDateTime fechaProceso) {
		this.fechaProceso = fechaProceso;
	}

	public Integer getTallosIngresados() {
		return tallosIngresados;
	}

	public void setTallosIngresados(Integer tallosIngresados) {
		this.tallosIngresados = tallosIngresados;
	}

	public Integer getTallosProcesados() {
		return tallosProcesados;
	}

	public void setTallosProcesados(Integer tallosProcesados) {
		this.tallosProcesados = tallosProcesados;
	}

	public Integer getTallosNacionalTotal() {
		return tallosNacionalTotal;
	}

	public void setTallosNacionalTotal(Integer tallosNacionalTotal) {
		this.tallosNacionalTotal = tallosNacionalTotal;
	}

}
