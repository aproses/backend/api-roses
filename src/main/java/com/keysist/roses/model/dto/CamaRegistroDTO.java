package com.keysist.roses.model.dto;

import java.io.Serializable;

public class CamaRegistroDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6388777771853861328L;
	private Integer bloqueId;
	private Integer empresaVariedadId;
	private Integer cantidadCamas;

	public CamaRegistroDTO() {
		// TODO Auto-generated constructor stub
	}

	public Integer getBloqueId() {
		return bloqueId;
	}

	public void setBloqueId(Integer bloqueId) {
		this.bloqueId = bloqueId;
	}

	public Integer getEmpresaVariedadId() {
		return empresaVariedadId;
	}

	public void setEmpresaVariedadId(Integer empresaVariedadId) {
		this.empresaVariedadId = empresaVariedadId;
	}

	public Integer getCantidadCamas() {
		return cantidadCamas;
	}

	public void setCantidadCamas(Integer cantidadCamas) {
		this.cantidadCamas = cantidadCamas;
	}

}
