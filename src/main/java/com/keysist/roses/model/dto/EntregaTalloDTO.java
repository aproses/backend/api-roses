package com.keysist.roses.model.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import com.keysist.roses.model.entity.Catalogo;
import com.keysist.roses.model.entity.EntregaPoscosecha;
import com.keysist.roses.model.entity.EntregaTallo;
import com.keysist.roses.model.enums.EstadoEntregaProcesoEnum;

public class EntregaTalloDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5494631274223867891L;

	private Integer id;
	private BigDecimal precio;

	private Long fechaProcesoTime;
	private LocalDateTime fechaProceso;
	private EstadoEntregaProcesoEnum estado;
	private Integer entregaPoscosechaId;
	private Integer longitudId;
	private Integer longitudValor;
	private Integer tallosIngresados;
	private Integer talloTotal;
	private Integer tallosDescab;
	private Integer tallosDevoluc;
	private Integer tallosNacional;

	public EntregaTalloDTO() {
		// TODO Auto-generated constructor stub
	}

	public EntregaTalloDTO(EntregaPoscosecha entregaPoscosecha) {
		// TODO Auto-generated constructor stub
		super();
		this.tallosIngresados = entregaPoscosecha.getTallos();
		this.tallosNacional = entregaPoscosecha.getNacional();
		this.tallosDescab = entregaPoscosecha.getDescabezado();
		this.tallosDevoluc = entregaPoscosecha.getDevolucion();
		this.entregaPoscosechaId = entregaPoscosecha.getId();
	}

	public EntregaTalloDTO(EntregaPoscosecha entregaPoscosecha, Catalogo longi) {
		this(entregaPoscosecha);
		this.longitudId = longi.getId();
		this.longitudValor = Integer.valueOf(longi.getValor().intValue());
		this.talloTotal = 0;

	}

	public EntregaTalloDTO(Catalogo l, Integer tallo, BigDecimal precio) {
		this.longitudId = l.getId();
		this.longitudValor = Integer.valueOf(l.getValor().intValue());
		this.talloTotal = tallo;
		this.precio = precio;

	}

	public EntregaTalloDTO(Catalogo longi, Catalogo talloBonche) {
		this.longitudId = longi.getId();
		this.longitudValor = Integer.valueOf(longi.getValor().intValue());

		talloTotal = 0;
	}

	public EntregaTalloDTO(EntregaTallo det) {
		this(det.getEntregaPoscosecha());
		this.id = det.getId();
		this.talloTotal = det.getTalloTotal();
		this.precio = det.getPrecio();
		this.fechaProceso = det.getFechaProceso();
		this.estado = det.getEstado();
		this.longitudId = det.getLongitud().getId();
		this.longitudValor = det.getLongitud().getValor().intValue();
		this.tallosDescab = det.getEntregaPoscosecha().getDescabezado();
		this.tallosDevoluc = det.getEntregaPoscosecha().getDevolucion();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public BigDecimal getPrecio() {
		return precio;
	}

	public void setPrecio(BigDecimal precio) {
		this.precio = precio;
	}

	public LocalDateTime getFechaProceso() {
		return fechaProceso;
	}

	public void setFechaProceso(LocalDateTime fechaProceso) {
		this.fechaProceso = fechaProceso;
	}

	public EstadoEntregaProcesoEnum getEstado() {
		return estado;
	}

	public void setEstado(EstadoEntregaProcesoEnum estado) {
		this.estado = estado;
	}

	public Integer getLongitudId() {
		return longitudId;
	}

	public void setLongitudId(Integer longitudId) {
		this.longitudId = longitudId;
	}

	public Integer getLongitudValor() {
		return longitudValor;
	}

	public void setLongitudValor(Integer longitudValor) {
		this.longitudValor = longitudValor;
	}

	public Integer getTalloTotal() {
		return talloTotal;
	}

	public void setTalloTotal(Integer talloTotal) {
		this.talloTotal = talloTotal;
	}

	public Integer getEntregaPoscosechaId() {
		return entregaPoscosechaId;
	}

	public void setEntregaPoscosechaId(Integer entregaPoscosechaId) {
		this.entregaPoscosechaId = entregaPoscosechaId;
	}

	public Long getFechaProcesoTime() {
		return fechaProcesoTime;
	}

	public void setFechaProcesoTime(Long fechaProcesoTime) {
		this.fechaProcesoTime = fechaProcesoTime;
	}

	public Integer getTallosIngresados() {
		return tallosIngresados;
	}

	public void setTallosIngresados(Integer tallosIngresados) {
		this.tallosIngresados = tallosIngresados;
	}

	public Integer getTallosDescab() {
		return tallosDescab;
	}

	public void setTallosDescab(Integer tallosDescab) {
		this.tallosDescab = tallosDescab;
	}

	public Integer getTallosDevoluc() {
		return tallosDevoluc;
	}

	public void setTallosDevoluc(Integer tallosDevoluc) {
		this.tallosDevoluc = tallosDevoluc;
	}

	public Integer getTallosNacional() {
		return tallosNacional;
	}

	public void setTallosNacional(Integer tallosNacional) {
		this.tallosNacional = tallosNacional;
	}

}
