package com.keysist.roses.model.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

import com.keysist.roses.model.entity.EmpresaVariedadPrecio;

public class EmpresaVariedadPrecioDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6388777771853861328L;

	private Integer id;
	private Integer idCliente;
	private String nombreCliente;
	private Long desde;
	private Long hasta;
	private BigDecimal precio;
	private Integer idEmpresa;
	private String nombreEmpresa;
	private Integer idEmpresaVariedad;
	private String nombreEmpresaVariedad;
	private Integer idLongitud;
	private String nombreLongitud;
	private LocalDate fechaDesde;
	private LocalDate fechaHasta;

	public EmpresaVariedadPrecioDTO() {
	}

	public EmpresaVariedadPrecioDTO(EmpresaVariedadPrecio evp) {
		super();
		this.id = evp.getId();
		if (evp.getCliente() != null) {
			this.idCliente = evp.getCliente().getId();
			this.nombreCliente = evp.getCliente().getNombre();
		}
		this.desde = java.sql.Date.valueOf(evp.getFechaDesde()).getTime();
		this.hasta = java.sql.Date.valueOf(evp.getFechaHasta()).getTime();
		this.fechaDesde = evp.getFechaDesde();
		this.fechaHasta = evp.getFechaHasta();
		this.precio = evp.getPrecio();
		if (evp.getEmpresaVariedad() != null) {
			this.idEmpresaVariedad = evp.getEmpresaVariedad().getId();
			this.nombreEmpresaVariedad = evp.getEmpresaVariedad().getVariedad().getNombre();
			this.idEmpresa = evp.getEmpresaVariedad().getEmpresa().getId();
			this.nombreEmpresa = evp.getEmpresaVariedad().getEmpresa().getNombre();
		}
		if (evp.getLongitud() != null) {
			this.idLongitud = evp.getLongitud().getId();
			this.nombreLongitud = evp.getLongitud().getNombre();
		}

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(Integer idCliente) {
		this.idCliente = idCliente;
	}

	public Long getDesde() {
		return desde;
	}

	public void setDesde(Long desde) {
		this.desde = desde;
	}

	public Long getHasta() {
		return hasta;
	}

	public void setHasta(Long hasta) {
		this.hasta = hasta;
	}

	public BigDecimal getPrecio() {
		return precio;
	}

	public void setPrecio(BigDecimal precio) {
		this.precio = precio;
	}

	public Integer getIdEmpresaVariedad() {
		return idEmpresaVariedad;
	}

	public void setIdEmpresaVariedad(Integer idEmpresaVariedad) {
		this.idEmpresaVariedad = idEmpresaVariedad;
	}

	public String getNombreEmpresaVariedad() {
		return nombreEmpresaVariedad;
	}

	public void setNombreEmpresaVariedad(String nombreEmpresaVariedad) {
		this.nombreEmpresaVariedad = nombreEmpresaVariedad;
	}

	public Integer getIdLongitud() {
		return idLongitud;
	}

	public void setIdLongitud(Integer idLongitud) {
		this.idLongitud = idLongitud;
	}

	public String getNombreLongitud() {
		return nombreLongitud;
	}

	public void setNombreLongitud(String nombreLongitud) {
		this.nombreLongitud = nombreLongitud;
	}

	public String getNombreCliente() {
		return nombreCliente;
	}

	public void setNombreCliente(String nombreCliente) {
		this.nombreCliente = nombreCliente;
	}

	public LocalDate getFechaDesde() {
		return fechaDesde;
	}

	public void setFechaDesde(LocalDate fechaDesde) {
		this.fechaDesde = fechaDesde;
	}

	public LocalDate getFechaHasta() {
		return fechaHasta;
	}

	public void setFechaHasta(LocalDate fechaHasta) {
		this.fechaHasta = fechaHasta;
	}

	public Integer getIdEmpresa() {
		return idEmpresa;
	}

	public void setIdEmpresa(Integer idEmpresa) {
		this.idEmpresa = idEmpresa;
	}

	public String getNombreEmpresa() {
		return nombreEmpresa;
	}

	public void setNombreEmpresa(String nombreEmpresa) {
		this.nombreEmpresa = nombreEmpresa;
	}

}
