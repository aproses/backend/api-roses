package com.keysist.roses.model.dto;

import java.io.Serializable;

public class PagoProveedorDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5494631274223867891L;

	private Integer id;

	private Integer cantidad;

	private Integer cantidadExportar;

	private Integer cantidadNacional;

	private Double efectividad;

	private Integer idEntrega;

	private Integer idVariedadUsuario;

	public PagoProveedorDTO() {
		// TODO Auto-generated constructor stub
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getCantidad() {
		return cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public Integer getCantidadExportar() {
		return cantidadExportar;
	}

	public void setCantidadExportar(Integer cantidadExportar) {
		this.cantidadExportar = cantidadExportar;
	}

	public Integer getCantidadNacional() {
		return cantidadNacional;
	}

	public void setCantidadNacional(Integer cantidadNacional) {
		this.cantidadNacional = cantidadNacional;
	}

	public Double getEfectividad() {
		return efectividad;
	}

	public void setEfectividad(Double efectividad) {
		this.efectividad = efectividad;
	}

	public Integer getIdEntrega() {
		return idEntrega;
	}

	public void setIdEntrega(Integer idEntrega) {
		this.idEntrega = idEntrega;
	}

	public Integer getIdVariedadUsuario() {
		return idVariedadUsuario;
	}

	public void setIdVariedadUsuario(Integer idVariedadUsuario) {
		this.idVariedadUsuario = idVariedadUsuario;
	}

}
