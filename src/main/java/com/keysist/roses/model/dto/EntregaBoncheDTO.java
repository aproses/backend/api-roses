package com.keysist.roses.model.dto;

import java.io.Serializable;
import java.time.LocalDateTime;

import com.keysist.roses.model.entity.Catalogo;
import com.keysist.roses.model.entity.EmpresaMesa;
import com.keysist.roses.model.entity.EntregaBonche;
import com.keysist.roses.model.entity.EntregaPoscosecha;
import com.keysist.roses.model.enums.EstadoEntregaProcesoEnum;

public class EntregaBoncheDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5494631274223867891L;

	private Integer id;
	private Integer bonches;
	private Integer bonchesDisponibles;
	private Long fechaProcesoTime;
	private LocalDateTime fechaProceso;
	private EstadoEntregaProcesoEnum estado;
	private Integer entregaPoscosechaId;
	private Integer longitudId;
	private Integer longitudValor;
	private Integer talloBoncheId;
	private Integer talloBoncheValor;
	private Integer puntoCorteId;
	private String puntoCorteValor;
	private Integer tipoCartonId;
	private String tipoCartonValor;
	private Integer mesaId;
	private String mesaValor;
	private String etiqueta;
	private Integer tallosIngresados;
	private Integer tallosProcesados;
	private Integer tallosNacional;

	public EntregaBoncheDTO() {
		// TODO Auto-generated constructor stub
	}

	public EntregaBoncheDTO(EntregaPoscosecha entregaPoscosecha) {
		// TODO Auto-generated constructor stub
		super();
		this.tallosIngresados = entregaPoscosecha.getTallos();
		this.tallosProcesados = entregaPoscosecha.getExportacion();
		this.tallosNacional = entregaPoscosecha.getNacional();
		this.entregaPoscosechaId = entregaPoscosecha.getId();
	}

	public EntregaBoncheDTO(EntregaPoscosecha entregaPoscosecha, Catalogo longi, Catalogo talloBonche, EmpresaMesa mesa,
			Catalogo puntoCorte, Catalogo tipoCarton) {
		this(entregaPoscosecha);
		this.longitudId = longi.getId();
		this.longitudValor = Integer.valueOf(longi.getValor().intValue());
		if (talloBonche != null) {
			this.talloBoncheId = talloBonche.getId();
			this.talloBoncheValor = Integer.valueOf(talloBonche.getValor().intValue());
		}
		if (mesa != null) {
			this.mesaId = mesa.getId();
			this.mesaValor = mesa.getNombre();
		}
		if (puntoCorte != null) {
			this.puntoCorteId = puntoCorte.getId();
			this.puntoCorteValor = puntoCorte.getNombre();
		}
		if (tipoCarton != null) {
			this.tipoCartonId = tipoCarton.getId();
			this.tipoCartonValor = tipoCarton.getNombre();
		}
		this.bonches = 0;

	}

	public EntregaBoncheDTO(Catalogo l) {
		this.longitudId = l.getId();
		this.longitudValor = Integer.valueOf(l.getValor().intValue());

	}

	public EntregaBoncheDTO(Catalogo longi, Catalogo talloBonche) {
		this.longitudId = longi.getId();
		this.longitudValor = Integer.valueOf(longi.getValor().intValue());
		if (talloBonche != null) {
			this.talloBoncheId = talloBonche.getId();
			this.talloBoncheValor = Integer.valueOf(talloBonche.getValor().intValue());
		}
		bonches = 0;
	}

	public EntregaBoncheDTO(EntregaBonche det) {
		this(det.getEntregaPoscosecha());
		this.id = det.getId();
		this.bonches = det.getBonches();
		this.bonchesDisponibles = det.getBonchesDisponibles();
		this.fechaProceso = det.getFechaProceso();
		this.estado = det.getEstado();
		this.longitudId = det.getLongitud().getId();
		this.longitudValor = det.getLongitud().getValor().intValue();
		this.talloBoncheId = det.getTalloBonche().getId();
		this.talloBoncheValor = det.getTalloBonche().getValor().intValue();
		if (det.getMesa() != null) {
			this.mesaId = det.getMesa().getId();
			this.mesaValor = det.getMesa().getNombre();
		}
		if (det.getPuntoCorte() != null) {
			this.puntoCorteId = det.getPuntoCorte().getId();
			this.puntoCorteValor = det.getPuntoCorte().getNombre();
		}
		if (det.getTipoCarton() != null) {
			this.tipoCartonId = det.getTipoCarton().getId();
			this.tipoCartonValor = det.getTipoCarton().getNombre();
		}
		this.etiqueta = det.getEtiqueta();

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getBonches() {
		return bonches;
	}

	public void setBonches(Integer bonches) {
		this.bonches = bonches;
	}

	public LocalDateTime getFechaProceso() {
		return fechaProceso;
	}

	public void setFechaProceso(LocalDateTime fechaProceso) {
		this.fechaProceso = fechaProceso;
	}

	public EstadoEntregaProcesoEnum getEstado() {
		return estado;
	}

	public void setEstado(EstadoEntregaProcesoEnum estado) {
		this.estado = estado;
	}

	public Integer getLongitudId() {
		return longitudId;
	}

	public void setLongitudId(Integer longitudId) {
		this.longitudId = longitudId;
	}

	public Integer getLongitudValor() {
		return longitudValor;
	}

	public void setLongitudValor(Integer longitudValor) {
		this.longitudValor = longitudValor;
	}

	public Integer getBonchesDisponibles() {
		return bonchesDisponibles;
	}

	public void setBonchesDisponibles(Integer bonchesDisponibles) {
		this.bonchesDisponibles = bonchesDisponibles;
	}

	public Integer getTalloBoncheId() {
		return talloBoncheId;
	}

	public void setTalloBoncheId(Integer talloBoncheId) {
		this.talloBoncheId = talloBoncheId;
	}

	public Integer getTalloBoncheValor() {
		return talloBoncheValor;
	}

	public void setTalloBoncheValor(Integer talloBoncheValor) {
		this.talloBoncheValor = talloBoncheValor;
	}

	public Integer getEntregaPoscosechaId() {
		return entregaPoscosechaId;
	}

	public void setEntregaPoscosechaId(Integer entregaPoscosechaId) {
		this.entregaPoscosechaId = entregaPoscosechaId;
	}

	public Integer getPuntoCorteId() {
		return puntoCorteId;
	}

	public void setPuntoCorteId(Integer puntoCorteId) {
		this.puntoCorteId = puntoCorteId;
	}

	public String getPuntoCorteValor() {
		return puntoCorteValor;
	}

	public void setPuntoCorteValor(String puntoCorteValor) {
		this.puntoCorteValor = puntoCorteValor;
	}

	public Integer getTipoCartonId() {
		return tipoCartonId;
	}

	public void setTipoCartonId(Integer tipoCartonId) {
		this.tipoCartonId = tipoCartonId;
	}

	public String getTipoCartonValor() {
		return tipoCartonValor;
	}

	public void setTipoCartonValor(String tipoCartonValor) {
		this.tipoCartonValor = tipoCartonValor;
	}

	public Integer getMesaId() {
		return mesaId;
	}

	public void setMesaId(Integer mesaId) {
		this.mesaId = mesaId;
	}

	public String getMesaValor() {
		return mesaValor;
	}

	public void setMesaValor(String mesaValor) {
		this.mesaValor = mesaValor;
	}

	public Long getFechaProcesoTime() {
		return fechaProcesoTime;
	}

	public void setFechaProcesoTime(Long fechaProcesoTime) {
		this.fechaProcesoTime = fechaProcesoTime;
	}

	public String getEtiqueta() {
		return etiqueta;
	}

	public void setEtiqueta(String etiqueta) {
		this.etiqueta = etiqueta;
	}

	public Integer getTallosIngresados() {
		return tallosIngresados;
	}

	public void setTallosIngresados(Integer tallosIngresados) {
		this.tallosIngresados = tallosIngresados;
	}

	public Integer getTallosProcesados() {
		return tallosProcesados;
	}

	public void setTallosProcesados(Integer tallosProcesados) {
		this.tallosProcesados = tallosProcesados;
	}

	public Integer getTallosNacional() {
		return tallosNacional;
	}

	public void setTallosNacional(Integer tallosNacional) {
		this.tallosNacional = tallosNacional;
	}

}
