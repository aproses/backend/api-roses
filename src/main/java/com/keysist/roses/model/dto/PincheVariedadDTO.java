package com.keysist.roses.model.dto;

import java.io.Serializable;

import com.keysist.roses.model.entity.EmpresaVariedad;

public class PincheVariedadDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6388777771853861328L;
	private Integer id;
	private Integer empresaVariedadId;
	private String empresaVariedadNombre;
	private Integer cantidad;

	public PincheVariedadDTO() {
		// TODO Auto-generated constructor stub
	}

	public PincheVariedadDTO(EmpresaVariedad pv) {
		// TODO Auto-generated constructor stub
		this.empresaVariedadId = pv.getId();
		this.empresaVariedadNombre = pv.getVariedad().getNombre();
		this.cantidad = 0;
	}

	public PincheVariedadDTO(Integer id, Integer empresaVariedadId, String empresaVariedadNombre, Integer cantidad) {
		super();
		this.id = id;
		this.empresaVariedadId = empresaVariedadId;
		this.empresaVariedadNombre = empresaVariedadNombre;
		this.cantidad = cantidad;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getCantidad() {
		return cantidad != null ? cantidad : 0;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public Integer getEmpresaVariedadId() {
		return empresaVariedadId;
	}

	public void setEmpresaVariedadId(Integer empresaVariedadId) {
		this.empresaVariedadId = empresaVariedadId;
	}

	public String getEmpresaVariedadNombre() {
		return empresaVariedadNombre;
	}

	public void setEmpresaVariedadNombre(String empresaVariedadNombre) {
		this.empresaVariedadNombre = empresaVariedadNombre;
	}

}
