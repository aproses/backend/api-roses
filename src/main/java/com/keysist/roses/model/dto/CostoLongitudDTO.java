package com.keysist.roses.model.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class CostoLongitudDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5494631274223867891L;

	private Integer id;

	private Integer longitudId;
	private String longitudNombre;

	private BigDecimal precio;

	public CostoLongitudDTO() {
		// TODO Auto-generated constructor stub
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getLongitudId() {
		return longitudId;
	}

	public void setLongitudId(Integer longitudId) {
		this.longitudId = longitudId;
	}

	public String getLongitudNombre() {
		return longitudNombre;
	}

	public void setLongitudNombre(String longitudNombre) {
		this.longitudNombre = longitudNombre;
	}

	public BigDecimal getPrecio() {
		return precio;
	}

	public void setPrecio(BigDecimal precio) {
		this.precio = precio;
	}

}
