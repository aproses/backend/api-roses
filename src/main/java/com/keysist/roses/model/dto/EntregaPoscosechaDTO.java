package com.keysist.roses.model.dto;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;

import com.keysist.roses.model.entity.EmpresaVariedad;
import com.keysist.roses.model.entity.EntregaPoscosecha;
import com.keysist.roses.model.enums.EstadoEntregaProcesoEnum;

public class EntregaPoscosechaDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6388777771853861328L;
	private Integer id;
	private String observacion;
	private String codigoPoveedorCliente;
	private LocalDate fechaEntrega;
	private LocalTime horaEntrega;
	private LocalDateTime fechaProcesamiento;
	private Integer empresaVariedadId;
	private String empresaVariedadNombre;
	private String clienteNombre;
	private String proveedorNombre;
	private Integer tallos;
	private Integer nacional;
	private Integer exportable;
	private Integer tallosDescab;
	private Integer tallosDevoluc;
	private EstadoEntregaProcesoEnum estado;
	private List<EntregaBoncheDTO> boncheDTOs;
	private List<EntregaTalloDTO> talloDTOs;
	private List<EntregaNacionalDTO> nacionalDTOs;

	public EntregaPoscosechaDTO() {
	}

	public EntregaPoscosechaDTO(EntregaPoscosecha er, EmpresaVariedad ev) {
		this.id = er.getId();
		this.fechaEntrega = er.getEntrega().getFechaEntrega();
		this.horaEntrega = er.getEntrega().getHoraEntrega();
		this.empresaVariedadId = ev.getId();
		this.empresaVariedadNombre = ev.getVariedad().getNombre();
		this.tallos = 0;
		this.nacional = 0;
		this.exportable = 0;
		this.tallosDescab = 0;
		this.tallosDevoluc = 0;
		this.estado = er.getEstado();
		this.proveedorNombre = er.getEntrega().getProveedor() != null ? er.getEntrega().getProveedor().getNombre() : "";
		this.clienteNombre = er.getEntrega().getCliente() != null ? er.getEntrega().getCliente().getNombre() : "";
	}

	public EntregaPoscosechaDTO(EntregaPoscosecha er) {
		this.id = er.getId();
		this.fechaEntrega = er.getEntrega().getFechaEntrega();
		this.horaEntrega = er.getEntrega().getHoraEntrega();
		this.empresaVariedadId = er.getEmpresaVariedad().getId();
		this.empresaVariedadNombre = er.getEmpresaVariedad().getVariedad().getNombre();
		this.tallos = er.getTallos();
		this.nacional = er.getNacional();
		this.exportable = er.getExportacion();
		this.tallosDescab = er.getDescabezado();
		this.tallosDevoluc = er.getDevolucion();
		this.estado = er.getEstado();
		this.proveedorNombre = er.getEntrega().getProveedor() != null ? er.getEntrega().getProveedor().getNombre() : "";
		this.clienteNombre = er.getEntrega().getCliente() != null ? er.getEntrega().getCliente().getNombre() : "";
	}

	public void setValores(String codigoPoveedorCliente, Integer tallos, Integer nacional, Integer exportable,
			Integer tallosDescab, Integer tallosDevoluc) {
		this.codigoPoveedorCliente = codigoPoveedorCliente;
		this.tallos = tallos;
		this.nacional = nacional;
		this.exportable = exportable;
		this.tallosDevoluc = tallosDevoluc;
		this.tallosDescab = tallosDescab;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public LocalDate getFechaEntrega() {
		return fechaEntrega;
	}

	public void setFechaEntrega(LocalDate fechaEntrega) {
		this.fechaEntrega = fechaEntrega;
	}

	public LocalTime getHoraEntrega() {
		return horaEntrega;
	}

	public void setHoraEntrega(LocalTime horaEntrega) {
		this.horaEntrega = horaEntrega;
	}

	public LocalDateTime getFechaProcesamiento() {
		return fechaProcesamiento;
	}

	public void setFechaProcesamiento(LocalDateTime fechaProcesamiento) {
		this.fechaProcesamiento = fechaProcesamiento;
	}

	public Integer getEmpresaVariedadId() {
		return empresaVariedadId;
	}

	public void setEmpresaVariedadId(Integer empresaVariedadId) {
		this.empresaVariedadId = empresaVariedadId;
	}

	public String getEmpresaVariedadNombre() {
		return empresaVariedadNombre;
	}

	public void setEmpresaVariedadNombre(String empresaVariedadNombre) {
		this.empresaVariedadNombre = empresaVariedadNombre;
	}

	public Integer getTallos() {
		return tallos;
	}

	public void setTallos(Integer tallos) {
		this.tallos = tallos;
	}

	public Integer getNacional() {
		return nacional;
	}

	public void setNacional(Integer nacional) {
		this.nacional = nacional;
	}

	public Integer getExportable() {
		return exportable;
	}

	public void setExportable(Integer exportable) {
		this.exportable = exportable;
	}

	public EstadoEntregaProcesoEnum getEstado() {
		return estado;
	}

	public void setEstado(EstadoEntregaProcesoEnum estado) {
		this.estado = estado;
	}

	public List<EntregaBoncheDTO> getBoncheDTOs() {
		return boncheDTOs;
	}

	public void setBoncheDTOs(List<EntregaBoncheDTO> boncheDTOs) {
		this.boncheDTOs = boncheDTOs;
	}

	public String getCodigoPoveedorCliente() {
		return codigoPoveedorCliente;
	}

	public void setCodigoPoveedorCliente(String codigoPoveedorCliente) {
		this.codigoPoveedorCliente = codigoPoveedorCliente;
	}

	public String getClienteNombre() {
		return clienteNombre;
	}

	public void setClienteNombre(String clienteNombre) {
		this.clienteNombre = clienteNombre;
	}

	public String getProveedorNombre() {
		return proveedorNombre;
	}

	public void setProveedorNombre(String proveedorNombre) {
		this.proveedorNombre = proveedorNombre;
	}

	public List<EntregaNacionalDTO> getNacionalDTOs() {
		return nacionalDTOs;
	}

	public void setNacionalDTOs(List<EntregaNacionalDTO> nacionalDTOs) {
		this.nacionalDTOs = nacionalDTOs;
	}

	public Integer getTallosDescab() {
		return tallosDescab;
	}

	public void setTallosDescab(Integer tallosDescab) {
		this.tallosDescab = tallosDescab;
	}

	public Integer getTallosDevoluc() {
		return tallosDevoluc;
	}

	public void setTallosDevoluc(Integer tallosDevoluc) {
		this.tallosDevoluc = tallosDevoluc;
	}

	public List<EntregaTalloDTO> getTalloDTOs() {
		return talloDTOs;
	}

	public void setTalloDTOs(List<EntregaTalloDTO> talloDTOs) {
		this.talloDTOs = talloDTOs;
	}

}
