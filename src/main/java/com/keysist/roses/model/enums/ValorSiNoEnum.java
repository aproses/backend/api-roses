package com.keysist.roses.model.enums;

public enum ValorSiNoEnum {
	NO("NO"), SI("SI"), TODOS("TODOS");

	private String descripcion;

	private ValorSiNoEnum(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getDescripcion() {
		return descripcion;
	}

}
