package com.keysist.roses.model.enums;

public enum EstadoEntregaProcesoEnum {
	RECEPCIONADO("RECEPCIONADO"),FINALIZADO("FINALIZADO"), PAGADO("PAGADO");

	private String descripcion;

	private EstadoEntregaProcesoEnum(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getDescripcion() {
		return descripcion;
	}

}
