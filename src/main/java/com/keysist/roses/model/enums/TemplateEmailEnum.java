package com.keysist.roses.model.enums;

public enum TemplateEmailEnum {

	REGISTRO("REGISTRO"), ACTIVACION("ACTIVACION"), ACTUALIZACION("ACTUALIZACION"), RECUPERACION_CLAVE("CAMBIO_CLAVE"),
	CAMBIO_CLAVE("CAMBIO_CLAVE"), ENVIO_OTP("ENVIO_OTP"), CAMBIO_PASS_EXITO("CAMBIO_PASS_EXITO");

	private String descripcion;

	private TemplateEmailEnum(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getDescripcion() {
		return descripcion;
	}
}
