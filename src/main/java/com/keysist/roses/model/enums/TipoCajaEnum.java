package com.keysist.roses.model.enums;

public enum TipoCajaEnum {
	QB("QB"), HB("HB"), FULL("FULL");

	private String descripcion;

	private TipoCajaEnum(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getDescripcion() {
		return descripcion;
	}

}
