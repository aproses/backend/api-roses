package com.keysist.roses.model.enums;

public enum NombrePerfilEnum {
	ROLE_PROVEEDOR, ROLE_CLIENTE;

	private NombrePerfilEnum() {
	}

}
