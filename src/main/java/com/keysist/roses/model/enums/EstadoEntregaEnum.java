package com.keysist.roses.model.enums;

public enum EstadoEntregaEnum {
	BORRADOR("BORRADOR"), ENVIADO("ENVIADO"),RECEPCIONADO("RECEPCIONADO"), PROCESADO("PROCESADO"),FINALIZADO("FINALIZADO"), PAGADO("PAGADO");

	private String descripcion;

	private EstadoEntregaEnum(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getDescripcion() {
		return descripcion;
	}

}
