package com.keysist.roses.model.enums;

public enum EstadoCosechaVariedadEnum {
	COSECHADO("COSECHADO"), ENVIADO("ENVIADO"), ENVIADO_PARCIAL("ENVIADO_PARCIAL");

	private String descripcion;

	private EstadoCosechaVariedadEnum(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getDescripcion() {
		return descripcion;
	}

}
