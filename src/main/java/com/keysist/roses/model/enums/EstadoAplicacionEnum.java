package com.keysist.roses.model.enums;

public enum EstadoAplicacionEnum {
	INGRESADO("INGRESADO"), INICIADO("INICIADO"), FINALIZADO("FINALIZADO"), ANULADO("ANULADO");

	private String descripcion;

	private EstadoAplicacionEnum(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getDescripcion() {
		return descripcion;
	}

}
