package com.keysist.roses.model.enums;

public enum ValorEstadoEnum {
	ACTIVO("ACTIVO"), INACTIVO("INACTIVO");

	private String descripcion;

	private ValorEstadoEnum(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getDescripcion() {
		return descripcion;
	}

}
