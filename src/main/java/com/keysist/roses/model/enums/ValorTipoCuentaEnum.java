package com.keysist.roses.model.enums;

public enum ValorTipoCuentaEnum {
	AHORRO("AHORRO"),  CORRIENTE("CORRIENTE");

	private String descripcion;

	private ValorTipoCuentaEnum(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getDescripcion() {
		return descripcion;
	}

}
