package com.keysist.roses.model.repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import com.keysist.roses.model.entity.EntregaBonche;
import com.keysist.roses.model.entity.EntregaTallo;

@RepositoryRestResource(path = "entregatallo")
public interface EntregaTalloRepository extends PagingAndSortingRepository<EntregaTallo, Integer> {

	@RestResource(path = "by-id")
	EntregaBonche findById(@Param(value = "id") String id);

	
	@Query("select det from EntregaTallo det where det.entregaPoscosecha.id=:entregaPoscosecha")
	List<EntregaTallo> findByEntregaPoscosecha(@Param(value = "entregaPoscosecha") Integer entregaPoscosecha);

	@Query("select det from EntregaTallo det inner join det.entregaPoscosecha ep inner join ep.empresaVariedad"
			+ " vari where date(det.fechaProceso)=date(:fechaProceso)  "
			+ " and vari.id=:idEmpresaVariedad and det.longitud.id=:idLongitud")
	Optional<EntregaTallo> findTallosByLongitud(@Param(value = "fechaProceso") LocalDate fechaProceso,
			@Param(value = "idEmpresaVariedad") Integer idVariedad, @Param(value = "idLongitud") Integer idLongitud);
}
