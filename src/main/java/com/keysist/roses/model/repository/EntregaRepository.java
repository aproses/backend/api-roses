package com.keysist.roses.model.repository;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import com.keysist.roses.model.entity.Entrega;
import com.keysist.roses.model.entity.RelacionComercial;
import com.keysist.roses.model.enums.EstadoEntregaEnum;
import com.keysist.roses.model.enums.TipoEntregaEnum;

@RepositoryRestResource(path = "entrega")
public interface EntregaRepository extends PagingAndSortingRepository<Entrega, Integer> {

	@RestResource(path = "by-id")
	Entrega findById(@Param(value = "id") String id);

	@RestResource(path = "by-filtro")
	@Query("select new Entrega(ent) from Entrega ent inner join ent.proveedor usu where (UPPER(usu.ruc) like UPPER(concat('%',:filtro,'%')) or UPPER(usu.nombre) like UPPER(concat('%',:filtro,'%'))) and (ent.estado=:estado or :estado='TODOS')")
	List<Entrega> findByFiltro(@Param(value = "filtro") String filtro,
			@Param(value = "estado") EstadoEntregaEnum estado);

	@Query("select ent from Entrega ent inner join ent.proveedor usu where usu.id=:idEmpresa and (date(ent.fechaEntrega) between  :fechaInicio and :fechaFin) order by ent.fechaEntrega desc")
	List<Entrega> findByFechaAndProveedor(@Param(value = "idEmpresa") Integer idEmpresa,
			@Param(value = "fechaInicio") LocalDate fechaInicio, @Param(value = "fechaFin") LocalDate fechaFin);

	@Query("select ent from Entrega ent inner join ent.cliente usu where usu.id=:idEmpresa and (date(ent.fechaEntrega) between  :fechaInicio and :fechaFin) order by ent.fechaEntrega desc")
	List<Entrega> findByFechaAndCliente(@Param(value = "idEmpresa") Integer idEmpresa,
			@Param(value = "fechaInicio") LocalDate fechaInicio, @Param(value = "fechaFin") LocalDate fechaFin);

	@Query("select ent from Entrega ent inner join ent.proveedor prov left outer join ent.cliente cli "
			+ " where prov.id=:idProveedor and date(ent.fechaEntrega) = date(:fechaEntrega) and ((:idCliente=-1 and cli is null) or (:idCliente!=-1 and cli.id=:idCliente))")
	List<Entrega> findByProveedorClienteFecha(@Param(value = "idProveedor") Integer idProveedor,
			@Param(value = "idCliente") Integer idCliente, @Param(value = "fechaEntrega") LocalDate fechaEntrega);

	@Query("select ent from Entrega ent  where ent.id=:idEntrega ")
	List<Entrega> findByIdEntrega(@Param(value = "idEntrega") Integer idEntrega);

	@Query("select rc from RelacionComercial rc where rc in ( select distinct usu from Entrega ent inner join ent.proveedor usu where  (date(ent.fechaEntrega) between  :fechaDesde and :fechaHasta) and ent.estado in (:estado1,:estado2,:estado3) and ent.id in (select de.entrega.id from EntregaVariedad de inner join de.empresaVariedad uv where uv.disponible='SI')) order by rc.proveedor.nombre")
	List<RelacionComercial> getProveedores(@Param(value = "fechaDesde") Date fechaDesde,
			@Param(value = "fechaHasta") Date fechaHasta, @Param(value = "estado1") EstadoEntregaEnum estado1,
			@Param(value = "estado2") EstadoEntregaEnum estado2, @Param(value = "estado3") EstadoEntregaEnum estado3);

	@Query("select ent from Entrega ent inner join ent.proveedor pro left join ent.cliente cli where pro.id=:idProveedor and (cli.id=:idCliente or :idCliente=-1) and date(ent.fechaEntrega) = date(:fechaEntrega) and ent.tipo=:tipoEntrega")
	Optional<Entrega> findByUsuarioFechaTipo(@Param(value = "idProveedor") Integer idUsuario,
			@Param(value = "idCliente") Integer idCliente, @Param(value = "fechaEntrega") LocalDate fechaEntrega,
			@Param(value = "tipoEntrega") TipoEntregaEnum tipoEntrega);

}
