package com.keysist.roses.model.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.keysist.roses.model.entity.Parametro;

@RepositoryRestResource(path = "parametro")
public interface ParametroRepository extends PagingAndSortingRepository<Parametro, String> {

	
}
