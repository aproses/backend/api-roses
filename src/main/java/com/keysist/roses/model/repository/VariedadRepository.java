package com.keysist.roses.model.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import com.keysist.roses.model.entity.Variedad;

@RepositoryRestResource(path = "variedad")
public interface VariedadRepository extends PagingAndSortingRepository<Variedad, Integer> {

	@RestResource(path = "by-idVariedad")
	@Query("select new Variedad(var) from Variedad var where var.id=:idVariedad or :idVariedad=-1 order by var.nombre")
	List<Variedad> findByIdVariedad(@Param(value = "idVariedad") Integer idVariedad);

	@RestResource(path = "all")
	@Query("select new Variedad(var) from Variedad var where var.disponible='SI'")
	List<Variedad> all(@Param(value = "idVariedad") Integer idVariedad);

	@RestResource(path = "all-by-filter")
	@Query("select var from Variedad var where upper(var.nombre) like concat('%',upper(:nombre),'%') order by var.nombre")
	List<Variedad> findAllByFilter(@Param(value = "nombre") String filtro);

}
