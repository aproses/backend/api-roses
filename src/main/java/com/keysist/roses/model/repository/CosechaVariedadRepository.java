package com.keysist.roses.model.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import com.keysist.roses.model.entity.CosechaVariedad;
import com.keysist.roses.model.enums.EstadoCosechaVariedadEnum;

@RepositoryRestResource(path = "cosechaVariedad")
public interface CosechaVariedadRepository extends PagingAndSortingRepository<CosechaVariedad, Integer> {

	@RestResource(path = "by-cosecha")
	@Query("select det from CosechaVariedad det where det.cosecha.id=:idCosecha order by det.empresaVariedad.variedad.nombre,det.cantidadMalla desc ")
	List<CosechaVariedad> findByCosecha(@Param(value = "idCosecha") Integer id);

	@RestResource(path = "by-cosecha")
	@Query("select det from CosechaVariedad det where det.cosecha.id=:idCosecha and det.estado!=:estado")
	List<CosechaVariedad> findNoEnviadoByCosecha(@Param(value = "idCosecha") Integer id,
			@Param(value = "estado") EstadoCosechaVariedadEnum stado);

	/**
	 * 
	 * @param id
	 * @return
	 */
	@Query("select uvar.id, sum(det.totalTalloDisponible) from CosechaVariedad det inner join det.cosecha.empresa prov  inner join det.cosecha cose inner join det.empresaVariedad uvar "
			+ " where prov.id=:idProveedor and  date(cose.fechaCorte) <=date(:fechaActual) and det.totalTalloDisponible>0 "
			+ " group by uvar.id,uvar.variedad.nombre order by uvar.variedad.nombre")
	List<Object[]> findToEntregaProveedor(@Param(value = "idProveedor") Integer idProveedor,
			@Param(value = "fechaActual") LocalDate fechaActual);

	@Query("select uvar.id, sum(det.totalTallo) from CosechaVariedad det inner join det.cosecha.empresa user inner join det.cosecha cose inner join det.empresaVariedad uvar where user.id=:idEmpresa and date(cose.fechaCorte) =date(:fechaActual) group by uvar.id,uvar.variedad.nombre order by uvar.variedad.nombre")
	List<Object[]> findCosecha(@Param(value = "idEmpresa") Integer idEmpresa,
			@Param(value = "fechaActual") LocalDate fechaActual);

	@Query("select  uvar.id, sum(det.totalTallo), cose.fechaCorte from CosechaVariedad det inner join det.cosecha.empresa user inner join det.cosecha cose inner join det.empresaVariedad uvar where user.id=:idEmpresa and year(cose.fechaCorte) =:anio group by cose.fechaCorte, uvar.id, uvar.variedad.nombre order by cose.fechaCorte, uvar.variedad.nombre")
	List<Object[]> findCosechaAnual(@Param(value = "idEmpresa") Integer idEmpresa, @Param(value = "anio") Integer anio);

	@Query("select  uvar.id, sum(det.totalTallo), cose.semana,year(cose.fechaCorte) from CosechaVariedad det inner join det.cosecha.empresa user inner join det.cosecha cose inner join det.empresaVariedad uvar where user.id=:idEmpresa  group by year(cose.fechaCorte), cose.semana, uvar.id, uvar.variedad.nombre order by year(cose.fechaCorte), cose.semana, uvar.variedad.nombre")
	List<Object[]> findCosechaSemanaAnual(@Param(value = "idEmpresa") Integer idEmpresa);
	
	
	@Query("select  uvar.id, sum(det.totalTallo), month(cose.fechaCorte),year(cose.fechaCorte) from CosechaVariedad det inner join det.cosecha.empresa user inner join det.cosecha cose inner join det.empresaVariedad uvar where user.id=:idEmpresa   group by year(cose.fechaCorte), month(cose.fechaCorte), uvar.id, uvar.variedad.nombre order by year(cose.fechaCorte), month(cose.fechaCorte), uvar.variedad.nombre")
	List<Object[]> findCosechaMensualAnual(@Param(value = "idEmpresa") Integer idEmpresa);

	@Query("select det from CosechaVariedad det inner join det.empresaVariedad uva  inner join det.cosecha cose inner join det.empresaVariedad uvar "
			+ "where uva.id=:idEmpresaVariedad and det.estado!=:estado and cose.fechaCorte<=:fechaEntrega  order by cose.fechaCorte, det.id asc")
	List<CosechaVariedad> getCosecha(
			@Param(value = "idEmpresaVariedad") Integer idEmpresaVariedad,
			@Param(value = "estado") EstadoCosechaVariedadEnum stado,
			@Param(value = "fechaEntrega") LocalDate fechaEntrega);

	@Query("select sum(det.totalTallo), cose.semana from CosechaVariedad det inner join det.empresaVariedad ev inner join det.cosecha cose where ev.id=:idEmpresaVariedad  and cose.semana between :semanaDesde and :semanaHasta  group by cose.semana order by cose.semana")
	List<Object[]> findCosechaByVariedad(@Param(value = "idEmpresaVariedad") Integer idEmpresa,
			@Param(value = "semanaDesde") Integer desde, @Param(value = "semanaHasta") Integer hasta);

}
