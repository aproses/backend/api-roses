package com.keysist.roses.model.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.keysist.roses.model.entity.Empresa;

@RepositoryRestResource(path = "empresa")
public interface EmpresaRepository extends PagingAndSortingRepository<Empresa, Integer> {
	Empresa findByRucSobrante(@Param(value = "rucSobrante") String rucSobrante);

	List<Empresa> findByRuc(@Param(value = "ruc") String ruc);
	
	@Query("select em from Empresa em where upper(em.nombre) like concat('%',upper(:filtro),'%') or upper(em.ruc) like concat('%',upper(:filtro),'%') order by em.fechaRegistro, em.nombre")
	List<Empresa> filter(@Param(value = "filtro") String ruc);
	
	
	@Query("select em from Empresa em inner join em.tipo tipo where em.activo='SI' and (upper(em.nombre) like concat('%',upper(:filtro),'%') or upper(em.ruc) like concat('%',upper(:filtro),'%')) and (tipo.codigo!=:tipoCode or em.id=:idEmpresa) order by em.nombre")
	List<Empresa> filterDistinct(@Param(value = "filtro") String ruc,@Param(value = "tipoCode") String tipo,Integer idEmpresa);
	
	@Query("select em from Empresa em where em.activo='SI' order by em.nombre")
	List<Empresa> findActive();
}
