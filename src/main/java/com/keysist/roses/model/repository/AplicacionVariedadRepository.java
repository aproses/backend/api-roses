package com.keysist.roses.model.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.keysist.roses.model.entity.AplicacionVariedad;

@RepositoryRestResource(path = "aplicacionvar")
public interface AplicacionVariedadRepository extends PagingAndSortingRepository<AplicacionVariedad, Integer> {

	@Query("select av from AplicacionVariedad av where av.aplicacion.id=:id")
	List<AplicacionVariedad> findByAplicacion(@Param(value = "id") Integer id);

}
