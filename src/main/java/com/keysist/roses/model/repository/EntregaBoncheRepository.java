package com.keysist.roses.model.repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import com.keysist.roses.model.entity.EntregaBonche;
import com.keysist.roses.model.enums.EstadoEntregaEnum;

@RepositoryRestResource(path = "entregaBonche")
public interface EntregaBoncheRepository extends PagingAndSortingRepository<EntregaBonche, Integer> {

	@RestResource(path = "by-id")
	EntregaBonche findById(@Param(value = "id") String id);

	@Query("select det from EntregaBonche det where det.entregaPoscosecha.id=:entregaPoscosecha")
	List<EntregaBonche> findByEntregaPoscosecha(@Param(value = "entregaPoscosecha") Integer entregaPoscosecha);

	@RestResource(path = "by-Dia")
	@Query("select sum(det.bonches) from EntregaBonche det inner join det.entregaPoscosecha ep inner join ep.empresaVariedad"
			+ " vari where (date_part('dow',det.fechaProceso)=:filtroDia or :filtroDia=-1) and (vari.id=:idVariedad or :idVariedad=-1) and det.estado=:estado and det.longitud.id=:idLongitud group by date_part('dow',det.fechaProceso),vari.id,det.estado,det.longitud.id")
	Long findByDia(@Param(value = "filtroDia") Integer filtroDia, @Param(value = "idVariedad") Integer idVariedad,
			@Param(value = "estado") EstadoEntregaEnum estado, @Param(value = "idLongitud") Integer idLongitud);

	@Query("select det from EntregaBonche det inner join det.entregaPoscosecha ep inner join ep.empresaVariedad"
			+ " vari where date(det.fechaProceso)=date(:fechaProceso)  "
			+ " and vari.id=:idEmpresaVariedad and det.longitud.id=:idLongitud  and det.mesa.id=:idMesa"
			+ " and det.puntoCorte.id=:idPuntoCorte and det.talloBonche.id=:idTalloBonche"
			+ " and det.tipoCarton.id=:idTipoCarton")
	Optional<EntregaBonche> findByLongitud(@Param(value = "fechaProceso") LocalDate fechaProceso,
			@Param(value = "idEmpresaVariedad") Integer idVariedad, @Param(value = "idLongitud") Integer idLongitud,
			@Param(value = "idMesa") Integer idMesa, @Param(value = "idPuntoCorte") Integer idPuntoCorte,
			@Param(value = "idTalloBonche") Integer idTalloBonche, @Param(value = "idTipoCarton") Integer idTipoCarton);
}
