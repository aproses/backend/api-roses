package com.keysist.roses.model.repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.keysist.roses.model.entity.EntregaNacional;

@RepositoryRestResource(path = "entregaNacional")
public interface EntregaNacionalRepository extends PagingAndSortingRepository<EntregaNacional , Integer> {

	
	@Query("select det from EntregaNacional det where det.entregaPoscosecha.id=:entregaPoscosecha")
	List<EntregaNacional> findByEntregaPoscosecha(@Param(value = "entregaPoscosecha") Integer entregaPoscosecha);
	
	@Query("select det from EntregaNacional det inner join det.entregaPoscosecha ep inner join ep.empresaVariedad"
			+ " vari where date(det.fechaProceso)=date(:fechaProceso)  "
			+ " and vari.id=:idEmpresaVariedad and det.categoria.id=:idCategoria  and det.subCategoria.id=:idSubCategoria"
			+ " and det.bloque.id=:idBloque and det.clasificador.id=:idClasificador")
	Optional<EntregaNacional> findByFilter(@Param(value = "fechaProceso") LocalDate fechaProceso,
			@Param(value = "idEmpresaVariedad") Integer idVariedad, @Param(value = "idCategoria") Integer idCategoria,
			@Param(value = "idSubCategoria") Integer idSubCategoria, @Param(value = "idBloque") Integer idBloque,
			@Param(value = "idClasificador") Integer idClasificador);
	
}
