package com.keysist.roses.model.repository;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import com.keysist.roses.model.entity.EmpresaBloque;
import com.keysist.roses.model.entity.Cama;

@RepositoryRestResource( path = "cama")
public interface CamaRepository extends PagingAndSortingRepository<Cama, Integer> {

	
	
	@RestResource(path = "by-bloque")
	List<Cama> findByBloque(EmpresaBloque bloque);
}
