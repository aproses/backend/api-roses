package com.keysist.roses.model.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.keysist.roses.model.entity.AplicacionProducto;
import com.keysist.roses.model.enums.EstadoAplicacionEnum;

@RepositoryRestResource(path = "aplicacionprod")
public interface AplicacionProductoRepository extends PagingAndSortingRepository<AplicacionProducto, Integer> {

	@Query("select sum(apr.necesario), apr.empresaProducto.producto.nombre,apr.empresaProducto.id from AplicacionProducto apr inner join apr.aplicacion ap inner join ap.empresaBloque.empresa usu where apr.empresaProducto.producto.esNitrato=true and usu.id=:idEmpresa and  (date(ap.fechaAplicacion) between  :fechaDesde and :fechaHasta) and ap.id in (select av.aplicacion.id from AplicacionVariedad av inner join av.empresaVariedad uv where uv.disponible='SI') group by apr.empresaProducto.producto.nombre,apr.empresaProducto.id order by apr.empresaProducto.producto.nombre ")
	List<Object[]> obtenerConsumo(@Param(value = "fechaDesde") LocalDate fechaDesde,
			@Param(value = "fechaHasta") LocalDate fechaHasta, @Param(value = "idEmpresa") Integer idEmpresa);

	@Query("select sum(apr.necesario), apr.empresaProducto.producto.nombre,apr.empresaProducto.id,ap.tipo.nombre,apr.empresaProducto.cantidadPorUnidad,apr.empresaProducto.precio from AplicacionProducto apr inner join apr.aplicacion ap inner join ap.empresaBloque.empresa usu where ap.estado=:estado and usu.id=:idEmpresa and  (date(ap.fechaAplicacion) between  :fechaDesde and :fechaHasta) and (ap.empresaBloque.id=:idEmpresaBloque or :idEmpresaBloque=-1) group by ap.tipo.nombre,apr.empresaProducto.producto.nombre,apr.empresaProducto.id,apr.empresaProducto.cantidadPorUnidad,apr.empresaProducto.precio order by ap.tipo.nombre, apr.empresaProducto.producto.nombre,apr.empresaProducto.cantidadPorUnidad,apr.empresaProducto.precio ")
	List<Object[]> obtenerConsumoTotal(@Param(value = "fechaDesde") LocalDate fechaDesde,
			@Param(value = "fechaHasta") LocalDate fechaHasta, @Param(value = "idEmpresa") Integer idEmpresa,
			Integer idEmpresaBloque,EstadoAplicacionEnum estado);

	@Query("select ap from AplicacionProducto ap where ap.aplicacion.id=:id order by ap.tanque.id, ap.orden")
	List<AplicacionProducto> findByAplicacion(@Param(value = "id") Integer id);

	@Query("select ap from AplicacionProducto ap where ap.aplicacion.id=:id and ap.empresaProducto.id=:idEmpresaProducto order by ap.tanque.id, ap.orden")
	List<AplicacionProducto> findByAplicacionAndProducto(@Param(value = "id") Integer id,
			@Param(value = "idEmpresaProducto") Integer idEmpresaProducto);

	@Query("select sum((apr.necesario/apr.empresaProducto.cantidadPorUnidad)*apr.empresaProducto.precio),ap.tipo.nombre,month(ap.fechaAplicacion) from AplicacionProducto apr inner join apr.aplicacion ap inner join ap.empresaBloque.empresa usu where ap.estado=:estado and usu.id=:idEmpresa and  (date(ap.fechaAplicacion) between  :fechaDesde and :fechaHasta) and (ap.empresaBloque.id=:idEmpresaBloque or :idEmpresaBloque=-1) group by month(ap.fechaAplicacion), ap.tipo.nombre order by month(ap.fechaAplicacion), ap.tipo.nombre ")
	List<Object[]> obtenerConsumoTotalByMes(@Param(value = "fechaDesde") LocalDate fechaDesde,
			@Param(value = "fechaHasta") LocalDate fechaHasta, @Param(value = "idEmpresa") Integer idEmpresa,
			Integer idEmpresaBloque, EstadoAplicacionEnum estado);

}
