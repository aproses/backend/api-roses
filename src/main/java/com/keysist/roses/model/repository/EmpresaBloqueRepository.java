package com.keysist.roses.model.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import com.keysist.roses.model.entity.EmpresaBloque;

@RepositoryRestResource( path = "bloque")
public interface EmpresaBloqueRepository extends PagingAndSortingRepository<EmpresaBloque, Integer> {

	
	
	@RestResource(path = "by-empresa")
	@Query("select usv from EmpresaBloque usv inner join usv.empresa usu  where usu.id=:idEmpresa and usu.activo='SI' order by usv.numero")
	List<EmpresaBloque> findByEmpresa(Integer idEmpresa);
	
	@Query("select usv from EmpresaBloque usv inner join usv.empresa usu  where usu.id=:idEmpresa and usv.numero=:numero order by usv.numero")
	List<EmpresaBloque> findByEmpresaAndNumero(Integer idEmpresa,String numero);
}
