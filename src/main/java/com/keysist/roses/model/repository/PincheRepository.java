package com.keysist.roses.model.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.keysist.roses.model.entity.Pinche;

@RepositoryRestResource(path = "pinche")
public interface PincheRepository extends PagingAndSortingRepository<Pinche, Integer> {

	@Query("select pin from Pinche pin inner join pin.empresa usu where usu.id=:idEmpresa and pin.semana= :semanaPinche and pin.anio=:anio")
	List<Pinche> findBySemanaAndEmpresa(@Param(value = "idEmpresa") Integer idUsuario,
			@Param(value = "semanaPinche") int semanaPinche,
			@Param(value = "anio") int anio);

	@Query("select pin from Pinche pin inner join pin.empresa usu where usu.id=:idEmpresa and date(pin.fechaPinche) between date(:fechaPincheDesde) and date(:fechaPincheHasta)  and pin.id in (select pv.pinche.id from PincheVariedad pv inner join pv.empresaVariedad uv where uv.disponible='SI' ) order by pin.fechaPinche desc")
	List<Pinche> consultar(@Param(value = "idEmpresa") Integer idEmpresa,
			@Param(value = "fechaPincheDesde") LocalDate desde, @Param(value = "fechaPincheHasta") LocalDate hasta);

}
