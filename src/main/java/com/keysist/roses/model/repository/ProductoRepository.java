package com.keysist.roses.model.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.keysist.roses.model.entity.Producto;

@RepositoryRestResource(path = "productorr")
public interface ProductoRepository extends PagingAndSortingRepository<Producto, Integer> {

	@Query("select pro from Producto pro where ( upper(pro.nombre) like upper(concat('%',:filtro,'%')) or upper(pro.caracteristicas) like upper(concat('%',:filtro,'%'))) order by pro.nombre")
	List<Producto> findByFilter(@Param(value = "filtro") String filtro);

}
