package com.keysist.roses.model.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.keysist.roses.model.entity.EmpresaProducto;
import com.keysist.roses.model.enums.ValorSiNoEnum;

@RepositoryRestResource(path = "empresaproducto")
public interface EmpresaProductoRepository extends PagingAndSortingRepository<EmpresaProducto, Integer> {

	@Query("select usv from EmpresaProducto usv inner join usv.empresa usu  where usu.id=:idEmpresa order by usv.producto.nombre")
	List<EmpresaProducto> findByEmpresa(@Param(value = "idEmpresa") Integer idEmpresa);

	@Query("select usv from EmpresaProducto usv inner join usv.empresa usu inner join usv.producto pro where usu.id=:idEmpresa and pro in (select pt.producto from ProductoTipo pt where pt.tipo.id=:idTipo and pt.activo='SI' ) order by usv.producto.nombre")
	List<EmpresaProducto> findByEmpresaAndTipo(@Param(value = "idEmpresa") Integer idEmpresa,
			@Param(value = "idTipo") Integer idTipo);

	@Query("select usv from EmpresaProducto usv inner join usv.empresa usu  where usu.id=:idEmpresa and usv.producto.id=:idProducto order by usv.producto.nombre")
	List<EmpresaProducto> findByEmpresaAndProducto(@Param(value = "idEmpresa") Integer idEmpresa,
			@Param(value = "idProducto") Integer idProducto);

	@Query("select usv from EmpresaProducto usv inner join usv.empresa usu  where usu.id=:idEmpresa and usv.disponible<=usv.disponibleMinimo and usv.activo=:activo order by usv.producto.nombre")
	List<EmpresaProducto> findByEmpresaMinimo(@Param(value = "idEmpresa") Integer idEmpresa, ValorSiNoEnum activo);

	@Query("select pro from EmpresaProducto pro inner join pro.empresa usu  where usu.id=:idEmpresa and pro.activo=:activo and ( upper(pro.producto.nombre) like upper(concat('%',:filtro,'%')) or upper(pro.producto.caracteristicas) like upper(concat('%',:filtro,'%'))) order by pro.producto.nombre")
	List<EmpresaProducto> findByEmpresaFilter(@Param(value = "idEmpresa") Integer idEmpresa,
			@Param(value = "filtro") String filtro,@Param(value = "activo") ValorSiNoEnum activo);

}
