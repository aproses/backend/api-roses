package com.keysist.roses.model.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.List;
import com.keysist.roses.model.entity.Perfil;
import com.keysist.roses.model.entity.Usuario;
import com.keysist.roses.model.entity.UsuarioPerfil;

@RepositoryRestResource( path = "usuarioPerfil")
public interface UsuarioPerfilRepository extends PagingAndSortingRepository<UsuarioPerfil, Integer> {

	@RestResource(path = "by-Usuario")
	List<UsuarioPerfil> findByUsuario(@Param(value = "usuario") Usuario usuario);
	
	@RestResource(path = "by-Perfil")
	List<UsuarioPerfil> findByPerfil(@Param(value = "perfil") Perfil perfil);
	
	@RestResource(path = "by-UsuarioAndPerfil")
	List<UsuarioPerfil> findByUsuarioAndPerfil(@Param(value = "usuario") Usuario usuario, @Param(value = "perfil") Perfil perfil);
	
}
