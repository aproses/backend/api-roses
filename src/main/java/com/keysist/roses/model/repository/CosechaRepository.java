package com.keysist.roses.model.repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import com.keysist.roses.model.entity.Cosecha;

@RepositoryRestResource(path = "cosecha")
public interface CosechaRepository extends PagingAndSortingRepository<Cosecha, Integer> {

	@RestResource(path = "by-usuario-fecha")
	@Query("select ent from Cosecha ent inner join ent.empresa usu where usu.id=:idEmpresa and date(ent.fechaCorte) = date(:fechaCorte) and ent.id in (select de.cosecha.id from CosechaVariedad de inner join de.empresaVariedad uv where  uv.disponible='SI')")
	Optional<Cosecha> findByEmpresaFecha(@Param(value = "idEmpresa") Integer idEmpresa,
			@Param(value = "fechaCorte") LocalDate fechaCorte);

	@Query("select ent from Cosecha ent inner join ent.empresa usu where usu.id=:idEmpresa and  date(ent.fechaCorte) between  :fechaDesde and :fechaHasta order by ent.fechaCorte desc")
	List<Cosecha> findByFecha(@Param(value = "idEmpresa") Integer idEmpresa,
			@Param(value = "fechaDesde") LocalDate fechaDesde, @Param(value = "fechaHasta") LocalDate fechaHasta);

}
