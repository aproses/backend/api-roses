package com.keysist.roses.model.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.keysist.roses.model.entity.Pinche;
import com.keysist.roses.model.entity.PincheVariedad;

@RepositoryRestResource( path = "pinchevariedad")
public interface PincheVariedadRepository extends PagingAndSortingRepository<PincheVariedad, Integer> {

	
	
	List<PincheVariedad> findByPincheOrderById(Pinche pinche);
	
	@Query("select sum(det.cantidad), pinche.semana from PincheVariedad det inner join det.empresaVariedad ev inner join det.pinche pinche where ev.id=:idEmpresaVariedad  and pinche.semana between :semanaDesde and :semanaHasta  group by pinche.semana order by pinche.semana")
	List<Object[]> findCosechaByVariedad(@Param(value = "idEmpresaVariedad") Integer idEmpresa,
			@Param(value = "semanaDesde") Integer desde, @Param(value = "semanaHasta") Integer hasta);

}
