package com.keysist.roses.model.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import com.keysist.roses.model.entity.Usuario;

@RepositoryRestResource(path = "usuario")
public interface UsuarioRepository extends PagingAndSortingRepository<Usuario, Integer> {

	@RestResource(path = "by-cedula")
	Usuario findByIdentificacion(@Param(value = "identificacion") String cedula);

	@RestResource(path = "by-tipo")
	@Query("select new Usuario(usu.id, usu.activo, usu.nombres, usu.apellidos, usu.identificacion) from Usuario usu where usu.empresa.tipo.codigo=:codigo and usu.activo='SI' and usu.empresa.id in (select uv.empresa.id from EmpresaVariedad uv where  uv.disponible='SI') and usu.identificacion not in (select cat.codigo from Catalogo cat where cat.id=:s) order by usu.nombres,usu.apellidos")
	List<Usuario> findByTipo(@Param(value = "codigo") String codigo);
	
	@RestResource(path = "by-tipo-sob")
	@Query("select new Usuario(usu.id, usu.activo, usu.nombres, usu.apellidos, usu.identificacion) from Usuario usu where usu.empresa.tipo.codigo=:codigo and usu.activo='SI' and usu.empresa.id in (select uv.empresa.id from EmpresaVariedad uv where uv.disponible='SI') order by usu.nombres,usu.apellidos")
	List<Usuario> findByTipoAndSobr(@Param(value = "codigo") String codigo);
	
	@RestResource(path = "by-idUsuario")
	@Query("select usu from Usuario usu inner join usu.empresa emp where usu.id=:idUsuario or :idUsuario=-1 order by usu.apellidos")
	Usuario findByIdUsuario(@Param(value = "idUsuario") Integer idUsuario);
	
	@RestResource(path = "all")
	@Query("select new Usuario(usu) from Usuario usu where usu.activo='SI' order by usu.apellidos")
	List<Usuario> all();
	
	@RestResource(path = "find-by-filter")
	@Query("select usu from Usuario usu where (upper(usu.nombres) like concat('%',upper(:filtro),'%') or upper(usu.apellidos) like concat('%',upper(:filtro),'%') or upper(usu.empresa.nombre) like concat('%',upper(:filtro),'%') or usu.identificacion like concat('%',:filtro,'%')) and usu.empresa.id=:idEmpresa and usu.activo='SI' order by usu.apellidos")
	List<Usuario> findByFilter(@Param(value = "filtro") String filtro,@Param(value = "idEmpresa") Integer idEmpresa);
	
	@RestResource(path = "find-by-perfil")
	@Query("select distinct usu from Usuario usu inner join usu.usuarioPerfiles up inner join up.perfil per where usu.empresa.id=:idEmpresa and usu.activo='SI' and per.nombre=:perfil order by usu.apellidos")
	List<Usuario> findByPerfil(@Param(value = "perfil") String filtro,@Param(value = "idEmpresa") Integer idEmpresa);

}
