package com.keysist.roses.model.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import com.keysist.roses.model.entity.Empresa;
import com.keysist.roses.model.entity.EmpresaVariedad;
import com.keysist.roses.model.entity.Variedad;
import com.keysist.roses.model.enums.ValorSiNoEnum;

@RepositoryRestResource(path = "empresavariedad")
public interface EmpresaVariedadRepository extends PagingAndSortingRepository<EmpresaVariedad, Integer> {

	@RestResource(path = "by-empresa")
	@Query("select new EmpresaVariedad(usv) from EmpresaVariedad usv inner join usv.empresa usu inner join usv.variedad va where va.disponible='SI' and usu.id=:idEmpresa and (usv.disponible=:disponible or :disponible='TODOS') order by va.nombre")
	List<EmpresaVariedad> findCutomByUsuarioAndDisponible(@Param(value = "idEmpresa") Integer idEmpresa, @Param(value = "disponible") ValorSiNoEnum disponible);
	
	@Query("select usv from EmpresaVariedad usv inner join usv.empresa usu where usu.id=:idEmpresa and (usv.disponible=:disponible or :disponible='TODOS')")
	List<EmpresaVariedad> findByUsuarioAndDisponible(@Param(value = "idEmpresa") Integer idEmpresa, @Param(value = "disponible") ValorSiNoEnum disponible);
	
	@RestResource(path = "by-idUsuario")
	List<EmpresaVariedad> findByEmpresaAndDisponible(@Param(value = "empresa") Empresa empresa,@Param(value = "disponible") ValorSiNoEnum disponible);
	
	List<EmpresaVariedad> findByEmpresaAndVariedad(@Param(value = "empresa") Empresa empresa,@Param(value = "variedad") Variedad variedad);
	
	
	List<EmpresaVariedad> findByVariedad( @Param(value = "variedad") Variedad variedad);
	
	@RestResource(path = "by-usuarioAndSucursal") 
	@Query("select usv from EmpresaVariedad usv inner join usv.empresa usu  where usu.id=:idEmpresa and usv.disponible='SI' ")
	List<EmpresaVariedad> findByUsuario(@Param(value = "idEmpresa") Integer idEmpresa);
	
	
	
	List<EmpresaVariedad> findByIdIn(List<Integer> idEmpresasVariedad);

}
