package com.keysist.roses.model.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import com.keysist.roses.model.entity.CostoVariedad;
import com.keysist.roses.model.entity.EmpresaVariedad;

@RepositoryRestResource(path = "costovariedad")
public interface CostoVariedadRepository extends PagingAndSortingRepository<CostoVariedad, Integer> {

	@RestResource(path = "by-idUsuario")
	@Query("select new CostoVariedad(cv) from CostoVariedad cv inner join cv.empresaVariedad uv where uv.empresa.id=:idEmpresa ")
	List<CostoVariedad> findByIdEmpresa(@Param(value = "idEmpresa") Integer idUsuario);
	
	List<CostoVariedad> findByEmpresaVariedad(@Param(value = "empresaVariedad") EmpresaVariedad empresaVariedad);
	
	@RestResource(path = "by-All")
	@Query("select cv from CostoVariedad cv ")
	List<CostoVariedad> findAll();
	
}
