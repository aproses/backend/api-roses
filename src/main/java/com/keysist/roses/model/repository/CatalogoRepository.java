package com.keysist.roses.model.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import com.keysist.roses.model.entity.Catalogo;

@RepositoryRestResource(path = "catalogo")
public interface CatalogoRepository extends PagingAndSortingRepository<Catalogo, Integer> {

	@RestResource(path = "by-grupo")
	@Query("select new Catalogo(cat) from Catalogo cat where cat.codigoGrupo=:codigoGrupo and cat.global='SI' order by cat.nombre,cat.valor asc")
	List<Catalogo> findByGrupoRest(@Param(value = "codigoGrupo") String codigoGrupo);
	
	
	@RestResource(path = "by-grupo-ordered")
	@Query("select new Catalogo(cat) from Catalogo cat where cat.codigoGrupo=:codigoGrupo and cat.global='SI' order by cat.orden")
	List<Catalogo> findByGrupoOrderesRest(@Param(value = "codigoGrupo") String codigoGrupo);
	
	@Query("select cat from Catalogo cat where cat.codigoGrupo=:codigoGrupo and cat.estado='ACTIVO' and cat.global='SI' order by cat.valor asc")
	List<Catalogo> findByGrupo(@Param(value = "codigoGrupo") String codigoGrupo);
	
	@RestResource(path = "by-grupo-native")
	@Query(name = "select cat.id from s_catalogo cat where cat.sc_codigoGrupo=:codigoGrupo and cat.sc_estado='ACTIVO' and cat.global='SI' order by cat.sc_valor asc",nativeQuery = true)
	List<Integer> findByCodigoGrupo(@Param(value = "codigoGrupo") String codigoGrupo);

	@RestResource(path = "groups-active")
	@Query("select cat from Catalogo cat where cat.codigoGrupo is null and cat.estado='ACTIVO' and cat.global='SI' order by cat.nombre asc")
	List<Catalogo> findGroupsActive();

	@RestResource(path = "by-filter")
	@Query("select cat from Catalogo cat where cat.codigoGrupo=:c and upper(cat.nombre) like concat(upper(:f),'%') and cat.global='SI' order by cat.codigo, cat.valor asc")
	List<Catalogo> findByGrupoAndName(@Param(value = "c") String codigoGrupo,
			@Param(value = "f") String filtro);

	@Query("select cat from Catalogo cat where cat.codigo=:codigo and cat.id!=:id and cat.global='SI'")
	List<Catalogo> findByCodigoAndId(@Param(value = "codigo") String codigo, @Param(value = "id") Integer id);
	
	
	@Query("select cat from Catalogo cat where cat.codigo=:codigo order by cat.orden")
	List<Catalogo> findByCodigoEmpresa(@Param(value = "codigo") String codigo);

	@Query("select cat from Catalogo cat where cat.codigo=:codigo and cat.global='SI'")
	List<Catalogo> findByCodigo(@Param(value = "codigo") String codigo);
	
	
	@RestResource(path = "by-global")
	@Query(name = "select cat.id from s_catalogo cat where cat.sc_codigoGrupo=:codigoGrupo and cat.sc_estado='ACTIVO' and cat.global='SI' order by cat.sc_valor asc",nativeQuery = true)
	List<Integer> findByGlobal(@Param(value = "codigoGrupo") String codigoGrupo);

}
