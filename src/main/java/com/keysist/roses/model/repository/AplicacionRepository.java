package com.keysist.roses.model.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.keysist.roses.model.entity.Aplicacion;
import com.keysist.roses.model.enums.EstadoAplicacionEnum;

@RepositoryRestResource(path = "aplicacion")
public interface AplicacionRepository extends PagingAndSortingRepository<Aplicacion, Integer> {

	@Query("select ap from Aplicacion ap inner join ap.empresaBloque emb inner join emb.empresa usu where (emb.id=:idEmpresaBloque or :idEmpresaBloque=-1) and  ap.tipo.id=:idTipo and usu.id=:idEmpresa and  (ap.fechaAplicacion is null or date(ap.fechaAplicacion) between  date(:fechaDesde) and date(:fechaHasta)) and ap in (select ap1.aplicacion from AplicacionProducto ap1 where ap1.aplicacion.empresaBloque.empresa=ap.empresaBloque.empresa and ( ap1.empresaProducto.id=:idEmpresaProducto or :idEmpresaProducto=-1 ) )  order by ap.fechaAplicacion desc")
	List<Aplicacion> findByFecha(@Param(value = "fechaDesde") LocalDate fechaDesde,
			@Param(value = "fechaHasta") LocalDate fechaHasta, @Param(value = "idEmpresa") Integer idEmpresa,
			@Param(value = "idTipo") Integer idTipo, @Param(value = "idEmpresaProducto") Integer idEmpresaProducto,
			@Param(value = "idEmpresaBloque") Integer idEmpresaBloque);

	@Query("select ap from Aplicacion ap inner join ap.empresaBloque.empresa usu where  usu.id=:idEmpresa and ap.estado='INGRESADO'  order by ap.fechaAplicacion, ap.id asc")
	List<Aplicacion> obtenerPendientes(@Param(value = "idEmpresa") Integer idEmpresa);

	@Query("select ap from Aplicacion ap inner join ap.empresaBloque.empresa usu where  usu.id=:idEmpresa and ap.estado=:estado and ap.tipo in (select cat from Catalogo cat where cat.codigo in ( :codigo1,:codigo2))  order by ap.fechaAplicacion, ap.id asc")
	List<Aplicacion> obtenerByTipoAndEstado(@Param(value = "idEmpresa") Integer idEmpresa,
			@Param(value = "codigo1") String codigo1, @Param(value = "codigo2") String codigo2,
			@Param(value = "estado") EstadoAplicacionEnum estado);

}
