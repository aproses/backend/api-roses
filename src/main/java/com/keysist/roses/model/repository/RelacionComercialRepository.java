package com.keysist.roses.model.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.keysist.roses.model.entity.RelacionComercial;

@RepositoryRestResource( path = "relacioncomercial")
public interface RelacionComercialRepository extends PagingAndSortingRepository<RelacionComercial, Integer> {

	@Query("select rc from RelacionComercial rc inner join rc.proveedor pro inner join rc.proveedor cl where pro.id=:idEmpresa  and ( upper(cl.nombre) like concat('%',upper(:filtro),'%')  or cl.ruc like concat('%',:filtro,'%'))  order by cl.nombre")
	List<RelacionComercial> findClienteByFilter(@Param(value = "filtro") String filtro,@Param(value = "idEmpresa") Integer idEmpresa);
	
	@Query("select rc from RelacionComercial rc inner join rc.proveedor pro inner join rc.cliente cl where  cl.id=:idEmpresa and (upper(pro.nombre) like concat('%',upper(:filtro),'%')  or pro.ruc like concat('%',:filtro,'%'))  order by pro.nombre")
	List<RelacionComercial> findProveedoresByFilter(@Param(value = "filtro") String filtro,@Param(value = "idEmpresa") Integer idEmpresa);
	
	@Query("select rc from RelacionComercial rc inner join rc.proveedor pro inner join rc.cliente cl where cl.ruc=:cliente and pro.ruc=:proveedor")
	Optional<RelacionComercial> findByProveedorAndCliente(@Param(value = "proveedor") String rucProveedor,@Param(value = "cliente") String rucCliente);
	
	
	@Query("select rc from RelacionComercial rc inner join rc.proveedor pro inner join rc.cliente cl where cl.id=:idCliente and pro.id=:idProveedor ")
	Optional<RelacionComercial> findByProveedorAndClienteId(@Param(value = "idProveedor") Integer idProveedor,@Param(value = "idCliente") Integer idCliente);
	
	
}
