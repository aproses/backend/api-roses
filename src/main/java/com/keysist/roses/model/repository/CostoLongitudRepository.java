package com.keysist.roses.model.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import com.keysist.roses.model.entity.Catalogo;
import com.keysist.roses.model.entity.CostoLongitud;
import com.keysist.roses.model.entity.CostoVariedad;

@RepositoryRestResource(path = "costolongitud")
public interface CostoLongitudRepository extends PagingAndSortingRepository<CostoLongitud, Integer> {

	@RestResource(path = "by-CostoVariedadAndLongitud")
	@Query("select new CostoLongitud(cv) from CostoLongitud cv where cv.costoVariedad=:costoVariedad and cv.longitud=:longitud ")
	List<CostoLongitud> findByCostoVariedadAndLongitud (@Param(value = "costoVariedad") CostoVariedad costoVariedad, @Param(value = "longitud") Catalogo longitud);
	
	@RestResource(path = "by-idCostoVariedad")
	@Query("select new CostoLongitud(cv) from CostoLongitud cv inner join cv.costoVariedad c inner join cv.longitud l where c.id=:idCostoVariedad order by l.valor")
	List<CostoLongitud> findByIdCostoVariedad (@Param(value = "idCostoVariedad") Integer idCostoVariedad); 
	
}
