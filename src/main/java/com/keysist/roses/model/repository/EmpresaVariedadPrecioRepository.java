package com.keysist.roses.model.repository;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.keysist.roses.model.entity.EmpresaVariedadPrecio;
import com.keysist.roses.model.enums.EstadoEntregaProcesoEnum;
import com.keysist.roses.model.enums.TipoEntregaEnum;

@RepositoryRestResource(path = "empresavariedadprecio")
public interface EmpresaVariedadPrecioRepository extends PagingAndSortingRepository<EmpresaVariedadPrecio, Integer> {

	@Query("select det from EmpresaVariedadPrecio det inner join det.empresaVariedad vari"
			+ " where det.id!=:id and (det.cliente.id=:idCliente or :idCliente=-1) and (vari.id=:idEmpresaVariedad or :idEmpresaVariedad=-1) and (det.longitud.id=:idLongitud or :idLongitud=-1) "
			+ "and  (date(det.fechaDesde) between date(:fechaDesde) and date(:fechaHasta)  or date(det.fechaHasta) between date(:fechaDesde) and date(:fechaHasta)) order by det.fechaDesde,vari.variedad.nombre,det.longitud.nombre asc")
	List<EmpresaVariedadPrecio> filterPrecioByLongitud(
			@Param(value = "fechaDesde") LocalDate fechaDesde,
			@Param(value = "fechaHasta") LocalDate fechaHasta,
			@Param(value = "idEmpresaVariedad") Integer idEmpresaVariedad,
			@Param(value = "idLongitud") Integer idLongitud, 
			@Param(value = "id") Integer id,
			@Param(value = "idCliente") Integer idCliente);
	
	@Query("select det from EmpresaVariedadPrecio det inner join det.empresaVariedad vari"
			+ " where det.id!=:id and (det.cliente.id=:idCliente or :idCliente=-1) and (vari.id=:idEmpresaVariedad or :idEmpresaVariedad=-1) and (det.longitud.id=:idLongitud or :idLongitud=-1) "
			+ "and  (date(:fechaDesde) between date(det.fechaDesde) and date(det.fechaHasta)  or date(:fechaHasta) between date(det.fechaDesde) and date(det.fechaHasta))")
	List<EmpresaVariedadPrecio> findPrecioByLongitud(@Param(value = "fechaDesde") LocalDate fechaDesde,
			@Param(value = "fechaHasta") LocalDate fechaHasta,
			@Param(value = "idEmpresaVariedad") Integer idEmpresaVariedad,
			@Param(value = "idLongitud") Integer idLongitud, 
			@Param(value = "id") Integer id,
			@Param(value = "idCliente") Integer idCliente);

	@Modifying
	@Transactional
	@Query("update EntregaTallo u set u.precio = :precio where u.longitud.id=:idLongitud and "
			+ "u.entregaPoscosecha in "
			+ "		(select entp from EntregaPoscosecha entp inner join entp.entrega ent inner join ent.cliente cli inner join entp.empresaVariedad empv inner join empv.variedad varie "
			+ " 		where  cli.id=:idCliente and "
			+ "			date(ent.fechaEntrega) between date(:desde) and date(:hasta) and "
			+ "			ent.tipo=:tipoEntrega and entp.estado=:estado and varie.id in ( "
			+ "				select empvar.variedad.id from EmpresaVariedad empvar where empvar.id=:idEmpresaVariedad"
			+ "			))")
	void updatePrecio(@Param(value = "precio") BigDecimal precio, @Param(value = "idCliente") Integer idCliente,
			@Param(value = "desde") LocalDate desde, @Param(value = "hasta") LocalDate hasta,
			@Param(value = "tipoEntrega") TipoEntregaEnum tipoEntrega,
			@Param(value = "idEmpresaVariedad") Integer idEmpresaVariedad,
			@Param(value = "estado") EstadoEntregaProcesoEnum estado, @Param(value = "idLongitud") Integer idLongitud);

}
