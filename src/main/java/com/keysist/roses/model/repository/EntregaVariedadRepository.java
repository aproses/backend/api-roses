package com.keysist.roses.model.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import com.keysist.roses.model.entity.Entrega;
import com.keysist.roses.model.entity.EntregaVariedad;
import com.keysist.roses.model.enums.EstadoEntregaEnum;

@RepositoryRestResource(path = "entregaVariedad")
public interface EntregaVariedadRepository extends PagingAndSortingRepository<EntregaVariedad, Integer> {

	@RestResource(path = "by-entrega-and-usvariedad")
	@Query("select det from EntregaVariedad det where det.empresaVariedad.id=:idEmpresaVariedad and det.entrega.id=:idEntrega and det.estado=:estado order by det.totalTallos desc")
	List<EntregaVariedad> findByEntAndEmpresaVariedad(@Param(value = "idEntrega") Integer idEntrega,
			@Param(value = "idEmpresaVariedad") Integer idEmpresaVariedad,@Param(value = "estado") EstadoEntregaEnum estado);

	@RestResource(path = "by-entrega")
	@Query("select det from EntregaVariedad det where det.entrega.id=:idEntrega order by det.empresaVariedad.variedad.nombre")
	List<EntregaVariedad> findByEntrega(@Param(value = "idEntrega") Integer idEntrega);

	// @Query("select sum(det.sobrante) from EntregaVariedad det where det.id in
	// (select max(dets.id) from EntregaVariedad dets where
	// dets.empresaVariedad.id=:idEmpresaVariedad)")
	// Long findByVariedadAndUsuario(@Param(value = "idEmpresaVariedad") Integer
	// idEmpresaVariedad);

	@Query("select distinct vari.nombre, sum(det.mallas) from EntregaVariedad det inner join det.entrega ent inner join det.empresaVariedad uv inner join uv.variedad vari  where ent.proveedor.id=:idEntrega and (date(ent.fechaEntrega) between  :fechaDesde and :fechaHasta) and ent.estado in (:estado1,:estado2,:estado3) and ent.id in (select de.entrega.id from EntregaVariedad de inner join de.empresaVariedad uv where uv.disponible='SI') group by vari.nombre order by vari.nombre")
	List<Object[]> obtenerTotalVariedad(@Param(value = "idEntrega") Integer idEntrega,
			@Param(value = "fechaDesde") Date fechaDesde, @Param(value = "fechaHasta") Date fechaHasta,
			@Param(value = "estado1") EstadoEntregaEnum estado1, @Param(value = "estado2") EstadoEntregaEnum estado2,
			@Param(value = "estado3") EstadoEntregaEnum estado3);

	@Query("select det.estado ,va.nombre,sum(det.mallas) from EntregaVariedad det inner join det.entrega er inner join det.empresaVariedad.variedad va where det.estado in (:estado1,:estado2,:estado3) group by det.estado, va.nombre order by va.nombre")
	List<Object[]> getEntregaDiaria(@Param(value = "estado1") EstadoEntregaEnum estado,
			@Param(value = "estado2") EstadoEntregaEnum estado2, @Param(value = "estado3") EstadoEntregaEnum estado3);

	@RestResource(path = "by-entrega")
	@Query("select sum(det.totalTallos), det.empresaVariedad.id from EntregaVariedad det inner join det.entrega ent where ent=:entrega and det.estado=:estadoDetalle group by det.empresaVariedad.id order by det.empresaVariedad")
	List<Object[]> findByEntregaAndEstado(@Param(value = "entrega") Entrega entrega,
			@Param(value = "estadoDetalle") EstadoEntregaEnum estado);

}
