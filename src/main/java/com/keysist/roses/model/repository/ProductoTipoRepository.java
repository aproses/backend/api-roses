package com.keysist.roses.model.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.keysist.roses.model.entity.Catalogo;
import com.keysist.roses.model.entity.Producto;
import com.keysist.roses.model.entity.ProductoTipo;
import com.keysist.roses.model.enums.ValorSiNoEnum;

@RepositoryRestResource(path = "productotipo")
public interface ProductoTipoRepository extends PagingAndSortingRepository<ProductoTipo, Integer> {

	List<ProductoTipo> findByProductoAndActivo(Producto producto, ValorSiNoEnum activo);

	List<ProductoTipo> findByProducto(Producto producto);

	Optional<ProductoTipo> findByProductoAndTipo(Producto producto, Catalogo tipo);

	List<ProductoTipo> findByTipoNotInAndProducto(List<Catalogo> tipos, Producto producto);
	
	void deleteAllByProductoIs(Producto p);
}
