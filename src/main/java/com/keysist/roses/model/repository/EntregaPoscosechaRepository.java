package com.keysist.roses.model.repository;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.keysist.roses.model.entity.EmpresaVariedad;
import com.keysist.roses.model.entity.Entrega;
import com.keysist.roses.model.entity.EntregaPoscosecha;
import com.keysist.roses.model.enums.EstadoEntregaProcesoEnum;
import com.keysist.roses.model.enums.TipoEntregaEnum;

@RepositoryRestResource(path = "entregaposcosecha")
public interface EntregaPoscosechaRepository extends PagingAndSortingRepository<EntregaPoscosecha, Integer> {

	@Query("select det from EntregaPoscosecha det inner join det.entrega ent left join ent.proveedor pro left join ent.cliente cli inner join det.empresaVariedad ev"
			+ " where ( pro.id=:idProveedor or :idProveedor=-1) and (cli.id=:idCliente or :idCliente=-1) "
			+ " and date(ent.fechaEntrega) = date(:fechaEntrega) "
			+ "and ent.tipo=:tipoEntrega and ev.disponible='SI' and ev.id=:idEmpresaVariedad")
	Optional<EntregaPoscosecha> findByEntregaVariedad(@Param(value = "idProveedor") Integer idProveedor,
			@Param(value = "idCliente") Integer idCliente, @Param(value = "fechaEntrega") LocalDate fechaEntrega,
			@Param(value = "tipoEntrega") TipoEntregaEnum tipoEntrega,
			@Param(value = "idEmpresaVariedad") Integer idEmpresaVariedad);

	@Query("select det from EntregaPoscosecha det inner join det.entrega ent left join ent.proveedor pro left join ent.cliente cli inner join det.empresaVariedad ev inner join ev.variedad vari"
			+ " where ( pro.id=:idProveedor or :idProveedor=-1) and (cli.id=:idCliente or :idCliente=-1) "
			+ " and date(ent.fechaEntrega) = date(:fechaEntrega) "
			+ "and ent.tipo=:tipoEntrega  and ev.disponible='SI' and ev.id=:idEmpresaVariedad and det.estado=:estado order by vari.nombre")
	Optional<EntregaPoscosecha> findByEntregaVariedad(@Param(value = "idProveedor") Integer idProveedor,
			@Param(value = "idCliente") Integer idCliente, @Param(value = "fechaEntrega") LocalDate fechaEntrega,
			@Param(value = "tipoEntrega") TipoEntregaEnum tipoEntrega,
			@Param(value = "idEmpresaVariedad") Integer idEmpresaVariedad,
			@Param(value = "estado") EstadoEntregaProcesoEnum estado);

	@Query("select distinct ev from EntregaPoscosecha det inner join det.entrega ent left join ent.proveedor pro left join ent.cliente cli inner join det.empresaVariedad ev"
			+ " where ( pro.id=:idProveedor or :idProveedor=-1) and (cli.id=:idCliente or :idCliente=-1) "
			+ " and date(ent.fechaEntrega) = date(:fechaEntrega) and ent.tipo=:tipoEntrega and ev.disponible='SI' "
			+ "")
	List<EmpresaVariedad> findEmpresaVariedad(@Param(value = "idProveedor") Integer idProveedor,
			@Param(value = "idCliente") Integer idCliente, @Param(value = "fechaEntrega") LocalDate fechaEntrega,
			@Param(value = "tipoEntrega") TipoEntregaEnum tipoEntrega);

	@Query("select distinct ev from EntregaPoscosecha ev where ev.entrega=:entrega  and ev.empresaVariedad=:empresaVariedad")
	Optional<EntregaPoscosecha> findEntregaAndEmpresaVariedad(@Param(value = "entrega") Entrega entrega,
			@Param(value = "empresaVariedad") EmpresaVariedad empresaVariedad);

	@Query("select det.id,vari.nombre,ent.fechaEntrega from EntregaPoscosecha det inner join det.entrega ent left join ent.proveedor pro left join ent.cliente cli inner join det.empresaVariedad ev inner join ev.variedad vari"
			+ " where ( pro.id=:idProveedor or :idProveedor=-1) and (cli.id=:idCliente or :idCliente=-1) "
			+ " and date(ent.fechaEntrega) between date(:desde) and date(:hasta) "
			+ "and ent.tipo=:tipoEntrega and det.estado!='RECEPCIONADO'  group by vari.nombre,det.id,ent.fechaEntrega order by vari.nombre, ent.fechaEntrega")
	List<Object[]> findByRange(@Param(value = "idProveedor") Integer idProveedor,
			@Param(value = "idCliente") Integer idCliente, @Param(value = "desde") LocalDate desde,
			@Param(value = "hasta") LocalDate hasta, @Param(value = "tipoEntrega") TipoEntregaEnum tipoEntrega);
	
	
	
	@Query("select det.id,vari.nombre,ent.fechaEntrega from EntregaPoscosecha det inner join det.entrega ent left join ent.proveedor pro left join ent.cliente cli inner join det.empresaVariedad ev inner join ev.variedad vari"
			+ " where ( pro.id=:idProveedor or :idProveedor=-1) and (cli.id=:idCliente or :idCliente=-1) "
			+ " and date(ent.fechaEntrega) between date(:desde) and date(:hasta) "
			+ "and ent.tipo=:tipoEntrega   group by vari.nombre,det.id,ent.fechaEntrega order by vari.nombre, ent.fechaEntrega")
	List<Object[]> findProveedorByRange(@Param(value = "idProveedor") Integer idProveedor,
			@Param(value = "idCliente") Integer idCliente, @Param(value = "desde") LocalDate desde,
			@Param(value = "hasta") LocalDate hasta, @Param(value = "tipoEntrega") TipoEntregaEnum tipoEntrega);

	@Modifying
	@Transactional
	@Query("update EntregaTallo u set u.precio = :precio where u.longitud.id=:idLongitud and "
			+ "u.entregaPoscosecha in "
			+ "		(select entp from EntregaPoscosecha entp inner join entp.entrega ent inner join ent.cliente cli inner join entp.empresaVariedad empv inner join empv.variedad varie "
			+ " 		where  cli.id=:idCliente and "
			+ "			date(ent.fechaEntrega) between date(:desde) and date(:hasta) and "
			+ "			ent.tipo=:tipoEntrega and entp.estado=:estado and varie.id in ( "
			+ "				select empvar.variedad.id from EmpresaVariedad empvar where empvar.id=:idEmpresaVariedad"
			+ "			))")
	void updatePrecio(@Param(value = "precio") BigDecimal precio, @Param(value = "idCliente") Integer idCliente,
			@Param(value = "desde") LocalDate desde, @Param(value = "hasta") LocalDate hasta,
			@Param(value = "tipoEntrega") TipoEntregaEnum tipoEntrega,
			@Param(value = "idEmpresaVariedad") Integer idEmpresaVariedad,
			@Param(value = "estado") EstadoEntregaProcesoEnum estado, @Param(value = "idLongitud") Integer idLongitud);

}
