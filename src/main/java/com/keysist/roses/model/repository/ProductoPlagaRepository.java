package com.keysist.roses.model.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.keysist.roses.model.entity.Catalogo;
import com.keysist.roses.model.entity.Producto;
import com.keysist.roses.model.entity.ProductoPlaga;

@RepositoryRestResource(path = "productoplaga")
public interface ProductoPlagaRepository extends PagingAndSortingRepository<ProductoPlaga, Integer> {

	List<ProductoPlaga> findByProducto(Producto producto);

	Optional<ProductoPlaga> findByProductoAndPlaga(Producto producto, Catalogo plaga);
	
	List<ProductoPlaga> findByPlagaNotInAndProducto(List<Catalogo> plagas,Producto producto);
	
	void deleteAllByProductoIs(Producto p);
}
