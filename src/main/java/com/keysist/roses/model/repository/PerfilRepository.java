package com.keysist.roses.model.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

import com.keysist.roses.model.entity.Catalogo;
import com.keysist.roses.model.entity.Perfil;
import com.keysist.roses.model.enums.ValorSiNoEnum;

@RepositoryRestResource( path = "perfil")
public interface PerfilRepository extends PagingAndSortingRepository<Perfil, Integer> {

	@RestResource(path = "by-user")
	@Query("select new Perfil(per.id,per.activo,per.nombre) from Perfil per inner join per.usuarioPerfiles up where up.usuario.id=:id and up.activo=:estado and per.activo=:estado")
	List<Perfil> findByUserAndActivo(@Param(value = "id") Integer id, @Param(value = "estado") ValorSiNoEnum activo);
	
	@RestResource(path = "all-perfil")
	@Query("select new Perfil(per.id,per.activo,per.nombre) from Perfil per where per.activo='SI' ")
	List<Perfil> getAllPerfil();
	
	@RestResource(path = "all-perfil-tipo")
	@Query("select new Perfil(per.id,per.activo,per.nombre) from Perfil per where per.tipo.id=:idt and per.activo='SI' order by per.nombre")
	List<Perfil> getAllPerfilByTipo(@Param(value = "idt") Integer idt);
	
	@RestResource(path = "by-idPerfil")
	@Query("select new Perfil(per.id,per.activo,per.nombre) from Perfil per where per.id=:idP ")
	List<Perfil> findByIdPerfil(@Param(value = "idP") Integer idP);
	
	Optional<Perfil> findByNombreAndTipo(String nombre,Catalogo tipo);
}
