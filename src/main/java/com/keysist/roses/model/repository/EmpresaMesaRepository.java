package com.keysist.roses.model.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import com.keysist.roses.model.entity.EmpresaMesa;

@RepositoryRestResource(path = "empresamesa")
public interface EmpresaMesaRepository extends PagingAndSortingRepository<EmpresaMesa, Integer> {

	@RestResource(path = "by-empresa")
	@Query("select usv from EmpresaMesa usv inner join usv.empresa usu  where usu.id=:idEmpresa and usu.activo='SI' order by usv.mesa.nombre")
	List<EmpresaMesa> findByEmpresa(@Param(value = "idEmpresa") Integer idEmpresa);
	
	

	@Query("select usv from EmpresaMesa usv inner join usv.empresa usu  where usu.id=:idEmpresa and usv.mesa.id=:idCatalogo order by usv.mesa.nombre")
	List<EmpresaMesa> findByEmpresaAndCatalogo(Integer idEmpresa,Integer idCatalogo);
}
