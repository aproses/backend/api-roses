package com.keysist.roses.model.repository;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.keysist.roses.model.entity.Catalogo;
import com.keysist.roses.model.entity.Producto;
import com.keysist.roses.model.entity.ProductoComposicion;

@RepositoryRestResource(path = "productocomposicion")
public interface ProductoComposicionRepository extends PagingAndSortingRepository<ProductoComposicion, Integer> {

	List<ProductoComposicion> findByProducto(Producto producto);
	
	
	List<ProductoComposicion> findByElementoNotInAndProducto(List<Catalogo> plagas,Producto producto);
	
	void deleteAllByProductoIs(Producto p);
}
