package com.keysist.roses;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootApplication
@EntityScan({ "com.keysist.roses.model.entity" })
@EnableScheduling
public class ApiRosesApplication implements CommandLineRunner{

	@Autowired
	private BCryptPasswordEncoder passwordEncoder;
	
	public static void main(String[] args) {
		SpringApplication.run(ApiRosesApplication.class, args);
	}
	@Override
	public void run(String... args) throws Exception {
		String pass= passwordEncoder.encode("Admin1411");
		System.out.println(pass);
		
	}

}
