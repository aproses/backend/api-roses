package com.keysist.roses.util;

import java.util.HashMap;

import com.keysist.roses.model.enums.TemplateEmailEnum;

public class TemplateEmail {

	public static String buildEmail(HashMap<String, String> data, TemplateEmailEnum id) {
		StringBuilder b = new StringBuilder();
		b.append(TemplateEmail.contenedorAbre());
		switch (id) {
		case REGISTRO:
			String links = data.get("links");
			b.append(TemplateEmail.titulo("Bienvenido a Roses"));
			b.append(TemplateEmail.saludo(String.format("Saludos %1s", data.get("usuario"))));
			b.append(TemplateEmail.contenido("Confirmamos su registro en nuestra plataforma"));
			b.append(TemplateEmail.contenido(
					"Ahora solo queda pendiente que validemos tu actividad económica y procederemos a activar tu cuenta."));

			if (!links.isEmpty()) {
				b.append("<br/>");
				b.append(TemplateEmail.contenido("Te sugerimos ver los siguientes videos de como usar el app"));
				String[] linksArray = links.split(",");
				for (int i = 0; i < linksArray.length; i++) {
					b.append(TemplateEmail.boton(linksArray[i], linksArray[i]));
				}
			}
			break;
		case ACTIVACION:
			String page = data.get("url_page");
			b.append(TemplateEmail.titulo("Cuenta activa"));
			b.append(TemplateEmail.saludo(String.format("Saludos %1s", data.get("usuario"))));
			b.append(TemplateEmail.contenido("Su usuario ya se encuentra activo en nuestra plataforma."));

			if (!page.isEmpty()) {
				b.append("<br/>");
				b.append(TemplateEmail.contenido(
						String.format("Ingresa al siguiente link <a href=\"%1s\">roses</a> y ya puedes configurar tu negocio", page)));

			}
			break;
		case CAMBIO_CLAVE:
			b.append(TemplateEmail.titulo("Cambio de Contraseña exitosa"));
			b.append(TemplateEmail.saludo(String.format("Saludos %1s", data.get("usuario"))));
			b.append(TemplateEmail
					.contenido(String.format("<strong>%1s</strong> es tú nueva clave.", data.get("clave"))));
			break;
		case RECUPERACION_CLAVE:
			b.append(TemplateEmail.titulo("Recuperación de Contraseña exitosa"));
			b.append(TemplateEmail.saludo(String.format("Saludos %1s", data.get("usuario"))));
			b.append(TemplateEmail
					.contenido(String.format("<strong>%1s</strong> es tú nueva clave.", data.get("clave"))));
			break;
		case ACTUALIZACION:
			b.append(TemplateEmail.titulo("Actualización de Datos"));
			b.append(TemplateEmail.saludo(String.format("Saludos %1s", data.get("usuario"))));
			b.append(TemplateEmail.contenido("Se ha actualizado tus datos exitosamente"));
			b.append(TemplateEmail.contenido("Si tú no lo has realizado, comunícate con nosotros."));
			break;
		case ENVIO_OTP:
			b.append(TemplateEmail.titulo("Código de Verificación"));
			b.append(TemplateEmail.saludo("Saludos"));
			b.append(TemplateEmail.contenido(String.format(
					"Copie este código <strong style='font-size:20px;'>%1s</strong> e ingrese en la aplicación para continuar con su registro. Válida hasta %2s",
					data.get("clave"), data.get("tiempo"))));
			b.append(TemplateEmail.contenido("Si tú no lo has realizado, comunícate con nosotros"));
			break;
		case CAMBIO_PASS_EXITO:
			b.append(TemplateEmail.titulo("Confirmación de cambio de Contraseña"));
			b.append(TemplateEmail.saludo(String.format("Saludos %1s", data.get("usuario"))));
			b.append(TemplateEmail.contenido("Tú contraseña de actualizo correctamente"));
			b.append(TemplateEmail.contenido("Si tú no lo has realizado, comunícate con nosotros"));
		default:
			break;
		}
		b.append(TemplateEmail.footer1());
		b.append(TemplateEmail.footer2());
		b.append(TemplateEmail.contenedorCierra());
		return b.toString();

	}

	static String titulo(String valor) {
		if (valor != null && !valor.isEmpty())
			return String.format("<h1 style=\"text-align: center;padding: 10px;font-family: monospace;\">%1s</h1>",
					valor);
		return "";
	}

	static String saludo(String valor) {
		if (valor != null && !valor.isEmpty())
			return String.format("<div style=\"padding: 0px 20px;font-family: system-ui; color: #363636;\">%1s</div>",
					valor);
		return "";
	}

	static String contenido(String valor) {
		if (valor != null && !valor.isEmpty())
			return String.format("<div style=\"padding: 10px 20px;font-family: system-ui; color: #363636;\">%1s</div>",
					valor);
		return "";
	}

	static String boton(String texto, String url) {
		if (texto != null && !texto.isEmpty())
			return String.format(
					"<div style=\"padding: 20px 20px;\"><a href=\"%1s\" style=\"background-color: #680281;display: block;width: auto;text-decoration: none;color: #ffffff;font-size: 17px;font-family: ArialMT,Arial,sans-serif;font-weight: normal;border-radius: 5px;padding: 12px 10px;margin: 0;text-align: center;\">%2s</a></div>",
					url, texto);
		return "";
	}

	static String footer1() {
		return "<div style=\"border-top: 1px solid #f14c4c;padding: 0px 20px;border-spacing: 0;color: #363636;font-family: ArialMT,Arial,sans-serif;font-size: 15px;width: auto;font-size: 12px;\">Roses</div>";
	}

	static String footer2() {
		return "<div style=\"padding:0px 0px 20px 20px;border-spacing: 0;color: #363636;font-family: ArialMT,Arial,sans-serif;font-size: 15px;width: auto;font-size: 12px;\">\"Siempre innovando\"</div>";
	}

	static String contenedorAbre() {
		return "<div style=\"margin: 2vh 8vw; border: 0px solid #1C6EA4;border-radius: 15px;-webkit-box-shadow: 0px 0px 37px 12px rgba(210,160,160,0.78);box-shadow: 0px 0px 37px 12px rgba(210,160,160,0.78);background: #fd9a9a;background: -moz-linear-gradient(-45deg, #fd9a9a 0%, #FFE5F2 80%, #FFFBF6 100%);background: -webkit-linear-gradient(-45deg, #fd9a9a 0%, #FFE5F2 80%, #FFFBF6 100%);background: linear-gradient(135deg, #fd9a9a 0%, #FFE5F2 80%, #FFFBF6 100%);width: auto;\">";
	}

	static String contenedorCierra() {
		return "</div>";
	}
}
