INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor) VALUES ( 'TE_PROV', 'TIPOS_USUARIO', 'ACTIVO', 'Proveedor', '', '', 0.0);
INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor) VALUES ( 'TU_POSC', 'TIPOS_USUARIO', 'ACTIVO', 'Poscosecha', '', '', 0.0);

INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor,sc_global) VALUES ( 'BLOQUES', null, 'ACTIVO', 'Bloques', '', '', 0.0,'NO');

INSERT INTO public.s_perfil(sp_activo, sp_nombre,sp_tipo)VALUES ('SI', 'ROLE_SUPERADMIN',null);

INSERT INTO public.s_perfil(sp_activo, sp_nombre,sp_tipo)VALUES ('SI', 'ROLE_ADMIN',1);
INSERT INTO public.s_perfil(sp_activo, sp_nombre,sp_tipo)VALUES ('SI', 'ROLE_FERTILIZACION',1);
INSERT INTO public.s_perfil(sp_activo, sp_nombre,sp_tipo)VALUES ('SI', 'ROLE_FUMNIGACION',1);
INSERT INTO public.s_perfil(sp_activo, sp_nombre,sp_tipo)VALUES ('SI', 'ROLE_CULTIVO',1);
INSERT INTO public.s_perfil(sp_activo, sp_nombre,sp_tipo)VALUES ('SI', 'ROLE_ASESOR',1);
INSERT INTO public.s_perfil(sp_activo, sp_nombre,sp_tipo)VALUES ('SI', 'ROLE_REPORTE',1);

INSERT INTO public.s_perfil(sp_activo, sp_nombre,sp_tipo)VALUES ('SI', 'ROLE_ADMIN',2);
INSERT INTO public.s_perfil(sp_activo, sp_nombre,sp_tipo)VALUES ('SI', 'ROLE_FERTILIZACION',2);
INSERT INTO public.s_perfil(sp_activo, sp_nombre,sp_tipo)VALUES ('SI', 'ROLE_FUMNIGACION',2);
INSERT INTO public.s_perfil(sp_activo, sp_nombre,sp_tipo)VALUES ('SI', 'ROLE_CULTIVO',2);
INSERT INTO public.s_perfil(sp_activo, sp_nombre,sp_tipo)VALUES ('SI', 'ROLE_ASESOR',2);
INSERT INTO public.s_perfil(sp_activo, sp_nombre,sp_tipo)VALUES ('SI', 'ROLE_REPORTE',2);

INSERT INTO public.s_perfil(sp_activo, sp_nombre,sp_tipo)VALUES ('SI', 'ROLE_RECEPCION',2);
INSERT INTO public.s_perfil(sp_activo, sp_nombre,sp_tipo)VALUES ('SI', 'ROLE_PROCESO',2);
INSERT INTO public.s_perfil(sp_activo, sp_nombre,sp_tipo)VALUES ('SI', 'ROLE_CLASIFICADOR',2);
INSERT INTO public.s_perfil(sp_activo, sp_nombre,sp_tipo)VALUES ('SI', 'ROLE_VENTAS',2);
INSERT INTO public.s_perfil(sp_activo, sp_nombre,sp_tipo)VALUES ('SI', 'ROLE_CLIENTE',2);
INSERT INTO public.s_perfil(sp_activo, sp_nombre,sp_tipo)VALUES ('SI', 'ROLE_CONTADOR',2);
INSERT INTO public.s_perfil(sp_activo, sp_nombre,sp_tipo)VALUES ('SI', 'ROLE_MONITOREO',1);
INSERT INTO public.s_perfil(sp_activo, sp_nombre,sp_tipo)VALUES ('SI', 'ROLE_MONITOREO',2);


INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor) VALUES ( 'COLORES_ROSAS', null, 'ACTIVO', 'Colores de rosas', null, null, null);
INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor) VALUES ( 'LONGITUD_TALLO', null, 'ACTIVO', 'Longitud de tallos', null, null, null);
INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor) VALUES ( 'ENFERMEDADES', null, 'ACTIVO', 'Enfermedades', null, null, null);
INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor) VALUES ( 'UNIDAD_MEDIDA', null, 'ACTIVO', 'Unidad de Medidad', null, null, null);
INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor) VALUES ( 'TIPOS_APLICACIONES', null, 'ACTIVO', 'Tipos de aplicaciones', null, null, null);
INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor) VALUES ( 'TANQUES', null, 'ACTIVO', 'Tanques', null, null, null);
INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor) VALUES ( 'ELEMENTOS_QUIMICOS', null, 'ACTIVO', 'Elementos químicos', null, null, null);
INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor) VALUES ( 'TALLO_BONCHE', null, 'ACTIVO', 'Tallos por bonche', null, null, null);


INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor) VALUES ( 'GRUPO_NACIONAL', null, 'ACTIVO', 'Categoria de nacional', null, null, null);

INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor) VALUES ( 'COLOR_ROJO', 'COLORES_ROSAS', 'ACTIVO', 'Rojo', '', '', 0.0);
INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor) VALUES ( 'COLOR_BLANCO', 'COLORES_ROSAS', 'ACTIVO', 'Blanco', '', '', 0.0);
INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor) VALUES ( 'COLOR_VERDE', 'COLORES_ROSAS', 'ACTIVO', 'Verde', '', '', 0.0);
INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor) VALUES ( 'COLOR_ROSA', 'COLORES_ROSAS', 'ACTIVO', 'Rosa', '', '', 0.0);
INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor) VALUES ( 'COLOR_TOMATE', 'COLORES_ROSAS', 'ACTIVO', 'Tomate', '', '', 0.0);
INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor) VALUES ( 'COLOR_AMARILLO', 'COLORES_ROSAS', 'ACTIVO', 'Amarillo', '', '', 0.0);

INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor) VALUES ( 'LONGITUD_40', 'LONGITUD_TALLO', 'ACTIVO', '', '', '', 40.0);
INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor) VALUES ( 'LONGITUD_50', 'LONGITUD_TALLO', 'ACTIVO', '', '', '', 50.0);
INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor) VALUES ( 'LONGITUD_60', 'LONGITUD_TALLO', 'ACTIVO', '', '', '', 60.0);
INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor) VALUES ( 'LONGITUD_70', 'LONGITUD_TALLO', 'ACTIVO', '', '', '', 70.0);
INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor) VALUES ( 'LONGITUD_80', 'LONGITUD_TALLO', 'ACTIVO', '', '', '', 80.0);
INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor) VALUES ( 'LONGITUD_90', 'LONGITUD_TALLO', 'ACTIVO', '', '', '', 90.0);
INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor) VALUES ( 'LONGITUD_100', 'LONGITUD_TALLO', 'ACTIVO', '', '', '', 100.0);
INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor) VALUES ( 'LONGITUD_110', 'LONGITUD_TALLO', 'ACTIVO', '', '', '', 110.0);
INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor) VALUES ( 'LONGITUD_120', 'LONGITUD_TALLO', 'ACTIVO', '', '', '', 120.0);

INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor) VALUES ( 'BELLOSO', 'ENFERMEDADES', 'ACTIVO', 'Belloso', null, null, null);
INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor) VALUES ( 'VOTRITYS', 'ENFERMEDADES', 'ACTIVO', 'Votritys', null, null, null);
INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor) VALUES ( 'TRIPS', 'ENFERMEDADES', 'ACTIVO', 'Trips', null, null, null);
INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor) VALUES ( 'ACAROS', 'ENFERMEDADES', 'ACTIVO', 'Acaros', null, null, null);
INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor) VALUES ( 'OIDIO', 'ENFERMEDADES', 'ACTIVO', 'Oidio', null, null, null);
INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor) VALUES ( 'CLOROSIS', 'ENFERMEDADES', 'ACTIVO', 'Clorosis', null, null, null);
INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor) VALUES ( 'DEFORMES', 'ENFERMEDADES', 'ACTIVO', 'Deformes', null, null, null);
INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor) VALUES ( 'CORTOS', 'ENFERMEDADES', 'ACTIVO', 'Cortos', null, null, null);
INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor) VALUES ( 'DELGADOS', 'ENFERMEDADES', 'ACTIVO', 'Delgados', null, null, null);
INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor) VALUES ( 'CIEGOS', 'ENFERMEDADES', 'ACTIVO', 'Ciegos', null, null, null);

INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor) VALUES ( 'GRAMOS', 'UNIDAD_MEDIDA', 'ACTIVO', 'gr', null, null, null);
INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor) VALUES ( 'CC', 'UNIDAD_MEDIDA', 'ACTIVO', 'cc', null, null, null);

INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor) VALUES ( 'FERTILIZACION', 'TIPOS_APLICACIONES', 'ACTIVO', 'Fertilización', 'NO', null, null);
INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor) VALUES ( 'FUMNIGACION', 'TIPOS_APLICACIONES', 'ACTIVO', 'Fumnigación', 'SI', null, null);
INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor) VALUES ( 'DRENCH', 'TIPOS_APLICACIONES', 'ACTIVO', 'Drench', 'SI', null, null);
INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor) VALUES ( 'BASALEO', 'TIPOS_APLICACIONES', 'ACTIVO', 'Basaleo', 'SI', null, null);


INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor) VALUES ( 'TANQUE_A', 'TANQUES', 'ACTIVO', 'Tanque A', null, null, null);
INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor) VALUES ( 'TANQUE_B', 'TANQUES', 'ACTIVO', 'Tanque B', null, null, null);
INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor) VALUES ( 'TANQUE_FUMNIGACION', 'TANQUES', 'ACTIVO', 'Tanque de fumnigación', null, null, null);
INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor) VALUES ( 'TANQUE_DRENCH', 'TANQUES', 'ACTIVO', 'Tanque de drench', null, null, null);

INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor) VALUES ( 'TM25', 'TALLO_BONCHE', 'ACTIVO', '', '', '', 25);
INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor) VALUES ( 'TM24', 'TALLO_BONCHE', 'ACTIVO', '', 'SI', '', 24);


INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor) VALUES ( 'GRUPO_NACIONAL', null, 'ACTIVO', 'Grupo de nacional', null, null, null);

INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor) VALUES ( 'CULTIVO', 'GRUPO_NACIONAL', 'ACTIVO', 'Cultivo', null, null, null);
INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor) VALUES ( 'NACIONAL_SOBRANTE', 'GRUPO_NACIONAL', 'ACTIVO', 'Nacional Sobrante', null, null, null);
INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor) VALUES ( 'SANIDAD_VEGETAL', 'GRUPO_NACIONAL', 'ACTIVO', 'Sanidad Vegetal', null, null, null);
INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor) VALUES ( 'FERTILIZACION_RIEGO', 'GRUPO_NACIONAL', 'ACTIVO', 'Fertilización y riego', null, null, null);

INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor) VALUES ( 'MALTRATO_BOTON', 'CULTIVO', 'ACTIVO', 'Maltrato Boton', null, null, null);
INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor) VALUES ( 'TALLO TORCIDO', 'CULTIVO', 'ACTIVO', 'Tallo Torcido', null, null, null);
INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor) VALUES ( 'FLOR ABIERTA', 'CULTIVO', 'ACTIVO', 'Flor Abierta', null, null, null);
INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor) VALUES ( 'TALLOS QUEBRADO', 'CULTIVO', 'ACTIVO', 'Tallos Quebrados', null, null, null);
INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor) VALUES ( 'MALTRATO POR PRESION', 'CULTIVO', 'ACTIVO', 'Maltrato por presión', null, null, null);
INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor) VALUES ( 'BASALES', 'CULTIVO', 'ACTIVO', 'Basales', null, null, null);
INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor) VALUES ( 'MALTRATO_FOLLAJE', 'CULTIVO', 'ACTIVO', 'Maltrato Follaje', null, null, null);
INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor) VALUES ( 'DESYEME_TARDIO', 'CULTIVO', 'ACTIVO', 'Desyeme Tardio', null, null, null);

INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor) VALUES ( 'CLOROSIS', 'FERTILIZACION_RIEGO', 'ACTIVO', 'Clorosis', null, null, null);
INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor) VALUES ( 'BOTON_FOFO', 'FERTILIZACION_RIEGO', 'ACTIVO', 'Botón Fofo', null, null, null);
INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor) VALUES ( 'BOTON_DEFORME', 'FERTILIZACION_RIEGO', 'ACTIVO', 'Botón deforme', null, null, null);
INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor) VALUES ( 'CUELLO_CISNE', 'FERTILIZACION_RIEGO', 'ACTIVO', 'Cuello de Cisne', null, null, null);
INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor) VALUES ( 'FOLLAJE_ESCASO', 'FERTILIZACION_RIEGO', 'ACTIVO', 'Follaje escaso', null, null, null);
INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor) VALUES ( 'BOTON_PEQUENIO', 'FERTILIZACION_RIEGO', 'ACTIVO', 'Botón pequeño', null, null, null);


INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor) VALUES ( 'BOTRYTIS', 'SANIDAD_VEGETAL', 'ACTIVO', 'Botrytis', null, null, null);
INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor) VALUES ( 'VELLOSO', 'SANIDAD_VEGETAL', 'ACTIVO', 'Velloso', null, null, null);
INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor) VALUES ( 'ACAROS', 'SANIDAD_VEGETAL', 'ACTIVO', 'Acaros', null, null, null);
INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor) VALUES ( 'OIDIO', 'SANIDAD_VEGETAL', 'ACTIVO', 'Oidio', null, null, null);
INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor) VALUES ( 'TRIPS', 'SANIDAD_VEGETAL', 'ACTIVO', 'Trips', null, null, null);


INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor) VALUES ( 'NACIONAL_SACA_SOBRANTES', 'NACIONAL_SOBRANTE', 'ACTIVO', 'Nacional saca de sobrante', null, null, null);

INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor) VALUES ( 'MESAS', null, 'ACTIVO', 'Mesas', null, null, null);
INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor) VALUES ( 'UNO', 'MESAS', 'ACTIVO', '1', null, null, null);
INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor) VALUES ( 'DOS', 'MESAS', 'ACTIVO', '2', null, null, null);
INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor) VALUES ( 'TRES', 'MESAS', 'ACTIVO', '3', null, null, null);
INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor) VALUES ( 'CUATRO', 'MESAS', 'ACTIVO', '4', null, null, null);
INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor) VALUES ( 'CINCO', 'MESAS', 'ACTIVO', '5', null, null, null);
INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor) VALUES ( 'SEIS', 'MESAS', 'ACTIVO', '6', null, null, null);
INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor) VALUES ( 'SIENTE', 'MESAS', 'ACTIVO', '7', null, null, null);



INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor) VALUES ( 'PUNTO_CORTE', null, 'ACTIVO', 'Punto de Corte', null, null, null);
INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor) VALUES ( 'UNO', 'PUNTO_CORTE', 'ACTIVO', '1', null, null, null);
INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor) VALUES ( 'DOS', 'PUNTO_CORTE', 'ACTIVO', '2', null, null, null);
INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor) VALUES ( 'TRES', 'PUNTO_CORTE', 'ACTIVO', '3', null, null, null);

INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor) VALUES ( 'TIPO_CARTON', null, 'ACTIVO', 'Tipo de Carton', null, null, null);
INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor) VALUES ( 'CRA', 'TIPO_CARTON', 'ACTIVO', 'CRA', null, null, null);
INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor) VALUES ( 'CRB', 'TIPO_CARTON', 'ACTIVO', 'CRB', null, null, null);


INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor,sc_global) VALUES ( 'DIAS_SEMANA', null, 'ACTIVO', 'Dias de la semana', null, null, null,'SI');
INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor,sc_global) VALUES ( 'MONDAY', 'DIAS_SEMANA', 'ACTIVO', 'lUNES', null, null, null,'SI');
INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor,sc_global) VALUES ( 'TUESDAY', 'DIAS_SEMANA', 'ACTIVO', 'Martes', null, null, null,'SI');
INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor,sc_global) VALUES ( 'WEDNESDAY', 'DIAS_SEMANA', 'ACTIVO', 'Miercoles', null, null, null,'SI');
INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor,sc_global) VALUES ( 'THURSDAY', 'DIAS_SEMANA', 'ACTIVO', 'Jueves', null, null, null,'SI');
INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor,sc_global) VALUES ( 'FRIDAY', 'DIAS_SEMANA', 'ACTIVO', 'Viernes', null, null, null,'SI');
INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor,sc_global) VALUES ( 'SATURDAY', 'DIAS_SEMANA', 'ACTIVO', 'Sabado', null, null, null,'SI');
INSERT INTO public.s_catalogo(sc_codigo, sc_codigo_grupo, sc_estado, sc_nombre, sc_texto1, sc_texto2, sc_valor,sc_global) VALUES ( 'SUNDAY', 'DIAS_SEMANA', 'ACTIVO', 'Domingo', null, null, null,'SI');




INSERT INTO public.s_usuario(su_activo, su_apellidos, su_contrasenia, su_correo, su_fecha_registro, su_identificacion, su_nombres) VALUES ('SI', 'Super', '$2a$10$QHAla0EQeOyK.fGvnrshE.XmNEbN1lTuPv9nN6D//Dc/3RQKuSRy6', 'freddy.geovanni@gmail.com', CURRENT_DATE, 'admin', 'Super');

INSERT INTO public.s_usuario_perfil(sup_activo, id_perfil, id_usuario) VALUES ('SI', 1, 1);

INSERT INTO public.s_parametro(spa_id, spa_activo, spa_descripcion, spa_valor) VALUES ('URL_PAGE', 'SI', '', 'Url de página web del app');

INSERT INTO public.s_parametro(spa_id, spa_activo, spa_descripcion, spa_valor) VALUES ('LINKS_VIDEOS', 'SI', '', 'Links de video tutoriales');