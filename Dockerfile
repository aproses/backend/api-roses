FROM adoptopenjdk/openjdk16:jre-16_36
ENV TZ="America/Bogota"
VOLUME /tmp
ENV PORT 8051
EXPOSE $PORT
ADD ./target/api-roses-0.0.1-SNAPSHOT.jar api-roses.jar
ENTRYPOINT ["java", "-jar","/api-roses.jar"]
